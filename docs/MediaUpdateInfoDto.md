
# MediaUpdateInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updates** | [**kotlin.collections.List&lt;MediaUpdateInfoPathDto&gt;**](MediaUpdateInfoPathDto.md) | Gets or sets the list of updates. |  [optional]



