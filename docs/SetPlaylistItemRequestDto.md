
# SetPlaylistItemRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlistItemId** | **kotlin.String** | Gets or sets the playlist identifier of the playing item. |  [optional]



