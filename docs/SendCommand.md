
# SendCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **kotlin.String** | Gets the group identifier. |  [optional]
**playlistItemId** | **kotlin.String** | Gets the playlist identifier of the playing item. |  [optional]
**&#x60;when&#x60;** | **kotlin.String** | Gets or sets the UTC time when to execute the command. |  [optional]
**positionTicks** | **kotlin.Long** | Gets the position ticks. |  [optional]
**command** | [**SendCommandType**](SendCommandType.md) |  |  [optional]
**emittedAt** | **kotlin.String** | Gets the UTC time when this command has been emitted. |  [optional]



