# LocalizationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCountries**](LocalizationApi.md#getCountries) | **GET** /Localization/Countries | Gets known countries.
[**getCultures**](LocalizationApi.md#getCultures) | **GET** /Localization/Cultures | Gets known cultures.
[**getLocalizationOptions**](LocalizationApi.md#getLocalizationOptions) | **GET** /Localization/Options | Gets localization options.
[**getParentalRatings**](LocalizationApi.md#getParentalRatings) | **GET** /Localization/ParentalRatings | Gets known parental ratings.


<a name="getCountries"></a>
# **getCountries**
> kotlin.collections.List&lt;CountryInfo&gt; getCountries()

Gets known countries.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LocalizationApi()
try {
    val result : kotlin.collections.List<CountryInfo> = apiInstance.getCountries()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LocalizationApi#getCountries")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LocalizationApi#getCountries")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;CountryInfo&gt;**](CountryInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getCultures"></a>
# **getCultures**
> kotlin.collections.List&lt;CultureDto&gt; getCultures()

Gets known cultures.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LocalizationApi()
try {
    val result : kotlin.collections.List<CultureDto> = apiInstance.getCultures()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LocalizationApi#getCultures")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LocalizationApi#getCultures")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;CultureDto&gt;**](CultureDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLocalizationOptions"></a>
# **getLocalizationOptions**
> kotlin.collections.List&lt;LocalizationOption&gt; getLocalizationOptions()

Gets localization options.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LocalizationApi()
try {
    val result : kotlin.collections.List<LocalizationOption> = apiInstance.getLocalizationOptions()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LocalizationApi#getLocalizationOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LocalizationApi#getLocalizationOptions")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;LocalizationOption&gt;**](LocalizationOption.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getParentalRatings"></a>
# **getParentalRatings**
> kotlin.collections.List&lt;ParentalRating&gt; getParentalRatings()

Gets known parental ratings.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LocalizationApi()
try {
    val result : kotlin.collections.List<ParentalRating> = apiInstance.getParentalRatings()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LocalizationApi#getParentalRatings")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LocalizationApi#getParentalRatings")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;ParentalRating&gt;**](ParentalRating.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

