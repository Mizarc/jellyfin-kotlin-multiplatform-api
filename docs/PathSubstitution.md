
# PathSubstitution

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **kotlin.String** | Gets or sets the value to substitute. |  [optional]
**to** | **kotlin.String** | Gets or sets the value to substitution with. |  [optional]



