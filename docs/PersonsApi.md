# PersonsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPerson**](PersonsApi.md#getPerson) | **GET** /Persons/{name} | Get person by name.
[**getPersons**](PersonsApi.md#getPersons) | **GET** /Persons | Gets all persons.


<a name="getPerson"></a>
# **getPerson**
> BaseItemDto getPerson(name, userId)

Get person by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PersonsApi()
val name : kotlin.String = name_example // kotlin.String | Person name.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
try {
    val result : BaseItemDto = apiInstance.getPerson(name, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PersonsApi#getPerson")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PersonsApi#getPerson")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Person name. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPersons"></a>
# **getPersons**
> BaseItemDtoQueryResult getPersons(limit, searchTerm, fields, filters, isFavorite, enableUserData, imageTypeLimit, enableImageTypes, excludePersonTypes, personTypes, appearsInItemId, userId, enableImages)

Gets all persons.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PersonsApi()
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val searchTerm : kotlin.String = searchTerm_example // kotlin.String | The search term.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val filters : kotlin.collections.List<ItemFilter> =  // kotlin.collections.List<ItemFilter> | Optional. Specify additional filters to apply.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are marked as favorite, or not. userId is required.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional, include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional, the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val excludePersonTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified results will be filtered to exclude those containing the specified PersonType. Allows multiple, comma-delimited.
val personTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified results will be filtered to include only those containing the specified PersonType. Allows multiple, comma-delimited.
val appearsInItemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. If specified, person results will be filtered on items related to said persons.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional, include image information in output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getPersons(limit, searchTerm, fields, filters, isFavorite, enableUserData, imageTypeLimit, enableImageTypes, excludePersonTypes, personTypes, appearsInItemId, userId, enableImages)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PersonsApi#getPersons")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PersonsApi#getPersons")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **searchTerm** | **kotlin.String**| The search term. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **filters** | [**kotlin.collections.List&lt;ItemFilter&gt;**](ItemFilter.md)| Optional. Specify additional filters to apply. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional filter by items that are marked as favorite, or not. userId is required. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional, include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional, the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **excludePersonTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified results will be filtered to exclude those containing the specified PersonType. Allows multiple, comma-delimited. | [optional]
 **personTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified results will be filtered to include only those containing the specified PersonType. Allows multiple, comma-delimited. | [optional]
 **appearsInItemId** | **kotlin.String**| Optional. If specified, person results will be filtered on items related to said persons. | [optional]
 **userId** | **kotlin.String**| User id. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional, include image information in output. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

