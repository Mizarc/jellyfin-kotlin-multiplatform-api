
# ClientCapabilitiesDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playableMediaTypes** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the list of playable media types. |  [optional]
**supportedCommands** | [**kotlin.collections.List&lt;GeneralCommandType&gt;**](GeneralCommandType.md) | Gets or sets the list of supported commands. |  [optional]
**supportsMediaControl** | **kotlin.Boolean** | Gets or sets a value indicating whether session supports media control. |  [optional]
**supportsContentUploading** | **kotlin.Boolean** | Gets or sets a value indicating whether session supports content uploading. |  [optional]
**messageCallbackUrl** | **kotlin.String** | Gets or sets the message callback url. |  [optional]
**supportsPersistentIdentifier** | **kotlin.Boolean** | Gets or sets a value indicating whether session supports a persistent identifier. |  [optional]
**supportsSync** | **kotlin.Boolean** | Gets or sets a value indicating whether session supports sync. |  [optional]
**deviceProfile** | [**ClientCapabilitiesDtoDeviceProfile**](ClientCapabilitiesDtoDeviceProfile.md) |  |  [optional]
**appStoreUrl** | **kotlin.String** | Gets or sets the app store url. |  [optional]
**iconUrl** | **kotlin.String** | Gets or sets the icon url. |  [optional]



