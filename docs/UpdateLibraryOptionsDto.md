
# UpdateLibraryOptionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | Gets or sets the library item id. |  [optional]
**libraryOptions** | [**AddVirtualFolderDtoLibraryOptions**](AddVirtualFolderDtoLibraryOptions.md) |  |  [optional]



