
# UpdateUserPassword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentPassword** | **kotlin.String** | Gets or sets the current sha1-hashed password. |  [optional]
**currentPw** | **kotlin.String** | Gets or sets the current plain text password. |  [optional]
**newPw** | **kotlin.String** | Gets or sets the new plain text password. |  [optional]
**resetPassword** | **kotlin.Boolean** | Gets or sets a value indicating whether to reset the password. |  [optional]



