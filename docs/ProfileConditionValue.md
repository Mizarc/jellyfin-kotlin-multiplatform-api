
# ProfileConditionValue

## Enum


    * `audioChannels` (value: `"AudioChannels"`)

    * `audioBitrate` (value: `"AudioBitrate"`)

    * `audioProfile` (value: `"AudioProfile"`)

    * `width` (value: `"Width"`)

    * `height` (value: `"Height"`)

    * `has64BitOffsets` (value: `"Has64BitOffsets"`)

    * `packetLength` (value: `"PacketLength"`)

    * `videoBitDepth` (value: `"VideoBitDepth"`)

    * `videoBitrate` (value: `"VideoBitrate"`)

    * `videoFramerate` (value: `"VideoFramerate"`)

    * `videoLevel` (value: `"VideoLevel"`)

    * `videoProfile` (value: `"VideoProfile"`)

    * `videoTimestamp` (value: `"VideoTimestamp"`)

    * `isAnamorphic` (value: `"IsAnamorphic"`)

    * `refFrames` (value: `"RefFrames"`)

    * `numAudioStreams` (value: `"NumAudioStreams"`)

    * `numVideoStreams` (value: `"NumVideoStreams"`)

    * `isSecondaryAudio` (value: `"IsSecondaryAudio"`)

    * `videoCodecTag` (value: `"VideoCodecTag"`)

    * `isAvc` (value: `"IsAvc"`)

    * `isInterlaced` (value: `"IsInterlaced"`)

    * `audioSampleRate` (value: `"AudioSampleRate"`)

    * `audioBitDepth` (value: `"AudioBitDepth"`)

    * `videoRangeType` (value: `"VideoRangeType"`)



