# SuggestionsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSuggestions**](SuggestionsApi.md#getSuggestions) | **GET** /Users/{userId}/Suggestions | Gets suggestions.


<a name="getSuggestions"></a>
# **getSuggestions**
> BaseItemDtoQueryResult getSuggestions(userId, mediaType, type, startIndex, limit, enableTotalRecordCount)

Gets suggestions.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SuggestionsApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val mediaType : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The media types.
val type : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | The type.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The start index.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The limit.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Whether to enable the total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSuggestions(userId, mediaType, type, startIndex, limit, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SuggestionsApi#getSuggestions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SuggestionsApi#getSuggestions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |
 **mediaType** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The media types. | [optional]
 **type** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| The type. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The start index. | [optional]
 **limit** | **kotlin.Int**| Optional. The limit. | [optional]
 **enableTotalRecordCount** | **kotlin.Boolean**| Whether to enable the total record count. | [optional] [default to false]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

