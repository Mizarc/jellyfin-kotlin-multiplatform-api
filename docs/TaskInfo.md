
# TaskInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**state** | [**TaskState**](TaskState.md) |  |  [optional]
**currentProgressPercentage** | **kotlin.Double** | Gets or sets the progress. |  [optional]
**id** | **kotlin.String** | Gets or sets the id. |  [optional]
**lastExecutionResult** | [**TaskInfoLastExecutionResult**](TaskInfoLastExecutionResult.md) |  |  [optional]
**triggers** | [**kotlin.collections.List&lt;TaskTriggerInfo&gt;**](TaskTriggerInfo.md) | Gets or sets the triggers. |  [optional]
**description** | **kotlin.String** | Gets or sets the description. |  [optional]
**category** | **kotlin.String** | Gets or sets the category. |  [optional]
**isHidden** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is hidden. |  [optional]
**key** | **kotlin.String** | Gets or sets the key. |  [optional]



