
# ImageType

## Enum


    * `primary` (value: `"Primary"`)

    * `art` (value: `"Art"`)

    * `backdrop` (value: `"Backdrop"`)

    * `banner` (value: `"Banner"`)

    * `logo` (value: `"Logo"`)

    * `thumb` (value: `"Thumb"`)

    * `disc` (value: `"Disc"`)

    * `box` (value: `"Box"`)

    * `screenshot` (value: `"Screenshot"`)

    * `menu` (value: `"Menu"`)

    * `chapter` (value: `"Chapter"`)

    * `boxRear` (value: `"BoxRear"`)

    * `profile` (value: `"Profile"`)



