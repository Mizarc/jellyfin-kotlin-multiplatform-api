
# PlaybackStopInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | [**PlaybackProgressInfoItem**](PlaybackProgressInfoItem.md) |  |  [optional]
**itemId** | **kotlin.String** | Gets or sets the item identifier. |  [optional]
**sessionId** | **kotlin.String** | Gets or sets the session id. |  [optional]
**mediaSourceId** | **kotlin.String** | Gets or sets the media version identifier. |  [optional]
**positionTicks** | **kotlin.Long** | Gets or sets the position ticks. |  [optional]
**liveStreamId** | **kotlin.String** | Gets or sets the live stream identifier. |  [optional]
**playSessionId** | **kotlin.String** | Gets or sets the play session identifier. |  [optional]
**failed** | **kotlin.Boolean** | Gets or sets a value indicating whether this MediaBrowser.Model.Session.PlaybackStopInfo is failed. |  [optional]
**nextMediaType** | **kotlin.String** |  |  [optional]
**playlistItemId** | **kotlin.String** |  |  [optional]
**nowPlayingQueue** | [**kotlin.collections.List&lt;QueueItem&gt;**](QueueItem.md) |  |  [optional]



