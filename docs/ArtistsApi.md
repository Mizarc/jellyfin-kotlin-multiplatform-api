# ArtistsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAlbumArtists**](ArtistsApi.md#getAlbumArtists) | **GET** /Artists/AlbumArtists | Gets all album artists from a given item, folder, or the entire library.
[**getArtistByName**](ArtistsApi.md#getArtistByName) | **GET** /Artists/{name} | Gets an artist by name.
[**getArtists**](ArtistsApi.md#getArtists) | **GET** /Artists | Gets all artists from a given item, folder, or the entire library.


<a name="getAlbumArtists"></a>
# **getAlbumArtists**
> BaseItemDtoQueryResult getAlbumArtists(minCommunityRating, startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, filters, isFavorite, mediaTypes, genres, genreIds, officialRatings, tags, years, enableUserData, imageTypeLimit, enableImageTypes, person, personIds, personTypes, studios, studioIds, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, sortBy, sortOrder, enableImages, enableTotalRecordCount)

Gets all album artists from a given item, folder, or the entire library.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ArtistsApi()
val minCommunityRating : kotlin.Double = 1.2 // kotlin.Double | Optional filter by minimum community rating.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val searchTerm : kotlin.String = searchTerm_example // kotlin.String | Optional. Search term.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val excludeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited.
val filters : kotlin.collections.List<ItemFilter> =  // kotlin.collections.List<ItemFilter> | Optional. Specify additional filters to apply.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are marked as favorite, or not.
val mediaTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional filter by MediaType. Allows multiple, comma delimited.
val genres : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on genre. This allows multiple, pipe delimited.
val genreIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on genre id. This allows multiple, pipe delimited.
val officialRatings : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on OfficialRating. This allows multiple, pipe delimited.
val tags : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on tag. This allows multiple, pipe delimited.
val years : kotlin.collections.List<kotlin.Int> =  // kotlin.collections.List<kotlin.Int> | Optional. If specified, results will be filtered based on production year. This allows multiple, comma delimited.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional, include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional, the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val person : kotlin.String = person_example // kotlin.String | Optional. If specified, results will be filtered to include only those containing the specified person.
val personIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered to include only those containing the specified person ids.
val personTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, along with Person, results will be filtered to include only those containing the specified person and PersonType. Allows multiple, comma-delimited.
val studios : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on studio. This allows multiple, pipe delimited.
val studioIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on studio id. This allows multiple, pipe delimited.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val nameStartsWithOrGreater : kotlin.String = nameStartsWithOrGreater_example // kotlin.String | Optional filter by items whose name is sorted equally or greater than a given input string.
val nameStartsWith : kotlin.String = nameStartsWith_example // kotlin.String | Optional filter by items whose name is sorted equally than a given input string.
val nameLessThan : kotlin.String = nameLessThan_example // kotlin.String | Optional filter by items whose name is equally or lesser than a given input string.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more sort orders, comma delimited.
val sortOrder : kotlin.collections.List<SortOrder> =  // kotlin.collections.List<SortOrder> | Sort Order - Ascending,Descending.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional, include image information in output.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getAlbumArtists(minCommunityRating, startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, filters, isFavorite, mediaTypes, genres, genreIds, officialRatings, tags, years, enableUserData, imageTypeLimit, enableImageTypes, person, personIds, personTypes, studios, studioIds, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, sortBy, sortOrder, enableImages, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ArtistsApi#getAlbumArtists")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ArtistsApi#getAlbumArtists")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **minCommunityRating** | **kotlin.Double**| Optional filter by minimum community rating. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **searchTerm** | **kotlin.String**| Optional. Search term. | [optional]
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **excludeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. | [optional]
 **filters** | [**kotlin.collections.List&lt;ItemFilter&gt;**](ItemFilter.md)| Optional. Specify additional filters to apply. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional filter by items that are marked as favorite, or not. | [optional]
 **mediaTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional filter by MediaType. Allows multiple, comma delimited. | [optional]
 **genres** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on genre. This allows multiple, pipe delimited. | [optional]
 **genreIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on genre id. This allows multiple, pipe delimited. | [optional]
 **officialRatings** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on OfficialRating. This allows multiple, pipe delimited. | [optional]
 **tags** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on tag. This allows multiple, pipe delimited. | [optional]
 **years** | [**kotlin.collections.List&lt;kotlin.Int&gt;**](kotlin.Int.md)| Optional. If specified, results will be filtered based on production year. This allows multiple, comma delimited. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional, include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional, the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **person** | **kotlin.String**| Optional. If specified, results will be filtered to include only those containing the specified person. | [optional]
 **personIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered to include only those containing the specified person ids. | [optional]
 **personTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, along with Person, results will be filtered to include only those containing the specified person and PersonType. Allows multiple, comma-delimited. | [optional]
 **studios** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on studio. This allows multiple, pipe delimited. | [optional]
 **studioIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on studio id. This allows multiple, pipe delimited. | [optional]
 **userId** | **kotlin.String**| User id. | [optional]
 **nameStartsWithOrGreater** | **kotlin.String**| Optional filter by items whose name is sorted equally or greater than a given input string. | [optional]
 **nameStartsWith** | **kotlin.String**| Optional filter by items whose name is sorted equally than a given input string. | [optional]
 **nameLessThan** | **kotlin.String**| Optional filter by items whose name is equally or lesser than a given input string. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more sort orders, comma delimited. | [optional]
 **sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md)| Sort Order - Ascending,Descending. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional, include image information in output. | [optional] [default to true]
 **enableTotalRecordCount** | **kotlin.Boolean**| Total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getArtistByName"></a>
# **getArtistByName**
> BaseItemDto getArtistByName(name, userId)

Gets an artist by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ArtistsApi()
val name : kotlin.String = name_example // kotlin.String | Studio name.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
try {
    val result : BaseItemDto = apiInstance.getArtistByName(name, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ArtistsApi#getArtistByName")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ArtistsApi#getArtistByName")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Studio name. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getArtists"></a>
# **getArtists**
> BaseItemDtoQueryResult getArtists(minCommunityRating, startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, filters, isFavorite, mediaTypes, genres, genreIds, officialRatings, tags, years, enableUserData, imageTypeLimit, enableImageTypes, person, personIds, personTypes, studios, studioIds, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, sortBy, sortOrder, enableImages, enableTotalRecordCount)

Gets all artists from a given item, folder, or the entire library.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ArtistsApi()
val minCommunityRating : kotlin.Double = 1.2 // kotlin.Double | Optional filter by minimum community rating.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val searchTerm : kotlin.String = searchTerm_example // kotlin.String | Optional. Search term.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val excludeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited.
val filters : kotlin.collections.List<ItemFilter> =  // kotlin.collections.List<ItemFilter> | Optional. Specify additional filters to apply.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are marked as favorite, or not.
val mediaTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional filter by MediaType. Allows multiple, comma delimited.
val genres : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on genre. This allows multiple, pipe delimited.
val genreIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on genre id. This allows multiple, pipe delimited.
val officialRatings : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on OfficialRating. This allows multiple, pipe delimited.
val tags : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on tag. This allows multiple, pipe delimited.
val years : kotlin.collections.List<kotlin.Int> =  // kotlin.collections.List<kotlin.Int> | Optional. If specified, results will be filtered based on production year. This allows multiple, comma delimited.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional, include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional, the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val person : kotlin.String = person_example // kotlin.String | Optional. If specified, results will be filtered to include only those containing the specified person.
val personIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered to include only those containing the specified person ids.
val personTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, along with Person, results will be filtered to include only those containing the specified person and PersonType. Allows multiple, comma-delimited.
val studios : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on studio. This allows multiple, pipe delimited.
val studioIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on studio id. This allows multiple, pipe delimited.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val nameStartsWithOrGreater : kotlin.String = nameStartsWithOrGreater_example // kotlin.String | Optional filter by items whose name is sorted equally or greater than a given input string.
val nameStartsWith : kotlin.String = nameStartsWith_example // kotlin.String | Optional filter by items whose name is sorted equally than a given input string.
val nameLessThan : kotlin.String = nameLessThan_example // kotlin.String | Optional filter by items whose name is equally or lesser than a given input string.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more sort orders, comma delimited.
val sortOrder : kotlin.collections.List<SortOrder> =  // kotlin.collections.List<SortOrder> | Sort Order - Ascending,Descending.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional, include image information in output.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getArtists(minCommunityRating, startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, filters, isFavorite, mediaTypes, genres, genreIds, officialRatings, tags, years, enableUserData, imageTypeLimit, enableImageTypes, person, personIds, personTypes, studios, studioIds, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, sortBy, sortOrder, enableImages, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ArtistsApi#getArtists")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ArtistsApi#getArtists")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **minCommunityRating** | **kotlin.Double**| Optional filter by minimum community rating. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **searchTerm** | **kotlin.String**| Optional. Search term. | [optional]
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **excludeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. | [optional]
 **filters** | [**kotlin.collections.List&lt;ItemFilter&gt;**](ItemFilter.md)| Optional. Specify additional filters to apply. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional filter by items that are marked as favorite, or not. | [optional]
 **mediaTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional filter by MediaType. Allows multiple, comma delimited. | [optional]
 **genres** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on genre. This allows multiple, pipe delimited. | [optional]
 **genreIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on genre id. This allows multiple, pipe delimited. | [optional]
 **officialRatings** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on OfficialRating. This allows multiple, pipe delimited. | [optional]
 **tags** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on tag. This allows multiple, pipe delimited. | [optional]
 **years** | [**kotlin.collections.List&lt;kotlin.Int&gt;**](kotlin.Int.md)| Optional. If specified, results will be filtered based on production year. This allows multiple, comma delimited. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional, include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional, the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **person** | **kotlin.String**| Optional. If specified, results will be filtered to include only those containing the specified person. | [optional]
 **personIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered to include only those containing the specified person ids. | [optional]
 **personTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, along with Person, results will be filtered to include only those containing the specified person and PersonType. Allows multiple, comma-delimited. | [optional]
 **studios** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on studio. This allows multiple, pipe delimited. | [optional]
 **studioIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on studio id. This allows multiple, pipe delimited. | [optional]
 **userId** | **kotlin.String**| User id. | [optional]
 **nameStartsWithOrGreater** | **kotlin.String**| Optional filter by items whose name is sorted equally or greater than a given input string. | [optional]
 **nameStartsWith** | **kotlin.String**| Optional filter by items whose name is sorted equally than a given input string. | [optional]
 **nameLessThan** | **kotlin.String**| Optional filter by items whose name is equally or lesser than a given input string. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more sort orders, comma delimited. | [optional]
 **sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md)| Sort Order - Ascending,Descending. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional, include image information in output. | [optional] [default to true]
 **enableTotalRecordCount** | **kotlin.Boolean**| Total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

