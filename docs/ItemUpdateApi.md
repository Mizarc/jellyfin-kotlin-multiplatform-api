# ItemUpdateApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMetadataEditorInfo**](ItemUpdateApi.md#getMetadataEditorInfo) | **GET** /Items/{itemId}/MetadataEditor | Gets metadata editor info for an item.
[**updateItem**](ItemUpdateApi.md#updateItem) | **POST** /Items/{itemId} | Updates an item.
[**updateItemContentType**](ItemUpdateApi.md#updateItemContentType) | **POST** /Items/{itemId}/ContentType | Updates an item&#39;s content type.


<a name="getMetadataEditorInfo"></a>
# **getMetadataEditorInfo**
> MetadataEditorInfo getMetadataEditorInfo(itemId)

Gets metadata editor info for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemUpdateApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
try {
    val result : MetadataEditorInfo = apiInstance.getMetadataEditorInfo(itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemUpdateApi#getMetadataEditorInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemUpdateApi#getMetadataEditorInfo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |

### Return type

[**MetadataEditorInfo**](MetadataEditorInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateItem"></a>
# **updateItem**
> updateItem(itemId, updateItemRequest)

Updates an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemUpdateApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val updateItemRequest : UpdateItemRequest =  // UpdateItemRequest | The new item properties.
try {
    apiInstance.updateItem(itemId, updateItemRequest)
} catch (e: ClientException) {
    println("4xx response calling ItemUpdateApi#updateItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemUpdateApi#updateItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **updateItemRequest** | [**UpdateItemRequest**](UpdateItemRequest.md)| The new item properties. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateItemContentType"></a>
# **updateItemContentType**
> updateItemContentType(itemId, contentType)

Updates an item&#39;s content type.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemUpdateApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val contentType : kotlin.String = contentType_example // kotlin.String | The content type of the item.
try {
    apiInstance.updateItemContentType(itemId, contentType)
} catch (e: ClientException) {
    println("4xx response calling ItemUpdateApi#updateItemContentType")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemUpdateApi#updateItemContentType")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **contentType** | **kotlin.String**| The content type of the item. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

