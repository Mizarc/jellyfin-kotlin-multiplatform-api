# NotificationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAdminNotification**](NotificationsApi.md#createAdminNotification) | **POST** /Notifications/Admin | Sends a notification to all admins.
[**getNotificationServices**](NotificationsApi.md#getNotificationServices) | **GET** /Notifications/Services | Gets notification services.
[**getNotificationTypes**](NotificationsApi.md#getNotificationTypes) | **GET** /Notifications/Types | Gets notification types.
[**getNotifications**](NotificationsApi.md#getNotifications) | **GET** /Notifications/{userId} | Gets a user&#39;s notifications.
[**getNotificationsSummary**](NotificationsApi.md#getNotificationsSummary) | **GET** /Notifications/{userId}/Summary | Gets a user&#39;s notification summary.
[**setRead**](NotificationsApi.md#setRead) | **POST** /Notifications/{userId}/Read | Sets notifications as read.
[**setUnread**](NotificationsApi.md#setUnread) | **POST** /Notifications/{userId}/Unread | Sets notifications as unread.


<a name="createAdminNotification"></a>
# **createAdminNotification**
> createAdminNotification(createAdminNotificationRequest)

Sends a notification to all admins.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = NotificationsApi()
val createAdminNotificationRequest : CreateAdminNotificationRequest =  // CreateAdminNotificationRequest | The notification request.
try {
    apiInstance.createAdminNotification(createAdminNotificationRequest)
} catch (e: ClientException) {
    println("4xx response calling NotificationsApi#createAdminNotification")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling NotificationsApi#createAdminNotification")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createAdminNotificationRequest** | [**CreateAdminNotificationRequest**](CreateAdminNotificationRequest.md)| The notification request. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="getNotificationServices"></a>
# **getNotificationServices**
> kotlin.collections.List&lt;NameIdPair&gt; getNotificationServices()

Gets notification services.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = NotificationsApi()
try {
    val result : kotlin.collections.List<NameIdPair> = apiInstance.getNotificationServices()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling NotificationsApi#getNotificationServices")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling NotificationsApi#getNotificationServices")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;NameIdPair&gt;**](NameIdPair.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getNotificationTypes"></a>
# **getNotificationTypes**
> kotlin.collections.List&lt;NotificationTypeInfo&gt; getNotificationTypes()

Gets notification types.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = NotificationsApi()
try {
    val result : kotlin.collections.List<NotificationTypeInfo> = apiInstance.getNotificationTypes()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling NotificationsApi#getNotificationTypes")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling NotificationsApi#getNotificationTypes")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;NotificationTypeInfo&gt;**](NotificationTypeInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getNotifications"></a>
# **getNotifications**
> NotificationResultDto getNotifications(userId)

Gets a user&#39;s notifications.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = NotificationsApi()
val userId : kotlin.String = userId_example // kotlin.String | 
try {
    val result : NotificationResultDto = apiInstance.getNotifications(userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling NotificationsApi#getNotifications")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling NotificationsApi#getNotifications")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**|  |

### Return type

[**NotificationResultDto**](NotificationResultDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getNotificationsSummary"></a>
# **getNotificationsSummary**
> NotificationsSummaryDto getNotificationsSummary(userId)

Gets a user&#39;s notification summary.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = NotificationsApi()
val userId : kotlin.String = userId_example // kotlin.String | 
try {
    val result : NotificationsSummaryDto = apiInstance.getNotificationsSummary(userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling NotificationsApi#getNotificationsSummary")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling NotificationsApi#getNotificationsSummary")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**|  |

### Return type

[**NotificationsSummaryDto**](NotificationsSummaryDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="setRead"></a>
# **setRead**
> setRead(userId)

Sets notifications as read.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = NotificationsApi()
val userId : kotlin.String = userId_example // kotlin.String | 
try {
    apiInstance.setRead(userId)
} catch (e: ClientException) {
    println("4xx response calling NotificationsApi#setRead")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling NotificationsApi#setRead")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**|  |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setUnread"></a>
# **setUnread**
> setUnread(userId)

Sets notifications as unread.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = NotificationsApi()
val userId : kotlin.String = userId_example // kotlin.String | 
try {
    apiInstance.setUnread(userId)
} catch (e: ClientException) {
    println("4xx response calling NotificationsApi#setUnread")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling NotificationsApi#setUnread")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**|  |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

