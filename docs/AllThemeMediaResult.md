
# AllThemeMediaResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**themeVideosResult** | [**AllThemeMediaResultThemeVideosResult**](AllThemeMediaResultThemeVideosResult.md) |  |  [optional]
**themeSongsResult** | [**AllThemeMediaResultThemeVideosResult**](AllThemeMediaResultThemeVideosResult.md) |  |  [optional]
**soundtrackSongsResult** | [**AllThemeMediaResultThemeVideosResult**](AllThemeMediaResultThemeVideosResult.md) |  |  [optional]



