
# GuideInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startDate** | **kotlin.String** | Gets or sets the start date. |  [optional]
**endDate** | **kotlin.String** | Gets or sets the end date. |  [optional]



