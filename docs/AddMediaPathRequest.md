
# AddMediaPathRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name of the library. | 
**path** | **kotlin.String** | Gets or sets the path to add. |  [optional]
**pathInfo** | [**MediaPathDtoPathInfo**](MediaPathDtoPathInfo.md) |  |  [optional]



