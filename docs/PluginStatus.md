
# PluginStatus

## Enum


    * `active` (value: `"Active"`)

    * `restart` (value: `"Restart"`)

    * `deleted` (value: `"Deleted"`)

    * `superceded` (value: `"Superceded"`)

    * `malfunctioned` (value: `"Malfunctioned"`)

    * `notSupported` (value: `"NotSupported"`)

    * `disabled` (value: `"Disabled"`)



