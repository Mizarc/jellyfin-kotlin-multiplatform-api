
# ExternalIdInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the display name of the external id provider (IE: IMDB, MusicBrainz, etc). |  [optional]
**key** | **kotlin.String** | Gets or sets the unique key for this id. This key should be unique across all providers. |  [optional]
**type** | [**ExternalIdMediaType**](ExternalIdMediaType.md) |  |  [optional]
**urlFormatString** | **kotlin.String** | Gets or sets the URL format string. |  [optional]



