# FilterApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getQueryFilters**](FilterApi.md#getQueryFilters) | **GET** /Items/Filters2 | Gets query filters.
[**getQueryFiltersLegacy**](FilterApi.md#getQueryFiltersLegacy) | **GET** /Items/Filters | Gets legacy query filters.


<a name="getQueryFilters"></a>
# **getQueryFilters**
> QueryFilters getQueryFilters(userId, parentId, includeItemTypes, isAiring, isMovie, isSports, isKids, isNews, isSeries, recursive)

Gets query filters.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = FilterApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. User id.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Specify this to localize the search to a specific item or folder. Omit to use the root.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited.
val isAiring : kotlin.Boolean = true // kotlin.Boolean | Optional. Is item airing.
val isMovie : kotlin.Boolean = true // kotlin.Boolean | Optional. Is item movie.
val isSports : kotlin.Boolean = true // kotlin.Boolean | Optional. Is item sports.
val isKids : kotlin.Boolean = true // kotlin.Boolean | Optional. Is item kids.
val isNews : kotlin.Boolean = true // kotlin.Boolean | Optional. Is item news.
val isSeries : kotlin.Boolean = true // kotlin.Boolean | Optional. Is item series.
val recursive : kotlin.Boolean = true // kotlin.Boolean | Optional. Search recursive.
try {
    val result : QueryFilters = apiInstance.getQueryFilters(userId, parentId, includeItemTypes, isAiring, isMovie, isSports, isKids, isNews, isSeries, recursive)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling FilterApi#getQueryFilters")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling FilterApi#getQueryFilters")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. User id. | [optional]
 **parentId** | **kotlin.String**| Optional. Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. | [optional]
 **isAiring** | **kotlin.Boolean**| Optional. Is item airing. | [optional]
 **isMovie** | **kotlin.Boolean**| Optional. Is item movie. | [optional]
 **isSports** | **kotlin.Boolean**| Optional. Is item sports. | [optional]
 **isKids** | **kotlin.Boolean**| Optional. Is item kids. | [optional]
 **isNews** | **kotlin.Boolean**| Optional. Is item news. | [optional]
 **isSeries** | **kotlin.Boolean**| Optional. Is item series. | [optional]
 **recursive** | **kotlin.Boolean**| Optional. Search recursive. | [optional]

### Return type

[**QueryFilters**](QueryFilters.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getQueryFiltersLegacy"></a>
# **getQueryFiltersLegacy**
> QueryFiltersLegacy getQueryFiltersLegacy(userId, parentId, includeItemTypes, mediaTypes)

Gets legacy query filters.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = FilterApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. User id.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Parent id.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited.
val mediaTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Filter by MediaType. Allows multiple, comma delimited.
try {
    val result : QueryFiltersLegacy = apiInstance.getQueryFiltersLegacy(userId, parentId, includeItemTypes, mediaTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling FilterApi#getQueryFiltersLegacy")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling FilterApi#getQueryFiltersLegacy")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. User id. | [optional]
 **parentId** | **kotlin.String**| Optional. Parent id. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. | [optional]
 **mediaTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Filter by MediaType. Allows multiple, comma delimited. | [optional]

### Return type

[**QueryFiltersLegacy**](QueryFiltersLegacy.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

