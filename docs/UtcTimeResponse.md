
# UtcTimeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestReceptionTime** | **kotlin.String** | Gets the UTC time when request has been received. |  [optional]
**responseTransmissionTime** | **kotlin.String** | Gets the UTC time when response has been sent. |  [optional]



