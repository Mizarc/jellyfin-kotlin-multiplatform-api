# PlaystateApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**markPlayedItem**](PlaystateApi.md#markPlayedItem) | **POST** /Users/{userId}/PlayedItems/{itemId} | Marks an item as played for user.
[**markUnplayedItem**](PlaystateApi.md#markUnplayedItem) | **DELETE** /Users/{userId}/PlayedItems/{itemId} | Marks an item as unplayed for user.
[**onPlaybackProgress**](PlaystateApi.md#onPlaybackProgress) | **POST** /Users/{userId}/PlayingItems/{itemId}/Progress | Reports a user&#39;s playback progress.
[**onPlaybackStart**](PlaystateApi.md#onPlaybackStart) | **POST** /Users/{userId}/PlayingItems/{itemId} | Reports that a user has begun playing an item.
[**onPlaybackStopped**](PlaystateApi.md#onPlaybackStopped) | **DELETE** /Users/{userId}/PlayingItems/{itemId} | Reports that a user has stopped playing an item.
[**pingPlaybackSession**](PlaystateApi.md#pingPlaybackSession) | **POST** /Sessions/Playing/Ping | Pings a playback session.
[**reportPlaybackProgress**](PlaystateApi.md#reportPlaybackProgress) | **POST** /Sessions/Playing/Progress | Reports playback progress within a session.
[**reportPlaybackStart**](PlaystateApi.md#reportPlaybackStart) | **POST** /Sessions/Playing | Reports playback has started within a session.
[**reportPlaybackStopped**](PlaystateApi.md#reportPlaybackStopped) | **POST** /Sessions/Playing/Stopped | Reports playback has stopped within a session.


<a name="markPlayedItem"></a>
# **markPlayedItem**
> UserItemDataDto markPlayedItem(userId, itemId, datePlayed)

Marks an item as played for user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val datePlayed : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The date the item was played.
try {
    val result : UserItemDataDto = apiInstance.markPlayedItem(userId, itemId, datePlayed)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#markPlayedItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#markPlayedItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |
 **datePlayed** | **kotlin.String**| Optional. The date the item was played. | [optional]

### Return type

[**UserItemDataDto**](UserItemDataDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="markUnplayedItem"></a>
# **markUnplayedItem**
> UserItemDataDto markUnplayedItem(userId, itemId)

Marks an item as unplayed for user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : UserItemDataDto = apiInstance.markUnplayedItem(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#markUnplayedItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#markUnplayedItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**UserItemDataDto**](UserItemDataDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="onPlaybackProgress"></a>
# **onPlaybackProgress**
> onPlaybackProgress(userId, itemId, mediaSourceId, positionTicks, audioStreamIndex, subtitleStreamIndex, volumeLevel, playMethod, liveStreamId, playSessionId, repeatMode, isPaused, isMuted)

Reports a user&#39;s playback progress.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The id of the MediaSource.
val positionTicks : kotlin.Long = 789 // kotlin.Long | Optional. The current position, in ticks. 1 tick = 10000 ms.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | The audio stream index.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | The subtitle stream index.
val volumeLevel : kotlin.Int = 56 // kotlin.Int | Scale of 0-100.
val playMethod : PlayMethod =  // PlayMethod | The play method.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val repeatMode : RepeatMode =  // RepeatMode | The repeat mode.
val isPaused : kotlin.Boolean = true // kotlin.Boolean | Indicates if the player is paused.
val isMuted : kotlin.Boolean = true // kotlin.Boolean | Indicates if the player is muted.
try {
    apiInstance.onPlaybackProgress(userId, itemId, mediaSourceId, positionTicks, audioStreamIndex, subtitleStreamIndex, volumeLevel, playMethod, liveStreamId, playSessionId, repeatMode, isPaused, isMuted)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#onPlaybackProgress")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#onPlaybackProgress")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |
 **mediaSourceId** | **kotlin.String**| The id of the MediaSource. | [optional]
 **positionTicks** | **kotlin.Long**| Optional. The current position, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **audioStreamIndex** | **kotlin.Int**| The audio stream index. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| The subtitle stream index. | [optional]
 **volumeLevel** | **kotlin.Int**| Scale of 0-100. | [optional]
 **playMethod** | [**PlayMethod**](.md)| The play method. | [optional] [enum: Transcode, DirectStream, DirectPlay]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **repeatMode** | [**RepeatMode**](.md)| The repeat mode. | [optional] [enum: RepeatNone, RepeatAll, RepeatOne]
 **isPaused** | **kotlin.Boolean**| Indicates if the player is paused. | [optional] [default to false]
 **isMuted** | **kotlin.Boolean**| Indicates if the player is muted. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="onPlaybackStart"></a>
# **onPlaybackStart**
> onPlaybackStart(userId, itemId, mediaSourceId, audioStreamIndex, subtitleStreamIndex, playMethod, liveStreamId, playSessionId, canSeek)

Reports that a user has begun playing an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The id of the MediaSource.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | The audio stream index.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | The subtitle stream index.
val playMethod : PlayMethod =  // PlayMethod | The play method.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val canSeek : kotlin.Boolean = true // kotlin.Boolean | Indicates if the client can seek.
try {
    apiInstance.onPlaybackStart(userId, itemId, mediaSourceId, audioStreamIndex, subtitleStreamIndex, playMethod, liveStreamId, playSessionId, canSeek)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#onPlaybackStart")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#onPlaybackStart")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |
 **mediaSourceId** | **kotlin.String**| The id of the MediaSource. | [optional]
 **audioStreamIndex** | **kotlin.Int**| The audio stream index. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| The subtitle stream index. | [optional]
 **playMethod** | [**PlayMethod**](.md)| The play method. | [optional] [enum: Transcode, DirectStream, DirectPlay]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **canSeek** | **kotlin.Boolean**| Indicates if the client can seek. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="onPlaybackStopped"></a>
# **onPlaybackStopped**
> onPlaybackStopped(userId, itemId, mediaSourceId, nextMediaType, positionTicks, liveStreamId, playSessionId)

Reports that a user has stopped playing an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The id of the MediaSource.
val nextMediaType : kotlin.String = nextMediaType_example // kotlin.String | The next media type that will play.
val positionTicks : kotlin.Long = 789 // kotlin.Long | Optional. The position, in ticks, where playback stopped. 1 tick = 10000 ms.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
try {
    apiInstance.onPlaybackStopped(userId, itemId, mediaSourceId, nextMediaType, positionTicks, liveStreamId, playSessionId)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#onPlaybackStopped")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#onPlaybackStopped")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |
 **mediaSourceId** | **kotlin.String**| The id of the MediaSource. | [optional]
 **nextMediaType** | **kotlin.String**| The next media type that will play. | [optional]
 **positionTicks** | **kotlin.Long**| Optional. The position, in ticks, where playback stopped. 1 tick &#x3D; 10000 ms. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="pingPlaybackSession"></a>
# **pingPlaybackSession**
> pingPlaybackSession(playSessionId)

Pings a playback session.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | Playback session id.
try {
    apiInstance.pingPlaybackSession(playSessionId)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#pingPlaybackSession")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#pingPlaybackSession")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playSessionId** | **kotlin.String**| Playback session id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="reportPlaybackProgress"></a>
# **reportPlaybackProgress**
> reportPlaybackProgress(reportPlaybackProgressRequest)

Reports playback progress within a session.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val reportPlaybackProgressRequest : ReportPlaybackProgressRequest =  // ReportPlaybackProgressRequest | The playback progress info.
try {
    apiInstance.reportPlaybackProgress(reportPlaybackProgressRequest)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#reportPlaybackProgress")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#reportPlaybackProgress")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reportPlaybackProgressRequest** | [**ReportPlaybackProgressRequest**](ReportPlaybackProgressRequest.md)| The playback progress info. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="reportPlaybackStart"></a>
# **reportPlaybackStart**
> reportPlaybackStart(reportPlaybackStartRequest)

Reports playback has started within a session.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val reportPlaybackStartRequest : ReportPlaybackStartRequest =  // ReportPlaybackStartRequest | The playback start info.
try {
    apiInstance.reportPlaybackStart(reportPlaybackStartRequest)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#reportPlaybackStart")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#reportPlaybackStart")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reportPlaybackStartRequest** | [**ReportPlaybackStartRequest**](ReportPlaybackStartRequest.md)| The playback start info. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="reportPlaybackStopped"></a>
# **reportPlaybackStopped**
> reportPlaybackStopped(reportPlaybackStoppedRequest)

Reports playback has stopped within a session.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaystateApi()
val reportPlaybackStoppedRequest : ReportPlaybackStoppedRequest =  // ReportPlaybackStoppedRequest | The playback stop info.
try {
    apiInstance.reportPlaybackStopped(reportPlaybackStoppedRequest)
} catch (e: ClientException) {
    println("4xx response calling PlaystateApi#reportPlaybackStopped")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaystateApi#reportPlaybackStopped")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reportPlaybackStoppedRequest** | [**ReportPlaybackStoppedRequest**](ReportPlaybackStoppedRequest.md)| The playback stop info. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

