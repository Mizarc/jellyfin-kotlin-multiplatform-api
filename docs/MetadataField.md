
# MetadataField

## Enum


    * `cast` (value: `"Cast"`)

    * `genres` (value: `"Genres"`)

    * `productionLocations` (value: `"ProductionLocations"`)

    * `studios` (value: `"Studios"`)

    * `tags` (value: `"Tags"`)

    * `name` (value: `"Name"`)

    * `overview` (value: `"Overview"`)

    * `runtime` (value: `"Runtime"`)

    * `officialRating` (value: `"OfficialRating"`)



