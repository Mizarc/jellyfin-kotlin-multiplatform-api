
# ImageFormat

## Enum


    * `bmp` (value: `"Bmp"`)

    * `gif` (value: `"Gif"`)

    * `jpg` (value: `"Jpg"`)

    * `png` (value: `"Png"`)

    * `webp` (value: `"Webp"`)



