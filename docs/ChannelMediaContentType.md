
# ChannelMediaContentType

## Enum


    * `clip` (value: `"Clip"`)

    * `podcast` (value: `"Podcast"`)

    * `trailer` (value: `"Trailer"`)

    * `movie` (value: `"Movie"`)

    * `episode` (value: `"Episode"`)

    * `song` (value: `"Song"`)

    * `movieExtra` (value: `"MovieExtra"`)

    * `tvExtra` (value: `"TvExtra"`)



