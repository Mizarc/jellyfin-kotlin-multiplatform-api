
# BaseItemPerson

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**id** | **kotlin.String** | Gets or sets the identifier. |  [optional]
**role** | **kotlin.String** | Gets or sets the role. |  [optional]
**type** | **kotlin.String** | Gets or sets the type. |  [optional]
**primaryImageTag** | **kotlin.String** | Gets or sets the primary image tag. |  [optional]
**imageBlurHashes** | [**BaseItemPersonImageBlurHashes**](BaseItemPersonImageBlurHashes.md) |  |  [optional]



