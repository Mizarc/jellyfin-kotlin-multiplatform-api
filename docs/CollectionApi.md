# CollectionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addToCollection**](CollectionApi.md#addToCollection) | **POST** /Collections/{collectionId}/Items | Adds items to a collection.
[**createCollection**](CollectionApi.md#createCollection) | **POST** /Collections | Creates a new collection.
[**removeFromCollection**](CollectionApi.md#removeFromCollection) | **DELETE** /Collections/{collectionId}/Items | Removes items from a collection.


<a name="addToCollection"></a>
# **addToCollection**
> addToCollection(collectionId, ids)

Adds items to a collection.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = CollectionApi()
val collectionId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The collection id.
val ids : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Item ids, comma delimited.
try {
    apiInstance.addToCollection(collectionId, ids)
} catch (e: ClientException) {
    println("4xx response calling CollectionApi#addToCollection")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling CollectionApi#addToCollection")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **kotlin.String**| The collection id. |
 **ids** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Item ids, comma delimited. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createCollection"></a>
# **createCollection**
> CollectionCreationResult createCollection(name, ids, parentId, isLocked)

Creates a new collection.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = CollectionApi()
val name : kotlin.String = name_example // kotlin.String | The name of the collection.
val ids : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Item Ids to add to the collection.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Create the collection within a specific folder.
val isLocked : kotlin.Boolean = true // kotlin.Boolean | Whether or not to lock the new collection.
try {
    val result : CollectionCreationResult = apiInstance.createCollection(name, ids, parentId, isLocked)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling CollectionApi#createCollection")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling CollectionApi#createCollection")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the collection. | [optional]
 **ids** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Item Ids to add to the collection. | [optional]
 **parentId** | **kotlin.String**| Optional. Create the collection within a specific folder. | [optional]
 **isLocked** | **kotlin.Boolean**| Whether or not to lock the new collection. | [optional] [default to false]

### Return type

[**CollectionCreationResult**](CollectionCreationResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="removeFromCollection"></a>
# **removeFromCollection**
> removeFromCollection(collectionId, ids)

Removes items from a collection.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = CollectionApi()
val collectionId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The collection id.
val ids : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Item ids, comma delimited.
try {
    apiInstance.removeFromCollection(collectionId, ids)
} catch (e: ClientException) {
    println("4xx response calling CollectionApi#removeFromCollection")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling CollectionApi#removeFromCollection")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **kotlin.String**| The collection id. |
 **ids** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Item ids, comma delimited. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

