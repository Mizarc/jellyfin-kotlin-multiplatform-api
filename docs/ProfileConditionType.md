
# ProfileConditionType

## Enum


    * `equals` (value: `"Equals"`)

    * `notEquals` (value: `"NotEquals"`)

    * `lessThanEqual` (value: `"LessThanEqual"`)

    * `greaterThanEqual` (value: `"GreaterThanEqual"`)

    * `equalsAny` (value: `"EqualsAny"`)



