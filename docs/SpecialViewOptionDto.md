
# SpecialViewOptionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets view option name. |  [optional]
**id** | **kotlin.String** | Gets or sets view option id. |  [optional]



