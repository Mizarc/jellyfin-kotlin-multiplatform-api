# PluginsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**disablePlugin**](PluginsApi.md#disablePlugin) | **POST** /Plugins/{pluginId}/{version}/Disable | Disable a plugin.
[**enablePlugin**](PluginsApi.md#enablePlugin) | **POST** /Plugins/{pluginId}/{version}/Enable | Enables a disabled plugin.
[**getPluginConfiguration**](PluginsApi.md#getPluginConfiguration) | **GET** /Plugins/{pluginId}/Configuration | Gets plugin configuration.
[**getPluginImage**](PluginsApi.md#getPluginImage) | **GET** /Plugins/{pluginId}/{version}/Image | Gets a plugin&#39;s image.
[**getPluginManifest**](PluginsApi.md#getPluginManifest) | **POST** /Plugins/{pluginId}/Manifest | Gets a plugin&#39;s manifest.
[**getPlugins**](PluginsApi.md#getPlugins) | **GET** /Plugins | Gets a list of currently installed plugins.
[**uninstallPlugin**](PluginsApi.md#uninstallPlugin) | **DELETE** /Plugins/{pluginId} | Uninstalls a plugin.
[**uninstallPluginByVersion**](PluginsApi.md#uninstallPluginByVersion) | **DELETE** /Plugins/{pluginId}/{version} | Uninstalls a plugin by version.
[**updatePluginConfiguration**](PluginsApi.md#updatePluginConfiguration) | **POST** /Plugins/{pluginId}/Configuration | Updates plugin configuration.


<a name="disablePlugin"></a>
# **disablePlugin**
> disablePlugin(pluginId, version)

Disable a plugin.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
val version : kotlin.String = version_example // kotlin.String | Plugin version.
try {
    apiInstance.disablePlugin(pluginId, version)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#disablePlugin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#disablePlugin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |
 **version** | **kotlin.String**| Plugin version. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="enablePlugin"></a>
# **enablePlugin**
> enablePlugin(pluginId, version)

Enables a disabled plugin.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
val version : kotlin.String = version_example // kotlin.String | Plugin version.
try {
    apiInstance.enablePlugin(pluginId, version)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#enablePlugin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#enablePlugin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |
 **version** | **kotlin.String**| Plugin version. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPluginConfiguration"></a>
# **getPluginConfiguration**
> kotlin.String getPluginConfiguration(pluginId)

Gets plugin configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
try {
    val result : kotlin.String = apiInstance.getPluginConfiguration(pluginId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#getPluginConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#getPluginConfiguration")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |

### Return type

**kotlin.String**

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPluginImage"></a>
# **getPluginImage**
> org.openapitools.client.infrastructure.OctetByteArray getPluginImage(pluginId, version)

Gets a plugin&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
val version : kotlin.String = version_example // kotlin.String | Plugin version.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getPluginImage(pluginId, version)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#getPluginImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#getPluginImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |
 **version** | **kotlin.String**| Plugin version. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPluginManifest"></a>
# **getPluginManifest**
> getPluginManifest(pluginId)

Gets a plugin&#39;s manifest.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
try {
    apiInstance.getPluginManifest(pluginId)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#getPluginManifest")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#getPluginManifest")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPlugins"></a>
# **getPlugins**
> kotlin.collections.List&lt;PluginInfo&gt; getPlugins()

Gets a list of currently installed plugins.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
try {
    val result : kotlin.collections.List<PluginInfo> = apiInstance.getPlugins()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#getPlugins")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#getPlugins")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;PluginInfo&gt;**](PluginInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="uninstallPlugin"></a>
# **uninstallPlugin**
> uninstallPlugin(pluginId)

Uninstalls a plugin.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
try {
    apiInstance.uninstallPlugin(pluginId)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#uninstallPlugin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#uninstallPlugin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="uninstallPluginByVersion"></a>
# **uninstallPluginByVersion**
> uninstallPluginByVersion(pluginId, version)

Uninstalls a plugin by version.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
val version : kotlin.String = version_example // kotlin.String | Plugin version.
try {
    apiInstance.uninstallPluginByVersion(pluginId, version)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#uninstallPluginByVersion")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#uninstallPluginByVersion")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |
 **version** | **kotlin.String**| Plugin version. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updatePluginConfiguration"></a>
# **updatePluginConfiguration**
> updatePluginConfiguration(pluginId)

Updates plugin configuration.

Accepts plugin configuration as JSON body.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PluginsApi()
val pluginId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Plugin id.
try {
    apiInstance.updatePluginConfiguration(pluginId)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#updatePluginConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#updatePluginConfiguration")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginId** | **kotlin.String**| Plugin id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

