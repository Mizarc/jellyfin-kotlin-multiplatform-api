
# CountryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**displayName** | **kotlin.String** | Gets or sets the display name. |  [optional]
**twoLetterISORegionName** | **kotlin.String** | Gets or sets the name of the two letter ISO region. |  [optional]
**threeLetterISORegionName** | **kotlin.String** | Gets or sets the name of the three letter ISO region. |  [optional]



