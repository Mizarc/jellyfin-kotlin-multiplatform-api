
# PlaystateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**command** | [**PlaystateCommand**](PlaystateCommand.md) |  |  [optional]
**seekPositionTicks** | **kotlin.Long** |  |  [optional]
**controllingUserId** | **kotlin.String** | Gets or sets the controlling user identifier. |  [optional]



