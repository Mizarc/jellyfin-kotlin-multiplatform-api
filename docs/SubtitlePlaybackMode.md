
# SubtitlePlaybackMode

## Enum


    * `default` (value: `"Default"`)

    * `always` (value: `"Always"`)

    * `onlyForced` (value: `"OnlyForced"`)

    * `none` (value: `"None"`)

    * `smart` (value: `"Smart"`)



