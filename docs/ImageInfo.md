
# ImageInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imageType** | [**ImageType**](ImageType.md) |  |  [optional]
**imageIndex** | **kotlin.Int** | Gets or sets the index of the image. |  [optional]
**imageTag** | **kotlin.String** | Gets or sets the image tag. |  [optional]
**path** | **kotlin.String** | Gets or sets the path. |  [optional]
**blurHash** | **kotlin.String** | Gets or sets the blurhash. |  [optional]
**height** | **kotlin.Int** | Gets or sets the height. |  [optional]
**width** | **kotlin.Int** | Gets or sets the width. |  [optional]
**propertySize** | **kotlin.Long** | Gets or sets the size. |  [optional]



