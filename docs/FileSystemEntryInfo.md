
# FileSystemEntryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets the name. |  [optional]
**path** | **kotlin.String** | Gets the path. |  [optional]
**type** | [**FileSystemEntryType**](FileSystemEntryType.md) |  |  [optional]



