
# TunerChannelMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  |  [optional]
**providerChannelName** | **kotlin.String** |  |  [optional]
**providerChannelId** | **kotlin.String** |  |  [optional]
**id** | **kotlin.String** |  |  [optional]



