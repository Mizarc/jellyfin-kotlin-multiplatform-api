
# ChannelFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**id** | **kotlin.String** | Gets or sets the identifier. |  [optional]
**canSearch** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance can search. |  [optional]
**mediaTypes** | [**kotlin.collections.List&lt;ChannelMediaType&gt;**](ChannelMediaType.md) | Gets or sets the media types. |  [optional]
**contentTypes** | [**kotlin.collections.List&lt;ChannelMediaContentType&gt;**](ChannelMediaContentType.md) | Gets or sets the content types. |  [optional]
**maxPageSize** | **kotlin.Int** | Gets or sets the maximum number of records the channel allows retrieving at a time. |  [optional]
**autoRefreshLevels** | **kotlin.Int** | Gets or sets the automatic refresh levels. |  [optional]
**defaultSortFields** | [**kotlin.collections.List&lt;ChannelItemSortField&gt;**](ChannelItemSortField.md) | Gets or sets the default sort orders. |  [optional]
**supportsSortOrderToggle** | **kotlin.Boolean** | Gets or sets a value indicating whether a sort ascending/descending toggle is supported. |  [optional]
**supportsLatestMedia** | **kotlin.Boolean** | Gets or sets a value indicating whether [supports latest media]. |  [optional]
**canFilter** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance can filter. |  [optional]
**supportsContentDownloading** | **kotlin.Boolean** | Gets or sets a value indicating whether [supports content downloading]. |  [optional]



