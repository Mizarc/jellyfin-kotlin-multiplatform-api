
# MessageCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **kotlin.String** |  | 
**header** | **kotlin.String** |  |  [optional]
**timeoutMs** | **kotlin.Long** |  |  [optional]



