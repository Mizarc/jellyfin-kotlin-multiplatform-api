
# RepositoryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**url** | **kotlin.String** | Gets or sets the URL. |  [optional]
**enabled** | **kotlin.Boolean** | Gets or sets a value indicating whether the repository is enabled. |  [optional]



