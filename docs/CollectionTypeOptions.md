
# CollectionTypeOptions

## Enum


    * `movies` (value: `"Movies"`)

    * `tvShows` (value: `"TvShows"`)

    * `music` (value: `"Music"`)

    * `musicVideos` (value: `"MusicVideos"`)

    * `homeVideos` (value: `"HomeVideos"`)

    * `boxSets` (value: `"BoxSets"`)

    * `books` (value: `"Books"`)

    * `mixed` (value: `"Mixed"`)



