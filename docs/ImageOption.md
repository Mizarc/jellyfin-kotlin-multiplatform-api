
# ImageOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**ImageType**](ImageType.md) |  |  [optional]
**limit** | **kotlin.Int** | Gets or sets the limit. |  [optional]
**minWidth** | **kotlin.Int** | Gets or sets the minimum width. |  [optional]



