
# SyncPlaySetIgnoreWaitRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ignoreWait** | **kotlin.Boolean** | Gets or sets a value indicating whether the client should be ignored. |  [optional]



