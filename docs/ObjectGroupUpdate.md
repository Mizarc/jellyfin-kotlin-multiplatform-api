
# ObjectGroupUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **kotlin.String** | Gets the group identifier. |  [optional]
**type** | [**GroupUpdateType**](GroupUpdateType.md) |  |  [optional]
**&#x60;data&#x60;** | [**kotlin.Any**](.md) | Gets the update data. |  [optional]



