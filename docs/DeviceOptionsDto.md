
# DeviceOptionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | Gets or sets the id. |  [optional]
**deviceId** | **kotlin.String** | Gets or sets the device id. |  [optional]
**customName** | **kotlin.String** | Gets or sets the custom name. |  [optional]



