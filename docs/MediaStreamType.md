
# MediaStreamType

## Enum


    * `audio` (value: `"Audio"`)

    * `video` (value: `"Video"`)

    * `subtitle` (value: `"Subtitle"`)

    * `embeddedImage` (value: `"EmbeddedImage"`)

    * `&#x60;data&#x60;` (value: `"Data"`)



