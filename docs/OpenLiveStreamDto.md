
# OpenLiveStreamDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**openToken** | **kotlin.String** | Gets or sets the open token. |  [optional]
**userId** | **kotlin.String** | Gets or sets the user id. |  [optional]
**playSessionId** | **kotlin.String** | Gets or sets the play session id. |  [optional]
**maxStreamingBitrate** | **kotlin.Int** | Gets or sets the max streaming bitrate. |  [optional]
**startTimeTicks** | **kotlin.Long** | Gets or sets the start time in ticks. |  [optional]
**audioStreamIndex** | **kotlin.Int** | Gets or sets the audio stream index. |  [optional]
**subtitleStreamIndex** | **kotlin.Int** | Gets or sets the subtitle stream index. |  [optional]
**maxAudioChannels** | **kotlin.Int** | Gets or sets the max audio channels. |  [optional]
**itemId** | **kotlin.String** | Gets or sets the item id. |  [optional]
**enableDirectPlay** | **kotlin.Boolean** | Gets or sets a value indicating whether to enable direct play. |  [optional]
**enableDirectStream** | **kotlin.Boolean** | Gets or sets a value indicating whether to enale direct stream. |  [optional]
**deviceProfile** | [**ClientCapabilitiesDtoDeviceProfile**](ClientCapabilitiesDtoDeviceProfile.md) |  |  [optional]
**directPlayProtocols** | [**kotlin.collections.List&lt;MediaProtocol&gt;**](MediaProtocol.md) | Gets or sets the device play protocols. |  [optional]



