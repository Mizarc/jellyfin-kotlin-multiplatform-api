
# ChapterInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startPositionTicks** | **kotlin.Long** | Gets or sets the start position ticks. |  [optional]
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**imagePath** | **kotlin.String** | Gets or sets the image path. |  [optional]
**imageDateModified** | **kotlin.String** |  |  [optional]
**imageTag** | **kotlin.String** |  |  [optional]



