
# NotificationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | Gets or sets the notification ID. Defaults to an empty string. |  [optional]
**userId** | **kotlin.String** | Gets or sets the notification&#39;s user ID. Defaults to an empty string. |  [optional]
**date** | **kotlin.String** | Gets or sets the notification date. |  [optional]
**isRead** | **kotlin.Boolean** | Gets or sets a value indicating whether the notification has been read. Defaults to false. |  [optional]
**name** | **kotlin.String** | Gets or sets the notification&#39;s name. Defaults to an empty string. |  [optional]
**description** | **kotlin.String** | Gets or sets the notification&#39;s description. Defaults to an empty string. |  [optional]
**url** | **kotlin.String** | Gets or sets the notification&#39;s URL. Defaults to an empty string. |  [optional]
**level** | [**NotificationLevel**](NotificationLevel.md) |  |  [optional]



