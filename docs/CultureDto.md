
# CultureDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets the name. |  [optional]
**displayName** | **kotlin.String** | Gets the display name. |  [optional]
**twoLetterISOLanguageName** | **kotlin.String** | Gets the name of the two letter ISO language. |  [optional]
**threeLetterISOLanguageName** | **kotlin.String** | Gets the name of the three letter ISO language. |  [optional] [readonly]
**threeLetterISOLanguageNames** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



