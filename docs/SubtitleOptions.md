
# SubtitleOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skipIfEmbeddedSubtitlesPresent** | **kotlin.Boolean** |  |  [optional]
**skipIfAudioTrackMatches** | **kotlin.Boolean** |  |  [optional]
**downloadLanguages** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**downloadMovieSubtitles** | **kotlin.Boolean** |  |  [optional]
**downloadEpisodeSubtitles** | **kotlin.Boolean** |  |  [optional]
**openSubtitlesUsername** | **kotlin.String** |  |  [optional]
**openSubtitlesPasswordHash** | **kotlin.String** |  |  [optional]
**isOpenSubtitleVipAccount** | **kotlin.Boolean** |  |  [optional]
**requirePerfectMatch** | **kotlin.Boolean** |  |  [optional]



