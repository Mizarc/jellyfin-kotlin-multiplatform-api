
# BufferRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**&#x60;when&#x60;** | **kotlin.String** | Gets or sets when the request has been made by the client. |  [optional]
**positionTicks** | **kotlin.Long** | Gets or sets the position ticks. |  [optional]
**isPlaying** | **kotlin.Boolean** | Gets or sets a value indicating whether the client playback is unpaused. |  [optional]
**playlistItemId** | **kotlin.String** | Gets or sets the playlist item identifier of the playing item. |  [optional]



