
# ClientLogDocumentResponseDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileName** | **kotlin.String** | Gets the resulting filename. |  [optional]



