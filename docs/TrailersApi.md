# TrailersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTrailers**](TrailersApi.md#getTrailers) | **GET** /Trailers | Finds movies and trailers similar to a given trailer.


<a name="getTrailers"></a>
# **getTrailers**
> BaseItemDtoQueryResult getTrailers(userId, maxOfficialRating, hasThemeSong, hasThemeVideo, hasSubtitles, hasSpecialFeature, hasTrailer, adjacentTo, parentIndexNumber, hasParentalRating, isHd, is4K, locationTypes, excludeLocationTypes, isMissing, isUnaired, minCommunityRating, minCriticRating, minPremiereDate, minDateLastSaved, minDateLastSavedForUser, maxPremiereDate, hasOverview, hasImdbId, hasTmdbId, hasTvdbId, isMovie, isSeries, isNews, isKids, isSports, excludeItemIds, startIndex, limit, recursive, searchTerm, sortOrder, parentId, fields, excludeItemTypes, filters, isFavorite, mediaTypes, imageTypes, sortBy, isPlayed, genres, officialRatings, tags, years, enableUserData, imageTypeLimit, enableImageTypes, person, personIds, personTypes, studios, artists, excludeArtistIds, artistIds, albumArtistIds, contributingArtistIds, albums, albumIds, ids, videoTypes, minOfficialRating, isLocked, isPlaceHolder, hasOfficialRating, collapseBoxSetItems, minWidth, minHeight, maxWidth, maxHeight, is3D, seriesStatus, nameStartsWithOrGreater, nameStartsWith, nameLessThan, studioIds, genreIds, enableTotalRecordCount, enableImages)

Finds movies and trailers similar to a given trailer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = TrailersApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val maxOfficialRating : kotlin.String = maxOfficialRating_example // kotlin.String | Optional filter by maximum official rating (PG, PG-13, TV-MA, etc).
val hasThemeSong : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items with theme songs.
val hasThemeVideo : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items with theme videos.
val hasSubtitles : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items with subtitles.
val hasSpecialFeature : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items with special features.
val hasTrailer : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items with trailers.
val adjacentTo : kotlin.String = adjacentTo_example // kotlin.String | Optional. Return items that are siblings of a supplied item.
val parentIndexNumber : kotlin.Int = 56 // kotlin.Int | Optional filter by parent index number.
val hasParentalRating : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that have or do not have a parental rating.
val isHd : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are HD or not.
val is4K : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are 4K or not.
val locationTypes : kotlin.collections.List<LocationType> =  // kotlin.collections.List<LocationType> | Optional. If specified, results will be filtered based on LocationType. This allows multiple, comma delimited.
val excludeLocationTypes : kotlin.collections.List<LocationType> =  // kotlin.collections.List<LocationType> | Optional. If specified, results will be filtered based on the LocationType. This allows multiple, comma delimited.
val isMissing : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are missing episodes or not.
val isUnaired : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are unaired episodes or not.
val minCommunityRating : kotlin.Double = 1.2 // kotlin.Double | Optional filter by minimum community rating.
val minCriticRating : kotlin.Double = 1.2 // kotlin.Double | Optional filter by minimum critic rating.
val minPremiereDate : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The minimum premiere date. Format = ISO.
val minDateLastSaved : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The minimum last saved date. Format = ISO.
val minDateLastSavedForUser : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The minimum last saved date for the current user. Format = ISO.
val maxPremiereDate : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The maximum premiere date. Format = ISO.
val hasOverview : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that have an overview or not.
val hasImdbId : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that have an imdb id or not.
val hasTmdbId : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that have a tmdb id or not.
val hasTvdbId : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that have a tvdb id or not.
val isMovie : kotlin.Boolean = true // kotlin.Boolean | Optional filter for live tv movies.
val isSeries : kotlin.Boolean = true // kotlin.Boolean | Optional filter for live tv series.
val isNews : kotlin.Boolean = true // kotlin.Boolean | Optional filter for live tv news.
val isKids : kotlin.Boolean = true // kotlin.Boolean | Optional filter for live tv kids.
val isSports : kotlin.Boolean = true // kotlin.Boolean | Optional filter for live tv sports.
val excludeItemIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered by excluding item ids. This allows multiple, comma delimited.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val recursive : kotlin.Boolean = true // kotlin.Boolean | When searching within folders, this determines whether or not the search will be recursive. true/false.
val searchTerm : kotlin.String = searchTerm_example // kotlin.String | Optional. Filter based on a search term.
val sortOrder : kotlin.collections.List<SortOrder> =  // kotlin.collections.List<SortOrder> | Sort Order - Ascending,Descending.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines.
val excludeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited.
val filters : kotlin.collections.List<ItemFilter> =  // kotlin.collections.List<ItemFilter> | Optional. Specify additional filters to apply. This allows multiple, comma delimited. Options: IsFolder, IsNotFolder, IsUnplayed, IsPlayed, IsFavorite, IsResumable, Likes, Dislikes.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are marked as favorite, or not.
val mediaTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional filter by MediaType. Allows multiple, comma delimited.
val imageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. If specified, results will be filtered based on those containing image types. This allows multiple, comma delimited.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime.
val isPlayed : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are played, or not.
val genres : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on genre. This allows multiple, pipe delimited.
val officialRatings : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on OfficialRating. This allows multiple, pipe delimited.
val tags : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on tag. This allows multiple, pipe delimited.
val years : kotlin.collections.List<kotlin.Int> =  // kotlin.collections.List<kotlin.Int> | Optional. If specified, results will be filtered based on production year. This allows multiple, comma delimited.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional, include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional, the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val person : kotlin.String = person_example // kotlin.String | Optional. If specified, results will be filtered to include only those containing the specified person.
val personIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered to include only those containing the specified person id.
val personTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, along with Person, results will be filtered to include only those containing the specified person and PersonType. Allows multiple, comma-delimited.
val studios : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on studio. This allows multiple, pipe delimited.
val artists : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on artists. This allows multiple, pipe delimited.
val excludeArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on artist id. This allows multiple, pipe delimited.
val artistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered to include only those containing the specified artist id.
val albumArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered to include only those containing the specified album artist id.
val contributingArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered to include only those containing the specified contributing artist id.
val albums : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on album. This allows multiple, pipe delimited.
val albumIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on album id. This allows multiple, pipe delimited.
val ids : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specific items are needed, specify a list of item id's to retrieve. This allows multiple, comma delimited.
val videoTypes : kotlin.collections.List<VideoType> =  // kotlin.collections.List<VideoType> | Optional filter by VideoType (videofile, dvd, bluray, iso). Allows multiple, comma delimited.
val minOfficialRating : kotlin.String = minOfficialRating_example // kotlin.String | Optional filter by minimum official rating (PG, PG-13, TV-MA, etc).
val isLocked : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are locked.
val isPlaceHolder : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are placeholders.
val hasOfficialRating : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that have official ratings.
val collapseBoxSetItems : kotlin.Boolean = true // kotlin.Boolean | Whether or not to hide items behind their boxsets.
val minWidth : kotlin.Int = 56 // kotlin.Int | Optional. Filter by the minimum width of the item.
val minHeight : kotlin.Int = 56 // kotlin.Int | Optional. Filter by the minimum height of the item.
val maxWidth : kotlin.Int = 56 // kotlin.Int | Optional. Filter by the maximum width of the item.
val maxHeight : kotlin.Int = 56 // kotlin.Int | Optional. Filter by the maximum height of the item.
val is3D : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are 3D, or not.
val seriesStatus : kotlin.collections.List<SeriesStatus> =  // kotlin.collections.List<SeriesStatus> | Optional filter by Series Status. Allows multiple, comma delimited.
val nameStartsWithOrGreater : kotlin.String = nameStartsWithOrGreater_example // kotlin.String | Optional filter by items whose name is sorted equally or greater than a given input string.
val nameStartsWith : kotlin.String = nameStartsWith_example // kotlin.String | Optional filter by items whose name is sorted equally than a given input string.
val nameLessThan : kotlin.String = nameLessThan_example // kotlin.String | Optional filter by items whose name is equally or lesser than a given input string.
val studioIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on studio id. This allows multiple, pipe delimited.
val genreIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. If specified, results will be filtered based on genre id. This allows multiple, pipe delimited.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Optional. Enable the total record count.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional, include image information in output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getTrailers(userId, maxOfficialRating, hasThemeSong, hasThemeVideo, hasSubtitles, hasSpecialFeature, hasTrailer, adjacentTo, parentIndexNumber, hasParentalRating, isHd, is4K, locationTypes, excludeLocationTypes, isMissing, isUnaired, minCommunityRating, minCriticRating, minPremiereDate, minDateLastSaved, minDateLastSavedForUser, maxPremiereDate, hasOverview, hasImdbId, hasTmdbId, hasTvdbId, isMovie, isSeries, isNews, isKids, isSports, excludeItemIds, startIndex, limit, recursive, searchTerm, sortOrder, parentId, fields, excludeItemTypes, filters, isFavorite, mediaTypes, imageTypes, sortBy, isPlayed, genres, officialRatings, tags, years, enableUserData, imageTypeLimit, enableImageTypes, person, personIds, personTypes, studios, artists, excludeArtistIds, artistIds, albumArtistIds, contributingArtistIds, albums, albumIds, ids, videoTypes, minOfficialRating, isLocked, isPlaceHolder, hasOfficialRating, collapseBoxSetItems, minWidth, minHeight, maxWidth, maxHeight, is3D, seriesStatus, nameStartsWithOrGreater, nameStartsWith, nameLessThan, studioIds, genreIds, enableTotalRecordCount, enableImages)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling TrailersApi#getTrailers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling TrailersApi#getTrailers")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. | [optional]
 **maxOfficialRating** | **kotlin.String**| Optional filter by maximum official rating (PG, PG-13, TV-MA, etc). | [optional]
 **hasThemeSong** | **kotlin.Boolean**| Optional filter by items with theme songs. | [optional]
 **hasThemeVideo** | **kotlin.Boolean**| Optional filter by items with theme videos. | [optional]
 **hasSubtitles** | **kotlin.Boolean**| Optional filter by items with subtitles. | [optional]
 **hasSpecialFeature** | **kotlin.Boolean**| Optional filter by items with special features. | [optional]
 **hasTrailer** | **kotlin.Boolean**| Optional filter by items with trailers. | [optional]
 **adjacentTo** | **kotlin.String**| Optional. Return items that are siblings of a supplied item. | [optional]
 **parentIndexNumber** | **kotlin.Int**| Optional filter by parent index number. | [optional]
 **hasParentalRating** | **kotlin.Boolean**| Optional filter by items that have or do not have a parental rating. | [optional]
 **isHd** | **kotlin.Boolean**| Optional filter by items that are HD or not. | [optional]
 **is4K** | **kotlin.Boolean**| Optional filter by items that are 4K or not. | [optional]
 **locationTypes** | [**kotlin.collections.List&lt;LocationType&gt;**](LocationType.md)| Optional. If specified, results will be filtered based on LocationType. This allows multiple, comma delimited. | [optional]
 **excludeLocationTypes** | [**kotlin.collections.List&lt;LocationType&gt;**](LocationType.md)| Optional. If specified, results will be filtered based on the LocationType. This allows multiple, comma delimited. | [optional]
 **isMissing** | **kotlin.Boolean**| Optional filter by items that are missing episodes or not. | [optional]
 **isUnaired** | **kotlin.Boolean**| Optional filter by items that are unaired episodes or not. | [optional]
 **minCommunityRating** | **kotlin.Double**| Optional filter by minimum community rating. | [optional]
 **minCriticRating** | **kotlin.Double**| Optional filter by minimum critic rating. | [optional]
 **minPremiereDate** | **kotlin.String**| Optional. The minimum premiere date. Format &#x3D; ISO. | [optional]
 **minDateLastSaved** | **kotlin.String**| Optional. The minimum last saved date. Format &#x3D; ISO. | [optional]
 **minDateLastSavedForUser** | **kotlin.String**| Optional. The minimum last saved date for the current user. Format &#x3D; ISO. | [optional]
 **maxPremiereDate** | **kotlin.String**| Optional. The maximum premiere date. Format &#x3D; ISO. | [optional]
 **hasOverview** | **kotlin.Boolean**| Optional filter by items that have an overview or not. | [optional]
 **hasImdbId** | **kotlin.Boolean**| Optional filter by items that have an imdb id or not. | [optional]
 **hasTmdbId** | **kotlin.Boolean**| Optional filter by items that have a tmdb id or not. | [optional]
 **hasTvdbId** | **kotlin.Boolean**| Optional filter by items that have a tvdb id or not. | [optional]
 **isMovie** | **kotlin.Boolean**| Optional filter for live tv movies. | [optional]
 **isSeries** | **kotlin.Boolean**| Optional filter for live tv series. | [optional]
 **isNews** | **kotlin.Boolean**| Optional filter for live tv news. | [optional]
 **isKids** | **kotlin.Boolean**| Optional filter for live tv kids. | [optional]
 **isSports** | **kotlin.Boolean**| Optional filter for live tv sports. | [optional]
 **excludeItemIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered by excluding item ids. This allows multiple, comma delimited. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **recursive** | **kotlin.Boolean**| When searching within folders, this determines whether or not the search will be recursive. true/false. | [optional]
 **searchTerm** | **kotlin.String**| Optional. Filter based on a search term. | [optional]
 **sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md)| Sort Order - Ascending,Descending. | [optional]
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines. | [optional]
 **excludeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. | [optional]
 **filters** | [**kotlin.collections.List&lt;ItemFilter&gt;**](ItemFilter.md)| Optional. Specify additional filters to apply. This allows multiple, comma delimited. Options: IsFolder, IsNotFolder, IsUnplayed, IsPlayed, IsFavorite, IsResumable, Likes, Dislikes. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional filter by items that are marked as favorite, or not. | [optional]
 **mediaTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional filter by MediaType. Allows multiple, comma delimited. | [optional]
 **imageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. If specified, results will be filtered based on those containing image types. This allows multiple, comma delimited. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime. | [optional]
 **isPlayed** | **kotlin.Boolean**| Optional filter by items that are played, or not. | [optional]
 **genres** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on genre. This allows multiple, pipe delimited. | [optional]
 **officialRatings** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on OfficialRating. This allows multiple, pipe delimited. | [optional]
 **tags** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on tag. This allows multiple, pipe delimited. | [optional]
 **years** | [**kotlin.collections.List&lt;kotlin.Int&gt;**](kotlin.Int.md)| Optional. If specified, results will be filtered based on production year. This allows multiple, comma delimited. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional, include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional, the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **person** | **kotlin.String**| Optional. If specified, results will be filtered to include only those containing the specified person. | [optional]
 **personIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered to include only those containing the specified person id. | [optional]
 **personTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, along with Person, results will be filtered to include only those containing the specified person and PersonType. Allows multiple, comma-delimited. | [optional]
 **studios** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on studio. This allows multiple, pipe delimited. | [optional]
 **artists** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on artists. This allows multiple, pipe delimited. | [optional]
 **excludeArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on artist id. This allows multiple, pipe delimited. | [optional]
 **artistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered to include only those containing the specified artist id. | [optional]
 **albumArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered to include only those containing the specified album artist id. | [optional]
 **contributingArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered to include only those containing the specified contributing artist id. | [optional]
 **albums** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on album. This allows multiple, pipe delimited. | [optional]
 **albumIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on album id. This allows multiple, pipe delimited. | [optional]
 **ids** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specific items are needed, specify a list of item id&#39;s to retrieve. This allows multiple, comma delimited. | [optional]
 **videoTypes** | [**kotlin.collections.List&lt;VideoType&gt;**](VideoType.md)| Optional filter by VideoType (videofile, dvd, bluray, iso). Allows multiple, comma delimited. | [optional]
 **minOfficialRating** | **kotlin.String**| Optional filter by minimum official rating (PG, PG-13, TV-MA, etc). | [optional]
 **isLocked** | **kotlin.Boolean**| Optional filter by items that are locked. | [optional]
 **isPlaceHolder** | **kotlin.Boolean**| Optional filter by items that are placeholders. | [optional]
 **hasOfficialRating** | **kotlin.Boolean**| Optional filter by items that have official ratings. | [optional]
 **collapseBoxSetItems** | **kotlin.Boolean**| Whether or not to hide items behind their boxsets. | [optional]
 **minWidth** | **kotlin.Int**| Optional. Filter by the minimum width of the item. | [optional]
 **minHeight** | **kotlin.Int**| Optional. Filter by the minimum height of the item. | [optional]
 **maxWidth** | **kotlin.Int**| Optional. Filter by the maximum width of the item. | [optional]
 **maxHeight** | **kotlin.Int**| Optional. Filter by the maximum height of the item. | [optional]
 **is3D** | **kotlin.Boolean**| Optional filter by items that are 3D, or not. | [optional]
 **seriesStatus** | [**kotlin.collections.List&lt;SeriesStatus&gt;**](SeriesStatus.md)| Optional filter by Series Status. Allows multiple, comma delimited. | [optional]
 **nameStartsWithOrGreater** | **kotlin.String**| Optional filter by items whose name is sorted equally or greater than a given input string. | [optional]
 **nameStartsWith** | **kotlin.String**| Optional filter by items whose name is sorted equally than a given input string. | [optional]
 **nameLessThan** | **kotlin.String**| Optional filter by items whose name is equally or lesser than a given input string. | [optional]
 **studioIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on studio id. This allows multiple, pipe delimited. | [optional]
 **genreIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. If specified, results will be filtered based on genre id. This allows multiple, pipe delimited. | [optional]
 **enableTotalRecordCount** | **kotlin.Boolean**| Optional. Enable the total record count. | [optional] [default to true]
 **enableImages** | **kotlin.Boolean**| Optional, include image information in output. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

