# ItemLookupApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**applySearchCriteria**](ItemLookupApi.md#applySearchCriteria) | **POST** /Items/RemoteSearch/Apply/{itemId} | Applies search criteria to an item and refreshes metadata.
[**getBookRemoteSearchResults**](ItemLookupApi.md#getBookRemoteSearchResults) | **POST** /Items/RemoteSearch/Book | Get book remote search.
[**getBoxSetRemoteSearchResults**](ItemLookupApi.md#getBoxSetRemoteSearchResults) | **POST** /Items/RemoteSearch/BoxSet | Get box set remote search.
[**getExternalIdInfos**](ItemLookupApi.md#getExternalIdInfos) | **GET** /Items/{itemId}/ExternalIdInfos | Get the item&#39;s external id info.
[**getMovieRemoteSearchResults**](ItemLookupApi.md#getMovieRemoteSearchResults) | **POST** /Items/RemoteSearch/Movie | Get movie remote search.
[**getMusicAlbumRemoteSearchResults**](ItemLookupApi.md#getMusicAlbumRemoteSearchResults) | **POST** /Items/RemoteSearch/MusicAlbum | Get music album remote search.
[**getMusicArtistRemoteSearchResults**](ItemLookupApi.md#getMusicArtistRemoteSearchResults) | **POST** /Items/RemoteSearch/MusicArtist | Get music artist remote search.
[**getMusicVideoRemoteSearchResults**](ItemLookupApi.md#getMusicVideoRemoteSearchResults) | **POST** /Items/RemoteSearch/MusicVideo | Get music video remote search.
[**getPersonRemoteSearchResults**](ItemLookupApi.md#getPersonRemoteSearchResults) | **POST** /Items/RemoteSearch/Person | Get person remote search.
[**getSeriesRemoteSearchResults**](ItemLookupApi.md#getSeriesRemoteSearchResults) | **POST** /Items/RemoteSearch/Series | Get series remote search.
[**getTrailerRemoteSearchResults**](ItemLookupApi.md#getTrailerRemoteSearchResults) | **POST** /Items/RemoteSearch/Trailer | Get trailer remote search.


<a name="applySearchCriteria"></a>
# **applySearchCriteria**
> applySearchCriteria(itemId, applySearchCriteriaRequest, replaceAllImages)

Applies search criteria to an item and refreshes metadata.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val applySearchCriteriaRequest : ApplySearchCriteriaRequest =  // ApplySearchCriteriaRequest | The remote search result.
val replaceAllImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether or not to replace all images. Default: True.
try {
    apiInstance.applySearchCriteria(itemId, applySearchCriteriaRequest, replaceAllImages)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#applySearchCriteria")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#applySearchCriteria")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **applySearchCriteriaRequest** | [**ApplySearchCriteriaRequest**](ApplySearchCriteriaRequest.md)| The remote search result. |
 **replaceAllImages** | **kotlin.Boolean**| Optional. Whether or not to replace all images. Default: True. | [optional] [default to true]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="getBookRemoteSearchResults"></a>
# **getBookRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getBookRemoteSearchResults(getBookRemoteSearchResultsRequest)

Get book remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getBookRemoteSearchResultsRequest : GetBookRemoteSearchResultsRequest =  // GetBookRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getBookRemoteSearchResults(getBookRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getBookRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getBookRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getBookRemoteSearchResultsRequest** | [**GetBookRemoteSearchResultsRequest**](GetBookRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getBoxSetRemoteSearchResults"></a>
# **getBoxSetRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getBoxSetRemoteSearchResults(getBoxSetRemoteSearchResultsRequest)

Get box set remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getBoxSetRemoteSearchResultsRequest : GetBoxSetRemoteSearchResultsRequest =  // GetBoxSetRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getBoxSetRemoteSearchResults(getBoxSetRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getBoxSetRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getBoxSetRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getBoxSetRemoteSearchResultsRequest** | [**GetBoxSetRemoteSearchResultsRequest**](GetBoxSetRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getExternalIdInfos"></a>
# **getExternalIdInfos**
> kotlin.collections.List&lt;ExternalIdInfo&gt; getExternalIdInfos(itemId)

Get the item&#39;s external id info.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : kotlin.collections.List<ExternalIdInfo> = apiInstance.getExternalIdInfos(itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getExternalIdInfos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getExternalIdInfos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**kotlin.collections.List&lt;ExternalIdInfo&gt;**](ExternalIdInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMovieRemoteSearchResults"></a>
# **getMovieRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getMovieRemoteSearchResults(getMovieRemoteSearchResultsRequest)

Get movie remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getMovieRemoteSearchResultsRequest : GetMovieRemoteSearchResultsRequest =  // GetMovieRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getMovieRemoteSearchResults(getMovieRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getMovieRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getMovieRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getMovieRemoteSearchResultsRequest** | [**GetMovieRemoteSearchResultsRequest**](GetMovieRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMusicAlbumRemoteSearchResults"></a>
# **getMusicAlbumRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getMusicAlbumRemoteSearchResults(getMusicAlbumRemoteSearchResultsRequest)

Get music album remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getMusicAlbumRemoteSearchResultsRequest : GetMusicAlbumRemoteSearchResultsRequest =  // GetMusicAlbumRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getMusicAlbumRemoteSearchResults(getMusicAlbumRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getMusicAlbumRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getMusicAlbumRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getMusicAlbumRemoteSearchResultsRequest** | [**GetMusicAlbumRemoteSearchResultsRequest**](GetMusicAlbumRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMusicArtistRemoteSearchResults"></a>
# **getMusicArtistRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getMusicArtistRemoteSearchResults(getMusicArtistRemoteSearchResultsRequest)

Get music artist remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getMusicArtistRemoteSearchResultsRequest : GetMusicArtistRemoteSearchResultsRequest =  // GetMusicArtistRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getMusicArtistRemoteSearchResults(getMusicArtistRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getMusicArtistRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getMusicArtistRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getMusicArtistRemoteSearchResultsRequest** | [**GetMusicArtistRemoteSearchResultsRequest**](GetMusicArtistRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMusicVideoRemoteSearchResults"></a>
# **getMusicVideoRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getMusicVideoRemoteSearchResults(getMusicVideoRemoteSearchResultsRequest)

Get music video remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getMusicVideoRemoteSearchResultsRequest : GetMusicVideoRemoteSearchResultsRequest =  // GetMusicVideoRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getMusicVideoRemoteSearchResults(getMusicVideoRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getMusicVideoRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getMusicVideoRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getMusicVideoRemoteSearchResultsRequest** | [**GetMusicVideoRemoteSearchResultsRequest**](GetMusicVideoRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPersonRemoteSearchResults"></a>
# **getPersonRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getPersonRemoteSearchResults(getPersonRemoteSearchResultsRequest)

Get person remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getPersonRemoteSearchResultsRequest : GetPersonRemoteSearchResultsRequest =  // GetPersonRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getPersonRemoteSearchResults(getPersonRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getPersonRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getPersonRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getPersonRemoteSearchResultsRequest** | [**GetPersonRemoteSearchResultsRequest**](GetPersonRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSeriesRemoteSearchResults"></a>
# **getSeriesRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getSeriesRemoteSearchResults(getSeriesRemoteSearchResultsRequest)

Get series remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getSeriesRemoteSearchResultsRequest : GetSeriesRemoteSearchResultsRequest =  // GetSeriesRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getSeriesRemoteSearchResults(getSeriesRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getSeriesRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getSeriesRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getSeriesRemoteSearchResultsRequest** | [**GetSeriesRemoteSearchResultsRequest**](GetSeriesRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getTrailerRemoteSearchResults"></a>
# **getTrailerRemoteSearchResults**
> kotlin.collections.List&lt;RemoteSearchResult&gt; getTrailerRemoteSearchResults(getTrailerRemoteSearchResultsRequest)

Get trailer remote search.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemLookupApi()
val getTrailerRemoteSearchResultsRequest : GetTrailerRemoteSearchResultsRequest =  // GetTrailerRemoteSearchResultsRequest | Remote search query.
try {
    val result : kotlin.collections.List<RemoteSearchResult> = apiInstance.getTrailerRemoteSearchResults(getTrailerRemoteSearchResultsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ItemLookupApi#getTrailerRemoteSearchResults")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemLookupApi#getTrailerRemoteSearchResults")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getTrailerRemoteSearchResultsRequest** | [**GetTrailerRemoteSearchResultsRequest**](GetTrailerRemoteSearchResultsRequest.md)| Remote search query. |

### Return type

[**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

