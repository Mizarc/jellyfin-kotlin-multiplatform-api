
# SessionMessageType

## Enum


    * `forceKeepAlive` (value: `"ForceKeepAlive"`)

    * `generalCommand` (value: `"GeneralCommand"`)

    * `userDataChanged` (value: `"UserDataChanged"`)

    * `sessions` (value: `"Sessions"`)

    * `play` (value: `"Play"`)

    * `syncPlayCommand` (value: `"SyncPlayCommand"`)

    * `syncPlayGroupUpdate` (value: `"SyncPlayGroupUpdate"`)

    * `playstate` (value: `"Playstate"`)

    * `restartRequired` (value: `"RestartRequired"`)

    * `serverShuttingDown` (value: `"ServerShuttingDown"`)

    * `serverRestarting` (value: `"ServerRestarting"`)

    * `libraryChanged` (value: `"LibraryChanged"`)

    * `userDeleted` (value: `"UserDeleted"`)

    * `userUpdated` (value: `"UserUpdated"`)

    * `seriesTimerCreated` (value: `"SeriesTimerCreated"`)

    * `timerCreated` (value: `"TimerCreated"`)

    * `seriesTimerCancelled` (value: `"SeriesTimerCancelled"`)

    * `timerCancelled` (value: `"TimerCancelled"`)

    * `refreshProgress` (value: `"RefreshProgress"`)

    * `scheduledTaskEnded` (value: `"ScheduledTaskEnded"`)

    * `packageInstallationCancelled` (value: `"PackageInstallationCancelled"`)

    * `packageInstallationFailed` (value: `"PackageInstallationFailed"`)

    * `packageInstallationCompleted` (value: `"PackageInstallationCompleted"`)

    * `packageInstalling` (value: `"PackageInstalling"`)

    * `packageUninstalled` (value: `"PackageUninstalled"`)

    * `activityLogEntry` (value: `"ActivityLogEntry"`)

    * `scheduledTasksInfo` (value: `"ScheduledTasksInfo"`)

    * `activityLogEntryStart` (value: `"ActivityLogEntryStart"`)

    * `activityLogEntryStop` (value: `"ActivityLogEntryStop"`)

    * `sessionsStart` (value: `"SessionsStart"`)

    * `sessionsStop` (value: `"SessionsStop"`)

    * `scheduledTasksInfoStart` (value: `"ScheduledTasksInfoStart"`)

    * `scheduledTasksInfoStop` (value: `"ScheduledTasksInfoStop"`)

    * `keepAlive` (value: `"KeepAlive"`)



