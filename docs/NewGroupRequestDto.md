
# NewGroupRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupName** | **kotlin.String** | Gets or sets the group name. |  [optional]



