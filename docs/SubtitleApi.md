# SubtitleApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteSubtitle**](SubtitleApi.md#deleteSubtitle) | **DELETE** /Videos/{itemId}/Subtitles/{index} | Deletes an external subtitle file.
[**downloadRemoteSubtitles**](SubtitleApi.md#downloadRemoteSubtitles) | **POST** /Items/{itemId}/RemoteSearch/Subtitles/{subtitleId} | Downloads a remote subtitle.
[**getFallbackFont**](SubtitleApi.md#getFallbackFont) | **GET** /FallbackFont/Fonts/{name} | Gets a fallback font file.
[**getFallbackFontList**](SubtitleApi.md#getFallbackFontList) | **GET** /FallbackFont/Fonts | Gets a list of available fallback font files.
[**getRemoteSubtitles**](SubtitleApi.md#getRemoteSubtitles) | **GET** /Providers/Subtitles/Subtitles/{id} | Gets the remote subtitles.
[**getSubtitle**](SubtitleApi.md#getSubtitle) | **GET** /Videos/{routeItemId}/{routeMediaSourceId}/Subtitles/{routeIndex}/Stream.{routeFormat} | Gets subtitles in a specified format.
[**getSubtitlePlaylist**](SubtitleApi.md#getSubtitlePlaylist) | **GET** /Videos/{itemId}/{mediaSourceId}/Subtitles/{index}/subtitles.m3u8 | Gets an HLS subtitle playlist.
[**getSubtitleWithTicks**](SubtitleApi.md#getSubtitleWithTicks) | **GET** /Videos/{routeItemId}/{routeMediaSourceId}/Subtitles/{routeIndex}/{routeStartPositionTicks}/Stream.{routeFormat} | Gets subtitles in a specified format.
[**searchRemoteSubtitles**](SubtitleApi.md#searchRemoteSubtitles) | **GET** /Items/{itemId}/RemoteSearch/Subtitles/{language} | Search remote subtitles.
[**uploadSubtitle**](SubtitleApi.md#uploadSubtitle) | **POST** /Videos/{itemId}/Subtitles | Upload an external subtitle file.


<a name="deleteSubtitle"></a>
# **deleteSubtitle**
> deleteSubtitle(itemId, index)

Deletes an external subtitle file.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val index : kotlin.Int = 56 // kotlin.Int | The index of the subtitle file.
try {
    apiInstance.deleteSubtitle(itemId, index)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#deleteSubtitle")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#deleteSubtitle")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **index** | **kotlin.Int**| The index of the subtitle file. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="downloadRemoteSubtitles"></a>
# **downloadRemoteSubtitles**
> downloadRemoteSubtitles(itemId, subtitleId)

Downloads a remote subtitle.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val subtitleId : kotlin.String = subtitleId_example // kotlin.String | The subtitle id.
try {
    apiInstance.downloadRemoteSubtitles(itemId, subtitleId)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#downloadRemoteSubtitles")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#downloadRemoteSubtitles")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **subtitleId** | **kotlin.String**| The subtitle id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getFallbackFont"></a>
# **getFallbackFont**
> org.openapitools.client.infrastructure.OctetByteArray getFallbackFont(name)

Gets a fallback font file.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val name : kotlin.String = name_example // kotlin.String | The name of the fallback font file to get.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getFallbackFont(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#getFallbackFont")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#getFallbackFont")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the fallback font file to get. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: font/*

<a name="getFallbackFontList"></a>
# **getFallbackFontList**
> kotlin.collections.List&lt;FontFile&gt; getFallbackFontList()

Gets a list of available fallback font files.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
try {
    val result : kotlin.collections.List<FontFile> = apiInstance.getFallbackFontList()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#getFallbackFontList")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#getFallbackFontList")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;FontFile&gt;**](FontFile.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRemoteSubtitles"></a>
# **getRemoteSubtitles**
> org.openapitools.client.infrastructure.OctetByteArray getRemoteSubtitles(id)

Gets the remote subtitles.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val id : kotlin.String = id_example // kotlin.String | The item id.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getRemoteSubtitles(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#getRemoteSubtitles")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#getRemoteSubtitles")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/*

<a name="getSubtitle"></a>
# **getSubtitle**
> org.openapitools.client.infrastructure.OctetByteArray getSubtitle(routeItemId, routeMediaSourceId, routeIndex, routeFormat, itemId, mediaSourceId, index, format, endPositionTicks, copyTimestamps, addVttTimeMap, startPositionTicks)

Gets subtitles in a specified format.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val routeItemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The (route) item id.
val routeMediaSourceId : kotlin.String = routeMediaSourceId_example // kotlin.String | The (route) media source id.
val routeIndex : kotlin.Int = 56 // kotlin.Int | The (route) subtitle stream index.
val routeFormat : kotlin.String = routeFormat_example // kotlin.String | The (route) format of the returned subtitle.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media source id.
val index : kotlin.Int = 56 // kotlin.Int | The subtitle stream index.
val format : kotlin.String = format_example // kotlin.String | The format of the returned subtitle.
val endPositionTicks : kotlin.Long = 789 // kotlin.Long | Optional. The end position of the subtitle in ticks.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to copy the timestamps.
val addVttTimeMap : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to add a VTT time map.
val startPositionTicks : kotlin.Long = 789 // kotlin.Long | The start position of the subtitle in ticks.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getSubtitle(routeItemId, routeMediaSourceId, routeIndex, routeFormat, itemId, mediaSourceId, index, format, endPositionTicks, copyTimestamps, addVttTimeMap, startPositionTicks)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#getSubtitle")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#getSubtitle")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **routeItemId** | **kotlin.String**| The (route) item id. |
 **routeMediaSourceId** | **kotlin.String**| The (route) media source id. |
 **routeIndex** | **kotlin.Int**| The (route) subtitle stream index. |
 **routeFormat** | **kotlin.String**| The (route) format of the returned subtitle. |
 **itemId** | **kotlin.String**| The item id. | [optional]
 **mediaSourceId** | **kotlin.String**| The media source id. | [optional]
 **index** | **kotlin.Int**| The subtitle stream index. | [optional]
 **format** | **kotlin.String**| The format of the returned subtitle. | [optional]
 **endPositionTicks** | **kotlin.Long**| Optional. The end position of the subtitle in ticks. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Optional. Whether to copy the timestamps. | [optional] [default to false]
 **addVttTimeMap** | **kotlin.Boolean**| Optional. Whether to add a VTT time map. | [optional] [default to false]
 **startPositionTicks** | **kotlin.Long**| The start position of the subtitle in ticks. | [optional] [default to 0L]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/*

<a name="getSubtitlePlaylist"></a>
# **getSubtitlePlaylist**
> org.openapitools.client.infrastructure.OctetByteArray getSubtitlePlaylist(itemId, index, mediaSourceId, segmentLength)

Gets an HLS subtitle playlist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val index : kotlin.Int = 56 // kotlin.Int | The subtitle stream index.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media source id.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The subtitle segment length.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getSubtitlePlaylist(itemId, index, mediaSourceId, segmentLength)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#getSubtitlePlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#getSubtitlePlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **index** | **kotlin.Int**| The subtitle stream index. |
 **mediaSourceId** | **kotlin.String**| The media source id. |
 **segmentLength** | **kotlin.Int**| The subtitle segment length. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="getSubtitleWithTicks"></a>
# **getSubtitleWithTicks**
> org.openapitools.client.infrastructure.OctetByteArray getSubtitleWithTicks(routeItemId, routeMediaSourceId, routeIndex, routeStartPositionTicks, routeFormat, itemId, mediaSourceId, index, startPositionTicks, format, endPositionTicks, copyTimestamps, addVttTimeMap)

Gets subtitles in a specified format.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val routeItemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The (route) item id.
val routeMediaSourceId : kotlin.String = routeMediaSourceId_example // kotlin.String | The (route) media source id.
val routeIndex : kotlin.Int = 56 // kotlin.Int | The (route) subtitle stream index.
val routeStartPositionTicks : kotlin.Long = 789 // kotlin.Long | The (route) start position of the subtitle in ticks.
val routeFormat : kotlin.String = routeFormat_example // kotlin.String | The (route) format of the returned subtitle.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media source id.
val index : kotlin.Int = 56 // kotlin.Int | The subtitle stream index.
val startPositionTicks : kotlin.Long = 789 // kotlin.Long | The start position of the subtitle in ticks.
val format : kotlin.String = format_example // kotlin.String | The format of the returned subtitle.
val endPositionTicks : kotlin.Long = 789 // kotlin.Long | Optional. The end position of the subtitle in ticks.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to copy the timestamps.
val addVttTimeMap : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to add a VTT time map.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getSubtitleWithTicks(routeItemId, routeMediaSourceId, routeIndex, routeStartPositionTicks, routeFormat, itemId, mediaSourceId, index, startPositionTicks, format, endPositionTicks, copyTimestamps, addVttTimeMap)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#getSubtitleWithTicks")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#getSubtitleWithTicks")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **routeItemId** | **kotlin.String**| The (route) item id. |
 **routeMediaSourceId** | **kotlin.String**| The (route) media source id. |
 **routeIndex** | **kotlin.Int**| The (route) subtitle stream index. |
 **routeStartPositionTicks** | **kotlin.Long**| The (route) start position of the subtitle in ticks. |
 **routeFormat** | **kotlin.String**| The (route) format of the returned subtitle. |
 **itemId** | **kotlin.String**| The item id. | [optional]
 **mediaSourceId** | **kotlin.String**| The media source id. | [optional]
 **index** | **kotlin.Int**| The subtitle stream index. | [optional]
 **startPositionTicks** | **kotlin.Long**| The start position of the subtitle in ticks. | [optional]
 **format** | **kotlin.String**| The format of the returned subtitle. | [optional]
 **endPositionTicks** | **kotlin.Long**| Optional. The end position of the subtitle in ticks. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Optional. Whether to copy the timestamps. | [optional] [default to false]
 **addVttTimeMap** | **kotlin.Boolean**| Optional. Whether to add a VTT time map. | [optional] [default to false]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/*

<a name="searchRemoteSubtitles"></a>
# **searchRemoteSubtitles**
> kotlin.collections.List&lt;RemoteSubtitleInfo&gt; searchRemoteSubtitles(itemId, language, isPerfectMatch)

Search remote subtitles.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val language : kotlin.String = language_example // kotlin.String | The language of the subtitles.
val isPerfectMatch : kotlin.Boolean = true // kotlin.Boolean | Optional. Only show subtitles which are a perfect match.
try {
    val result : kotlin.collections.List<RemoteSubtitleInfo> = apiInstance.searchRemoteSubtitles(itemId, language, isPerfectMatch)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#searchRemoteSubtitles")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#searchRemoteSubtitles")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **language** | **kotlin.String**| The language of the subtitles. |
 **isPerfectMatch** | **kotlin.Boolean**| Optional. Only show subtitles which are a perfect match. | [optional]

### Return type

[**kotlin.collections.List&lt;RemoteSubtitleInfo&gt;**](RemoteSubtitleInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="uploadSubtitle"></a>
# **uploadSubtitle**
> uploadSubtitle(itemId, uploadSubtitleRequest)

Upload an external subtitle file.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SubtitleApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item the subtitle belongs to.
val uploadSubtitleRequest : UploadSubtitleRequest =  // UploadSubtitleRequest | The request body.
try {
    apiInstance.uploadSubtitle(itemId, uploadSubtitleRequest)
} catch (e: ClientException) {
    println("4xx response calling SubtitleApi#uploadSubtitle")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SubtitleApi#uploadSubtitle")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item the subtitle belongs to. |
 **uploadSubtitleRequest** | [**UploadSubtitleRequest**](UploadSubtitleRequest.md)| The request body. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

