
# NotificationResultDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifications** | [**kotlin.collections.List&lt;NotificationDto&gt;**](NotificationDto.md) | Gets or sets the current page of notifications. |  [optional]
**totalRecordCount** | **kotlin.Int** | Gets or sets the total number of notifications. |  [optional]



