
# ExternalIdMediaType

## Enum


    * `album` (value: `"Album"`)

    * `albumArtist` (value: `"AlbumArtist"`)

    * `artist` (value: `"Artist"`)

    * `boxSet` (value: `"BoxSet"`)

    * `episode` (value: `"Episode"`)

    * `movie` (value: `"Movie"`)

    * `otherArtist` (value: `"OtherArtist"`)

    * `person` (value: `"Person"`)

    * `releaseGroup` (value: `"ReleaseGroup"`)

    * `season` (value: `"Season"`)

    * `series` (value: `"Series"`)

    * `track` (value: `"Track"`)



