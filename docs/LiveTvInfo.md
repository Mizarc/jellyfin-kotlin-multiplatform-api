
# LiveTvInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**services** | [**kotlin.collections.List&lt;LiveTvServiceInfo&gt;**](LiveTvServiceInfo.md) | Gets or sets the services. |  [optional]
**isEnabled** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is enabled. |  [optional]
**enabledUsers** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the enabled users. |  [optional]



