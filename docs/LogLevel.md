
# LogLevel

## Enum


    * `trace` (value: `"Trace"`)

    * `debug` (value: `"Debug"`)

    * `information` (value: `"Information"`)

    * `warning` (value: `"Warning"`)

    * `error` (value: `"Error"`)

    * `critical` (value: `"Critical"`)

    * `none` (value: `"None"`)



