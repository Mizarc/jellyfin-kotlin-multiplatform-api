
# MediaAttachment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codec** | **kotlin.String** | Gets or sets the codec. |  [optional]
**codecTag** | **kotlin.String** | Gets or sets the codec tag. |  [optional]
**comment** | **kotlin.String** | Gets or sets the comment. |  [optional]
**index** | **kotlin.Int** | Gets or sets the index. |  [optional]
**fileName** | **kotlin.String** | Gets or sets the filename. |  [optional]
**mimeType** | **kotlin.String** | Gets or sets the MIME type. |  [optional]
**deliveryUrl** | **kotlin.String** | Gets or sets the delivery URL. |  [optional]



