
# VersionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **kotlin.String** | Gets or sets the version. |  [optional]
**versionNumber** | **kotlin.String** | Gets the version as a System.Version. |  [optional] [readonly]
**changelog** | **kotlin.String** | Gets or sets the changelog for this version. |  [optional]
**targetAbi** | **kotlin.String** | Gets or sets the ABI that this version was built against. |  [optional]
**sourceUrl** | **kotlin.String** | Gets or sets the source URL. |  [optional]
**checksum** | **kotlin.String** | Gets or sets a checksum for the binary. |  [optional]
**timestamp** | **kotlin.String** | Gets or sets a timestamp of when the binary was built. |  [optional]
**repositoryName** | **kotlin.String** | Gets or sets the repository name. |  [optional]
**repositoryUrl** | **kotlin.String** | Gets or sets the repository url. |  [optional]



