
# LiveTvServiceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**homePageUrl** | **kotlin.String** | Gets or sets the home page URL. |  [optional]
**status** | [**LiveTvServiceStatus**](LiveTvServiceStatus.md) |  |  [optional]
**statusMessage** | **kotlin.String** | Gets or sets the status message. |  [optional]
**version** | **kotlin.String** | Gets or sets the version. |  [optional]
**hasUpdateAvailable** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance has update available. |  [optional]
**isVisible** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is visible. |  [optional]
**tuners** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



