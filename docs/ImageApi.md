# ImageApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCustomSplashscreen**](ImageApi.md#deleteCustomSplashscreen) | **DELETE** /Branding/Splashscreen | Delete a custom splashscreen.
[**deleteItemImage**](ImageApi.md#deleteItemImage) | **DELETE** /Items/{itemId}/Images/{imageType} | Delete an item&#39;s image.
[**deleteItemImageByIndex**](ImageApi.md#deleteItemImageByIndex) | **DELETE** /Items/{itemId}/Images/{imageType}/{imageIndex} | Delete an item&#39;s image.
[**deleteUserImage**](ImageApi.md#deleteUserImage) | **DELETE** /Users/{userId}/Images/{imageType} | Delete the user&#39;s image.
[**deleteUserImageByIndex**](ImageApi.md#deleteUserImageByIndex) | **DELETE** /Users/{userId}/Images/{imageType}/{index} | Delete the user&#39;s image.
[**getArtistImage**](ImageApi.md#getArtistImage) | **GET** /Artists/{name}/Images/{imageType}/{imageIndex} | Get artist image by name.
[**getGenreImage**](ImageApi.md#getGenreImage) | **GET** /Genres/{name}/Images/{imageType} | Get genre image by name.
[**getGenreImageByIndex**](ImageApi.md#getGenreImageByIndex) | **GET** /Genres/{name}/Images/{imageType}/{imageIndex} | Get genre image by name.
[**getItemImage**](ImageApi.md#getItemImage) | **GET** /Items/{itemId}/Images/{imageType} | Gets the item&#39;s image.
[**getItemImage2**](ImageApi.md#getItemImage2) | **GET** /Items/{itemId}/Images/{imageType}/{imageIndex}/{tag}/{format}/{maxWidth}/{maxHeight}/{percentPlayed}/{unplayedCount} | Gets the item&#39;s image.
[**getItemImageByIndex**](ImageApi.md#getItemImageByIndex) | **GET** /Items/{itemId}/Images/{imageType}/{imageIndex} | Gets the item&#39;s image.
[**getItemImageInfos**](ImageApi.md#getItemImageInfos) | **GET** /Items/{itemId}/Images | Get item image infos.
[**getMusicGenreImage**](ImageApi.md#getMusicGenreImage) | **GET** /MusicGenres/{name}/Images/{imageType} | Get music genre image by name.
[**getMusicGenreImageByIndex**](ImageApi.md#getMusicGenreImageByIndex) | **GET** /MusicGenres/{name}/Images/{imageType}/{imageIndex} | Get music genre image by name.
[**getPersonImage**](ImageApi.md#getPersonImage) | **GET** /Persons/{name}/Images/{imageType} | Get person image by name.
[**getPersonImageByIndex**](ImageApi.md#getPersonImageByIndex) | **GET** /Persons/{name}/Images/{imageType}/{imageIndex} | Get person image by name.
[**getSplashscreen**](ImageApi.md#getSplashscreen) | **GET** /Branding/Splashscreen | Generates or gets the splashscreen.
[**getStudioImage**](ImageApi.md#getStudioImage) | **GET** /Studios/{name}/Images/{imageType} | Get studio image by name.
[**getStudioImageByIndex**](ImageApi.md#getStudioImageByIndex) | **GET** /Studios/{name}/Images/{imageType}/{imageIndex} | Get studio image by name.
[**getUserImage**](ImageApi.md#getUserImage) | **GET** /Users/{userId}/Images/{imageType} | Get user profile image.
[**getUserImageByIndex**](ImageApi.md#getUserImageByIndex) | **GET** /Users/{userId}/Images/{imageType}/{imageIndex} | Get user profile image.
[**headArtistImage**](ImageApi.md#headArtistImage) | **HEAD** /Artists/{name}/Images/{imageType}/{imageIndex} | Get artist image by name.
[**headGenreImage**](ImageApi.md#headGenreImage) | **HEAD** /Genres/{name}/Images/{imageType} | Get genre image by name.
[**headGenreImageByIndex**](ImageApi.md#headGenreImageByIndex) | **HEAD** /Genres/{name}/Images/{imageType}/{imageIndex} | Get genre image by name.
[**headItemImage**](ImageApi.md#headItemImage) | **HEAD** /Items/{itemId}/Images/{imageType} | Gets the item&#39;s image.
[**headItemImage2**](ImageApi.md#headItemImage2) | **HEAD** /Items/{itemId}/Images/{imageType}/{imageIndex}/{tag}/{format}/{maxWidth}/{maxHeight}/{percentPlayed}/{unplayedCount} | Gets the item&#39;s image.
[**headItemImageByIndex**](ImageApi.md#headItemImageByIndex) | **HEAD** /Items/{itemId}/Images/{imageType}/{imageIndex} | Gets the item&#39;s image.
[**headMusicGenreImage**](ImageApi.md#headMusicGenreImage) | **HEAD** /MusicGenres/{name}/Images/{imageType} | Get music genre image by name.
[**headMusicGenreImageByIndex**](ImageApi.md#headMusicGenreImageByIndex) | **HEAD** /MusicGenres/{name}/Images/{imageType}/{imageIndex} | Get music genre image by name.
[**headPersonImage**](ImageApi.md#headPersonImage) | **HEAD** /Persons/{name}/Images/{imageType} | Get person image by name.
[**headPersonImageByIndex**](ImageApi.md#headPersonImageByIndex) | **HEAD** /Persons/{name}/Images/{imageType}/{imageIndex} | Get person image by name.
[**headStudioImage**](ImageApi.md#headStudioImage) | **HEAD** /Studios/{name}/Images/{imageType} | Get studio image by name.
[**headStudioImageByIndex**](ImageApi.md#headStudioImageByIndex) | **HEAD** /Studios/{name}/Images/{imageType}/{imageIndex} | Get studio image by name.
[**headUserImage**](ImageApi.md#headUserImage) | **HEAD** /Users/{userId}/Images/{imageType} | Get user profile image.
[**headUserImageByIndex**](ImageApi.md#headUserImageByIndex) | **HEAD** /Users/{userId}/Images/{imageType}/{imageIndex} | Get user profile image.
[**postUserImage**](ImageApi.md#postUserImage) | **POST** /Users/{userId}/Images/{imageType} | Sets the user image.
[**postUserImageByIndex**](ImageApi.md#postUserImageByIndex) | **POST** /Users/{userId}/Images/{imageType}/{index} | Sets the user image.
[**setItemImage**](ImageApi.md#setItemImage) | **POST** /Items/{itemId}/Images/{imageType} | Set item image.
[**setItemImageByIndex**](ImageApi.md#setItemImageByIndex) | **POST** /Items/{itemId}/Images/{imageType}/{imageIndex} | Set item image.
[**updateItemImageIndex**](ImageApi.md#updateItemImageIndex) | **POST** /Items/{itemId}/Images/{imageType}/{imageIndex}/Index | Updates the index for an item image.
[**uploadCustomSplashscreen**](ImageApi.md#uploadCustomSplashscreen) | **POST** /Branding/Splashscreen | Uploads a custom splashscreen.  The body is expected to the image contents base64 encoded.


<a name="deleteCustomSplashscreen"></a>
# **deleteCustomSplashscreen**
> deleteCustomSplashscreen()

Delete a custom splashscreen.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
try {
    apiInstance.deleteCustomSplashscreen()
} catch (e: ClientException) {
    println("4xx response calling ImageApi#deleteCustomSplashscreen")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#deleteCustomSplashscreen")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteItemImage"></a>
# **deleteItemImage**
> deleteItemImage(itemId, imageType, imageIndex)

Delete an item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | The image index.
try {
    apiInstance.deleteItemImage(itemId, imageType, imageIndex)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#deleteItemImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#deleteItemImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| The image index. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="deleteItemImageByIndex"></a>
# **deleteItemImageByIndex**
> deleteItemImageByIndex(itemId, imageType, imageIndex)

Delete an item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | The image index.
try {
    apiInstance.deleteItemImageByIndex(itemId, imageType, imageIndex)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#deleteItemImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#deleteItemImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| The image index. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="deleteUserImage"></a>
# **deleteUserImage**
> deleteUserImage(userId, imageType, index)

Delete the user&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User Id.
val imageType : ImageType =  // ImageType | (Unused) Image type.
val index : kotlin.Int = 56 // kotlin.Int | (Unused) Image index.
try {
    apiInstance.deleteUserImage(userId, imageType, index)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#deleteUserImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#deleteUserImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User Id. |
 **imageType** | [**ImageType**](.md)| (Unused) Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **index** | **kotlin.Int**| (Unused) Image index. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="deleteUserImageByIndex"></a>
# **deleteUserImageByIndex**
> deleteUserImageByIndex(userId, imageType, index)

Delete the user&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User Id.
val imageType : ImageType =  // ImageType | (Unused) Image type.
val index : kotlin.Int = 56 // kotlin.Int | (Unused) Image index.
try {
    apiInstance.deleteUserImageByIndex(userId, imageType, index)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#deleteUserImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#deleteUserImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User Id. |
 **imageType** | [**ImageType**](.md)| (Unused) Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **index** | **kotlin.Int**| (Unused) Image index. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getArtistImage"></a>
# **getArtistImage**
> org.openapitools.client.infrastructure.OctetByteArray getArtistImage(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get artist image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Artist name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getArtistImage(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getArtistImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getArtistImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Artist name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getGenreImage"></a>
# **getGenreImage**
> org.openapitools.client.infrastructure.OctetByteArray getGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Genre name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getGenreImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getGenreImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getGenreImageByIndex"></a>
# **getGenreImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray getGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Genre name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getGenreImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getGenreImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getItemImage"></a>
# **getItemImage**
> org.openapitools.client.infrastructure.OctetByteArray getItemImage(itemId, imageType, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer, imageIndex)

Gets the item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val format : ImageFormat =  // ImageFormat | Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getItemImage(itemId, imageType, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getItemImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getItemImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **format** | [**ImageFormat**](.md)| Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getItemImage2"></a>
# **getItemImage2**
> org.openapitools.client.infrastructure.OctetByteArray getItemImage2(itemId, imageType, maxWidth, maxHeight, tag, format, percentPlayed, unplayedCount, imageIndex, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Gets the item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getItemImage2(itemId, imageType, maxWidth, maxHeight, tag, format, percentPlayed, unplayedCount, imageIndex, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getItemImage2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getItemImage2")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. |
 **maxHeight** | **kotlin.Int**| The maximum image height to return. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. |
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [enum: Bmp, Gif, Jpg, Png, Webp]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. |
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. |
 **imageIndex** | **kotlin.Int**| Image index. |
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getItemImageByIndex"></a>
# **getItemImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray getItemImageByIndex(itemId, imageType, imageIndex, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer)

Gets the item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val format : ImageFormat =  // ImageFormat | Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getItemImageByIndex(itemId, imageType, imageIndex, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getItemImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getItemImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **format** | [**ImageFormat**](.md)| Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getItemImageInfos"></a>
# **getItemImageInfos**
> kotlin.collections.List&lt;ImageInfo&gt; getItemImageInfos(itemId)

Get item image infos.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : kotlin.collections.List<ImageInfo> = apiInstance.getItemImageInfos(itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getItemImageInfos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getItemImageInfos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**kotlin.collections.List&lt;ImageInfo&gt;**](ImageInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMusicGenreImage"></a>
# **getMusicGenreImage**
> org.openapitools.client.infrastructure.OctetByteArray getMusicGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get music genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Music genre name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMusicGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getMusicGenreImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getMusicGenreImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Music genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMusicGenreImageByIndex"></a>
# **getMusicGenreImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray getMusicGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get music genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Music genre name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMusicGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getMusicGenreImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getMusicGenreImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Music genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPersonImage"></a>
# **getPersonImage**
> org.openapitools.client.infrastructure.OctetByteArray getPersonImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get person image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Person name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getPersonImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getPersonImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getPersonImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Person name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPersonImageByIndex"></a>
# **getPersonImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray getPersonImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get person image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Person name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getPersonImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getPersonImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getPersonImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Person name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSplashscreen"></a>
# **getSplashscreen**
> org.openapitools.client.infrastructure.OctetByteArray getSplashscreen(tag, format, maxWidth, maxHeight, width, height, fillWidth, fillHeight, blur, backgroundColor, foregroundLayer, quality)

Generates or gets the splashscreen.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val tag : kotlin.String = tag_example // kotlin.String | Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val blur : kotlin.Int = 56 // kotlin.Int | Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Apply a foreground layer on top of the image.
val quality : kotlin.Int = 56 // kotlin.Int | Quality setting, from 0-100.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getSplashscreen(tag, format, maxWidth, maxHeight, width, height, fillWidth, fillHeight, blur, backgroundColor, foregroundLayer, quality)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getSplashscreen")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getSplashscreen")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **kotlin.String**| Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **blur** | **kotlin.Int**| Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Apply a foreground layer on top of the image. | [optional]
 **quality** | **kotlin.Int**| Quality setting, from 0-100. | [optional] [default to 90]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*

<a name="getStudioImage"></a>
# **getStudioImage**
> org.openapitools.client.infrastructure.OctetByteArray getStudioImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get studio image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Studio name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getStudioImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getStudioImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getStudioImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Studio name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getStudioImageByIndex"></a>
# **getStudioImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray getStudioImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get studio image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Studio name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getStudioImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getStudioImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getStudioImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Studio name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getUserImage"></a>
# **getUserImage**
> org.openapitools.client.infrastructure.OctetByteArray getUserImage(userId, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get user profile image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getUserImage(userId, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getUserImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getUserImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getUserImageByIndex"></a>
# **getUserImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray getUserImageByIndex(userId, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get user profile image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getUserImageByIndex(userId, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#getUserImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#getUserImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headArtistImage"></a>
# **headArtistImage**
> org.openapitools.client.infrastructure.OctetByteArray headArtistImage(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get artist image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Artist name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headArtistImage(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headArtistImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headArtistImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Artist name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headGenreImage"></a>
# **headGenreImage**
> org.openapitools.client.infrastructure.OctetByteArray headGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Genre name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headGenreImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headGenreImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headGenreImageByIndex"></a>
# **headGenreImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray headGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Genre name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headGenreImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headGenreImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headItemImage"></a>
# **headItemImage**
> org.openapitools.client.infrastructure.OctetByteArray headItemImage(itemId, imageType, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer, imageIndex)

Gets the item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val format : ImageFormat =  // ImageFormat | Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headItemImage(itemId, imageType, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headItemImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headItemImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **format** | [**ImageFormat**](.md)| Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headItemImage2"></a>
# **headItemImage2**
> org.openapitools.client.infrastructure.OctetByteArray headItemImage2(itemId, imageType, maxWidth, maxHeight, tag, format, percentPlayed, unplayedCount, imageIndex, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Gets the item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headItemImage2(itemId, imageType, maxWidth, maxHeight, tag, format, percentPlayed, unplayedCount, imageIndex, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headItemImage2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headItemImage2")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. |
 **maxHeight** | **kotlin.Int**| The maximum image height to return. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. |
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [enum: Bmp, Gif, Jpg, Png, Webp]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. |
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. |
 **imageIndex** | **kotlin.Int**| Image index. |
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headItemImageByIndex"></a>
# **headItemImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray headItemImageByIndex(itemId, imageType, imageIndex, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer)

Gets the item&#39;s image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val format : ImageFormat =  // ImageFormat | Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headItemImageByIndex(itemId, imageType, imageIndex, maxWidth, maxHeight, width, height, quality, fillWidth, fillHeight, tag, cropWhitespace, format, addPlayedIndicator, percentPlayed, unplayedCount, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headItemImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headItemImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **format** | [**ImageFormat**](.md)| Optional. The MediaBrowser.Model.Drawing.ImageFormat of the returned image. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headMusicGenreImage"></a>
# **headMusicGenreImage**
> org.openapitools.client.infrastructure.OctetByteArray headMusicGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get music genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Music genre name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headMusicGenreImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headMusicGenreImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headMusicGenreImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Music genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headMusicGenreImageByIndex"></a>
# **headMusicGenreImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray headMusicGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get music genre image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Music genre name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headMusicGenreImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headMusicGenreImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headMusicGenreImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Music genre name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headPersonImage"></a>
# **headPersonImage**
> org.openapitools.client.infrastructure.OctetByteArray headPersonImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get person image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Person name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headPersonImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headPersonImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headPersonImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Person name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headPersonImageByIndex"></a>
# **headPersonImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray headPersonImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get person image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Person name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headPersonImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headPersonImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headPersonImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Person name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headStudioImage"></a>
# **headStudioImage**
> org.openapitools.client.infrastructure.OctetByteArray headStudioImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get studio image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Studio name.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headStudioImage(name, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headStudioImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headStudioImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Studio name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headStudioImageByIndex"></a>
# **headStudioImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray headStudioImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get studio image by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val name : kotlin.String = name_example // kotlin.String | Studio name.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headStudioImageByIndex(name, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headStudioImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headStudioImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Studio name. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headUserImage"></a>
# **headUserImage**
> org.openapitools.client.infrastructure.OctetByteArray headUserImage(userId, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)

Get user profile image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val imageType : ImageType =  // ImageType | Image type.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headUserImage(userId, imageType, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer, imageIndex)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headUserImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headUserImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]
 **imageIndex** | **kotlin.Int**| Image index. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="headUserImageByIndex"></a>
# **headUserImageByIndex**
> org.openapitools.client.infrastructure.OctetByteArray headUserImageByIndex(userId, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)

Get user profile image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Image index.
val tag : kotlin.String = tag_example // kotlin.String | Optional. Supply the cache tag from the item object to receive strong caching headers.
val format : ImageFormat =  // ImageFormat | Determines the output format of the image - original,gif,jpg,png.
val maxWidth : kotlin.Int = 56 // kotlin.Int | The maximum image width to return.
val maxHeight : kotlin.Int = 56 // kotlin.Int | The maximum image height to return.
val percentPlayed : kotlin.Double = 1.2 // kotlin.Double | Optional. Percent to render for the percent played overlay.
val unplayedCount : kotlin.Int = 56 // kotlin.Int | Optional. Unplayed count overlay to render.
val width : kotlin.Int = 56 // kotlin.Int | The fixed image width to return.
val height : kotlin.Int = 56 // kotlin.Int | The fixed image height to return.
val quality : kotlin.Int = 56 // kotlin.Int | Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases.
val fillWidth : kotlin.Int = 56 // kotlin.Int | Width of box to fill.
val fillHeight : kotlin.Int = 56 // kotlin.Int | Height of box to fill.
val cropWhitespace : kotlin.Boolean = true // kotlin.Boolean | Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art.
val addPlayedIndicator : kotlin.Boolean = true // kotlin.Boolean | Optional. Add a played indicator.
val blur : kotlin.Int = 56 // kotlin.Int | Optional. Blur image.
val backgroundColor : kotlin.String = backgroundColor_example // kotlin.String | Optional. Apply a background color for transparent images.
val foregroundLayer : kotlin.String = foregroundLayer_example // kotlin.String | Optional. Apply a foreground layer on top of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headUserImageByIndex(userId, imageType, imageIndex, tag, format, maxWidth, maxHeight, percentPlayed, unplayedCount, width, height, quality, fillWidth, fillHeight, cropWhitespace, addPlayedIndicator, blur, backgroundColor, foregroundLayer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#headUserImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#headUserImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Image index. |
 **tag** | **kotlin.String**| Optional. Supply the cache tag from the item object to receive strong caching headers. | [optional]
 **format** | [**ImageFormat**](.md)| Determines the output format of the image - original,gif,jpg,png. | [optional] [enum: Bmp, Gif, Jpg, Png, Webp]
 **maxWidth** | **kotlin.Int**| The maximum image width to return. | [optional]
 **maxHeight** | **kotlin.Int**| The maximum image height to return. | [optional]
 **percentPlayed** | **kotlin.Double**| Optional. Percent to render for the percent played overlay. | [optional]
 **unplayedCount** | **kotlin.Int**| Optional. Unplayed count overlay to render. | [optional]
 **width** | **kotlin.Int**| The fixed image width to return. | [optional]
 **height** | **kotlin.Int**| The fixed image height to return. | [optional]
 **quality** | **kotlin.Int**| Optional. Quality setting, from 0-100. Defaults to 90 and should suffice in most cases. | [optional]
 **fillWidth** | **kotlin.Int**| Width of box to fill. | [optional]
 **fillHeight** | **kotlin.Int**| Height of box to fill. | [optional]
 **cropWhitespace** | **kotlin.Boolean**| Optional. Specify if whitespace should be cropped out of the image. True/False. If unspecified, whitespace will be cropped from logos and clear art. | [optional]
 **addPlayedIndicator** | **kotlin.Boolean**| Optional. Add a played indicator. | [optional]
 **blur** | **kotlin.Int**| Optional. Blur image. | [optional]
 **backgroundColor** | **kotlin.String**| Optional. Apply a background color for transparent images. | [optional]
 **foregroundLayer** | **kotlin.String**| Optional. Apply a foreground layer on top of the image. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="postUserImage"></a>
# **postUserImage**
> postUserImage(userId, imageType, index, body)

Sets the user image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User Id.
val imageType : ImageType =  // ImageType | (Unused) Image type.
val index : kotlin.Int = 56 // kotlin.Int | (Unused) Image index.
val body : org.openapitools.client.infrastructure.OctetByteArray = BINARY_DATA_HERE // org.openapitools.client.infrastructure.OctetByteArray | 
try {
    apiInstance.postUserImage(userId, imageType, index, body)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#postUserImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#postUserImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User Id. |
 **imageType** | [**ImageType**](.md)| (Unused) Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **index** | **kotlin.Int**| (Unused) Image index. | [optional]
 **body** | **org.openapitools.client.infrastructure.OctetByteArray**|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: image/*
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="postUserImageByIndex"></a>
# **postUserImageByIndex**
> postUserImageByIndex(userId, imageType, index, body)

Sets the user image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User Id.
val imageType : ImageType =  // ImageType | (Unused) Image type.
val index : kotlin.Int = 56 // kotlin.Int | (Unused) Image index.
val body : org.openapitools.client.infrastructure.OctetByteArray = BINARY_DATA_HERE // org.openapitools.client.infrastructure.OctetByteArray | 
try {
    apiInstance.postUserImageByIndex(userId, imageType, index, body)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#postUserImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#postUserImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User Id. |
 **imageType** | [**ImageType**](.md)| (Unused) Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **index** | **kotlin.Int**| (Unused) Image index. |
 **body** | **org.openapitools.client.infrastructure.OctetByteArray**|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: image/*
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="setItemImage"></a>
# **setItemImage**
> setItemImage(itemId, imageType, body)

Set item image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val body : org.openapitools.client.infrastructure.OctetByteArray = BINARY_DATA_HERE // org.openapitools.client.infrastructure.OctetByteArray | 
try {
    apiInstance.setItemImage(itemId, imageType, body)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#setItemImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#setItemImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **body** | **org.openapitools.client.infrastructure.OctetByteArray**|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: image/*
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="setItemImageByIndex"></a>
# **setItemImageByIndex**
> setItemImageByIndex(itemId, imageType, imageIndex, body)

Set item image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | (Unused) Image index.
val body : org.openapitools.client.infrastructure.OctetByteArray = BINARY_DATA_HERE // org.openapitools.client.infrastructure.OctetByteArray | 
try {
    apiInstance.setItemImageByIndex(itemId, imageType, imageIndex, body)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#setItemImageByIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#setItemImageByIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| (Unused) Image index. |
 **body** | **org.openapitools.client.infrastructure.OctetByteArray**|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: image/*
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateItemImageIndex"></a>
# **updateItemImageIndex**
> updateItemImageIndex(itemId, imageType, imageIndex, newIndex)

Updates the index for an item image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val imageType : ImageType =  // ImageType | Image type.
val imageIndex : kotlin.Int = 56 // kotlin.Int | Old image index.
val newIndex : kotlin.Int = 56 // kotlin.Int | New image index.
try {
    apiInstance.updateItemImageIndex(itemId, imageType, imageIndex, newIndex)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#updateItemImageIndex")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#updateItemImageIndex")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **imageType** | [**ImageType**](.md)| Image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageIndex** | **kotlin.Int**| Old image index. |
 **newIndex** | **kotlin.Int**| New image index. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="uploadCustomSplashscreen"></a>
# **uploadCustomSplashscreen**
> uploadCustomSplashscreen(body)

Uploads a custom splashscreen.  The body is expected to the image contents base64 encoded.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageApi()
val body : org.openapitools.client.infrastructure.OctetByteArray = BINARY_DATA_HERE // org.openapitools.client.infrastructure.OctetByteArray | 
try {
    apiInstance.uploadCustomSplashscreen(body)
} catch (e: ClientException) {
    println("4xx response calling ImageApi#uploadCustomSplashscreen")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageApi#uploadCustomSplashscreen")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **org.openapitools.client.infrastructure.OctetByteArray**|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: image/*
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

