
# SessionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playState** | [**PlayerStateInfo**](PlayerStateInfo.md) |  |  [optional]
**additionalUsers** | [**kotlin.collections.List&lt;SessionUserInfo&gt;**](SessionUserInfo.md) |  |  [optional]
**capabilities** | [**ClientCapabilities**](ClientCapabilities.md) |  |  [optional]
**remoteEndPoint** | **kotlin.String** | Gets or sets the remote end point. |  [optional]
**playableMediaTypes** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets the playable media types. |  [optional] [readonly]
**id** | **kotlin.String** | Gets or sets the id. |  [optional]
**userId** | **kotlin.String** | Gets or sets the user id. |  [optional]
**userName** | **kotlin.String** | Gets or sets the username. |  [optional]
**client** | **kotlin.String** | Gets or sets the type of the client. |  [optional]
**lastActivityDate** | **kotlin.String** | Gets or sets the last activity date. |  [optional]
**lastPlaybackCheckIn** | **kotlin.String** | Gets or sets the last playback check in. |  [optional]
**deviceName** | **kotlin.String** | Gets or sets the name of the device. |  [optional]
**deviceType** | **kotlin.String** | Gets or sets the type of the device. |  [optional]
**nowPlayingItem** | [**SessionInfoNowPlayingItem**](SessionInfoNowPlayingItem.md) |  |  [optional]
**fullNowPlayingItem** | [**SessionInfoFullNowPlayingItem**](SessionInfoFullNowPlayingItem.md) |  |  [optional]
**nowViewingItem** | [**SessionInfoNowPlayingItem**](SessionInfoNowPlayingItem.md) |  |  [optional]
**deviceId** | **kotlin.String** | Gets or sets the device id. |  [optional]
**applicationVersion** | **kotlin.String** | Gets or sets the application version. |  [optional]
**transcodingInfo** | [**TranscodingInfo**](TranscodingInfo.md) |  |  [optional]
**isActive** | **kotlin.Boolean** | Gets a value indicating whether this instance is active. |  [optional] [readonly]
**supportsMediaControl** | **kotlin.Boolean** |  |  [optional] [readonly]
**supportsRemoteControl** | **kotlin.Boolean** |  |  [optional] [readonly]
**nowPlayingQueue** | [**kotlin.collections.List&lt;QueueItem&gt;**](QueueItem.md) |  |  [optional]
**nowPlayingQueueFullItems** | [**kotlin.collections.List&lt;BaseItemDto&gt;**](BaseItemDto.md) |  |  [optional]
**hasCustomDeviceName** | **kotlin.Boolean** |  |  [optional]
**playlistItemId** | **kotlin.String** |  |  [optional]
**serverId** | **kotlin.String** |  |  [optional]
**userPrimaryImageTag** | **kotlin.String** |  |  [optional]
**supportedCommands** | [**kotlin.collections.List&lt;GeneralCommandType&gt;**](GeneralCommandType.md) | Gets the supported commands. |  [optional] [readonly]



