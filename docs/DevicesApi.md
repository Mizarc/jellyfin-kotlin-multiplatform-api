# DevicesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDevice**](DevicesApi.md#deleteDevice) | **DELETE** /Devices | Deletes a device.
[**getDeviceInfo**](DevicesApi.md#getDeviceInfo) | **GET** /Devices/Info | Get info for a device.
[**getDeviceOptions**](DevicesApi.md#getDeviceOptions) | **GET** /Devices/Options | Get options for a device.
[**getDevices**](DevicesApi.md#getDevices) | **GET** /Devices | Get Devices.
[**updateDeviceOptions**](DevicesApi.md#updateDeviceOptions) | **POST** /Devices/Options | Update device options.


<a name="deleteDevice"></a>
# **deleteDevice**
> deleteDevice(id)

Deletes a device.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DevicesApi()
val id : kotlin.String = id_example // kotlin.String | Device Id.
try {
    apiInstance.deleteDevice(id)
} catch (e: ClientException) {
    println("4xx response calling DevicesApi#deleteDevice")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DevicesApi#deleteDevice")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| Device Id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDeviceInfo"></a>
# **getDeviceInfo**
> DeviceInfo getDeviceInfo(id)

Get info for a device.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DevicesApi()
val id : kotlin.String = id_example // kotlin.String | Device Id.
try {
    val result : DeviceInfo = apiInstance.getDeviceInfo(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DevicesApi#getDeviceInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DevicesApi#getDeviceInfo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| Device Id. |

### Return type

[**DeviceInfo**](DeviceInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDeviceOptions"></a>
# **getDeviceOptions**
> DeviceOptions getDeviceOptions(id)

Get options for a device.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DevicesApi()
val id : kotlin.String = id_example // kotlin.String | Device Id.
try {
    val result : DeviceOptions = apiInstance.getDeviceOptions(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DevicesApi#getDeviceOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DevicesApi#getDeviceOptions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| Device Id. |

### Return type

[**DeviceOptions**](DeviceOptions.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDevices"></a>
# **getDevices**
> DeviceInfoQueryResult getDevices(supportsSync, userId)

Get Devices.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DevicesApi()
val supportsSync : kotlin.Boolean = true // kotlin.Boolean | Gets or sets a value indicating whether [supports synchronize].
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Gets or sets the user identifier.
try {
    val result : DeviceInfoQueryResult = apiInstance.getDevices(supportsSync, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DevicesApi#getDevices")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DevicesApi#getDevices")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supportsSync** | **kotlin.Boolean**| Gets or sets a value indicating whether [supports synchronize]. | [optional]
 **userId** | **kotlin.String**| Gets or sets the user identifier. | [optional]

### Return type

[**DeviceInfoQueryResult**](DeviceInfoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateDeviceOptions"></a>
# **updateDeviceOptions**
> updateDeviceOptions(id, updateDeviceOptionsRequest)

Update device options.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DevicesApi()
val id : kotlin.String = id_example // kotlin.String | Device Id.
val updateDeviceOptionsRequest : UpdateDeviceOptionsRequest =  // UpdateDeviceOptionsRequest | Device Options.
try {
    apiInstance.updateDeviceOptions(id, updateDeviceOptionsRequest)
} catch (e: ClientException) {
    println("4xx response calling DevicesApi#updateDeviceOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DevicesApi#updateDeviceOptions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| Device Id. |
 **updateDeviceOptionsRequest** | [**UpdateDeviceOptionsRequest**](UpdateDeviceOptionsRequest.md)| Device Options. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

