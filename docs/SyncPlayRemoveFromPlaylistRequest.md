
# SyncPlayRemoveFromPlaylistRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlistItemIds** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the playlist identifiers ot the items. Ignored when clearing the playlist. |  [optional]
**clearPlaylist** | **kotlin.Boolean** | Gets or sets a value indicating whether the entire playlist should be cleared. |  [optional]
**clearPlayingItem** | **kotlin.Boolean** | Gets or sets a value indicating whether the playing item should be removed as well. Used only when clearing the playlist. |  [optional]



