
# PlaybackErrorCode

## Enum


    * `notAllowed` (value: `"NotAllowed"`)

    * `noCompatibleStream` (value: `"NoCompatibleStream"`)

    * `rateLimitExceeded` (value: `"RateLimitExceeded"`)



