
# PinRedeemResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **kotlin.Boolean** | Gets or sets a value indicating whether this MediaBrowser.Model.Users.PinRedeemResult is success. |  [optional]
**usersReset** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the users reset. |  [optional]



