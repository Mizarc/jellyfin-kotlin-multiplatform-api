# DisplayPreferencesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDisplayPreferences**](DisplayPreferencesApi.md#getDisplayPreferences) | **GET** /DisplayPreferences/{displayPreferencesId} | Get Display Preferences.
[**updateDisplayPreferences**](DisplayPreferencesApi.md#updateDisplayPreferences) | **POST** /DisplayPreferences/{displayPreferencesId} | Update Display Preferences.


<a name="getDisplayPreferences"></a>
# **getDisplayPreferences**
> DisplayPreferencesDto getDisplayPreferences(displayPreferencesId, userId, client)

Get Display Preferences.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DisplayPreferencesApi()
val displayPreferencesId : kotlin.String = displayPreferencesId_example // kotlin.String | Display preferences id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val client : kotlin.String = client_example // kotlin.String | Client.
try {
    val result : DisplayPreferencesDto = apiInstance.getDisplayPreferences(displayPreferencesId, userId, client)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DisplayPreferencesApi#getDisplayPreferences")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DisplayPreferencesApi#getDisplayPreferences")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **displayPreferencesId** | **kotlin.String**| Display preferences id. |
 **userId** | **kotlin.String**| User id. |
 **client** | **kotlin.String**| Client. |

### Return type

[**DisplayPreferencesDto**](DisplayPreferencesDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateDisplayPreferences"></a>
# **updateDisplayPreferences**
> updateDisplayPreferences(displayPreferencesId, userId, client, updateDisplayPreferencesRequest)

Update Display Preferences.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DisplayPreferencesApi()
val displayPreferencesId : kotlin.String = displayPreferencesId_example // kotlin.String | Display preferences id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User Id.
val client : kotlin.String = client_example // kotlin.String | Client.
val updateDisplayPreferencesRequest : UpdateDisplayPreferencesRequest =  // UpdateDisplayPreferencesRequest | New Display Preferences object.
try {
    apiInstance.updateDisplayPreferences(displayPreferencesId, userId, client, updateDisplayPreferencesRequest)
} catch (e: ClientException) {
    println("4xx response calling DisplayPreferencesApi#updateDisplayPreferences")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DisplayPreferencesApi#updateDisplayPreferences")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **displayPreferencesId** | **kotlin.String**| Display preferences id. |
 **userId** | **kotlin.String**| User Id. |
 **client** | **kotlin.String**| Client. |
 **updateDisplayPreferencesRequest** | [**UpdateDisplayPreferencesRequest**](UpdateDisplayPreferencesRequest.md)| New Display Preferences object. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

