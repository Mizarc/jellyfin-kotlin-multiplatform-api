
# QueryFilters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**genres** | [**kotlin.collections.List&lt;NameGuidPair&gt;**](NameGuidPair.md) |  |  [optional]
**tags** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



