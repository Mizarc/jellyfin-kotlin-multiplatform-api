
# ChannelItemSortField

## Enum


    * `name` (value: `"Name"`)

    * `communityRating` (value: `"CommunityRating"`)

    * `premiereDate` (value: `"PremiereDate"`)

    * `dateCreated` (value: `"DateCreated"`)

    * `runtime` (value: `"Runtime"`)

    * `playCount` (value: `"PlayCount"`)

    * `communityPlayCount` (value: `"CommunityPlayCount"`)



