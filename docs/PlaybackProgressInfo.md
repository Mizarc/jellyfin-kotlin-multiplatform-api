
# PlaybackProgressInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canSeek** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance can seek. |  [optional]
**item** | [**PlaybackProgressInfoItem**](PlaybackProgressInfoItem.md) |  |  [optional]
**itemId** | **kotlin.String** | Gets or sets the item identifier. |  [optional]
**sessionId** | **kotlin.String** | Gets or sets the session id. |  [optional]
**mediaSourceId** | **kotlin.String** | Gets or sets the media version identifier. |  [optional]
**audioStreamIndex** | **kotlin.Int** | Gets or sets the index of the audio stream. |  [optional]
**subtitleStreamIndex** | **kotlin.Int** | Gets or sets the index of the subtitle stream. |  [optional]
**isPaused** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is paused. |  [optional]
**isMuted** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is muted. |  [optional]
**positionTicks** | **kotlin.Long** | Gets or sets the position ticks. |  [optional]
**playbackStartTimeTicks** | **kotlin.Long** |  |  [optional]
**volumeLevel** | **kotlin.Int** | Gets or sets the volume level. |  [optional]
**brightness** | **kotlin.Int** |  |  [optional]
**aspectRatio** | **kotlin.String** |  |  [optional]
**playMethod** | [**PlayMethod**](PlayMethod.md) |  |  [optional]
**liveStreamId** | **kotlin.String** | Gets or sets the live stream identifier. |  [optional]
**playSessionId** | **kotlin.String** | Gets or sets the play session identifier. |  [optional]
**repeatMode** | [**RepeatMode**](RepeatMode.md) |  |  [optional]
**nowPlayingQueue** | [**kotlin.collections.List&lt;QueueItem&gt;**](QueueItem.md) |  |  [optional]
**playlistItemId** | **kotlin.String** |  |  [optional]



