
# MediaUpdateInfoPathDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **kotlin.String** | Gets or sets media path. |  [optional]
**updateType** | **kotlin.String** | Gets or sets media update type.  Created, Modified, Deleted. |  [optional]



