
# GroupRepeatMode

## Enum


    * `repeatOne` (value: `"RepeatOne"`)

    * `repeatAll` (value: `"RepeatAll"`)

    * `repeatNone` (value: `"RepeatNone"`)



