
# ItemFields

## Enum


    * `airTime` (value: `"AirTime"`)

    * `canDelete` (value: `"CanDelete"`)

    * `canDownload` (value: `"CanDownload"`)

    * `channelInfo` (value: `"ChannelInfo"`)

    * `chapters` (value: `"Chapters"`)

    * `childCount` (value: `"ChildCount"`)

    * `cumulativeRunTimeTicks` (value: `"CumulativeRunTimeTicks"`)

    * `customRating` (value: `"CustomRating"`)

    * `dateCreated` (value: `"DateCreated"`)

    * `dateLastMediaAdded` (value: `"DateLastMediaAdded"`)

    * `displayPreferencesId` (value: `"DisplayPreferencesId"`)

    * `etag` (value: `"Etag"`)

    * `externalUrls` (value: `"ExternalUrls"`)

    * `genres` (value: `"Genres"`)

    * `homePageUrl` (value: `"HomePageUrl"`)

    * `itemCounts` (value: `"ItemCounts"`)

    * `mediaSourceCount` (value: `"MediaSourceCount"`)

    * `mediaSources` (value: `"MediaSources"`)

    * `originalTitle` (value: `"OriginalTitle"`)

    * `overview` (value: `"Overview"`)

    * `parentId` (value: `"ParentId"`)

    * `path` (value: `"Path"`)

    * `people` (value: `"People"`)

    * `playAccess` (value: `"PlayAccess"`)

    * `productionLocations` (value: `"ProductionLocations"`)

    * `providerIds` (value: `"ProviderIds"`)

    * `primaryImageAspectRatio` (value: `"PrimaryImageAspectRatio"`)

    * `recursiveItemCount` (value: `"RecursiveItemCount"`)

    * `settings` (value: `"Settings"`)

    * `screenshotImageTags` (value: `"ScreenshotImageTags"`)

    * `seriesPrimaryImage` (value: `"SeriesPrimaryImage"`)

    * `seriesStudio` (value: `"SeriesStudio"`)

    * `sortName` (value: `"SortName"`)

    * `specialEpisodeNumbers` (value: `"SpecialEpisodeNumbers"`)

    * `studios` (value: `"Studios"`)

    * `basicSyncInfo` (value: `"BasicSyncInfo"`)

    * `syncInfo` (value: `"SyncInfo"`)

    * `taglines` (value: `"Taglines"`)

    * `tags` (value: `"Tags"`)

    * `remoteTrailers` (value: `"RemoteTrailers"`)

    * `mediaStreams` (value: `"MediaStreams"`)

    * `seasonUserData` (value: `"SeasonUserData"`)

    * `serviceName` (value: `"ServiceName"`)

    * `themeSongIds` (value: `"ThemeSongIds"`)

    * `themeVideoIds` (value: `"ThemeVideoIds"`)

    * `externalEtag` (value: `"ExternalEtag"`)

    * `presentationUniqueKey` (value: `"PresentationUniqueKey"`)

    * `inheritedParentalRatingValue` (value: `"InheritedParentalRatingValue"`)

    * `externalSeriesId` (value: `"ExternalSeriesId"`)

    * `seriesPresentationUniqueKey` (value: `"SeriesPresentationUniqueKey"`)

    * `dateLastRefreshed` (value: `"DateLastRefreshed"`)

    * `dateLastSaved` (value: `"DateLastSaved"`)

    * `refreshState` (value: `"RefreshState"`)

    * `channelImage` (value: `"ChannelImage"`)

    * `enableMediaSourceDisplay` (value: `"EnableMediaSourceDisplay"`)

    * `width` (value: `"Width"`)

    * `height` (value: `"Height"`)

    * `extraIds` (value: `"ExtraIds"`)

    * `localTrailerCount` (value: `"LocalTrailerCount"`)

    * `isHD` (value: `"IsHD"`)

    * `specialFeatureCount` (value: `"SpecialFeatureCount"`)



