
# NotificationOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**options** | [**kotlin.collections.List&lt;NotificationOption&gt;**](NotificationOption.md) |  |  [optional]



