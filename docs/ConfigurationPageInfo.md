
# ConfigurationPageInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**enableInMainMenu** | **kotlin.Boolean** | Gets or sets a value indicating whether the configurations page is enabled in the main menu. |  [optional]
**menuSection** | **kotlin.String** | Gets or sets the menu section. |  [optional]
**menuIcon** | **kotlin.String** | Gets or sets the menu icon. |  [optional]
**displayName** | **kotlin.String** | Gets or sets the display name. |  [optional]
**pluginId** | **kotlin.String** | Gets or sets the plugin id. |  [optional]



