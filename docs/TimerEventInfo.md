
# TimerEventInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** |  |  [optional]
**programId** | **kotlin.String** |  |  [optional]



