
# ConfigImageTypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**backdropSizes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**baseUrl** | **kotlin.String** |  |  [optional]
**logoSizes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**posterSizes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**profileSizes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**secureBaseUrl** | **kotlin.String** |  |  [optional]
**stillSizes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



