
# ExternalUrl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**url** | **kotlin.String** | Gets or sets the type of the item. |  [optional]



