
# LiveTvOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guideDays** | **kotlin.Int** |  |  [optional]
**recordingPath** | **kotlin.String** |  |  [optional]
**movieRecordingPath** | **kotlin.String** |  |  [optional]
**seriesRecordingPath** | **kotlin.String** |  |  [optional]
**enableRecordingSubfolders** | **kotlin.Boolean** |  |  [optional]
**enableOriginalAudioWithEncodedRecordings** | **kotlin.Boolean** |  |  [optional]
**tunerHosts** | [**kotlin.collections.List&lt;TunerHostInfo&gt;**](TunerHostInfo.md) |  |  [optional]
**listingProviders** | [**kotlin.collections.List&lt;ListingsProviderInfo&gt;**](ListingsProviderInfo.md) |  |  [optional]
**prePaddingSeconds** | **kotlin.Int** |  |  [optional]
**postPaddingSeconds** | **kotlin.Int** |  |  [optional]
**mediaLocationsCreated** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**recordingPostProcessor** | **kotlin.String** |  |  [optional]
**recordingPostProcessorArguments** | **kotlin.String** |  |  [optional]



