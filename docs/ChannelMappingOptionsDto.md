
# ChannelMappingOptionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tunerChannels** | [**kotlin.collections.List&lt;TunerChannelMapping&gt;**](TunerChannelMapping.md) | Gets or sets list of tuner channels. |  [optional]
**providerChannels** | [**kotlin.collections.List&lt;NameIdPair&gt;**](NameIdPair.md) | Gets or sets list of provider channels. |  [optional]
**mappings** | [**kotlin.collections.List&lt;NameValuePair&gt;**](NameValuePair.md) | Gets or sets list of mappings. |  [optional]
**providerName** | **kotlin.String** | Gets or sets provider name. |  [optional]



