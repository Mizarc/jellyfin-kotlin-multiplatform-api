
# AuthenticateWithQuickConnectRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**secret** | **kotlin.String** | Gets or sets the quick connect secret. | 



