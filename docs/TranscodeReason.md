
# TranscodeReason

## Enum


    * `containerNotSupported` (value: `"ContainerNotSupported"`)

    * `videoCodecNotSupported` (value: `"VideoCodecNotSupported"`)

    * `audioCodecNotSupported` (value: `"AudioCodecNotSupported"`)

    * `subtitleCodecNotSupported` (value: `"SubtitleCodecNotSupported"`)

    * `audioIsExternal` (value: `"AudioIsExternal"`)

    * `secondaryAudioNotSupported` (value: `"SecondaryAudioNotSupported"`)

    * `videoProfileNotSupported` (value: `"VideoProfileNotSupported"`)

    * `videoLevelNotSupported` (value: `"VideoLevelNotSupported"`)

    * `videoResolutionNotSupported` (value: `"VideoResolutionNotSupported"`)

    * `videoBitDepthNotSupported` (value: `"VideoBitDepthNotSupported"`)

    * `videoFramerateNotSupported` (value: `"VideoFramerateNotSupported"`)

    * `refFramesNotSupported` (value: `"RefFramesNotSupported"`)

    * `anamorphicVideoNotSupported` (value: `"AnamorphicVideoNotSupported"`)

    * `interlacedVideoNotSupported` (value: `"InterlacedVideoNotSupported"`)

    * `audioChannelsNotSupported` (value: `"AudioChannelsNotSupported"`)

    * `audioProfileNotSupported` (value: `"AudioProfileNotSupported"`)

    * `audioSampleRateNotSupported` (value: `"AudioSampleRateNotSupported"`)

    * `audioBitDepthNotSupported` (value: `"AudioBitDepthNotSupported"`)

    * `containerBitrateExceedsLimit` (value: `"ContainerBitrateExceedsLimit"`)

    * `videoBitrateNotSupported` (value: `"VideoBitrateNotSupported"`)

    * `audioBitrateNotSupported` (value: `"AudioBitrateNotSupported"`)

    * `unknownVideoStreamInfo` (value: `"UnknownVideoStreamInfo"`)

    * `unknownAudioStreamInfo` (value: `"UnknownAudioStreamInfo"`)

    * `directPlayError` (value: `"DirectPlayError"`)

    * `videoRangeTypeNotSupported` (value: `"VideoRangeTypeNotSupported"`)



