
# TunerHostInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**type** | **kotlin.String** |  |  [optional]
**deviceId** | **kotlin.String** |  |  [optional]
**friendlyName** | **kotlin.String** |  |  [optional]
**importFavoritesOnly** | **kotlin.Boolean** |  |  [optional]
**allowHWTranscoding** | **kotlin.Boolean** |  |  [optional]
**enableStreamLooping** | **kotlin.Boolean** |  |  [optional]
**source** | **kotlin.String** |  |  [optional]
**tunerCount** | **kotlin.Int** |  |  [optional]
**userAgent** | **kotlin.String** |  |  [optional]



