# YearsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getYear**](YearsApi.md#getYear) | **GET** /Years/{year} | Gets a year.
[**getYears**](YearsApi.md#getYears) | **GET** /Years | Get years.


<a name="getYear"></a>
# **getYear**
> BaseItemDto getYear(year, userId)

Gets a year.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = YearsApi()
val year : kotlin.Int = 56 // kotlin.Int | The year.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
try {
    val result : BaseItemDto = apiInstance.getYear(year, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling YearsApi#getYear")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling YearsApi#getYear")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **kotlin.Int**| The year. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getYears"></a>
# **getYears**
> BaseItemDtoQueryResult getYears(startIndex, limit, sortOrder, parentId, fields, excludeItemTypes, includeItemTypes, mediaTypes, sortBy, enableUserData, imageTypeLimit, enableImageTypes, userId, recursive, enableImages)

Get years.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = YearsApi()
val startIndex : kotlin.Int = 56 // kotlin.Int | Skips over a given number of items within the results. Use for paging.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val sortOrder : kotlin.collections.List<SortOrder> =  // kotlin.collections.List<SortOrder> | Sort Order - Ascending,Descending.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val excludeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be excluded based on item type. This allows multiple, comma delimited.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be included based on item type. This allows multiple, comma delimited.
val mediaTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Filter by MediaType. Allows multiple, comma delimited.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User Id.
val recursive : kotlin.Boolean = true // kotlin.Boolean | Search recursively.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getYears(startIndex, limit, sortOrder, parentId, fields, excludeItemTypes, includeItemTypes, mediaTypes, sortBy, enableUserData, imageTypeLimit, enableImageTypes, userId, recursive, enableImages)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling YearsApi#getYears")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling YearsApi#getYears")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startIndex** | **kotlin.Int**| Skips over a given number of items within the results. Use for paging. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md)| Sort Order - Ascending,Descending. | [optional]
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **excludeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be excluded based on item type. This allows multiple, comma delimited. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be included based on item type. This allows multiple, comma delimited. | [optional]
 **mediaTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Filter by MediaType. Allows multiple, comma delimited. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **userId** | **kotlin.String**| User Id. | [optional]
 **recursive** | **kotlin.Boolean**| Search recursively. | [optional] [default to true]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

