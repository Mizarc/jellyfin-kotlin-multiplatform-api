
# XmlAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name of the attribute. |  [optional]
**&#x60;value&#x60;** | **kotlin.String** | Gets or sets the value of the attribute. |  [optional]



