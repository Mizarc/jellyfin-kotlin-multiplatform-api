
# EndPointInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isLocal** | **kotlin.Boolean** |  |  [optional]
**isInNetwork** | **kotlin.Boolean** |  |  [optional]



