# ActivityLogApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLogEntries**](ActivityLogApi.md#getLogEntries) | **GET** /System/ActivityLog/Entries | Gets activity log entries.


<a name="getLogEntries"></a>
# **getLogEntries**
> ActivityLogEntryQueryResult getLogEntries(startIndex, limit, minDate, hasUserId)

Gets activity log entries.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ActivityLogApi()
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val minDate : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The minimum date. Format = ISO.
val hasUserId : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter log entries if it has user id, or not.
try {
    val result : ActivityLogEntryQueryResult = apiInstance.getLogEntries(startIndex, limit, minDate, hasUserId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ActivityLogApi#getLogEntries")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ActivityLogApi#getLogEntries")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **minDate** | **kotlin.String**| Optional. The minimum date. Format &#x3D; ISO. | [optional]
 **hasUserId** | **kotlin.Boolean**| Optional. Filter log entries if it has user id, or not. | [optional]

### Return type

[**ActivityLogEntryQueryResult**](ActivityLogEntryQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

