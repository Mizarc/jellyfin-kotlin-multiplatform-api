
# SendFullGeneralCommandRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**GeneralCommandType**](GeneralCommandType.md) |  |  [optional]
**controllingUserId** | **kotlin.String** |  |  [optional]
**arguments** | **kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;** |  |  [optional]



