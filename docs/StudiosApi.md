# StudiosApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStudio**](StudiosApi.md#getStudio) | **GET** /Studios/{name} | Gets a studio by name.
[**getStudios**](StudiosApi.md#getStudios) | **GET** /Studios | Gets all studios from a given item, folder, or the entire library.


<a name="getStudio"></a>
# **getStudio**
> BaseItemDto getStudio(name, userId)

Gets a studio by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StudiosApi()
val name : kotlin.String = name_example // kotlin.String | Studio name.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
try {
    val result : BaseItemDto = apiInstance.getStudio(name, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling StudiosApi#getStudio")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StudiosApi#getStudio")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Studio name. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getStudios"></a>
# **getStudios**
> BaseItemDtoQueryResult getStudios(startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, isFavorite, enableUserData, imageTypeLimit, enableImageTypes, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, enableImages, enableTotalRecordCount)

Gets all studios from a given item, folder, or the entire library.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StudiosApi()
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val searchTerm : kotlin.String = searchTerm_example // kotlin.String | Optional. Search term.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val excludeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are marked as favorite, or not.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional, include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional, the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val nameStartsWithOrGreater : kotlin.String = nameStartsWithOrGreater_example // kotlin.String | Optional filter by items whose name is sorted equally or greater than a given input string.
val nameStartsWith : kotlin.String = nameStartsWith_example // kotlin.String | Optional filter by items whose name is sorted equally than a given input string.
val nameLessThan : kotlin.String = nameLessThan_example // kotlin.String | Optional filter by items whose name is equally or lesser than a given input string.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional, include image information in output.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getStudios(startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, isFavorite, enableUserData, imageTypeLimit, enableImageTypes, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, enableImages, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling StudiosApi#getStudios")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StudiosApi#getStudios")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **searchTerm** | **kotlin.String**| Optional. Search term. | [optional]
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **excludeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional filter by items that are marked as favorite, or not. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional, include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional, the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **userId** | **kotlin.String**| User id. | [optional]
 **nameStartsWithOrGreater** | **kotlin.String**| Optional filter by items whose name is sorted equally or greater than a given input string. | [optional]
 **nameStartsWith** | **kotlin.String**| Optional filter by items whose name is sorted equally than a given input string. | [optional]
 **nameLessThan** | **kotlin.String**| Optional filter by items whose name is equally or lesser than a given input string. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional, include image information in output. | [optional] [default to true]
 **enableTotalRecordCount** | **kotlin.Boolean**| Total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

