
# IPlugin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets the name of the plugin. |  [optional] [readonly]
**description** | **kotlin.String** | Gets the Description. |  [optional] [readonly]
**id** | **kotlin.String** | Gets the unique id. |  [optional] [readonly]
**version** | **kotlin.String** | Gets the plugin version. |  [optional] [readonly]
**assemblyFilePath** | **kotlin.String** | Gets the path to the assembly file. |  [optional] [readonly]
**canUninstall** | **kotlin.Boolean** | Gets a value indicating whether the plugin can be uninstalled. |  [optional] [readonly]
**dataFolderPath** | **kotlin.String** | Gets the full path to the data folder, where the plugin can store any miscellaneous files needed. |  [optional] [readonly]



