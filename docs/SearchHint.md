
# SearchHint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemId** | **kotlin.String** | Gets or sets the item id. |  [optional]
**id** | **kotlin.String** |  |  [optional]
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**matchedTerm** | **kotlin.String** | Gets or sets the matched term. |  [optional]
**indexNumber** | **kotlin.Int** | Gets or sets the index number. |  [optional]
**productionYear** | **kotlin.Int** | Gets or sets the production year. |  [optional]
**parentIndexNumber** | **kotlin.Int** | Gets or sets the parent index number. |  [optional]
**primaryImageTag** | **kotlin.String** | Gets or sets the image tag. |  [optional]
**thumbImageTag** | **kotlin.String** | Gets or sets the thumb image tag. |  [optional]
**thumbImageItemId** | **kotlin.String** | Gets or sets the thumb image item identifier. |  [optional]
**backdropImageTag** | **kotlin.String** | Gets or sets the backdrop image tag. |  [optional]
**backdropImageItemId** | **kotlin.String** | Gets or sets the backdrop image item identifier. |  [optional]
**type** | **kotlin.String** | Gets or sets the type. |  [optional]
**isFolder** | **kotlin.Boolean** |  |  [optional]
**runTimeTicks** | **kotlin.Long** | Gets or sets the run time ticks. |  [optional]
**mediaType** | **kotlin.String** | Gets or sets the type of the media. |  [optional]
**startDate** | **kotlin.String** |  |  [optional]
**endDate** | **kotlin.String** |  |  [optional]
**series** | **kotlin.String** | Gets or sets the series. |  [optional]
**status** | **kotlin.String** |  |  [optional]
**album** | **kotlin.String** | Gets or sets the album. |  [optional]
**albumId** | **kotlin.String** |  |  [optional]
**albumArtist** | **kotlin.String** | Gets or sets the album artist. |  [optional]
**artists** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the artists. |  [optional]
**songCount** | **kotlin.Int** | Gets or sets the song count. |  [optional]
**episodeCount** | **kotlin.Int** | Gets or sets the episode count. |  [optional]
**channelId** | **kotlin.String** | Gets or sets the channel identifier. |  [optional]
**channelName** | **kotlin.String** | Gets or sets the name of the channel. |  [optional]
**primaryImageAspectRatio** | **kotlin.Double** | Gets or sets the primary image aspect ratio. |  [optional]



