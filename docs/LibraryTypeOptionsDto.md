
# LibraryTypeOptionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **kotlin.String** | Gets or sets the type. |  [optional]
**metadataFetchers** | [**kotlin.collections.List&lt;LibraryOptionInfoDto&gt;**](LibraryOptionInfoDto.md) | Gets or sets the metadata fetchers. |  [optional]
**imageFetchers** | [**kotlin.collections.List&lt;LibraryOptionInfoDto&gt;**](LibraryOptionInfoDto.md) | Gets or sets the image fetchers. |  [optional]
**supportedImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md) | Gets or sets the supported image types. |  [optional]
**defaultImageOptions** | [**kotlin.collections.List&lt;ImageOption&gt;**](ImageOption.md) | Gets or sets the default image options. |  [optional]



