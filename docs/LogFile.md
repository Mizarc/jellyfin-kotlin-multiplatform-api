
# LogFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dateCreated** | **kotlin.String** | Gets or sets the date created. |  [optional]
**dateModified** | **kotlin.String** | Gets or sets the date modified. |  [optional]
**propertySize** | **kotlin.Long** | Gets or sets the size. |  [optional]
**name** | **kotlin.String** | Gets or sets the name. |  [optional]



