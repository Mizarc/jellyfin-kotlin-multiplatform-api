# MediaInfoApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**closeLiveStream**](MediaInfoApi.md#closeLiveStream) | **POST** /LiveStreams/Close | Closes a media source.
[**getBitrateTestBytes**](MediaInfoApi.md#getBitrateTestBytes) | **GET** /Playback/BitrateTest | Tests the network with a request with the size of the bitrate.
[**getPlaybackInfo**](MediaInfoApi.md#getPlaybackInfo) | **GET** /Items/{itemId}/PlaybackInfo | Gets live playback media info for an item.
[**getPostedPlaybackInfo**](MediaInfoApi.md#getPostedPlaybackInfo) | **POST** /Items/{itemId}/PlaybackInfo | Gets live playback media info for an item.
[**openLiveStream**](MediaInfoApi.md#openLiveStream) | **POST** /LiveStreams/Open | Opens a media source.


<a name="closeLiveStream"></a>
# **closeLiveStream**
> closeLiveStream(liveStreamId)

Closes a media source.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = MediaInfoApi()
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The livestream id.
try {
    apiInstance.closeLiveStream(liveStreamId)
} catch (e: ClientException) {
    println("4xx response calling MediaInfoApi#closeLiveStream")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MediaInfoApi#closeLiveStream")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **liveStreamId** | **kotlin.String**| The livestream id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBitrateTestBytes"></a>
# **getBitrateTestBytes**
> org.openapitools.client.infrastructure.OctetByteArray getBitrateTestBytes(size)

Tests the network with a request with the size of the bitrate.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = MediaInfoApi()
val size : kotlin.Int = 56 // kotlin.Int | The bitrate. Defaults to 102400.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getBitrateTestBytes(size)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MediaInfoApi#getBitrateTestBytes")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MediaInfoApi#getBitrateTestBytes")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **kotlin.Int**| The bitrate. Defaults to 102400. | [optional] [default to 102400]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getPlaybackInfo"></a>
# **getPlaybackInfo**
> PlaybackInfoResponse getPlaybackInfo(itemId, userId)

Gets live playback media info for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = MediaInfoApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
try {
    val result : PlaybackInfoResponse = apiInstance.getPlaybackInfo(itemId, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MediaInfoApi#getPlaybackInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MediaInfoApi#getPlaybackInfo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| The user id. |

### Return type

[**PlaybackInfoResponse**](PlaybackInfoResponse.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPostedPlaybackInfo"></a>
# **getPostedPlaybackInfo**
> PlaybackInfoResponse getPostedPlaybackInfo(itemId, userId, maxStreamingBitrate, startTimeTicks, audioStreamIndex, subtitleStreamIndex, maxAudioChannels, mediaSourceId, liveStreamId, autoOpenLiveStream, enableDirectPlay, enableDirectStream, enableTranscoding, allowVideoStreamCopy, allowAudioStreamCopy, getPostedPlaybackInfoRequest)

Gets live playback media info for an item.

For backwards compatibility parameters can be sent via Query or Body, with Query having higher precedence.  Query parameters are obsolete.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = MediaInfoApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | The maximum streaming bitrate.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | The start time in ticks.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | The audio stream index.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | The subtitle stream index.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | The maximum number of audio channels.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media source id.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The livestream id.
val autoOpenLiveStream : kotlin.Boolean = true // kotlin.Boolean | Whether to auto open the livestream.
val enableDirectPlay : kotlin.Boolean = true // kotlin.Boolean | Whether to enable direct play. Default: true.
val enableDirectStream : kotlin.Boolean = true // kotlin.Boolean | Whether to enable direct stream. Default: true.
val enableTranscoding : kotlin.Boolean = true // kotlin.Boolean | Whether to enable transcoding. Default: true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether to allow to copy the video stream. Default: true.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether to allow to copy the audio stream. Default: true.
val getPostedPlaybackInfoRequest : GetPostedPlaybackInfoRequest =  // GetPostedPlaybackInfoRequest | The playback info.
try {
    val result : PlaybackInfoResponse = apiInstance.getPostedPlaybackInfo(itemId, userId, maxStreamingBitrate, startTimeTicks, audioStreamIndex, subtitleStreamIndex, maxAudioChannels, mediaSourceId, liveStreamId, autoOpenLiveStream, enableDirectPlay, enableDirectStream, enableTranscoding, allowVideoStreamCopy, allowAudioStreamCopy, getPostedPlaybackInfoRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MediaInfoApi#getPostedPlaybackInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MediaInfoApi#getPostedPlaybackInfo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| The user id. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| The maximum streaming bitrate. | [optional]
 **startTimeTicks** | **kotlin.Long**| The start time in ticks. | [optional]
 **audioStreamIndex** | **kotlin.Int**| The audio stream index. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| The subtitle stream index. | [optional]
 **maxAudioChannels** | **kotlin.Int**| The maximum number of audio channels. | [optional]
 **mediaSourceId** | **kotlin.String**| The media source id. | [optional]
 **liveStreamId** | **kotlin.String**| The livestream id. | [optional]
 **autoOpenLiveStream** | **kotlin.Boolean**| Whether to auto open the livestream. | [optional]
 **enableDirectPlay** | **kotlin.Boolean**| Whether to enable direct play. Default: true. | [optional]
 **enableDirectStream** | **kotlin.Boolean**| Whether to enable direct stream. Default: true. | [optional]
 **enableTranscoding** | **kotlin.Boolean**| Whether to enable transcoding. Default: true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether to allow to copy the video stream. Default: true. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether to allow to copy the audio stream. Default: true. | [optional]
 **getPostedPlaybackInfoRequest** | [**GetPostedPlaybackInfoRequest**](GetPostedPlaybackInfoRequest.md)| The playback info. | [optional]

### Return type

[**PlaybackInfoResponse**](PlaybackInfoResponse.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="openLiveStream"></a>
# **openLiveStream**
> LiveStreamResponse openLiveStream(openToken, userId, playSessionId, maxStreamingBitrate, startTimeTicks, audioStreamIndex, subtitleStreamIndex, maxAudioChannels, itemId, enableDirectPlay, enableDirectStream, openLiveStreamRequest)

Opens a media source.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = MediaInfoApi()
val openToken : kotlin.String = openToken_example // kotlin.String | The open token.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | The maximum streaming bitrate.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | The start time in ticks.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | The audio stream index.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | The subtitle stream index.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | The maximum number of audio channels.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val enableDirectPlay : kotlin.Boolean = true // kotlin.Boolean | Whether to enable direct play. Default: true.
val enableDirectStream : kotlin.Boolean = true // kotlin.Boolean | Whether to enable direct stream. Default: true.
val openLiveStreamRequest : OpenLiveStreamRequest =  // OpenLiveStreamRequest | The open live stream dto.
try {
    val result : LiveStreamResponse = apiInstance.openLiveStream(openToken, userId, playSessionId, maxStreamingBitrate, startTimeTicks, audioStreamIndex, subtitleStreamIndex, maxAudioChannels, itemId, enableDirectPlay, enableDirectStream, openLiveStreamRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MediaInfoApi#openLiveStream")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MediaInfoApi#openLiveStream")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **openToken** | **kotlin.String**| The open token. | [optional]
 **userId** | **kotlin.String**| The user id. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| The maximum streaming bitrate. | [optional]
 **startTimeTicks** | **kotlin.Long**| The start time in ticks. | [optional]
 **audioStreamIndex** | **kotlin.Int**| The audio stream index. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| The subtitle stream index. | [optional]
 **maxAudioChannels** | **kotlin.Int**| The maximum number of audio channels. | [optional]
 **itemId** | **kotlin.String**| The item id. | [optional]
 **enableDirectPlay** | **kotlin.Boolean**| Whether to enable direct play. Default: true. | [optional]
 **enableDirectStream** | **kotlin.Boolean**| Whether to enable direct stream. Default: true. | [optional]
 **openLiveStreamRequest** | [**OpenLiveStreamRequest**](OpenLiveStreamRequest.md)| The open live stream dto. | [optional]

### Return type

[**LiveStreamResponse**](LiveStreamResponse.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

