
# PingRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ping** | **kotlin.Long** | Gets or sets the ping time. |  [optional]



