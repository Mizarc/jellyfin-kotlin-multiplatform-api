
# TranscodingProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container** | **kotlin.String** |  |  [optional]
**type** | [**DlnaProfileType**](DlnaProfileType.md) |  |  [optional]
**videoCodec** | **kotlin.String** |  |  [optional]
**audioCodec** | **kotlin.String** |  |  [optional]
**protocol** | **kotlin.String** |  |  [optional]
**estimateContentLength** | **kotlin.Boolean** |  |  [optional]
**enableMpegtsM2TsMode** | **kotlin.Boolean** |  |  [optional]
**transcodeSeekInfo** | [**TranscodeSeekInfo**](TranscodeSeekInfo.md) |  |  [optional]
**copyTimestamps** | **kotlin.Boolean** |  |  [optional]
**context** | [**EncodingContext**](EncodingContext.md) |  |  [optional]
**enableSubtitlesInManifest** | **kotlin.Boolean** |  |  [optional]
**maxAudioChannels** | **kotlin.String** |  |  [optional]
**minSegments** | **kotlin.Int** |  |  [optional]
**segmentLength** | **kotlin.Int** |  |  [optional]
**breakOnNonKeyFrames** | **kotlin.Boolean** |  |  [optional]
**conditions** | [**kotlin.collections.List&lt;ProfileCondition&gt;**](ProfileCondition.md) |  |  [optional]



