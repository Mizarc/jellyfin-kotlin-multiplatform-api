# DashboardApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getConfigurationPages**](DashboardApi.md#getConfigurationPages) | **GET** /web/ConfigurationPages | Gets the configuration pages.
[**getDashboardConfigurationPage**](DashboardApi.md#getDashboardConfigurationPage) | **GET** /web/ConfigurationPage | Gets a dashboard configuration page.


<a name="getConfigurationPages"></a>
# **getConfigurationPages**
> kotlin.collections.List&lt;ConfigurationPageInfo&gt; getConfigurationPages(enableInMainMenu)

Gets the configuration pages.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DashboardApi()
val enableInMainMenu : kotlin.Boolean = true // kotlin.Boolean | Whether to enable in the main menu.
try {
    val result : kotlin.collections.List<ConfigurationPageInfo> = apiInstance.getConfigurationPages(enableInMainMenu)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DashboardApi#getConfigurationPages")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DashboardApi#getConfigurationPages")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enableInMainMenu** | **kotlin.Boolean**| Whether to enable in the main menu. | [optional]

### Return type

[**kotlin.collections.List&lt;ConfigurationPageInfo&gt;**](ConfigurationPageInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDashboardConfigurationPage"></a>
# **getDashboardConfigurationPage**
> org.openapitools.client.infrastructure.OctetByteArray getDashboardConfigurationPage(name)

Gets a dashboard configuration page.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DashboardApi()
val name : kotlin.String = name_example // kotlin.String | The name of the page.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getDashboardConfigurationPage(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DashboardApi#getDashboardConfigurationPage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DashboardApi#getDashboardConfigurationPage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the page. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html, application/x-javascript, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

