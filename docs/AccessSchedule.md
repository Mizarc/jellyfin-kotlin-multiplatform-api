
# AccessSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | Gets the id of this instance. |  [optional] [readonly]
**userId** | **kotlin.String** | Gets the id of the associated user. |  [optional]
**dayOfWeek** | [**DynamicDayOfWeek**](DynamicDayOfWeek.md) |  |  [optional]
**startHour** | **kotlin.Double** | Gets or sets the start hour. |  [optional]
**endHour** | **kotlin.Double** | Gets or sets the end hour. |  [optional]



