
# LibraryOptionsResultDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadataSavers** | [**kotlin.collections.List&lt;LibraryOptionInfoDto&gt;**](LibraryOptionInfoDto.md) | Gets or sets the metadata savers. |  [optional]
**metadataReaders** | [**kotlin.collections.List&lt;LibraryOptionInfoDto&gt;**](LibraryOptionInfoDto.md) | Gets or sets the metadata readers. |  [optional]
**subtitleFetchers** | [**kotlin.collections.List&lt;LibraryOptionInfoDto&gt;**](LibraryOptionInfoDto.md) | Gets or sets the subtitle fetchers. |  [optional]
**typeOptions** | [**kotlin.collections.List&lt;LibraryTypeOptionsDto&gt;**](LibraryTypeOptionsDto.md) | Gets or sets the type options. |  [optional]



