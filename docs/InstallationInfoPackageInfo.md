
# InstallationInfoPackageInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**description** | **kotlin.String** | Gets or sets a long description of the plugin containing features or helpful explanations. |  [optional]
**overview** | **kotlin.String** | Gets or sets a short overview of what the plugin does. |  [optional]
**owner** | **kotlin.String** | Gets or sets the owner. |  [optional]
**category** | **kotlin.String** | Gets or sets the category. |  [optional]
**guid** | **kotlin.String** | Gets or sets the guid of the assembly associated with this plugin.  This is used to identify the proper item for automatic updates. |  [optional]
**versions** | [**kotlin.collections.List&lt;VersionInfo&gt;**](VersionInfo.md) | Gets or sets the versions. |  [optional]
**imageUrl** | **kotlin.String** | Gets or sets the image url for the package. |  [optional]



