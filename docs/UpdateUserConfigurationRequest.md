
# UpdateUserConfigurationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audioLanguagePreference** | **kotlin.String** | Gets or sets the audio language preference. |  [optional]
**playDefaultAudioTrack** | **kotlin.Boolean** | Gets or sets a value indicating whether [play default audio track]. |  [optional]
**subtitleLanguagePreference** | **kotlin.String** | Gets or sets the subtitle language preference. |  [optional]
**displayMissingEpisodes** | **kotlin.Boolean** |  |  [optional]
**groupedFolders** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**subtitleMode** | [**SubtitlePlaybackMode**](SubtitlePlaybackMode.md) |  |  [optional]
**displayCollectionsView** | **kotlin.Boolean** |  |  [optional]
**enableLocalPassword** | **kotlin.Boolean** |  |  [optional]
**orderedViews** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**latestItemsExcludes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**myMediaExcludes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**hidePlayedInLatest** | **kotlin.Boolean** |  |  [optional]
**rememberAudioSelections** | **kotlin.Boolean** |  |  [optional]
**rememberSubtitleSelections** | **kotlin.Boolean** |  |  [optional]
**enableNextEpisodeAutoPlay** | **kotlin.Boolean** |  |  [optional]



