# UserLibraryApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserItemRating**](UserLibraryApi.md#deleteUserItemRating) | **DELETE** /Users/{userId}/Items/{itemId}/Rating | Deletes a user&#39;s saved personal rating for an item.
[**getIntros**](UserLibraryApi.md#getIntros) | **GET** /Users/{userId}/Items/{itemId}/Intros | Gets intros to play before the main media item plays.
[**getItem**](UserLibraryApi.md#getItem) | **GET** /Users/{userId}/Items/{itemId} | Gets an item from a user&#39;s library.
[**getLatestMedia**](UserLibraryApi.md#getLatestMedia) | **GET** /Users/{userId}/Items/Latest | Gets latest media.
[**getLocalTrailers**](UserLibraryApi.md#getLocalTrailers) | **GET** /Users/{userId}/Items/{itemId}/LocalTrailers | Gets local trailers for an item.
[**getRootFolder**](UserLibraryApi.md#getRootFolder) | **GET** /Users/{userId}/Items/Root | Gets the root folder from a user&#39;s library.
[**getSpecialFeatures**](UserLibraryApi.md#getSpecialFeatures) | **GET** /Users/{userId}/Items/{itemId}/SpecialFeatures | Gets special features for an item.
[**markFavoriteItem**](UserLibraryApi.md#markFavoriteItem) | **POST** /Users/{userId}/FavoriteItems/{itemId} | Marks an item as a favorite.
[**unmarkFavoriteItem**](UserLibraryApi.md#unmarkFavoriteItem) | **DELETE** /Users/{userId}/FavoriteItems/{itemId} | Unmarks item as a favorite.
[**updateUserItemRating**](UserLibraryApi.md#updateUserItemRating) | **POST** /Users/{userId}/Items/{itemId}/Rating | Updates a user&#39;s rating for an item.


<a name="deleteUserItemRating"></a>
# **deleteUserItemRating**
> UserItemDataDto deleteUserItemRating(userId, itemId)

Deletes a user&#39;s saved personal rating for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : UserItemDataDto = apiInstance.deleteUserItemRating(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#deleteUserItemRating")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#deleteUserItemRating")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**UserItemDataDto**](UserItemDataDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getIntros"></a>
# **getIntros**
> BaseItemDtoQueryResult getIntros(userId, itemId)

Gets intros to play before the main media item plays.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getIntros(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#getIntros")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#getIntros")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getItem"></a>
# **getItem**
> BaseItemDto getItem(userId, itemId)

Gets an item from a user&#39;s library.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : BaseItemDto = apiInstance.getItem(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#getItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#getItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLatestMedia"></a>
# **getLatestMedia**
> kotlin.collections.List&lt;BaseItemDto&gt; getLatestMedia(userId, parentId, fields, includeItemTypes, isPlayed, enableImages, imageTypeLimit, enableImageTypes, enableUserData, limit, groupItems)

Gets latest media.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited.
val isPlayed : kotlin.Boolean = true // kotlin.Boolean | Filter by items that are played, or not.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. include user data.
val limit : kotlin.Int = 56 // kotlin.Int | Return item limit.
val groupItems : kotlin.Boolean = true // kotlin.Boolean | Whether or not to group items into a parent container.
try {
    val result : kotlin.collections.List<BaseItemDto> = apiInstance.getLatestMedia(userId, parentId, fields, includeItemTypes, isPlayed, enableImages, imageTypeLimit, enableImageTypes, enableUserData, limit, groupItems)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#getLatestMedia")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#getLatestMedia")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. | [optional]
 **isPlayed** | **kotlin.Boolean**| Filter by items that are played, or not. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. include user data. | [optional]
 **limit** | **kotlin.Int**| Return item limit. | [optional] [default to 20]
 **groupItems** | **kotlin.Boolean**| Whether or not to group items into a parent container. | [optional] [default to true]

### Return type

[**kotlin.collections.List&lt;BaseItemDto&gt;**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLocalTrailers"></a>
# **getLocalTrailers**
> kotlin.collections.List&lt;BaseItemDto&gt; getLocalTrailers(userId, itemId)

Gets local trailers for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : kotlin.collections.List<BaseItemDto> = apiInstance.getLocalTrailers(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#getLocalTrailers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#getLocalTrailers")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**kotlin.collections.List&lt;BaseItemDto&gt;**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRootFolder"></a>
# **getRootFolder**
> BaseItemDto getRootFolder(userId)

Gets the root folder from a user&#39;s library.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
try {
    val result : BaseItemDto = apiInstance.getRootFolder(userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#getRootFolder")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#getRootFolder")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSpecialFeatures"></a>
# **getSpecialFeatures**
> kotlin.collections.List&lt;BaseItemDto&gt; getSpecialFeatures(userId, itemId)

Gets special features for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : kotlin.collections.List<BaseItemDto> = apiInstance.getSpecialFeatures(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#getSpecialFeatures")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#getSpecialFeatures")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**kotlin.collections.List&lt;BaseItemDto&gt;**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="markFavoriteItem"></a>
# **markFavoriteItem**
> UserItemDataDto markFavoriteItem(userId, itemId)

Marks an item as a favorite.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : UserItemDataDto = apiInstance.markFavoriteItem(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#markFavoriteItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#markFavoriteItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**UserItemDataDto**](UserItemDataDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="unmarkFavoriteItem"></a>
# **unmarkFavoriteItem**
> UserItemDataDto unmarkFavoriteItem(userId, itemId)

Unmarks item as a favorite.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
try {
    val result : UserItemDataDto = apiInstance.unmarkFavoriteItem(userId, itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#unmarkFavoriteItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#unmarkFavoriteItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |

### Return type

[**UserItemDataDto**](UserItemDataDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateUserItemRating"></a>
# **updateUserItemRating**
> UserItemDataDto updateUserItemRating(userId, itemId, likes)

Updates a user&#39;s rating for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserLibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val likes : kotlin.Boolean = true // kotlin.Boolean | Whether this M:Jellyfin.Api.Controllers.UserLibraryController.UpdateUserItemRating(System.Guid,System.Guid,System.Nullable{System.Boolean}) is likes.
try {
    val result : UserItemDataDto = apiInstance.updateUserItemRating(userId, itemId, likes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserLibraryApi#updateUserItemRating")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserLibraryApi#updateUserItemRating")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **itemId** | **kotlin.String**| Item id. |
 **likes** | **kotlin.Boolean**| Whether this M:Jellyfin.Api.Controllers.UserLibraryController.UpdateUserItemRating(System.Guid,System.Guid,System.Nullable{System.Boolean}) is likes. | [optional]

### Return type

[**UserItemDataDto**](UserItemDataDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

