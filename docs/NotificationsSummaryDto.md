
# NotificationsSummaryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unreadCount** | **kotlin.Int** | Gets or sets the number of unread notifications. |  [optional]
**maxUnreadNotificationLevel** | [**NotificationLevel**](NotificationLevel.md) |  |  [optional]



