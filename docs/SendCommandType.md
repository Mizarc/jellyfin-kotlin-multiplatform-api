
# SendCommandType

## Enum


    * `unpause` (value: `"Unpause"`)

    * `pause` (value: `"Pause"`)

    * `stop` (value: `"Stop"`)

    * `seek` (value: `"Seek"`)



