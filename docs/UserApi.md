# UserApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authenticateUser**](UserApi.md#authenticateUser) | **POST** /Users/{userId}/Authenticate | Authenticates a user.
[**authenticateUserByName**](UserApi.md#authenticateUserByName) | **POST** /Users/AuthenticateByName | Authenticates a user by name.
[**authenticateWithQuickConnect**](UserApi.md#authenticateWithQuickConnect) | **POST** /Users/AuthenticateWithQuickConnect | Authenticates a user with quick connect.
[**createUserByName**](UserApi.md#createUserByName) | **POST** /Users/New | Creates a user.
[**deleteUser**](UserApi.md#deleteUser) | **DELETE** /Users/{userId} | Deletes a user.
[**forgotPassword**](UserApi.md#forgotPassword) | **POST** /Users/ForgotPassword | Initiates the forgot password process for a local user.
[**forgotPasswordPin**](UserApi.md#forgotPasswordPin) | **POST** /Users/ForgotPassword/Pin | Redeems a forgot password pin.
[**getCurrentUser**](UserApi.md#getCurrentUser) | **GET** /Users/Me | Gets the user based on auth token.
[**getPublicUsers**](UserApi.md#getPublicUsers) | **GET** /Users/Public | Gets a list of publicly visible users for display on a login screen.
[**getUserById**](UserApi.md#getUserById) | **GET** /Users/{userId} | Gets a user by Id.
[**getUsers**](UserApi.md#getUsers) | **GET** /Users | Gets a list of users.
[**updateUser**](UserApi.md#updateUser) | **POST** /Users/{userId} | Updates a user.
[**updateUserConfiguration**](UserApi.md#updateUserConfiguration) | **POST** /Users/{userId}/Configuration | Updates a user configuration.
[**updateUserEasyPassword**](UserApi.md#updateUserEasyPassword) | **POST** /Users/{userId}/EasyPassword | Updates a user&#39;s easy password.
[**updateUserPassword**](UserApi.md#updateUserPassword) | **POST** /Users/{userId}/Password | Updates a user&#39;s password.
[**updateUserPolicy**](UserApi.md#updateUserPolicy) | **POST** /Users/{userId}/Policy | Updates a user policy.


<a name="authenticateUser"></a>
# **authenticateUser**
> AuthenticationResult authenticateUser(userId, pw, password)

Authenticates a user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val pw : kotlin.String = pw_example // kotlin.String | The password as plain text.
val password : kotlin.String = password_example // kotlin.String | The password sha1-hash.
try {
    val result : AuthenticationResult = apiInstance.authenticateUser(userId, pw, password)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#authenticateUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#authenticateUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |
 **pw** | **kotlin.String**| The password as plain text. |
 **password** | **kotlin.String**| The password sha1-hash. | [optional]

### Return type

[**AuthenticationResult**](AuthenticationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="authenticateUserByName"></a>
# **authenticateUserByName**
> AuthenticationResult authenticateUserByName(authenticateUserByNameRequest)

Authenticates a user by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val authenticateUserByNameRequest : AuthenticateUserByNameRequest =  // AuthenticateUserByNameRequest | The M:Jellyfin.Api.Controllers.UserController.AuthenticateUserByName(Jellyfin.Api.Models.UserDtos.AuthenticateUserByName) request.
try {
    val result : AuthenticationResult = apiInstance.authenticateUserByName(authenticateUserByNameRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#authenticateUserByName")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#authenticateUserByName")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authenticateUserByNameRequest** | [**AuthenticateUserByNameRequest**](AuthenticateUserByNameRequest.md)| The M:Jellyfin.Api.Controllers.UserController.AuthenticateUserByName(Jellyfin.Api.Models.UserDtos.AuthenticateUserByName) request. |

### Return type

[**AuthenticationResult**](AuthenticationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="authenticateWithQuickConnect"></a>
# **authenticateWithQuickConnect**
> AuthenticationResult authenticateWithQuickConnect(authenticateWithQuickConnectRequest)

Authenticates a user with quick connect.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val authenticateWithQuickConnectRequest : AuthenticateWithQuickConnectRequest =  // AuthenticateWithQuickConnectRequest | The Jellyfin.Api.Models.UserDtos.QuickConnectDto request.
try {
    val result : AuthenticationResult = apiInstance.authenticateWithQuickConnect(authenticateWithQuickConnectRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#authenticateWithQuickConnect")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#authenticateWithQuickConnect")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authenticateWithQuickConnectRequest** | [**AuthenticateWithQuickConnectRequest**](AuthenticateWithQuickConnectRequest.md)| The Jellyfin.Api.Models.UserDtos.QuickConnectDto request. |

### Return type

[**AuthenticationResult**](AuthenticationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="createUserByName"></a>
# **createUserByName**
> UserDto createUserByName(createUserByNameRequest)

Creates a user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val createUserByNameRequest : CreateUserByNameRequest =  // CreateUserByNameRequest | The create user by name request body.
try {
    val result : UserDto = apiInstance.createUserByName(createUserByNameRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#createUserByName")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#createUserByName")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUserByNameRequest** | [**CreateUserByNameRequest**](CreateUserByNameRequest.md)| The create user by name request body. |

### Return type

[**UserDto**](UserDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="deleteUser"></a>
# **deleteUser**
> deleteUser(userId)

Deletes a user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
try {
    apiInstance.deleteUser(userId)
} catch (e: ClientException) {
    println("4xx response calling UserApi#deleteUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#deleteUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="forgotPassword"></a>
# **forgotPassword**
> ForgotPasswordResult forgotPassword(forgotPasswordRequest)

Initiates the forgot password process for a local user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val forgotPasswordRequest : ForgotPasswordRequest =  // ForgotPasswordRequest | The forgot password request containing the entered username.
try {
    val result : ForgotPasswordResult = apiInstance.forgotPassword(forgotPasswordRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#forgotPassword")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#forgotPassword")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forgotPasswordRequest** | [**ForgotPasswordRequest**](ForgotPasswordRequest.md)| The forgot password request containing the entered username. |

### Return type

[**ForgotPasswordResult**](ForgotPasswordResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="forgotPasswordPin"></a>
# **forgotPasswordPin**
> PinRedeemResult forgotPasswordPin(forgotPasswordPinRequest)

Redeems a forgot password pin.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val forgotPasswordPinRequest : ForgotPasswordPinRequest =  // ForgotPasswordPinRequest | The forgot password pin request containing the entered pin.
try {
    val result : PinRedeemResult = apiInstance.forgotPasswordPin(forgotPasswordPinRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#forgotPasswordPin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#forgotPasswordPin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forgotPasswordPinRequest** | [**ForgotPasswordPinRequest**](ForgotPasswordPinRequest.md)| The forgot password pin request containing the entered pin. |

### Return type

[**PinRedeemResult**](PinRedeemResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getCurrentUser"></a>
# **getCurrentUser**
> UserDto getCurrentUser()

Gets the user based on auth token.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
try {
    val result : UserDto = apiInstance.getCurrentUser()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#getCurrentUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#getCurrentUser")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDto**](UserDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPublicUsers"></a>
# **getPublicUsers**
> kotlin.collections.List&lt;UserDto&gt; getPublicUsers()

Gets a list of publicly visible users for display on a login screen.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
try {
    val result : kotlin.collections.List<UserDto> = apiInstance.getPublicUsers()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#getPublicUsers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#getPublicUsers")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;UserDto&gt;**](UserDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getUserById"></a>
# **getUserById**
> UserDto getUserById(userId)

Gets a user by Id.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
try {
    val result : UserDto = apiInstance.getUserById(userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#getUserById")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#getUserById")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |

### Return type

[**UserDto**](UserDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getUsers"></a>
# **getUsers**
> kotlin.collections.List&lt;UserDto&gt; getUsers(isHidden, isDisabled)

Gets a list of users.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val isHidden : kotlin.Boolean = true // kotlin.Boolean | Optional filter by IsHidden=true or false.
val isDisabled : kotlin.Boolean = true // kotlin.Boolean | Optional filter by IsDisabled=true or false.
try {
    val result : kotlin.collections.List<UserDto> = apiInstance.getUsers(isHidden, isDisabled)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#getUsers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#getUsers")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isHidden** | **kotlin.Boolean**| Optional filter by IsHidden&#x3D;true or false. | [optional]
 **isDisabled** | **kotlin.Boolean**| Optional filter by IsDisabled&#x3D;true or false. | [optional]

### Return type

[**kotlin.collections.List&lt;UserDto&gt;**](UserDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateUser"></a>
# **updateUser**
> updateUser(userId, updateUserRequest)

Updates a user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val updateUserRequest : UpdateUserRequest =  // UpdateUserRequest | The updated user model.
try {
    apiInstance.updateUser(userId, updateUserRequest)
} catch (e: ClientException) {
    println("4xx response calling UserApi#updateUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#updateUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |
 **updateUserRequest** | [**UpdateUserRequest**](UpdateUserRequest.md)| The updated user model. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateUserConfiguration"></a>
# **updateUserConfiguration**
> updateUserConfiguration(userId, updateUserConfigurationRequest)

Updates a user configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val updateUserConfigurationRequest : UpdateUserConfigurationRequest =  // UpdateUserConfigurationRequest | The new user configuration.
try {
    apiInstance.updateUserConfiguration(userId, updateUserConfigurationRequest)
} catch (e: ClientException) {
    println("4xx response calling UserApi#updateUserConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#updateUserConfiguration")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |
 **updateUserConfigurationRequest** | [**UpdateUserConfigurationRequest**](UpdateUserConfigurationRequest.md)| The new user configuration. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateUserEasyPassword"></a>
# **updateUserEasyPassword**
> updateUserEasyPassword(userId, updateUserEasyPasswordRequest)

Updates a user&#39;s easy password.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val updateUserEasyPasswordRequest : UpdateUserEasyPasswordRequest =  // UpdateUserEasyPasswordRequest | The M:Jellyfin.Api.Controllers.UserController.UpdateUserEasyPassword(System.Guid,Jellyfin.Api.Models.UserDtos.UpdateUserEasyPassword) request.
try {
    apiInstance.updateUserEasyPassword(userId, updateUserEasyPasswordRequest)
} catch (e: ClientException) {
    println("4xx response calling UserApi#updateUserEasyPassword")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#updateUserEasyPassword")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |
 **updateUserEasyPasswordRequest** | [**UpdateUserEasyPasswordRequest**](UpdateUserEasyPasswordRequest.md)| The M:Jellyfin.Api.Controllers.UserController.UpdateUserEasyPassword(System.Guid,Jellyfin.Api.Models.UserDtos.UpdateUserEasyPassword) request. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateUserPassword"></a>
# **updateUserPassword**
> updateUserPassword(userId, updateUserPasswordRequest)

Updates a user&#39;s password.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val updateUserPasswordRequest : UpdateUserPasswordRequest =  // UpdateUserPasswordRequest | The M:Jellyfin.Api.Controllers.UserController.UpdateUserPassword(System.Guid,Jellyfin.Api.Models.UserDtos.UpdateUserPassword) request.
try {
    apiInstance.updateUserPassword(userId, updateUserPasswordRequest)
} catch (e: ClientException) {
    println("4xx response calling UserApi#updateUserPassword")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#updateUserPassword")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |
 **updateUserPasswordRequest** | [**UpdateUserPasswordRequest**](UpdateUserPasswordRequest.md)| The M:Jellyfin.Api.Controllers.UserController.UpdateUserPassword(System.Guid,Jellyfin.Api.Models.UserDtos.UpdateUserPassword) request. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateUserPolicy"></a>
# **updateUserPolicy**
> updateUserPolicy(userId, updateUserPolicyRequest)

Updates a user policy.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val updateUserPolicyRequest : UpdateUserPolicyRequest =  // UpdateUserPolicyRequest | The new user policy.
try {
    apiInstance.updateUserPolicy(userId, updateUserPolicyRequest)
} catch (e: ClientException) {
    println("4xx response calling UserApi#updateUserPolicy")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#updateUserPolicy")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id. |
 **updateUserPolicyRequest** | [**UpdateUserPolicyRequest**](UpdateUserPolicyRequest.md)| The new user policy. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

