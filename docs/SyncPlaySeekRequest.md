
# SyncPlaySeekRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**positionTicks** | **kotlin.Long** | Gets or sets the position ticks. |  [optional]



