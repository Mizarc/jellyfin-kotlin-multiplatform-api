
# AuthenticationInfoQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**kotlin.collections.List&lt;AuthenticationInfo&gt;**](AuthenticationInfo.md) | Gets or sets the items. |  [optional]
**totalRecordCount** | **kotlin.Int** | Gets or sets the total number of records available. |  [optional]
**startIndex** | **kotlin.Int** | Gets or sets the index of the first record in Items. |  [optional]



