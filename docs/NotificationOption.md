
# NotificationOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **kotlin.String** |  |  [optional]
**disabledMonitorUsers** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets user Ids to not monitor (it&#39;s opt out). |  [optional]
**sendToUsers** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets user Ids to send to (if SendToUserMode &#x3D;&#x3D; Custom). |  [optional]
**enabled** | **kotlin.Boolean** | Gets or sets a value indicating whether this MediaBrowser.Model.Notifications.NotificationOption is enabled. |  [optional]
**disabledServices** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the disabled services. |  [optional]
**sendToUserMode** | [**SendToUserType**](SendToUserType.md) |  |  [optional]



