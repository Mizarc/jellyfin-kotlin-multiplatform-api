
# LibraryOptionInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets name. |  [optional]
**defaultEnabled** | **kotlin.Boolean** | Gets or sets a value indicating whether default enabled. |  [optional]



