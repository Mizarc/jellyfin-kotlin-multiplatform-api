
# DeviceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  |  [optional]
**accessToken** | **kotlin.String** | Gets or sets the access token. |  [optional]
**id** | **kotlin.String** | Gets or sets the identifier. |  [optional]
**lastUserName** | **kotlin.String** | Gets or sets the last name of the user. |  [optional]
**appName** | **kotlin.String** | Gets or sets the name of the application. |  [optional]
**appVersion** | **kotlin.String** | Gets or sets the application version. |  [optional]
**lastUserId** | **kotlin.String** | Gets or sets the last user identifier. |  [optional]
**dateLastActivity** | **kotlin.String** | Gets or sets the date last modified. |  [optional]
**capabilities** | [**DeviceInfoCapabilities**](DeviceInfoCapabilities.md) |  |  [optional]
**iconUrl** | **kotlin.String** |  |  [optional]



