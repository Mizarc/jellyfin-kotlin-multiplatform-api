
# DeviceProfileIdentification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**friendlyName** | **kotlin.String** | Gets or sets the name of the friendly. |  [optional]
**modelNumber** | **kotlin.String** | Gets or sets the model number. |  [optional]
**serialNumber** | **kotlin.String** | Gets or sets the serial number. |  [optional]
**modelName** | **kotlin.String** | Gets or sets the name of the model. |  [optional]
**modelDescription** | **kotlin.String** | Gets or sets the model description. |  [optional]
**modelUrl** | **kotlin.String** | Gets or sets the model URL. |  [optional]
**manufacturer** | **kotlin.String** | Gets or sets the manufacturer. |  [optional]
**manufacturerUrl** | **kotlin.String** | Gets or sets the manufacturer URL. |  [optional]
**headers** | [**kotlin.collections.List&lt;HttpHeaderInfo&gt;**](HttpHeaderInfo.md) | Gets or sets the headers. |  [optional]



