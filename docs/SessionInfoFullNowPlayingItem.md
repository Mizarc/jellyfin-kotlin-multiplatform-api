
# SessionInfoFullNowPlayingItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertySize** | **kotlin.Long** |  |  [optional]
**container** | **kotlin.String** |  |  [optional]
**isHD** | **kotlin.Boolean** |  |  [optional] [readonly]
**isShortcut** | **kotlin.Boolean** |  |  [optional]
**shortcutPath** | **kotlin.String** |  |  [optional]
**width** | **kotlin.Int** |  |  [optional]
**height** | **kotlin.Int** |  |  [optional]
**extraIds** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**dateLastSaved** | **kotlin.String** |  |  [optional]
**remoteTrailers** | [**kotlin.collections.List&lt;MediaUrl&gt;**](MediaUrl.md) | Gets or sets the remote trailers. |  [optional]
**supportsExternalTransfer** | **kotlin.Boolean** |  |  [optional] [readonly]



