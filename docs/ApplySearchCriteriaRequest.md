
# ApplySearchCriteriaRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**providerIds** | **kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;** | Gets or sets the provider ids. |  [optional]
**productionYear** | **kotlin.Int** | Gets or sets the year. |  [optional]
**indexNumber** | **kotlin.Int** |  |  [optional]
**indexNumberEnd** | **kotlin.Int** |  |  [optional]
**parentIndexNumber** | **kotlin.Int** |  |  [optional]
**premiereDate** | **kotlin.String** |  |  [optional]
**imageUrl** | **kotlin.String** |  |  [optional]
**searchProviderName** | **kotlin.String** |  |  [optional]
**overview** | **kotlin.String** |  |  [optional]
**albumArtist** | [**RemoteSearchResult**](RemoteSearchResult.md) |  |  [optional]
**artists** | [**kotlin.collections.List&lt;RemoteSearchResult&gt;**](RemoteSearchResult.md) |  |  [optional]



