# HlsSegmentApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHlsAudioSegmentLegacyAac**](HlsSegmentApi.md#getHlsAudioSegmentLegacyAac) | **GET** /Audio/{itemId}/hls/{segmentId}/stream.aac | Gets the specified audio segment for an audio item.
[**getHlsAudioSegmentLegacyMp3**](HlsSegmentApi.md#getHlsAudioSegmentLegacyMp3) | **GET** /Audio/{itemId}/hls/{segmentId}/stream.mp3 | Gets the specified audio segment for an audio item.
[**getHlsPlaylistLegacy**](HlsSegmentApi.md#getHlsPlaylistLegacy) | **GET** /Videos/{itemId}/hls/{playlistId}/stream.m3u8 | Gets a hls video playlist.
[**getHlsVideoSegmentLegacy**](HlsSegmentApi.md#getHlsVideoSegmentLegacy) | **GET** /Videos/{itemId}/hls/{playlistId}/{segmentId}.{segmentContainer} | Gets a hls video segment.
[**stopEncodingProcess**](HlsSegmentApi.md#stopEncodingProcess) | **DELETE** /Videos/ActiveEncodings | Stops an active encoding.


<a name="getHlsAudioSegmentLegacyAac"></a>
# **getHlsAudioSegmentLegacyAac**
> org.openapitools.client.infrastructure.OctetByteArray getHlsAudioSegmentLegacyAac(itemId, segmentId)

Gets the specified audio segment for an audio item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = HlsSegmentApi()
val itemId : kotlin.String = itemId_example // kotlin.String | The item id.
val segmentId : kotlin.String = segmentId_example // kotlin.String | The segment id.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getHlsAudioSegmentLegacyAac(itemId, segmentId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling HlsSegmentApi#getHlsAudioSegmentLegacyAac")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling HlsSegmentApi#getHlsAudioSegmentLegacyAac")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **segmentId** | **kotlin.String**| The segment id. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: audio/*

<a name="getHlsAudioSegmentLegacyMp3"></a>
# **getHlsAudioSegmentLegacyMp3**
> org.openapitools.client.infrastructure.OctetByteArray getHlsAudioSegmentLegacyMp3(itemId, segmentId)

Gets the specified audio segment for an audio item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = HlsSegmentApi()
val itemId : kotlin.String = itemId_example // kotlin.String | The item id.
val segmentId : kotlin.String = segmentId_example // kotlin.String | The segment id.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getHlsAudioSegmentLegacyMp3(itemId, segmentId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling HlsSegmentApi#getHlsAudioSegmentLegacyMp3")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling HlsSegmentApi#getHlsAudioSegmentLegacyMp3")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **segmentId** | **kotlin.String**| The segment id. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: audio/*

<a name="getHlsPlaylistLegacy"></a>
# **getHlsPlaylistLegacy**
> org.openapitools.client.infrastructure.OctetByteArray getHlsPlaylistLegacy(itemId, playlistId)

Gets a hls video playlist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = HlsSegmentApi()
val itemId : kotlin.String = itemId_example // kotlin.String | The video id.
val playlistId : kotlin.String = playlistId_example // kotlin.String | The playlist id.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getHlsPlaylistLegacy(itemId, playlistId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling HlsSegmentApi#getHlsPlaylistLegacy")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling HlsSegmentApi#getHlsPlaylistLegacy")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The video id. |
 **playlistId** | **kotlin.String**| The playlist id. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="getHlsVideoSegmentLegacy"></a>
# **getHlsVideoSegmentLegacy**
> org.openapitools.client.infrastructure.OctetByteArray getHlsVideoSegmentLegacy(itemId, playlistId, segmentId, segmentContainer)

Gets a hls video segment.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = HlsSegmentApi()
val itemId : kotlin.String = itemId_example // kotlin.String | The item id.
val playlistId : kotlin.String = playlistId_example // kotlin.String | The playlist id.
val segmentId : kotlin.String = segmentId_example // kotlin.String | The segment id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getHlsVideoSegmentLegacy(itemId, playlistId, segmentId, segmentContainer)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling HlsSegmentApi#getHlsVideoSegmentLegacy")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling HlsSegmentApi#getHlsVideoSegmentLegacy")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **playlistId** | **kotlin.String**| The playlist id. |
 **segmentId** | **kotlin.String**| The segment id. |
 **segmentContainer** | **kotlin.String**| The segment container. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: video/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="stopEncodingProcess"></a>
# **stopEncodingProcess**
> stopEncodingProcess(deviceId, playSessionId)

Stops an active encoding.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = HlsSegmentApi()
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
try {
    apiInstance.stopEncodingProcess(deviceId, playSessionId)
} catch (e: ClientException) {
    println("4xx response calling HlsSegmentApi#stopEncodingProcess")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling HlsSegmentApi#stopEncodingProcess")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. |
 **playSessionId** | **kotlin.String**| The play session id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

