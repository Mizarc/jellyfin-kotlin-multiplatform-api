
# InstallationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guid** | **kotlin.String** | Gets or sets the Id. |  [optional]
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**version** | **kotlin.String** | Gets or sets the version. |  [optional]
**changelog** | **kotlin.String** | Gets or sets the changelog for this version. |  [optional]
**sourceUrl** | **kotlin.String** | Gets or sets the source URL. |  [optional]
**checksum** | **kotlin.String** | Gets or sets a checksum for the binary. |  [optional]
**packageInfo** | [**InstallationInfoPackageInfo**](InstallationInfoPackageInfo.md) |  |  [optional]



