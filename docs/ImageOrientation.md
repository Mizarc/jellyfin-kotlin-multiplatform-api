
# ImageOrientation

## Enum


    * `topLeft` (value: `"TopLeft"`)

    * `topRight` (value: `"TopRight"`)

    * `bottomRight` (value: `"BottomRight"`)

    * `bottomLeft` (value: `"BottomLeft"`)

    * `leftTop` (value: `"LeftTop"`)

    * `rightTop` (value: `"RightTop"`)

    * `rightBottom` (value: `"RightBottom"`)

    * `leftBottom` (value: `"LeftBottom"`)



