
# ParentalRating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**&#x60;value&#x60;** | **kotlin.Int** | Gets or sets the value. |  [optional]



