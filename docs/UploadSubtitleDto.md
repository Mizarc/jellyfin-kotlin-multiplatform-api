
# UploadSubtitleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **kotlin.String** | Gets or sets the subtitle language. | 
**format** | **kotlin.String** | Gets or sets the subtitle format. | 
**isForced** | **kotlin.Boolean** | Gets or sets a value indicating whether the subtitle is forced. | 
**&#x60;data&#x60;** | **kotlin.String** | Gets or sets the subtitle data. | 



