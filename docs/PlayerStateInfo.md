
# PlayerStateInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**positionTicks** | **kotlin.Long** | Gets or sets the now playing position ticks. |  [optional]
**canSeek** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance can seek. |  [optional]
**isPaused** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is paused. |  [optional]
**isMuted** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is muted. |  [optional]
**volumeLevel** | **kotlin.Int** | Gets or sets the volume level. |  [optional]
**audioStreamIndex** | **kotlin.Int** | Gets or sets the index of the now playing audio stream. |  [optional]
**subtitleStreamIndex** | **kotlin.Int** | Gets or sets the index of the now playing subtitle stream. |  [optional]
**mediaSourceId** | **kotlin.String** | Gets or sets the now playing media version identifier. |  [optional]
**playMethod** | [**PlayMethod**](PlayMethod.md) |  |  [optional]
**repeatMode** | [**RepeatMode**](RepeatMode.md) |  |  [optional]
**liveStreamId** | **kotlin.String** | Gets or sets the now playing live stream identifier. |  [optional]



