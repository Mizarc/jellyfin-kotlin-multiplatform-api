
# AddVirtualFolderDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**libraryOptions** | [**AddVirtualFolderDtoLibraryOptions**](AddVirtualFolderDtoLibraryOptions.md) |  |  [optional]



