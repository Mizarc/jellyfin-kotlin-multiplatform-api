# UserViewsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGroupingOptions**](UserViewsApi.md#getGroupingOptions) | **GET** /Users/{userId}/GroupingOptions | Get user view grouping options.
[**getUserViews**](UserViewsApi.md#getUserViews) | **GET** /Users/{userId}/Views | Get user views.


<a name="getGroupingOptions"></a>
# **getGroupingOptions**
> kotlin.collections.List&lt;SpecialViewOptionDto&gt; getGroupingOptions(userId)

Get user view grouping options.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserViewsApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
try {
    val result : kotlin.collections.List<SpecialViewOptionDto> = apiInstance.getGroupingOptions(userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserViewsApi#getGroupingOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserViewsApi#getGroupingOptions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |

### Return type

[**kotlin.collections.List&lt;SpecialViewOptionDto&gt;**](SpecialViewOptionDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getUserViews"></a>
# **getUserViews**
> BaseItemDtoQueryResult getUserViews(userId, includeExternalContent, presetViews, includeHidden)

Get user views.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UserViewsApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val includeExternalContent : kotlin.Boolean = true // kotlin.Boolean | Whether or not to include external views such as channels or live tv.
val presetViews : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Preset views.
val includeHidden : kotlin.Boolean = true // kotlin.Boolean | Whether or not to include hidden content.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getUserViews(userId, includeExternalContent, presetViews, includeHidden)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserViewsApi#getUserViews")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserViewsApi#getUserViews")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User id. |
 **includeExternalContent** | **kotlin.Boolean**| Whether or not to include external views such as channels or live tv. | [optional]
 **presetViews** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Preset views. | [optional]
 **includeHidden** | **kotlin.Boolean**| Whether or not to include hidden content. | [optional] [default to false]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

