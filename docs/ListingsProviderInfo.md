
# ListingsProviderInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** |  |  [optional]
**type** | **kotlin.String** |  |  [optional]
**username** | **kotlin.String** |  |  [optional]
**password** | **kotlin.String** |  |  [optional]
**listingsId** | **kotlin.String** |  |  [optional]
**zipCode** | **kotlin.String** |  |  [optional]
**country** | **kotlin.String** |  |  [optional]
**path** | **kotlin.String** |  |  [optional]
**enabledTuners** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**enableAllTuners** | **kotlin.Boolean** |  |  [optional]
**newsCategories** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**sportsCategories** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**kidsCategories** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**movieCategories** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**channelMappings** | [**kotlin.collections.List&lt;NameValuePair&gt;**](NameValuePair.md) |  |  [optional]
**moviePrefix** | **kotlin.String** |  |  [optional]
**preferredLanguage** | **kotlin.String** |  |  [optional]
**userAgent** | **kotlin.String** |  |  [optional]



