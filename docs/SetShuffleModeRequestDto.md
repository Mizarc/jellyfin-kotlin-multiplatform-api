
# SetShuffleModeRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mode** | [**GroupShuffleMode**](GroupShuffleMode.md) |  |  [optional]



