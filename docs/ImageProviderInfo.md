
# ImageProviderInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets the name. |  [optional]
**supportedImages** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md) | Gets the supported image types. |  [optional]



