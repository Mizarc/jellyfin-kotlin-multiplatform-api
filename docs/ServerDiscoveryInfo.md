
# ServerDiscoveryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **kotlin.String** | Gets the address. |  [optional]
**id** | **kotlin.String** | Gets the server identifier. |  [optional]
**name** | **kotlin.String** | Gets the name. |  [optional]
**endpointAddress** | **kotlin.String** | Gets the endpoint address. |  [optional]



