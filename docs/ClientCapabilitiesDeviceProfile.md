
# ClientCapabilitiesDeviceProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name of this device profile. |  [optional]
**id** | **kotlin.String** | Gets or sets the Id. |  [optional]
**identification** | [**DeviceProfileIdentification**](DeviceProfileIdentification.md) |  |  [optional]
**friendlyName** | **kotlin.String** | Gets or sets the friendly name of the device profile, which can be shown to users. |  [optional]
**manufacturer** | **kotlin.String** | Gets or sets the manufacturer of the device which this profile represents. |  [optional]
**manufacturerUrl** | **kotlin.String** | Gets or sets an url for the manufacturer of the device which this profile represents. |  [optional]
**modelName** | **kotlin.String** | Gets or sets the model name of the device which this profile represents. |  [optional]
**modelDescription** | **kotlin.String** | Gets or sets the model description of the device which this profile represents. |  [optional]
**modelNumber** | **kotlin.String** | Gets or sets the model number of the device which this profile represents. |  [optional]
**modelUrl** | **kotlin.String** | Gets or sets the ModelUrl. |  [optional]
**serialNumber** | **kotlin.String** | Gets or sets the serial number of the device which this profile represents. |  [optional]
**enableAlbumArtInDidl** | **kotlin.Boolean** | Gets or sets a value indicating whether EnableAlbumArtInDidl. |  [optional]
**enableSingleAlbumArtLimit** | **kotlin.Boolean** | Gets or sets a value indicating whether EnableSingleAlbumArtLimit. |  [optional]
**enableSingleSubtitleLimit** | **kotlin.Boolean** | Gets or sets a value indicating whether EnableSingleSubtitleLimit. |  [optional]
**supportedMediaTypes** | **kotlin.String** | Gets or sets the SupportedMediaTypes. |  [optional]
**userId** | **kotlin.String** | Gets or sets the UserId. |  [optional]
**albumArtPn** | **kotlin.String** | Gets or sets the AlbumArtPn. |  [optional]
**maxAlbumArtWidth** | **kotlin.Int** | Gets or sets the MaxAlbumArtWidth. |  [optional]
**maxAlbumArtHeight** | **kotlin.Int** | Gets or sets the MaxAlbumArtHeight. |  [optional]
**maxIconWidth** | **kotlin.Int** | Gets or sets the maximum allowed width of embedded icons. |  [optional]
**maxIconHeight** | **kotlin.Int** | Gets or sets the maximum allowed height of embedded icons. |  [optional]
**maxStreamingBitrate** | **kotlin.Int** | Gets or sets the maximum allowed bitrate for all streamed content. |  [optional]
**maxStaticBitrate** | **kotlin.Int** | Gets or sets the maximum allowed bitrate for statically streamed content (&#x3D; direct played files). |  [optional]
**musicStreamingTranscodingBitrate** | **kotlin.Int** | Gets or sets the maximum allowed bitrate for transcoded music streams. |  [optional]
**maxStaticMusicBitrate** | **kotlin.Int** | Gets or sets the maximum allowed bitrate for statically streamed (&#x3D; direct played) music files. |  [optional]
**sonyAggregationFlags** | **kotlin.String** | Gets or sets the content of the aggregationFlags element in the urn:schemas-sonycom:av namespace. |  [optional]
**protocolInfo** | **kotlin.String** | Gets or sets the ProtocolInfo. |  [optional]
**timelineOffsetSeconds** | **kotlin.Int** | Gets or sets the TimelineOffsetSeconds. |  [optional]
**requiresPlainVideoItems** | **kotlin.Boolean** | Gets or sets a value indicating whether RequiresPlainVideoItems. |  [optional]
**requiresPlainFolders** | **kotlin.Boolean** | Gets or sets a value indicating whether RequiresPlainFolders. |  [optional]
**enableMSMediaReceiverRegistrar** | **kotlin.Boolean** | Gets or sets a value indicating whether EnableMSMediaReceiverRegistrar. |  [optional]
**ignoreTranscodeByteRangeRequests** | **kotlin.Boolean** | Gets or sets a value indicating whether IgnoreTranscodeByteRangeRequests. |  [optional]
**xmlRootAttributes** | [**kotlin.collections.List&lt;XmlAttribute&gt;**](XmlAttribute.md) | Gets or sets the XmlRootAttributes. |  [optional]
**directPlayProfiles** | [**kotlin.collections.List&lt;DirectPlayProfile&gt;**](DirectPlayProfile.md) | Gets or sets the direct play profiles. |  [optional]
**transcodingProfiles** | [**kotlin.collections.List&lt;TranscodingProfile&gt;**](TranscodingProfile.md) | Gets or sets the transcoding profiles. |  [optional]
**containerProfiles** | [**kotlin.collections.List&lt;ContainerProfile&gt;**](ContainerProfile.md) | Gets or sets the container profiles. |  [optional]
**codecProfiles** | [**kotlin.collections.List&lt;CodecProfile&gt;**](CodecProfile.md) | Gets or sets the codec profiles. |  [optional]
**responseProfiles** | [**kotlin.collections.List&lt;ResponseProfile&gt;**](ResponseProfile.md) | Gets or sets the ResponseProfiles. |  [optional]
**subtitleProfiles** | [**kotlin.collections.List&lt;SubtitleProfile&gt;**](SubtitleProfile.md) | Gets or sets the subtitle profiles. |  [optional]



