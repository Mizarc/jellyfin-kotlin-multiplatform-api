
# DeviceInfoQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**kotlin.collections.List&lt;DeviceInfo&gt;**](DeviceInfo.md) | Gets or sets the items. |  [optional]
**totalRecordCount** | **kotlin.Int** | Gets or sets the total number of records available. |  [optional]
**startIndex** | **kotlin.Int** | Gets or sets the index of the first record in Items. |  [optional]



