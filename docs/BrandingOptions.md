
# BrandingOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loginDisclaimer** | **kotlin.String** | Gets or sets the login disclaimer. |  [optional]
**customCss** | **kotlin.String** | Gets or sets the custom CSS. |  [optional]
**splashscreenEnabled** | **kotlin.Boolean** | Gets or sets a value indicating whether to enable the splashscreen. |  [optional]



