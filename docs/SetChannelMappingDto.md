
# SetChannelMappingDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**providerId** | **kotlin.String** | Gets or sets the provider id. | 
**tunerChannelId** | **kotlin.String** | Gets or sets the tuner channel id. | 
**providerChannelId** | **kotlin.String** | Gets or sets the provider channel id. | 



