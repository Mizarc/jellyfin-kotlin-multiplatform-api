
# TranscodingInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audioCodec** | **kotlin.String** |  |  [optional]
**videoCodec** | **kotlin.String** |  |  [optional]
**container** | **kotlin.String** |  |  [optional]
**isVideoDirect** | **kotlin.Boolean** |  |  [optional]
**isAudioDirect** | **kotlin.Boolean** |  |  [optional]
**bitrate** | **kotlin.Int** |  |  [optional]
**framerate** | **kotlin.Float** |  |  [optional]
**completionPercentage** | **kotlin.Double** |  |  [optional]
**width** | **kotlin.Int** |  |  [optional]
**height** | **kotlin.Int** |  |  [optional]
**audioChannels** | **kotlin.Int** |  |  [optional]
**hardwareAccelerationType** | [**HardwareEncodingType**](HardwareEncodingType.md) |  |  [optional]
**transcodeReasons** | [**kotlin.collections.List&lt;TranscodeReason&gt;**](TranscodeReason.md) |  |  [optional]



