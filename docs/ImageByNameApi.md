# ImageByNameApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGeneralImage**](ImageByNameApi.md#getGeneralImage) | **GET** /Images/General/{name}/{type} | Get General Image.
[**getGeneralImages**](ImageByNameApi.md#getGeneralImages) | **GET** /Images/General | Get all general images.
[**getMediaInfoImage**](ImageByNameApi.md#getMediaInfoImage) | **GET** /Images/MediaInfo/{theme}/{name} | Get media info image.
[**getMediaInfoImages**](ImageByNameApi.md#getMediaInfoImages) | **GET** /Images/MediaInfo | Get all media info images.
[**getRatingImage**](ImageByNameApi.md#getRatingImage) | **GET** /Images/Ratings/{theme}/{name} | Get rating image.
[**getRatingImages**](ImageByNameApi.md#getRatingImages) | **GET** /Images/Ratings | Get all general images.


<a name="getGeneralImage"></a>
# **getGeneralImage**
> org.openapitools.client.infrastructure.OctetByteArray getGeneralImage(name, type)

Get General Image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageByNameApi()
val name : kotlin.String = name_example // kotlin.String | The name of the image.
val type : kotlin.String = type_example // kotlin.String | Image Type (primary, backdrop, logo, etc).
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getGeneralImage(name, type)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageByNameApi#getGeneralImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageByNameApi#getGeneralImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the image. |
 **type** | **kotlin.String**| Image Type (primary, backdrop, logo, etc). |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/octet-stream, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getGeneralImages"></a>
# **getGeneralImages**
> kotlin.collections.List&lt;ImageByNameInfo&gt; getGeneralImages()

Get all general images.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageByNameApi()
try {
    val result : kotlin.collections.List<ImageByNameInfo> = apiInstance.getGeneralImages()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageByNameApi#getGeneralImages")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageByNameApi#getGeneralImages")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;ImageByNameInfo&gt;**](ImageByNameInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMediaInfoImage"></a>
# **getMediaInfoImage**
> org.openapitools.client.infrastructure.OctetByteArray getMediaInfoImage(theme, name)

Get media info image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageByNameApi()
val theme : kotlin.String = theme_example // kotlin.String | The theme to get the image from.
val name : kotlin.String = name_example // kotlin.String | The name of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMediaInfoImage(theme, name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageByNameApi#getMediaInfoImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageByNameApi#getMediaInfoImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **theme** | **kotlin.String**| The theme to get the image from. |
 **name** | **kotlin.String**| The name of the image. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/octet-stream, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMediaInfoImages"></a>
# **getMediaInfoImages**
> kotlin.collections.List&lt;ImageByNameInfo&gt; getMediaInfoImages()

Get all media info images.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageByNameApi()
try {
    val result : kotlin.collections.List<ImageByNameInfo> = apiInstance.getMediaInfoImages()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageByNameApi#getMediaInfoImages")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageByNameApi#getMediaInfoImages")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;ImageByNameInfo&gt;**](ImageByNameInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRatingImage"></a>
# **getRatingImage**
> org.openapitools.client.infrastructure.OctetByteArray getRatingImage(theme, name)

Get rating image.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageByNameApi()
val theme : kotlin.String = theme_example // kotlin.String | The theme to get the image from.
val name : kotlin.String = name_example // kotlin.String | The name of the image.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getRatingImage(theme, name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageByNameApi#getRatingImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageByNameApi#getRatingImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **theme** | **kotlin.String**| The theme to get the image from. |
 **name** | **kotlin.String**| The name of the image. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/octet-stream, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRatingImages"></a>
# **getRatingImages**
> kotlin.collections.List&lt;ImageByNameInfo&gt; getRatingImages()

Get all general images.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ImageByNameApi()
try {
    val result : kotlin.collections.List<ImageByNameInfo> = apiInstance.getRatingImages()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ImageByNameApi#getRatingImages")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ImageByNameApi#getRatingImages")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;ImageByNameInfo&gt;**](ImageByNameInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

