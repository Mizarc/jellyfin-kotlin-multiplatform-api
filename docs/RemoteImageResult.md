
# RemoteImageResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**images** | [**kotlin.collections.List&lt;RemoteImageInfo&gt;**](RemoteImageInfo.md) | Gets or sets the images. |  [optional]
**totalRecordCount** | **kotlin.Int** | Gets or sets the total record count. |  [optional]
**providers** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the providers. |  [optional]



