
# PlaybackInfoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediaSources** | [**kotlin.collections.List&lt;MediaSourceInfo&gt;**](MediaSourceInfo.md) | Gets or sets the media sources. |  [optional]
**playSessionId** | **kotlin.String** | Gets or sets the play session identifier. |  [optional]
**errorCode** | [**PlaybackErrorCode**](PlaybackErrorCode.md) |  |  [optional]



