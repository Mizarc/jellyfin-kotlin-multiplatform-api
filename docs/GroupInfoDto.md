
# GroupInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **kotlin.String** | Gets the group identifier. |  [optional]
**groupName** | **kotlin.String** | Gets the group name. |  [optional]
**state** | [**GroupStateType**](GroupStateType.md) |  |  [optional]
**participants** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets the participants. |  [optional]
**lastUpdatedAt** | **kotlin.String** | Gets the date when this DTO has been created. |  [optional]



