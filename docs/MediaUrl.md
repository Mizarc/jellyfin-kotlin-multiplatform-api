
# MediaUrl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **kotlin.String** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]



