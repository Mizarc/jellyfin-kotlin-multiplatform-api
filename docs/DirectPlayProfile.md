
# DirectPlayProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container** | **kotlin.String** |  |  [optional]
**audioCodec** | **kotlin.String** |  |  [optional]
**videoCodec** | **kotlin.String** |  |  [optional]
**type** | [**DlnaProfileType**](DlnaProfileType.md) |  |  [optional]



