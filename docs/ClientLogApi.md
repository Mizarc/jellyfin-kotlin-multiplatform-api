# ClientLogApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**logFile**](ClientLogApi.md#logFile) | **POST** /ClientLog/Document | Upload a document.


<a name="logFile"></a>
# **logFile**
> ClientLogDocumentResponseDto logFile(body)

Upload a document.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ClientLogApi()
val body : org.openapitools.client.infrastructure.OctetByteArray = BINARY_DATA_HERE // org.openapitools.client.infrastructure.OctetByteArray | 
try {
    val result : ClientLogDocumentResponseDto = apiInstance.logFile(body)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ClientLogApi#logFile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ClientLogApi#logFile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **org.openapitools.client.infrastructure.OctetByteArray**|  | [optional]

### Return type

[**ClientLogDocumentResponseDto**](ClientLogDocumentResponseDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

