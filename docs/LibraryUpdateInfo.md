
# LibraryUpdateInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**foldersAddedTo** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the folders added to. |  [optional]
**foldersRemovedFrom** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the folders removed from. |  [optional]
**itemsAdded** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the items added. |  [optional]
**itemsRemoved** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the items removed. |  [optional]
**itemsUpdated** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the items updated. |  [optional]
**collectionFolders** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**isEmpty** | **kotlin.Boolean** |  |  [optional] [readonly]



