# ScheduledTasksApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTask**](ScheduledTasksApi.md#getTask) | **GET** /ScheduledTasks/{taskId} | Get task by id.
[**getTasks**](ScheduledTasksApi.md#getTasks) | **GET** /ScheduledTasks | Get tasks.
[**startTask**](ScheduledTasksApi.md#startTask) | **POST** /ScheduledTasks/Running/{taskId} | Start specified task.
[**stopTask**](ScheduledTasksApi.md#stopTask) | **DELETE** /ScheduledTasks/Running/{taskId} | Stop specified task.
[**updateTask**](ScheduledTasksApi.md#updateTask) | **POST** /ScheduledTasks/{taskId}/Triggers | Update specified task triggers.


<a name="getTask"></a>
# **getTask**
> TaskInfo getTask(taskId)

Get task by id.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ScheduledTasksApi()
val taskId : kotlin.String = taskId_example // kotlin.String | Task Id.
try {
    val result : TaskInfo = apiInstance.getTask(taskId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ScheduledTasksApi#getTask")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ScheduledTasksApi#getTask")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskId** | **kotlin.String**| Task Id. |

### Return type

[**TaskInfo**](TaskInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getTasks"></a>
# **getTasks**
> kotlin.collections.List&lt;TaskInfo&gt; getTasks(isHidden, isEnabled)

Get tasks.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ScheduledTasksApi()
val isHidden : kotlin.Boolean = true // kotlin.Boolean | Optional filter tasks that are hidden, or not.
val isEnabled : kotlin.Boolean = true // kotlin.Boolean | Optional filter tasks that are enabled, or not.
try {
    val result : kotlin.collections.List<TaskInfo> = apiInstance.getTasks(isHidden, isEnabled)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ScheduledTasksApi#getTasks")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ScheduledTasksApi#getTasks")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isHidden** | **kotlin.Boolean**| Optional filter tasks that are hidden, or not. | [optional]
 **isEnabled** | **kotlin.Boolean**| Optional filter tasks that are enabled, or not. | [optional]

### Return type

[**kotlin.collections.List&lt;TaskInfo&gt;**](TaskInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="startTask"></a>
# **startTask**
> startTask(taskId)

Start specified task.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ScheduledTasksApi()
val taskId : kotlin.String = taskId_example // kotlin.String | Task Id.
try {
    apiInstance.startTask(taskId)
} catch (e: ClientException) {
    println("4xx response calling ScheduledTasksApi#startTask")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ScheduledTasksApi#startTask")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskId** | **kotlin.String**| Task Id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="stopTask"></a>
# **stopTask**
> stopTask(taskId)

Stop specified task.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ScheduledTasksApi()
val taskId : kotlin.String = taskId_example // kotlin.String | Task Id.
try {
    apiInstance.stopTask(taskId)
} catch (e: ClientException) {
    println("4xx response calling ScheduledTasksApi#stopTask")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ScheduledTasksApi#stopTask")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskId** | **kotlin.String**| Task Id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateTask"></a>
# **updateTask**
> updateTask(taskId, taskTriggerInfo)

Update specified task triggers.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ScheduledTasksApi()
val taskId : kotlin.String = taskId_example // kotlin.String | Task Id.
val taskTriggerInfo : kotlin.collections.List<TaskTriggerInfo> =  // kotlin.collections.List<TaskTriggerInfo> | Triggers.
try {
    apiInstance.updateTask(taskId, taskTriggerInfo)
} catch (e: ClientException) {
    println("4xx response calling ScheduledTasksApi#updateTask")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ScheduledTasksApi#updateTask")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskId** | **kotlin.String**| Task Id. |
 **taskTriggerInfo** | [**kotlin.collections.List&lt;TaskTriggerInfo&gt;**](TaskTriggerInfo.md)| Triggers. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

