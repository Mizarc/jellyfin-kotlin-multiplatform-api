
# ClientCapabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playableMediaTypes** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**supportedCommands** | [**kotlin.collections.List&lt;GeneralCommandType&gt;**](GeneralCommandType.md) |  |  [optional]
**supportsMediaControl** | **kotlin.Boolean** |  |  [optional]
**supportsContentUploading** | **kotlin.Boolean** |  |  [optional]
**messageCallbackUrl** | **kotlin.String** |  |  [optional]
**supportsPersistentIdentifier** | **kotlin.Boolean** |  |  [optional]
**supportsSync** | **kotlin.Boolean** |  |  [optional]
**deviceProfile** | [**ClientCapabilitiesDeviceProfile**](ClientCapabilitiesDeviceProfile.md) |  |  [optional]
**appStoreUrl** | **kotlin.String** |  |  [optional]
**iconUrl** | **kotlin.String** |  |  [optional]



