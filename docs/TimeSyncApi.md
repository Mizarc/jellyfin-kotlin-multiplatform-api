# TimeSyncApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUtcTime**](TimeSyncApi.md#getUtcTime) | **GET** /GetUtcTime | Gets the current UTC time.


<a name="getUtcTime"></a>
# **getUtcTime**
> UtcTimeResponse getUtcTime()

Gets the current UTC time.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = TimeSyncApi()
try {
    val result : UtcTimeResponse = apiInstance.getUtcTime()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling TimeSyncApi#getUtcTime")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling TimeSyncApi#getUtcTime")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UtcTimeResponse**](UtcTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

