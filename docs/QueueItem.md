
# QueueItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** |  |  [optional]
**playlistItemId** | **kotlin.String** |  |  [optional]



