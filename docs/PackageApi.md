# PackageApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelPackageInstallation**](PackageApi.md#cancelPackageInstallation) | **DELETE** /Packages/Installing/{packageId} | Cancels a package installation.
[**getPackageInfo**](PackageApi.md#getPackageInfo) | **GET** /Packages/{name} | Gets a package by name or assembly GUID.
[**getPackages**](PackageApi.md#getPackages) | **GET** /Packages | Gets available packages.
[**getRepositories**](PackageApi.md#getRepositories) | **GET** /Repositories | Gets all package repositories.
[**installPackage**](PackageApi.md#installPackage) | **POST** /Packages/Installed/{name} | Installs a package.
[**setRepositories**](PackageApi.md#setRepositories) | **POST** /Repositories | Sets the enabled and existing package repositories.


<a name="cancelPackageInstallation"></a>
# **cancelPackageInstallation**
> cancelPackageInstallation(packageId)

Cancels a package installation.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PackageApi()
val packageId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Installation Id.
try {
    apiInstance.cancelPackageInstallation(packageId)
} catch (e: ClientException) {
    println("4xx response calling PackageApi#cancelPackageInstallation")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PackageApi#cancelPackageInstallation")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **packageId** | **kotlin.String**| Installation Id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPackageInfo"></a>
# **getPackageInfo**
> PackageInfo getPackageInfo(name, assemblyGuid)

Gets a package by name or assembly GUID.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PackageApi()
val name : kotlin.String = name_example // kotlin.String | The name of the package.
val assemblyGuid : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The GUID of the associated assembly.
try {
    val result : PackageInfo = apiInstance.getPackageInfo(name, assemblyGuid)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PackageApi#getPackageInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PackageApi#getPackageInfo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the package. |
 **assemblyGuid** | **kotlin.String**| The GUID of the associated assembly. | [optional]

### Return type

[**PackageInfo**](PackageInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPackages"></a>
# **getPackages**
> kotlin.collections.List&lt;PackageInfo&gt; getPackages()

Gets available packages.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PackageApi()
try {
    val result : kotlin.collections.List<PackageInfo> = apiInstance.getPackages()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PackageApi#getPackages")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PackageApi#getPackages")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;PackageInfo&gt;**](PackageInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRepositories"></a>
# **getRepositories**
> kotlin.collections.List&lt;RepositoryInfo&gt; getRepositories()

Gets all package repositories.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PackageApi()
try {
    val result : kotlin.collections.List<RepositoryInfo> = apiInstance.getRepositories()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PackageApi#getRepositories")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PackageApi#getRepositories")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;RepositoryInfo&gt;**](RepositoryInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="installPackage"></a>
# **installPackage**
> installPackage(name, assemblyGuid, version, repositoryUrl)

Installs a package.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PackageApi()
val name : kotlin.String = name_example // kotlin.String | Package name.
val assemblyGuid : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | GUID of the associated assembly.
val version : kotlin.String = version_example // kotlin.String | Optional version. Defaults to latest version.
val repositoryUrl : kotlin.String = repositoryUrl_example // kotlin.String | Optional. Specify the repository to install from.
try {
    apiInstance.installPackage(name, assemblyGuid, version, repositoryUrl)
} catch (e: ClientException) {
    println("4xx response calling PackageApi#installPackage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PackageApi#installPackage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Package name. |
 **assemblyGuid** | **kotlin.String**| GUID of the associated assembly. | [optional]
 **version** | **kotlin.String**| Optional version. Defaults to latest version. | [optional]
 **repositoryUrl** | **kotlin.String**| Optional. Specify the repository to install from. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="setRepositories"></a>
# **setRepositories**
> setRepositories(repositoryInfo)

Sets the enabled and existing package repositories.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PackageApi()
val repositoryInfo : kotlin.collections.List<RepositoryInfo> =  // kotlin.collections.List<RepositoryInfo> | The list of package repositories.
try {
    apiInstance.setRepositories(repositoryInfo)
} catch (e: ClientException) {
    println("4xx response calling PackageApi#setRepositories")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PackageApi#setRepositories")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repositoryInfo** | [**kotlin.collections.List&lt;RepositoryInfo&gt;**](RepositoryInfo.md)| The list of package repositories. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

