
# ResponseProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container** | **kotlin.String** |  |  [optional]
**audioCodec** | **kotlin.String** |  |  [optional]
**videoCodec** | **kotlin.String** |  |  [optional]
**type** | [**DlnaProfileType**](DlnaProfileType.md) |  |  [optional]
**orgPn** | **kotlin.String** |  |  [optional]
**mimeType** | **kotlin.String** |  |  [optional]
**conditions** | [**kotlin.collections.List&lt;ProfileCondition&gt;**](ProfileCondition.md) |  |  [optional]



