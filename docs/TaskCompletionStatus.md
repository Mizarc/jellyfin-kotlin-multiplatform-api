
# TaskCompletionStatus

## Enum


    * `completed` (value: `"Completed"`)

    * `failed` (value: `"Failed"`)

    * `cancelled` (value: `"Cancelled"`)

    * `aborted` (value: `"Aborted"`)



