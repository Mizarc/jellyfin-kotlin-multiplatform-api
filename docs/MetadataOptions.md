
# MetadataOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemType** | **kotlin.String** |  |  [optional]
**disabledMetadataSavers** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**localMetadataReaderOrder** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**disabledMetadataFetchers** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**metadataFetcherOrder** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**disabledImageFetchers** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**imageFetcherOrder** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



