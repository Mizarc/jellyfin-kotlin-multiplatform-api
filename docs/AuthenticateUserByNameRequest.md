
# AuthenticateUserByNameRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **kotlin.String** | Gets or sets the username. |  [optional]
**pw** | **kotlin.String** | Gets or sets the plain text password. |  [optional]
**password** | **kotlin.String** | Gets or sets the sha1-hashed password. |  [optional]



