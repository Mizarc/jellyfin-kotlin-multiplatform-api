
# RecommendationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**kotlin.collections.List&lt;BaseItemDto&gt;**](BaseItemDto.md) |  |  [optional]
**recommendationType** | [**RecommendationType**](RecommendationType.md) |  |  [optional]
**baselineItemName** | **kotlin.String** |  |  [optional]
**categoryId** | **kotlin.String** |  |  [optional]



