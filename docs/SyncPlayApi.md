# SyncPlayApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**syncPlayBuffering**](SyncPlayApi.md#syncPlayBuffering) | **POST** /SyncPlay/Buffering | Notify SyncPlay group that member is buffering.
[**syncPlayCreateGroup**](SyncPlayApi.md#syncPlayCreateGroup) | **POST** /SyncPlay/New | Create a new SyncPlay group.
[**syncPlayGetGroups**](SyncPlayApi.md#syncPlayGetGroups) | **GET** /SyncPlay/List | Gets all SyncPlay groups.
[**syncPlayJoinGroup**](SyncPlayApi.md#syncPlayJoinGroup) | **POST** /SyncPlay/Join | Join an existing SyncPlay group.
[**syncPlayLeaveGroup**](SyncPlayApi.md#syncPlayLeaveGroup) | **POST** /SyncPlay/Leave | Leave the joined SyncPlay group.
[**syncPlayMovePlaylistItem**](SyncPlayApi.md#syncPlayMovePlaylistItem) | **POST** /SyncPlay/MovePlaylistItem | Request to move an item in the playlist in SyncPlay group.
[**syncPlayNextItem**](SyncPlayApi.md#syncPlayNextItem) | **POST** /SyncPlay/NextItem | Request next item in SyncPlay group.
[**syncPlayPause**](SyncPlayApi.md#syncPlayPause) | **POST** /SyncPlay/Pause | Request pause in SyncPlay group.
[**syncPlayPing**](SyncPlayApi.md#syncPlayPing) | **POST** /SyncPlay/Ping | Update session ping.
[**syncPlayPreviousItem**](SyncPlayApi.md#syncPlayPreviousItem) | **POST** /SyncPlay/PreviousItem | Request previous item in SyncPlay group.
[**syncPlayQueue**](SyncPlayApi.md#syncPlayQueue) | **POST** /SyncPlay/Queue | Request to queue items to the playlist of a SyncPlay group.
[**syncPlayReady**](SyncPlayApi.md#syncPlayReady) | **POST** /SyncPlay/Ready | Notify SyncPlay group that member is ready for playback.
[**syncPlayRemoveFromPlaylist**](SyncPlayApi.md#syncPlayRemoveFromPlaylist) | **POST** /SyncPlay/RemoveFromPlaylist | Request to remove items from the playlist in SyncPlay group.
[**syncPlaySeek**](SyncPlayApi.md#syncPlaySeek) | **POST** /SyncPlay/Seek | Request seek in SyncPlay group.
[**syncPlaySetIgnoreWait**](SyncPlayApi.md#syncPlaySetIgnoreWait) | **POST** /SyncPlay/SetIgnoreWait | Request SyncPlay group to ignore member during group-wait.
[**syncPlaySetNewQueue**](SyncPlayApi.md#syncPlaySetNewQueue) | **POST** /SyncPlay/SetNewQueue | Request to set new playlist in SyncPlay group.
[**syncPlaySetPlaylistItem**](SyncPlayApi.md#syncPlaySetPlaylistItem) | **POST** /SyncPlay/SetPlaylistItem | Request to change playlist item in SyncPlay group.
[**syncPlaySetRepeatMode**](SyncPlayApi.md#syncPlaySetRepeatMode) | **POST** /SyncPlay/SetRepeatMode | Request to set repeat mode in SyncPlay group.
[**syncPlaySetShuffleMode**](SyncPlayApi.md#syncPlaySetShuffleMode) | **POST** /SyncPlay/SetShuffleMode | Request to set shuffle mode in SyncPlay group.
[**syncPlayStop**](SyncPlayApi.md#syncPlayStop) | **POST** /SyncPlay/Stop | Request stop in SyncPlay group.
[**syncPlayUnpause**](SyncPlayApi.md#syncPlayUnpause) | **POST** /SyncPlay/Unpause | Request unpause in SyncPlay group.


<a name="syncPlayBuffering"></a>
# **syncPlayBuffering**
> syncPlayBuffering(syncPlayBufferingRequest)

Notify SyncPlay group that member is buffering.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayBufferingRequest : SyncPlayBufferingRequest =  // SyncPlayBufferingRequest | The player status.
try {
    apiInstance.syncPlayBuffering(syncPlayBufferingRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayBuffering")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayBuffering")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayBufferingRequest** | [**SyncPlayBufferingRequest**](SyncPlayBufferingRequest.md)| The player status. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayCreateGroup"></a>
# **syncPlayCreateGroup**
> syncPlayCreateGroup(syncPlayCreateGroupRequest)

Create a new SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayCreateGroupRequest : SyncPlayCreateGroupRequest =  // SyncPlayCreateGroupRequest | The settings of the new group.
try {
    apiInstance.syncPlayCreateGroup(syncPlayCreateGroupRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayCreateGroup")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayCreateGroup")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayCreateGroupRequest** | [**SyncPlayCreateGroupRequest**](SyncPlayCreateGroupRequest.md)| The settings of the new group. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayGetGroups"></a>
# **syncPlayGetGroups**
> kotlin.collections.List&lt;GroupInfoDto&gt; syncPlayGetGroups()

Gets all SyncPlay groups.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
try {
    val result : kotlin.collections.List<GroupInfoDto> = apiInstance.syncPlayGetGroups()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayGetGroups")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayGetGroups")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;GroupInfoDto&gt;**](GroupInfoDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="syncPlayJoinGroup"></a>
# **syncPlayJoinGroup**
> syncPlayJoinGroup(syncPlayJoinGroupRequest)

Join an existing SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayJoinGroupRequest : SyncPlayJoinGroupRequest =  // SyncPlayJoinGroupRequest | The group to join.
try {
    apiInstance.syncPlayJoinGroup(syncPlayJoinGroupRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayJoinGroup")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayJoinGroup")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayJoinGroupRequest** | [**SyncPlayJoinGroupRequest**](SyncPlayJoinGroupRequest.md)| The group to join. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayLeaveGroup"></a>
# **syncPlayLeaveGroup**
> syncPlayLeaveGroup()

Leave the joined SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
try {
    apiInstance.syncPlayLeaveGroup()
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayLeaveGroup")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayLeaveGroup")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="syncPlayMovePlaylistItem"></a>
# **syncPlayMovePlaylistItem**
> syncPlayMovePlaylistItem(syncPlayMovePlaylistItemRequest)

Request to move an item in the playlist in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayMovePlaylistItemRequest : SyncPlayMovePlaylistItemRequest =  // SyncPlayMovePlaylistItemRequest | The new position for the item.
try {
    apiInstance.syncPlayMovePlaylistItem(syncPlayMovePlaylistItemRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayMovePlaylistItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayMovePlaylistItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayMovePlaylistItemRequest** | [**SyncPlayMovePlaylistItemRequest**](SyncPlayMovePlaylistItemRequest.md)| The new position for the item. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayNextItem"></a>
# **syncPlayNextItem**
> syncPlayNextItem(syncPlayNextItemRequest)

Request next item in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayNextItemRequest : SyncPlayNextItemRequest =  // SyncPlayNextItemRequest | The current item information.
try {
    apiInstance.syncPlayNextItem(syncPlayNextItemRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayNextItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayNextItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayNextItemRequest** | [**SyncPlayNextItemRequest**](SyncPlayNextItemRequest.md)| The current item information. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayPause"></a>
# **syncPlayPause**
> syncPlayPause()

Request pause in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
try {
    apiInstance.syncPlayPause()
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayPause")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayPause")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="syncPlayPing"></a>
# **syncPlayPing**
> syncPlayPing(syncPlayPingRequest)

Update session ping.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayPingRequest : SyncPlayPingRequest =  // SyncPlayPingRequest | The new ping.
try {
    apiInstance.syncPlayPing(syncPlayPingRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayPing")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayPing")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayPingRequest** | [**SyncPlayPingRequest**](SyncPlayPingRequest.md)| The new ping. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayPreviousItem"></a>
# **syncPlayPreviousItem**
> syncPlayPreviousItem(syncPlayPreviousItemRequest)

Request previous item in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayPreviousItemRequest : SyncPlayPreviousItemRequest =  // SyncPlayPreviousItemRequest | The current item information.
try {
    apiInstance.syncPlayPreviousItem(syncPlayPreviousItemRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayPreviousItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayPreviousItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayPreviousItemRequest** | [**SyncPlayPreviousItemRequest**](SyncPlayPreviousItemRequest.md)| The current item information. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayQueue"></a>
# **syncPlayQueue**
> syncPlayQueue(syncPlayQueueRequest)

Request to queue items to the playlist of a SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayQueueRequest : SyncPlayQueueRequest =  // SyncPlayQueueRequest | The items to add.
try {
    apiInstance.syncPlayQueue(syncPlayQueueRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayQueue")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayQueue")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayQueueRequest** | [**SyncPlayQueueRequest**](SyncPlayQueueRequest.md)| The items to add. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayReady"></a>
# **syncPlayReady**
> syncPlayReady(syncPlayReadyRequest)

Notify SyncPlay group that member is ready for playback.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayReadyRequest : SyncPlayReadyRequest =  // SyncPlayReadyRequest | The player status.
try {
    apiInstance.syncPlayReady(syncPlayReadyRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayReady")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayReady")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayReadyRequest** | [**SyncPlayReadyRequest**](SyncPlayReadyRequest.md)| The player status. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayRemoveFromPlaylist"></a>
# **syncPlayRemoveFromPlaylist**
> syncPlayRemoveFromPlaylist(syncPlayRemoveFromPlaylistRequest)

Request to remove items from the playlist in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlayRemoveFromPlaylistRequest : SyncPlayRemoveFromPlaylistRequest =  // SyncPlayRemoveFromPlaylistRequest | The items to remove.
try {
    apiInstance.syncPlayRemoveFromPlaylist(syncPlayRemoveFromPlaylistRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayRemoveFromPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayRemoveFromPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlayRemoveFromPlaylistRequest** | [**SyncPlayRemoveFromPlaylistRequest**](SyncPlayRemoveFromPlaylistRequest.md)| The items to remove. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlaySeek"></a>
# **syncPlaySeek**
> syncPlaySeek(syncPlaySeekRequest)

Request seek in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlaySeekRequest : SyncPlaySeekRequest =  // SyncPlaySeekRequest | The new playback position.
try {
    apiInstance.syncPlaySeek(syncPlaySeekRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlaySeek")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlaySeek")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlaySeekRequest** | [**SyncPlaySeekRequest**](SyncPlaySeekRequest.md)| The new playback position. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlaySetIgnoreWait"></a>
# **syncPlaySetIgnoreWait**
> syncPlaySetIgnoreWait(syncPlaySetIgnoreWaitRequest)

Request SyncPlay group to ignore member during group-wait.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlaySetIgnoreWaitRequest : SyncPlaySetIgnoreWaitRequest =  // SyncPlaySetIgnoreWaitRequest | The settings to set.
try {
    apiInstance.syncPlaySetIgnoreWait(syncPlaySetIgnoreWaitRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlaySetIgnoreWait")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlaySetIgnoreWait")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlaySetIgnoreWaitRequest** | [**SyncPlaySetIgnoreWaitRequest**](SyncPlaySetIgnoreWaitRequest.md)| The settings to set. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlaySetNewQueue"></a>
# **syncPlaySetNewQueue**
> syncPlaySetNewQueue(syncPlaySetNewQueueRequest)

Request to set new playlist in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlaySetNewQueueRequest : SyncPlaySetNewQueueRequest =  // SyncPlaySetNewQueueRequest | The new playlist to play in the group.
try {
    apiInstance.syncPlaySetNewQueue(syncPlaySetNewQueueRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlaySetNewQueue")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlaySetNewQueue")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlaySetNewQueueRequest** | [**SyncPlaySetNewQueueRequest**](SyncPlaySetNewQueueRequest.md)| The new playlist to play in the group. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlaySetPlaylistItem"></a>
# **syncPlaySetPlaylistItem**
> syncPlaySetPlaylistItem(syncPlaySetPlaylistItemRequest)

Request to change playlist item in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlaySetPlaylistItemRequest : SyncPlaySetPlaylistItemRequest =  // SyncPlaySetPlaylistItemRequest | The new item to play.
try {
    apiInstance.syncPlaySetPlaylistItem(syncPlaySetPlaylistItemRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlaySetPlaylistItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlaySetPlaylistItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlaySetPlaylistItemRequest** | [**SyncPlaySetPlaylistItemRequest**](SyncPlaySetPlaylistItemRequest.md)| The new item to play. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlaySetRepeatMode"></a>
# **syncPlaySetRepeatMode**
> syncPlaySetRepeatMode(syncPlaySetRepeatModeRequest)

Request to set repeat mode in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlaySetRepeatModeRequest : SyncPlaySetRepeatModeRequest =  // SyncPlaySetRepeatModeRequest | The new repeat mode.
try {
    apiInstance.syncPlaySetRepeatMode(syncPlaySetRepeatModeRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlaySetRepeatMode")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlaySetRepeatMode")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlaySetRepeatModeRequest** | [**SyncPlaySetRepeatModeRequest**](SyncPlaySetRepeatModeRequest.md)| The new repeat mode. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlaySetShuffleMode"></a>
# **syncPlaySetShuffleMode**
> syncPlaySetShuffleMode(syncPlaySetShuffleModeRequest)

Request to set shuffle mode in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
val syncPlaySetShuffleModeRequest : SyncPlaySetShuffleModeRequest =  // SyncPlaySetShuffleModeRequest | The new shuffle mode.
try {
    apiInstance.syncPlaySetShuffleMode(syncPlaySetShuffleModeRequest)
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlaySetShuffleMode")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlaySetShuffleMode")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **syncPlaySetShuffleModeRequest** | [**SyncPlaySetShuffleModeRequest**](SyncPlaySetShuffleModeRequest.md)| The new shuffle mode. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="syncPlayStop"></a>
# **syncPlayStop**
> syncPlayStop()

Request stop in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
try {
    apiInstance.syncPlayStop()
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayStop")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayStop")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="syncPlayUnpause"></a>
# **syncPlayUnpause**
> syncPlayUnpause()

Request unpause in SyncPlay group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SyncPlayApi()
try {
    apiInstance.syncPlayUnpause()
} catch (e: ClientException) {
    println("4xx response calling SyncPlayApi#syncPlayUnpause")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SyncPlayApi#syncPlayUnpause")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

