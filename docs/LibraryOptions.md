
# LibraryOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enablePhotos** | **kotlin.Boolean** |  |  [optional]
**enableRealtimeMonitor** | **kotlin.Boolean** |  |  [optional]
**enableChapterImageExtraction** | **kotlin.Boolean** |  |  [optional]
**extractChapterImagesDuringLibraryScan** | **kotlin.Boolean** |  |  [optional]
**pathInfos** | [**kotlin.collections.List&lt;MediaPathInfo&gt;**](MediaPathInfo.md) |  |  [optional]
**saveLocalMetadata** | **kotlin.Boolean** |  |  [optional]
**enableInternetProviders** | **kotlin.Boolean** |  |  [optional]
**enableAutomaticSeriesGrouping** | **kotlin.Boolean** |  |  [optional]
**enableEmbeddedTitles** | **kotlin.Boolean** |  |  [optional]
**enableEmbeddedEpisodeInfos** | **kotlin.Boolean** |  |  [optional]
**automaticRefreshIntervalDays** | **kotlin.Int** |  |  [optional]
**preferredMetadataLanguage** | **kotlin.String** | Gets or sets the preferred metadata language. |  [optional]
**metadataCountryCode** | **kotlin.String** | Gets or sets the metadata country code. |  [optional]
**seasonZeroDisplayName** | **kotlin.String** |  |  [optional]
**metadataSavers** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**disabledLocalMetadataReaders** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**localMetadataReaderOrder** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**disabledSubtitleFetchers** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**subtitleFetcherOrder** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**skipSubtitlesIfEmbeddedSubtitlesPresent** | **kotlin.Boolean** |  |  [optional]
**skipSubtitlesIfAudioTrackMatches** | **kotlin.Boolean** |  |  [optional]
**subtitleDownloadLanguages** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**requirePerfectSubtitleMatch** | **kotlin.Boolean** |  |  [optional]
**saveSubtitlesWithMedia** | **kotlin.Boolean** |  |  [optional]
**automaticallyAddToCollection** | **kotlin.Boolean** |  |  [optional]
**allowEmbeddedSubtitles** | [**EmbeddedSubtitleOptions**](EmbeddedSubtitleOptions.md) |  |  [optional]
**typeOptions** | [**kotlin.collections.List&lt;TypeOptions&gt;**](TypeOptions.md) |  |  [optional]



