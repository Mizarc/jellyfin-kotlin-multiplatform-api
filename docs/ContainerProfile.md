
# ContainerProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**DlnaProfileType**](DlnaProfileType.md) |  |  [optional]
**conditions** | [**kotlin.collections.List&lt;ProfileCondition&gt;**](ProfileCondition.md) |  |  [optional]
**container** | **kotlin.String** |  |  [optional]



