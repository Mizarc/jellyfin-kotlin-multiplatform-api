
# ImageByNameInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**theme** | **kotlin.String** | Gets or sets the theme. |  [optional]
**context** | **kotlin.String** | Gets or sets the context. |  [optional]
**fileLength** | **kotlin.Long** | Gets or sets the length of the file. |  [optional]
**format** | **kotlin.String** | Gets or sets the format. |  [optional]



