
# JoinGroupRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **kotlin.String** | Gets or sets the group identifier. |  [optional]



