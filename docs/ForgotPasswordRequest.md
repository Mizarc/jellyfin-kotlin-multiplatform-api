
# ForgotPasswordRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enteredUsername** | **kotlin.String** | Gets or sets the entered username to have its password reset. | 



