
# NameGuidPair

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  |  [optional]
**id** | **kotlin.String** |  |  [optional]



