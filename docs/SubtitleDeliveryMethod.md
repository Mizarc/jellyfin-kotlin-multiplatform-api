
# SubtitleDeliveryMethod

## Enum


    * `encode` (value: `"Encode"`)

    * `embed` (value: `"Embed"`)

    * `&#x60;external&#x60;` (value: `"External"`)

    * `hls` (value: `"Hls"`)

    * `drop` (value: `"Drop"`)



