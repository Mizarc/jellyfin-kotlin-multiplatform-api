# ItemRefreshApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**refreshItem**](ItemRefreshApi.md#refreshItem) | **POST** /Items/{itemId}/Refresh | Refreshes metadata for an item.


<a name="refreshItem"></a>
# **refreshItem**
> refreshItem(itemId, metadataRefreshMode, imageRefreshMode, replaceAllMetadata, replaceAllImages)

Refreshes metadata for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ItemRefreshApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item id.
val metadataRefreshMode : MetadataRefreshMode =  // MetadataRefreshMode | (Optional) Specifies the metadata refresh mode.
val imageRefreshMode : MetadataRefreshMode =  // MetadataRefreshMode | (Optional) Specifies the image refresh mode.
val replaceAllMetadata : kotlin.Boolean = true // kotlin.Boolean | (Optional) Determines if metadata should be replaced. Only applicable if mode is FullRefresh.
val replaceAllImages : kotlin.Boolean = true // kotlin.Boolean | (Optional) Determines if images should be replaced. Only applicable if mode is FullRefresh.
try {
    apiInstance.refreshItem(itemId, metadataRefreshMode, imageRefreshMode, replaceAllMetadata, replaceAllImages)
} catch (e: ClientException) {
    println("4xx response calling ItemRefreshApi#refreshItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ItemRefreshApi#refreshItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item id. |
 **metadataRefreshMode** | [**MetadataRefreshMode**](.md)| (Optional) Specifies the metadata refresh mode. | [optional] [enum: None, ValidationOnly, Default, FullRefresh]
 **imageRefreshMode** | [**MetadataRefreshMode**](.md)| (Optional) Specifies the image refresh mode. | [optional] [enum: None, ValidationOnly, Default, FullRefresh]
 **replaceAllMetadata** | **kotlin.Boolean**| (Optional) Determines if metadata should be replaced. Only applicable if mode is FullRefresh. | [optional] [default to false]
 **replaceAllImages** | **kotlin.Boolean**| (Optional) Determines if images should be replaced. Only applicable if mode is FullRefresh. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

