
# AuthenticationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Long** | Gets or sets the identifier. |  [optional]
**accessToken** | **kotlin.String** | Gets or sets the access token. |  [optional]
**deviceId** | **kotlin.String** | Gets or sets the device identifier. |  [optional]
**appName** | **kotlin.String** | Gets or sets the name of the application. |  [optional]
**appVersion** | **kotlin.String** | Gets or sets the application version. |  [optional]
**deviceName** | **kotlin.String** | Gets or sets the name of the device. |  [optional]
**userId** | **kotlin.String** | Gets or sets the user identifier. |  [optional]
**isActive** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is active. |  [optional]
**dateCreated** | **kotlin.String** | Gets or sets the date created. |  [optional]
**dateRevoked** | **kotlin.String** | Gets or sets the date revoked. |  [optional]
**dateLastActivity** | **kotlin.String** |  |  [optional]
**userName** | **kotlin.String** |  |  [optional]



