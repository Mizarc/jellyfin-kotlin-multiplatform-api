
# UpdateInitialConfigurationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uiCulture** | **kotlin.String** | Gets or sets UI language culture. |  [optional]
**metadataCountryCode** | **kotlin.String** | Gets or sets the metadata country code. |  [optional]
**preferredMetadataLanguage** | **kotlin.String** | Gets or sets the preferred language for the metadata. |  [optional]



