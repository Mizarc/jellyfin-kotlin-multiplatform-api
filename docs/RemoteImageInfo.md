
# RemoteImageInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**providerName** | **kotlin.String** | Gets or sets the name of the provider. |  [optional]
**url** | **kotlin.String** | Gets or sets the URL. |  [optional]
**thumbnailUrl** | **kotlin.String** | Gets or sets a url used for previewing a smaller version. |  [optional]
**height** | **kotlin.Int** | Gets or sets the height. |  [optional]
**width** | **kotlin.Int** | Gets or sets the width. |  [optional]
**communityRating** | **kotlin.Double** | Gets or sets the community rating. |  [optional]
**voteCount** | **kotlin.Int** | Gets or sets the vote count. |  [optional]
**language** | **kotlin.String** | Gets or sets the language. |  [optional]
**type** | [**ImageType**](ImageType.md) |  |  [optional]
**ratingType** | [**RatingType**](RatingType.md) |  |  [optional]



