# MoviesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMovieRecommendations**](MoviesApi.md#getMovieRecommendations) | **GET** /Movies/Recommendations | Gets movie recommendations.


<a name="getMovieRecommendations"></a>
# **getMovieRecommendations**
> kotlin.collections.List&lt;RecommendationDto&gt; getMovieRecommendations(userId, parentId, fields, categoryLimit, itemLimit)

Gets movie recommendations.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = MoviesApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. The fields to return.
val categoryLimit : kotlin.Int = 56 // kotlin.Int | The max number of categories to return.
val itemLimit : kotlin.Int = 56 // kotlin.Int | The max number of items to return per category.
try {
    val result : kotlin.collections.List<RecommendationDto> = apiInstance.getMovieRecommendations(userId, parentId, fields, categoryLimit, itemLimit)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MoviesApi#getMovieRecommendations")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MoviesApi#getMovieRecommendations")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. The fields to return. | [optional]
 **categoryLimit** | **kotlin.Int**| The max number of categories to return. | [optional] [default to 5]
 **itemLimit** | **kotlin.Int**| The max number of items to return per category. | [optional] [default to 8]

### Return type

[**kotlin.collections.List&lt;RecommendationDto&gt;**](RecommendationDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

