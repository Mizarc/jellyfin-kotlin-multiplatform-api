# ApiKeyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createKey**](ApiKeyApi.md#createKey) | **POST** /Auth/Keys | Create a new api key.
[**getKeys**](ApiKeyApi.md#getKeys) | **GET** /Auth/Keys | Get all keys.
[**revokeKey**](ApiKeyApi.md#revokeKey) | **DELETE** /Auth/Keys/{key} | Remove an api key.


<a name="createKey"></a>
# **createKey**
> createKey(app)

Create a new api key.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ApiKeyApi()
val app : kotlin.String = app_example // kotlin.String | Name of the app using the authentication key.
try {
    apiInstance.createKey(app)
} catch (e: ClientException) {
    println("4xx response calling ApiKeyApi#createKey")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ApiKeyApi#createKey")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **kotlin.String**| Name of the app using the authentication key. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getKeys"></a>
# **getKeys**
> AuthenticationInfoQueryResult getKeys()

Get all keys.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ApiKeyApi()
try {
    val result : AuthenticationInfoQueryResult = apiInstance.getKeys()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ApiKeyApi#getKeys")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ApiKeyApi#getKeys")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthenticationInfoQueryResult**](AuthenticationInfoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="revokeKey"></a>
# **revokeKey**
> revokeKey(key)

Remove an api key.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ApiKeyApi()
val key : kotlin.String = key_example // kotlin.String | The access token to delete.
try {
    apiInstance.revokeKey(key)
} catch (e: ClientException) {
    println("4xx response calling ApiKeyApi#revokeKey")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ApiKeyApi#revokeKey")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **kotlin.String**| The access token to delete. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

