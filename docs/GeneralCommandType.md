
# GeneralCommandType

## Enum


    * `moveUp` (value: `"MoveUp"`)

    * `moveDown` (value: `"MoveDown"`)

    * `moveLeft` (value: `"MoveLeft"`)

    * `moveRight` (value: `"MoveRight"`)

    * `pageUp` (value: `"PageUp"`)

    * `pageDown` (value: `"PageDown"`)

    * `previousLetter` (value: `"PreviousLetter"`)

    * `nextLetter` (value: `"NextLetter"`)

    * `toggleOsd` (value: `"ToggleOsd"`)

    * `toggleContextMenu` (value: `"ToggleContextMenu"`)

    * `select` (value: `"Select"`)

    * `back` (value: `"Back"`)

    * `takeScreenshot` (value: `"TakeScreenshot"`)

    * `sendKey` (value: `"SendKey"`)

    * `sendString` (value: `"SendString"`)

    * `goHome` (value: `"GoHome"`)

    * `goToSettings` (value: `"GoToSettings"`)

    * `volumeUp` (value: `"VolumeUp"`)

    * `volumeDown` (value: `"VolumeDown"`)

    * `mute` (value: `"Mute"`)

    * `unmute` (value: `"Unmute"`)

    * `toggleMute` (value: `"ToggleMute"`)

    * `setVolume` (value: `"SetVolume"`)

    * `setAudioStreamIndex` (value: `"SetAudioStreamIndex"`)

    * `setSubtitleStreamIndex` (value: `"SetSubtitleStreamIndex"`)

    * `toggleFullscreen` (value: `"ToggleFullscreen"`)

    * `displayContent` (value: `"DisplayContent"`)

    * `goToSearch` (value: `"GoToSearch"`)

    * `displayMessage` (value: `"DisplayMessage"`)

    * `setRepeatMode` (value: `"SetRepeatMode"`)

    * `channelUp` (value: `"ChannelUp"`)

    * `channelDown` (value: `"ChannelDown"`)

    * `guide` (value: `"Guide"`)

    * `toggleStats` (value: `"ToggleStats"`)

    * `playMediaSource` (value: `"PlayMediaSource"`)

    * `playTrailers` (value: `"PlayTrailers"`)

    * `setShuffleQueue` (value: `"SetShuffleQueue"`)

    * `playState` (value: `"PlayState"`)

    * `playNext` (value: `"PlayNext"`)

    * `toggleOsdMenu` (value: `"ToggleOsdMenu"`)

    * `play` (value: `"Play"`)

    * `setMaxStreamingBitrate` (value: `"SetMaxStreamingBitrate"`)



