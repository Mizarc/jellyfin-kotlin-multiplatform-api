
# PlayCommand

## Enum


    * `playNow` (value: `"PlayNow"`)

    * `playNext` (value: `"PlayNext"`)

    * `playLast` (value: `"PlayLast"`)

    * `playInstantMix` (value: `"PlayInstantMix"`)

    * `playShuffle` (value: `"PlayShuffle"`)



