# QuickConnectApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorize**](QuickConnectApi.md#authorize) | **POST** /QuickConnect/Authorize | Authorizes a pending quick connect request.
[**connect**](QuickConnectApi.md#connect) | **GET** /QuickConnect/Connect | Attempts to retrieve authentication information.
[**getEnabled**](QuickConnectApi.md#getEnabled) | **GET** /QuickConnect/Enabled | Gets the current quick connect state.
[**initiate**](QuickConnectApi.md#initiate) | **GET** /QuickConnect/Initiate | Initiate a new quick connect request.


<a name="authorize"></a>
# **authorize**
> kotlin.Boolean authorize(code)

Authorizes a pending quick connect request.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = QuickConnectApi()
val code : kotlin.String = code_example // kotlin.String | Quick connect code to authorize.
try {
    val result : kotlin.Boolean = apiInstance.authorize(code)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling QuickConnectApi#authorize")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling QuickConnectApi#authorize")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **kotlin.String**| Quick connect code to authorize. |

### Return type

**kotlin.Boolean**

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="connect"></a>
# **connect**
> QuickConnectResult connect(secret)

Attempts to retrieve authentication information.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = QuickConnectApi()
val secret : kotlin.String = secret_example // kotlin.String | Secret previously returned from the Initiate endpoint.
try {
    val result : QuickConnectResult = apiInstance.connect(secret)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling QuickConnectApi#connect")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling QuickConnectApi#connect")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **secret** | **kotlin.String**| Secret previously returned from the Initiate endpoint. |

### Return type

[**QuickConnectResult**](QuickConnectResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getEnabled"></a>
# **getEnabled**
> kotlin.Boolean getEnabled()

Gets the current quick connect state.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = QuickConnectApi()
try {
    val result : kotlin.Boolean = apiInstance.getEnabled()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling QuickConnectApi#getEnabled")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling QuickConnectApi#getEnabled")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="initiate"></a>
# **initiate**
> QuickConnectResult initiate()

Initiate a new quick connect request.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = QuickConnectApi()
try {
    val result : QuickConnectResult = apiInstance.initiate()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling QuickConnectApi#initiate")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling QuickConnectApi#initiate")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**QuickConnectResult**](QuickConnectResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

