
# PlayRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemIds** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the item ids. |  [optional]
**startPositionTicks** | **kotlin.Long** | Gets or sets the start position ticks that the first item should be played at. |  [optional]
**playCommand** | [**PlayCommand**](PlayCommand.md) |  |  [optional]
**controllingUserId** | **kotlin.String** | Gets or sets the controlling user identifier. |  [optional]
**subtitleStreamIndex** | **kotlin.Int** |  |  [optional]
**audioStreamIndex** | **kotlin.Int** |  |  [optional]
**mediaSourceId** | **kotlin.String** |  |  [optional]
**startIndex** | **kotlin.Int** |  |  [optional]



