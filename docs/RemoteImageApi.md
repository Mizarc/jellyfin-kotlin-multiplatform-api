# RemoteImageApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**downloadRemoteImage**](RemoteImageApi.md#downloadRemoteImage) | **POST** /Items/{itemId}/RemoteImages/Download | Downloads a remote image for an item.
[**getRemoteImageProviders**](RemoteImageApi.md#getRemoteImageProviders) | **GET** /Items/{itemId}/RemoteImages/Providers | Gets available remote image providers for an item.
[**getRemoteImages**](RemoteImageApi.md#getRemoteImages) | **GET** /Items/{itemId}/RemoteImages | Gets available remote images for an item.


<a name="downloadRemoteImage"></a>
# **downloadRemoteImage**
> downloadRemoteImage(itemId, type, imageUrl)

Downloads a remote image for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = RemoteImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item Id.
val type : ImageType =  // ImageType | The image type.
val imageUrl : kotlin.String = imageUrl_example // kotlin.String | The image url.
try {
    apiInstance.downloadRemoteImage(itemId, type, imageUrl)
} catch (e: ClientException) {
    println("4xx response calling RemoteImageApi#downloadRemoteImage")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling RemoteImageApi#downloadRemoteImage")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item Id. |
 **type** | [**ImageType**](.md)| The image type. | [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **imageUrl** | **kotlin.String**| The image url. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRemoteImageProviders"></a>
# **getRemoteImageProviders**
> kotlin.collections.List&lt;ImageProviderInfo&gt; getRemoteImageProviders(itemId)

Gets available remote image providers for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = RemoteImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item Id.
try {
    val result : kotlin.collections.List<ImageProviderInfo> = apiInstance.getRemoteImageProviders(itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling RemoteImageApi#getRemoteImageProviders")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling RemoteImageApi#getRemoteImageProviders")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item Id. |

### Return type

[**kotlin.collections.List&lt;ImageProviderInfo&gt;**](ImageProviderInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRemoteImages"></a>
# **getRemoteImages**
> RemoteImageResult getRemoteImages(itemId, type, startIndex, limit, providerName, includeAllLanguages)

Gets available remote images for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = RemoteImageApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Item Id.
val type : ImageType =  // ImageType | The image type.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val providerName : kotlin.String = providerName_example // kotlin.String | Optional. The image provider to use.
val includeAllLanguages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include all languages.
try {
    val result : RemoteImageResult = apiInstance.getRemoteImages(itemId, type, startIndex, limit, providerName, includeAllLanguages)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling RemoteImageApi#getRemoteImages")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling RemoteImageApi#getRemoteImages")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| Item Id. |
 **type** | [**ImageType**](.md)| The image type. | [optional] [enum: Primary, Art, Backdrop, Banner, Logo, Thumb, Disc, Box, Screenshot, Menu, Chapter, BoxRear, Profile]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **providerName** | **kotlin.String**| Optional. The image provider to use. | [optional]
 **includeAllLanguages** | **kotlin.Boolean**| Optional. Include all languages. | [optional] [default to false]

### Return type

[**RemoteImageResult**](RemoteImageResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

