
# WakeOnLanInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**macAddress** | **kotlin.String** | Gets the MAC address of the device. |  [optional]
**port** | **kotlin.Int** | Gets or sets the wake-on-LAN port. |  [optional]



