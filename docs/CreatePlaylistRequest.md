
# CreatePlaylistRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name of the new playlist. |  [optional]
**ids** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets item ids to add to the playlist. |  [optional]
**userId** | **kotlin.String** | Gets or sets the user id. |  [optional]
**mediaType** | **kotlin.String** | Gets or sets the media type. |  [optional]



