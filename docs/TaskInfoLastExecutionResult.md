
# TaskInfoLastExecutionResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTimeUtc** | **kotlin.String** | Gets or sets the start time UTC. |  [optional]
**endTimeUtc** | **kotlin.String** | Gets or sets the end time UTC. |  [optional]
**status** | [**TaskCompletionStatus**](TaskCompletionStatus.md) |  |  [optional]
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**key** | **kotlin.String** | Gets or sets the key. |  [optional]
**id** | **kotlin.String** | Gets or sets the id. |  [optional]
**errorMessage** | **kotlin.String** | Gets or sets the error message. |  [optional]
**longErrorMessage** | **kotlin.String** | Gets or sets the long error message. |  [optional]



