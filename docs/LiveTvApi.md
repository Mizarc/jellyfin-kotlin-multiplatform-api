# LiveTvApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addListingProvider**](LiveTvApi.md#addListingProvider) | **POST** /LiveTv/ListingProviders | Adds a listings provider.
[**addTunerHost**](LiveTvApi.md#addTunerHost) | **POST** /LiveTv/TunerHosts | Adds a tuner host.
[**cancelSeriesTimer**](LiveTvApi.md#cancelSeriesTimer) | **DELETE** /LiveTv/SeriesTimers/{timerId} | Cancels a live tv series timer.
[**cancelTimer**](LiveTvApi.md#cancelTimer) | **DELETE** /LiveTv/Timers/{timerId} | Cancels a live tv timer.
[**createSeriesTimer**](LiveTvApi.md#createSeriesTimer) | **POST** /LiveTv/SeriesTimers | Creates a live tv series timer.
[**createTimer**](LiveTvApi.md#createTimer) | **POST** /LiveTv/Timers | Creates a live tv timer.
[**deleteListingProvider**](LiveTvApi.md#deleteListingProvider) | **DELETE** /LiveTv/ListingProviders | Delete listing provider.
[**deleteRecording**](LiveTvApi.md#deleteRecording) | **DELETE** /LiveTv/Recordings/{recordingId} | Deletes a live tv recording.
[**deleteTunerHost**](LiveTvApi.md#deleteTunerHost) | **DELETE** /LiveTv/TunerHosts | Deletes a tuner host.
[**discoverTuners**](LiveTvApi.md#discoverTuners) | **GET** /LiveTv/Tuners/Discover | Discover tuners.
[**discvoverTuners**](LiveTvApi.md#discvoverTuners) | **GET** /LiveTv/Tuners/Discvover | Discover tuners.
[**getChannel**](LiveTvApi.md#getChannel) | **GET** /LiveTv/Channels/{channelId} | Gets a live tv channel.
[**getChannelMappingOptions**](LiveTvApi.md#getChannelMappingOptions) | **GET** /LiveTv/ChannelMappingOptions | Get channel mapping options.
[**getDefaultListingProvider**](LiveTvApi.md#getDefaultListingProvider) | **GET** /LiveTv/ListingProviders/Default | Gets default listings provider info.
[**getDefaultTimer**](LiveTvApi.md#getDefaultTimer) | **GET** /LiveTv/Timers/Defaults | Gets the default values for a new timer.
[**getGuideInfo**](LiveTvApi.md#getGuideInfo) | **GET** /LiveTv/GuideInfo | Get guid info.
[**getLineups**](LiveTvApi.md#getLineups) | **GET** /LiveTv/ListingProviders/Lineups | Gets available lineups.
[**getLiveRecordingFile**](LiveTvApi.md#getLiveRecordingFile) | **GET** /LiveTv/LiveRecordings/{recordingId}/stream | Gets a live tv recording stream.
[**getLiveStreamFile**](LiveTvApi.md#getLiveStreamFile) | **GET** /LiveTv/LiveStreamFiles/{streamId}/stream.{container} | Gets a live tv channel stream.
[**getLiveTvChannels**](LiveTvApi.md#getLiveTvChannels) | **GET** /LiveTv/Channels | Gets available live tv channels.
[**getLiveTvInfo**](LiveTvApi.md#getLiveTvInfo) | **GET** /LiveTv/Info | Gets available live tv services.
[**getLiveTvPrograms**](LiveTvApi.md#getLiveTvPrograms) | **GET** /LiveTv/Programs | Gets available live tv epgs.
[**getProgram**](LiveTvApi.md#getProgram) | **GET** /LiveTv/Programs/{programId} | Gets a live tv program.
[**getPrograms**](LiveTvApi.md#getPrograms) | **POST** /LiveTv/Programs | Gets available live tv epgs.
[**getRecommendedPrograms**](LiveTvApi.md#getRecommendedPrograms) | **GET** /LiveTv/Programs/Recommended | Gets recommended live tv epgs.
[**getRecording**](LiveTvApi.md#getRecording) | **GET** /LiveTv/Recordings/{recordingId} | Gets a live tv recording.
[**getRecordingFolders**](LiveTvApi.md#getRecordingFolders) | **GET** /LiveTv/Recordings/Folders | Gets recording folders.
[**getRecordingGroup**](LiveTvApi.md#getRecordingGroup) | **GET** /LiveTv/Recordings/Groups/{groupId} | Get recording group.
[**getRecordingGroups**](LiveTvApi.md#getRecordingGroups) | **GET** /LiveTv/Recordings/Groups | Gets live tv recording groups.
[**getRecordings**](LiveTvApi.md#getRecordings) | **GET** /LiveTv/Recordings | Gets live tv recordings.
[**getRecordingsSeries**](LiveTvApi.md#getRecordingsSeries) | **GET** /LiveTv/Recordings/Series | Gets live tv recording series.
[**getSchedulesDirectCountries**](LiveTvApi.md#getSchedulesDirectCountries) | **GET** /LiveTv/ListingProviders/SchedulesDirect/Countries | Gets available countries.
[**getSeriesTimer**](LiveTvApi.md#getSeriesTimer) | **GET** /LiveTv/SeriesTimers/{timerId} | Gets a live tv series timer.
[**getSeriesTimers**](LiveTvApi.md#getSeriesTimers) | **GET** /LiveTv/SeriesTimers | Gets live tv series timers.
[**getTimer**](LiveTvApi.md#getTimer) | **GET** /LiveTv/Timers/{timerId} | Gets a timer.
[**getTimers**](LiveTvApi.md#getTimers) | **GET** /LiveTv/Timers | Gets the live tv timers.
[**getTunerHostTypes**](LiveTvApi.md#getTunerHostTypes) | **GET** /LiveTv/TunerHosts/Types | Get tuner host types.
[**resetTuner**](LiveTvApi.md#resetTuner) | **POST** /LiveTv/Tuners/{tunerId}/Reset | Resets a tv tuner.
[**setChannelMapping**](LiveTvApi.md#setChannelMapping) | **POST** /LiveTv/ChannelMappings | Set channel mappings.
[**updateSeriesTimer**](LiveTvApi.md#updateSeriesTimer) | **POST** /LiveTv/SeriesTimers/{timerId} | Updates a live tv series timer.
[**updateTimer**](LiveTvApi.md#updateTimer) | **POST** /LiveTv/Timers/{timerId} | Updates a live tv timer.


<a name="addListingProvider"></a>
# **addListingProvider**
> ListingsProviderInfo addListingProvider(pw, validateListings, validateLogin, addListingProviderRequest)

Adds a listings provider.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val pw : kotlin.String = pw_example // kotlin.String | Password.
val validateListings : kotlin.Boolean = true // kotlin.Boolean | Validate listings.
val validateLogin : kotlin.Boolean = true // kotlin.Boolean | Validate login.
val addListingProviderRequest : AddListingProviderRequest =  // AddListingProviderRequest | New listings info.
try {
    val result : ListingsProviderInfo = apiInstance.addListingProvider(pw, validateListings, validateLogin, addListingProviderRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#addListingProvider")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#addListingProvider")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pw** | **kotlin.String**| Password. | [optional]
 **validateListings** | **kotlin.Boolean**| Validate listings. | [optional] [default to false]
 **validateLogin** | **kotlin.Boolean**| Validate login. | [optional] [default to false]
 **addListingProviderRequest** | [**AddListingProviderRequest**](AddListingProviderRequest.md)| New listings info. | [optional]

### Return type

[**ListingsProviderInfo**](ListingsProviderInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="addTunerHost"></a>
# **addTunerHost**
> TunerHostInfo addTunerHost(addTunerHostRequest)

Adds a tuner host.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val addTunerHostRequest : AddTunerHostRequest =  // AddTunerHostRequest | New tuner host.
try {
    val result : TunerHostInfo = apiInstance.addTunerHost(addTunerHostRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#addTunerHost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#addTunerHost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addTunerHostRequest** | [**AddTunerHostRequest**](AddTunerHostRequest.md)| New tuner host. | [optional]

### Return type

[**TunerHostInfo**](TunerHostInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="cancelSeriesTimer"></a>
# **cancelSeriesTimer**
> cancelSeriesTimer(timerId)

Cancels a live tv series timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val timerId : kotlin.String = timerId_example // kotlin.String | Timer id.
try {
    apiInstance.cancelSeriesTimer(timerId)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#cancelSeriesTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#cancelSeriesTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **kotlin.String**| Timer id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cancelTimer"></a>
# **cancelTimer**
> cancelTimer(timerId)

Cancels a live tv timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val timerId : kotlin.String = timerId_example // kotlin.String | Timer id.
try {
    apiInstance.cancelTimer(timerId)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#cancelTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#cancelTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **kotlin.String**| Timer id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createSeriesTimer"></a>
# **createSeriesTimer**
> createSeriesTimer(createSeriesTimerRequest)

Creates a live tv series timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val createSeriesTimerRequest : CreateSeriesTimerRequest =  // CreateSeriesTimerRequest | New series timer info.
try {
    apiInstance.createSeriesTimer(createSeriesTimerRequest)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#createSeriesTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#createSeriesTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createSeriesTimerRequest** | [**CreateSeriesTimerRequest**](CreateSeriesTimerRequest.md)| New series timer info. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="createTimer"></a>
# **createTimer**
> createTimer(createTimerRequest)

Creates a live tv timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val createTimerRequest : CreateTimerRequest =  // CreateTimerRequest | New timer info.
try {
    apiInstance.createTimer(createTimerRequest)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#createTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#createTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createTimerRequest** | [**CreateTimerRequest**](CreateTimerRequest.md)| New timer info. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="deleteListingProvider"></a>
# **deleteListingProvider**
> deleteListingProvider(id)

Delete listing provider.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val id : kotlin.String = id_example // kotlin.String | Listing provider id.
try {
    apiInstance.deleteListingProvider(id)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#deleteListingProvider")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#deleteListingProvider")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| Listing provider id. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteRecording"></a>
# **deleteRecording**
> deleteRecording(recordingId)

Deletes a live tv recording.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val recordingId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Recording id.
try {
    apiInstance.deleteRecording(recordingId)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#deleteRecording")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#deleteRecording")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recordingId** | **kotlin.String**| Recording id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="deleteTunerHost"></a>
# **deleteTunerHost**
> deleteTunerHost(id)

Deletes a tuner host.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val id : kotlin.String = id_example // kotlin.String | Tuner host id.
try {
    apiInstance.deleteTunerHost(id)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#deleteTunerHost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#deleteTunerHost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| Tuner host id. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="discoverTuners"></a>
# **discoverTuners**
> kotlin.collections.List&lt;TunerHostInfo&gt; discoverTuners(newDevicesOnly)

Discover tuners.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val newDevicesOnly : kotlin.Boolean = true // kotlin.Boolean | Only discover new tuners.
try {
    val result : kotlin.collections.List<TunerHostInfo> = apiInstance.discoverTuners(newDevicesOnly)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#discoverTuners")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#discoverTuners")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **newDevicesOnly** | **kotlin.Boolean**| Only discover new tuners. | [optional] [default to false]

### Return type

[**kotlin.collections.List&lt;TunerHostInfo&gt;**](TunerHostInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="discvoverTuners"></a>
# **discvoverTuners**
> kotlin.collections.List&lt;TunerHostInfo&gt; discvoverTuners(newDevicesOnly)

Discover tuners.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val newDevicesOnly : kotlin.Boolean = true // kotlin.Boolean | Only discover new tuners.
try {
    val result : kotlin.collections.List<TunerHostInfo> = apiInstance.discvoverTuners(newDevicesOnly)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#discvoverTuners")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#discvoverTuners")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **newDevicesOnly** | **kotlin.Boolean**| Only discover new tuners. | [optional] [default to false]

### Return type

[**kotlin.collections.List&lt;TunerHostInfo&gt;**](TunerHostInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getChannel"></a>
# **getChannel**
> BaseItemDto getChannel(channelId, userId)

Gets a live tv channel.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val channelId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Channel id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Attach user data.
try {
    val result : BaseItemDto = apiInstance.getChannel(channelId, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getChannel")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getChannel")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.String**| Channel id. |
 **userId** | **kotlin.String**| Optional. Attach user data. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getChannelMappingOptions"></a>
# **getChannelMappingOptions**
> ChannelMappingOptionsDto getChannelMappingOptions(providerId)

Get channel mapping options.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val providerId : kotlin.String = providerId_example // kotlin.String | Provider id.
try {
    val result : ChannelMappingOptionsDto = apiInstance.getChannelMappingOptions(providerId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getChannelMappingOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getChannelMappingOptions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **providerId** | **kotlin.String**| Provider id. | [optional]

### Return type

[**ChannelMappingOptionsDto**](ChannelMappingOptionsDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDefaultListingProvider"></a>
# **getDefaultListingProvider**
> ListingsProviderInfo getDefaultListingProvider()

Gets default listings provider info.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
try {
    val result : ListingsProviderInfo = apiInstance.getDefaultListingProvider()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getDefaultListingProvider")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getDefaultListingProvider")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ListingsProviderInfo**](ListingsProviderInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDefaultTimer"></a>
# **getDefaultTimer**
> SeriesTimerInfoDto getDefaultTimer(programId)

Gets the default values for a new timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val programId : kotlin.String = programId_example // kotlin.String | Optional. To attach default values based on a program.
try {
    val result : SeriesTimerInfoDto = apiInstance.getDefaultTimer(programId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getDefaultTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getDefaultTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **programId** | **kotlin.String**| Optional. To attach default values based on a program. | [optional]

### Return type

[**SeriesTimerInfoDto**](SeriesTimerInfoDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getGuideInfo"></a>
# **getGuideInfo**
> GuideInfo getGuideInfo()

Get guid info.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
try {
    val result : GuideInfo = apiInstance.getGuideInfo()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getGuideInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getGuideInfo")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**GuideInfo**](GuideInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLineups"></a>
# **getLineups**
> kotlin.collections.List&lt;NameIdPair&gt; getLineups(id, type, location, country)

Gets available lineups.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val id : kotlin.String = id_example // kotlin.String | Provider id.
val type : kotlin.String = type_example // kotlin.String | Provider type.
val location : kotlin.String = location_example // kotlin.String | Location.
val country : kotlin.String = country_example // kotlin.String | Country.
try {
    val result : kotlin.collections.List<NameIdPair> = apiInstance.getLineups(id, type, location, country)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getLineups")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getLineups")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| Provider id. | [optional]
 **type** | **kotlin.String**| Provider type. | [optional]
 **location** | **kotlin.String**| Location. | [optional]
 **country** | **kotlin.String**| Country. | [optional]

### Return type

[**kotlin.collections.List&lt;NameIdPair&gt;**](NameIdPair.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLiveRecordingFile"></a>
# **getLiveRecordingFile**
> org.openapitools.client.infrastructure.OctetByteArray getLiveRecordingFile(recordingId)

Gets a live tv recording stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val recordingId : kotlin.String = recordingId_example // kotlin.String | Recording id.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getLiveRecordingFile(recordingId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getLiveRecordingFile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getLiveRecordingFile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recordingId** | **kotlin.String**| Recording id. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: video/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLiveStreamFile"></a>
# **getLiveStreamFile**
> org.openapitools.client.infrastructure.OctetByteArray getLiveStreamFile(streamId, container)

Gets a live tv channel stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val streamId : kotlin.String = streamId_example // kotlin.String | Stream id.
val container : kotlin.String = container_example // kotlin.String | Container type.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getLiveStreamFile(streamId, container)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getLiveStreamFile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getLiveStreamFile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **streamId** | **kotlin.String**| Stream id. |
 **container** | **kotlin.String**| Container type. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: video/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLiveTvChannels"></a>
# **getLiveTvChannels**
> BaseItemDtoQueryResult getLiveTvChannels(type, userId, startIndex, isMovie, isSeries, isNews, isKids, isSports, limit, isFavorite, isLiked, isDisliked, enableImages, imageTypeLimit, enableImageTypes, fields, enableUserData, sortBy, sortOrder, enableFavoriteSorting, addCurrentProgram)

Gets available live tv channels.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val type : ChannelType =  // ChannelType | Optional. Filter by channel type.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user and attach user data.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val isMovie : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for movies.
val isSeries : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for series.
val isNews : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for news.
val isKids : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for kids.
val isSports : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for sports.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by channels that are favorites, or not.
val isLiked : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by channels that are liked, or not.
val isDisliked : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by channels that are disliked, or not.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | \"Optional. The image types to include in the output.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Key to sort by.
val sortOrder : SortOrder =  // SortOrder | Optional. Sort order.
val enableFavoriteSorting : kotlin.Boolean = true // kotlin.Boolean | Optional. Incorporate favorite and like status into channel sorting.
val addCurrentProgram : kotlin.Boolean = true // kotlin.Boolean | Optional. Adds current program info to each channel.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getLiveTvChannels(type, userId, startIndex, isMovie, isSeries, isNews, isKids, isSports, limit, isFavorite, isLiked, isDisliked, enableImages, imageTypeLimit, enableImageTypes, fields, enableUserData, sortBy, sortOrder, enableFavoriteSorting, addCurrentProgram)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getLiveTvChannels")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getLiveTvChannels")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | [**ChannelType**](.md)| Optional. Filter by channel type. | [optional] [enum: TV, Radio]
 **userId** | **kotlin.String**| Optional. Filter by user and attach user data. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **isMovie** | **kotlin.Boolean**| Optional. Filter for movies. | [optional]
 **isSeries** | **kotlin.Boolean**| Optional. Filter for series. | [optional]
 **isNews** | **kotlin.Boolean**| Optional. Filter for news. | [optional]
 **isKids** | **kotlin.Boolean**| Optional. Filter for kids. | [optional]
 **isSports** | **kotlin.Boolean**| Optional. Filter for sports. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional. Filter by channels that are favorites, or not. | [optional]
 **isLiked** | **kotlin.Boolean**| Optional. Filter by channels that are liked, or not. | [optional]
 **isDisliked** | **kotlin.Boolean**| Optional. Filter by channels that are disliked, or not. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| \&quot;Optional. The image types to include in the output. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Key to sort by. | [optional]
 **sortOrder** | [**SortOrder**](.md)| Optional. Sort order. | [optional] [enum: Ascending, Descending]
 **enableFavoriteSorting** | **kotlin.Boolean**| Optional. Incorporate favorite and like status into channel sorting. | [optional] [default to false]
 **addCurrentProgram** | **kotlin.Boolean**| Optional. Adds current program info to each channel. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLiveTvInfo"></a>
# **getLiveTvInfo**
> LiveTvInfo getLiveTvInfo()

Gets available live tv services.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
try {
    val result : LiveTvInfo = apiInstance.getLiveTvInfo()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getLiveTvInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getLiveTvInfo")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LiveTvInfo**](LiveTvInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLiveTvPrograms"></a>
# **getLiveTvPrograms**
> BaseItemDtoQueryResult getLiveTvPrograms(channelIds, userId, minStartDate, hasAired, isAiring, maxStartDate, minEndDate, maxEndDate, isMovie, isSeries, isNews, isKids, isSports, startIndex, limit, sortBy, sortOrder, genres, genreIds, enableImages, imageTypeLimit, enableImageTypes, enableUserData, seriesTimerId, librarySeriesId, fields, enableTotalRecordCount)

Gets available live tv epgs.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val channelIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The channels to return guide information for.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id.
val minStartDate : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The minimum premiere start date.
val hasAired : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by programs that have completed airing, or not.
val isAiring : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by programs that are currently airing, or not.
val maxStartDate : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The maximum premiere start date.
val minEndDate : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The minimum premiere end date.
val maxEndDate : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. The maximum premiere end date.
val isMovie : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for movies.
val isSeries : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for series.
val isNews : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for news.
val isKids : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for kids.
val isSports : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for sports.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more sort orders, comma delimited. Options: Name, StartDate.
val sortOrder : kotlin.collections.List<SortOrder> =  // kotlin.collections.List<SortOrder> | Sort Order - Ascending,Descending.
val genres : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The genres to return guide information for.
val genreIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The genre ids to return guide information for.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val seriesTimerId : kotlin.String = seriesTimerId_example // kotlin.String | Optional. Filter by series timer id.
val librarySeriesId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by library series id.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Retrieve total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getLiveTvPrograms(channelIds, userId, minStartDate, hasAired, isAiring, maxStartDate, minEndDate, maxEndDate, isMovie, isSeries, isNews, isKids, isSports, startIndex, limit, sortBy, sortOrder, genres, genreIds, enableImages, imageTypeLimit, enableImageTypes, enableUserData, seriesTimerId, librarySeriesId, fields, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getLiveTvPrograms")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getLiveTvPrograms")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The channels to return guide information for. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user id. | [optional]
 **minStartDate** | **kotlin.String**| Optional. The minimum premiere start date. | [optional]
 **hasAired** | **kotlin.Boolean**| Optional. Filter by programs that have completed airing, or not. | [optional]
 **isAiring** | **kotlin.Boolean**| Optional. Filter by programs that are currently airing, or not. | [optional]
 **maxStartDate** | **kotlin.String**| Optional. The maximum premiere start date. | [optional]
 **minEndDate** | **kotlin.String**| Optional. The minimum premiere end date. | [optional]
 **maxEndDate** | **kotlin.String**| Optional. The maximum premiere end date. | [optional]
 **isMovie** | **kotlin.Boolean**| Optional. Filter for movies. | [optional]
 **isSeries** | **kotlin.Boolean**| Optional. Filter for series. | [optional]
 **isNews** | **kotlin.Boolean**| Optional. Filter for news. | [optional]
 **isKids** | **kotlin.Boolean**| Optional. Filter for kids. | [optional]
 **isSports** | **kotlin.Boolean**| Optional. Filter for sports. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more sort orders, comma delimited. Options: Name, StartDate. | [optional]
 **sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md)| Sort Order - Ascending,Descending. | [optional]
 **genres** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The genres to return guide information for. | [optional]
 **genreIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The genre ids to return guide information for. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **seriesTimerId** | **kotlin.String**| Optional. Filter by series timer id. | [optional]
 **librarySeriesId** | **kotlin.String**| Optional. Filter by library series id. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableTotalRecordCount** | **kotlin.Boolean**| Retrieve total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getProgram"></a>
# **getProgram**
> BaseItemDto getProgram(programId, userId)

Gets a live tv program.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val programId : kotlin.String = programId_example // kotlin.String | Program id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Attach user data.
try {
    val result : BaseItemDto = apiInstance.getProgram(programId, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getProgram")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getProgram")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **programId** | **kotlin.String**| Program id. |
 **userId** | **kotlin.String**| Optional. Attach user data. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPrograms"></a>
# **getPrograms**
> BaseItemDtoQueryResult getPrograms(getProgramsRequest)

Gets available live tv epgs.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val getProgramsRequest : GetProgramsRequest =  // GetProgramsRequest | Request body.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getPrograms(getProgramsRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getPrograms")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getPrograms")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getProgramsRequest** | [**GetProgramsRequest**](GetProgramsRequest.md)| Request body. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRecommendedPrograms"></a>
# **getRecommendedPrograms**
> BaseItemDtoQueryResult getRecommendedPrograms(userId, limit, isAiring, hasAired, isSeries, isMovie, isNews, isKids, isSports, enableImages, imageTypeLimit, enableImageTypes, genreIds, fields, enableUserData, enableTotalRecordCount)

Gets recommended live tv epgs.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. filter by user id.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val isAiring : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by programs that are currently airing, or not.
val hasAired : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by programs that have completed airing, or not.
val isSeries : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for series.
val isMovie : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for movies.
val isNews : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for news.
val isKids : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for kids.
val isSports : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for sports.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val genreIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The genres to return guide information for.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. include user data.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Retrieve total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getRecommendedPrograms(userId, limit, isAiring, hasAired, isSeries, isMovie, isNews, isKids, isSports, enableImages, imageTypeLimit, enableImageTypes, genreIds, fields, enableUserData, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getRecommendedPrograms")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getRecommendedPrograms")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. filter by user id. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **isAiring** | **kotlin.Boolean**| Optional. Filter by programs that are currently airing, or not. | [optional]
 **hasAired** | **kotlin.Boolean**| Optional. Filter by programs that have completed airing, or not. | [optional]
 **isSeries** | **kotlin.Boolean**| Optional. Filter for series. | [optional]
 **isMovie** | **kotlin.Boolean**| Optional. Filter for movies. | [optional]
 **isNews** | **kotlin.Boolean**| Optional. Filter for news. | [optional]
 **isKids** | **kotlin.Boolean**| Optional. Filter for kids. | [optional]
 **isSports** | **kotlin.Boolean**| Optional. Filter for sports. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **genreIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The genres to return guide information for. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. include user data. | [optional]
 **enableTotalRecordCount** | **kotlin.Boolean**| Retrieve total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRecording"></a>
# **getRecording**
> BaseItemDto getRecording(recordingId, userId)

Gets a live tv recording.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val recordingId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Recording id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Attach user data.
try {
    val result : BaseItemDto = apiInstance.getRecording(recordingId, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getRecording")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getRecording")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recordingId** | **kotlin.String**| Recording id. |
 **userId** | **kotlin.String**| Optional. Attach user data. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRecordingFolders"></a>
# **getRecordingFolders**
> BaseItemDtoQueryResult getRecordingFolders(userId)

Gets recording folders.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user and attach user data.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getRecordingFolders(userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getRecordingFolders")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getRecordingFolders")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. Filter by user and attach user data. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRecordingGroup"></a>
# **getRecordingGroup**
> getRecordingGroup(groupId)

Get recording group.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val groupId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Group id.
try {
    apiInstance.getRecordingGroup(groupId)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getRecordingGroup")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getRecordingGroup")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupId** | **kotlin.String**| Group id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRecordingGroups"></a>
# **getRecordingGroups**
> BaseItemDtoQueryResult getRecordingGroups(userId)

Gets live tv recording groups.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user and attach user data.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getRecordingGroups(userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getRecordingGroups")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getRecordingGroups")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. Filter by user and attach user data. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRecordings"></a>
# **getRecordings**
> BaseItemDtoQueryResult getRecordings(channelId, userId, startIndex, limit, status, isInProgress, seriesTimerId, enableImages, imageTypeLimit, enableImageTypes, fields, enableUserData, isMovie, isSeries, isKids, isSports, isNews, isLibraryItem, enableTotalRecordCount)

Gets live tv recordings.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val channelId : kotlin.String = channelId_example // kotlin.String | Optional. Filter by channel id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user and attach user data.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val status : RecordingStatus =  // RecordingStatus | Optional. Filter by recording status.
val isInProgress : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by recordings that are in progress, or not.
val seriesTimerId : kotlin.String = seriesTimerId_example // kotlin.String | Optional. Filter by recordings belonging to a series timer.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val isMovie : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for movies.
val isSeries : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for series.
val isKids : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for kids.
val isSports : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for sports.
val isNews : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for news.
val isLibraryItem : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter for is library item.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Optional. Return total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getRecordings(channelId, userId, startIndex, limit, status, isInProgress, seriesTimerId, enableImages, imageTypeLimit, enableImageTypes, fields, enableUserData, isMovie, isSeries, isKids, isSports, isNews, isLibraryItem, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getRecordings")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getRecordings")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.String**| Optional. Filter by channel id. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user and attach user data. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **status** | [**RecordingStatus**](.md)| Optional. Filter by recording status. | [optional] [enum: New, InProgress, Completed, Cancelled, ConflictedOk, ConflictedNotOk, Error]
 **isInProgress** | **kotlin.Boolean**| Optional. Filter by recordings that are in progress, or not. | [optional]
 **seriesTimerId** | **kotlin.String**| Optional. Filter by recordings belonging to a series timer. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **isMovie** | **kotlin.Boolean**| Optional. Filter for movies. | [optional]
 **isSeries** | **kotlin.Boolean**| Optional. Filter for series. | [optional]
 **isKids** | **kotlin.Boolean**| Optional. Filter for kids. | [optional]
 **isSports** | **kotlin.Boolean**| Optional. Filter for sports. | [optional]
 **isNews** | **kotlin.Boolean**| Optional. Filter for news. | [optional]
 **isLibraryItem** | **kotlin.Boolean**| Optional. Filter for is library item. | [optional]
 **enableTotalRecordCount** | **kotlin.Boolean**| Optional. Return total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getRecordingsSeries"></a>
# **getRecordingsSeries**
> BaseItemDtoQueryResult getRecordingsSeries(channelId, userId, groupId, startIndex, limit, status, isInProgress, seriesTimerId, enableImages, imageTypeLimit, enableImageTypes, fields, enableUserData, enableTotalRecordCount)

Gets live tv recording series.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val channelId : kotlin.String = channelId_example // kotlin.String | Optional. Filter by channel id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user and attach user data.
val groupId : kotlin.String = groupId_example // kotlin.String | Optional. Filter by recording group.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val status : RecordingStatus =  // RecordingStatus | Optional. Filter by recording status.
val isInProgress : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by recordings that are in progress, or not.
val seriesTimerId : kotlin.String = seriesTimerId_example // kotlin.String | Optional. Filter by recordings belonging to a series timer.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Optional. Return total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getRecordingsSeries(channelId, userId, groupId, startIndex, limit, status, isInProgress, seriesTimerId, enableImages, imageTypeLimit, enableImageTypes, fields, enableUserData, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getRecordingsSeries")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getRecordingsSeries")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.String**| Optional. Filter by channel id. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user and attach user data. | [optional]
 **groupId** | **kotlin.String**| Optional. Filter by recording group. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **status** | [**RecordingStatus**](.md)| Optional. Filter by recording status. | [optional] [enum: New, InProgress, Completed, Cancelled, ConflictedOk, ConflictedNotOk, Error]
 **isInProgress** | **kotlin.Boolean**| Optional. Filter by recordings that are in progress, or not. | [optional]
 **seriesTimerId** | **kotlin.String**| Optional. Filter by recordings belonging to a series timer. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **enableTotalRecordCount** | **kotlin.Boolean**| Optional. Return total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSchedulesDirectCountries"></a>
# **getSchedulesDirectCountries**
> org.openapitools.client.infrastructure.OctetByteArray getSchedulesDirectCountries()

Gets available countries.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getSchedulesDirectCountries()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getSchedulesDirectCountries")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getSchedulesDirectCountries")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSeriesTimer"></a>
# **getSeriesTimer**
> SeriesTimerInfoDto getSeriesTimer(timerId)

Gets a live tv series timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val timerId : kotlin.String = timerId_example // kotlin.String | Timer id.
try {
    val result : SeriesTimerInfoDto = apiInstance.getSeriesTimer(timerId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getSeriesTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getSeriesTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **kotlin.String**| Timer id. |

### Return type

[**SeriesTimerInfoDto**](SeriesTimerInfoDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSeriesTimers"></a>
# **getSeriesTimers**
> SeriesTimerInfoDtoQueryResult getSeriesTimers(sortBy, sortOrder)

Gets live tv series timers.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val sortBy : kotlin.String = sortBy_example // kotlin.String | Optional. Sort by SortName or Priority.
val sortOrder : SortOrder =  // SortOrder | Optional. Sort in Ascending or Descending order.
try {
    val result : SeriesTimerInfoDtoQueryResult = apiInstance.getSeriesTimers(sortBy, sortOrder)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getSeriesTimers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getSeriesTimers")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sortBy** | **kotlin.String**| Optional. Sort by SortName or Priority. | [optional]
 **sortOrder** | [**SortOrder**](.md)| Optional. Sort in Ascending or Descending order. | [optional] [enum: Ascending, Descending]

### Return type

[**SeriesTimerInfoDtoQueryResult**](SeriesTimerInfoDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getTimer"></a>
# **getTimer**
> TimerInfoDto getTimer(timerId)

Gets a timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val timerId : kotlin.String = timerId_example // kotlin.String | Timer id.
try {
    val result : TimerInfoDto = apiInstance.getTimer(timerId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **kotlin.String**| Timer id. |

### Return type

[**TimerInfoDto**](TimerInfoDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getTimers"></a>
# **getTimers**
> TimerInfoDtoQueryResult getTimers(channelId, seriesTimerId, isActive, isScheduled)

Gets the live tv timers.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val channelId : kotlin.String = channelId_example // kotlin.String | Optional. Filter by channel id.
val seriesTimerId : kotlin.String = seriesTimerId_example // kotlin.String | Optional. Filter by timers belonging to a series timer.
val isActive : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by timers that are active.
val isScheduled : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by timers that are scheduled.
try {
    val result : TimerInfoDtoQueryResult = apiInstance.getTimers(channelId, seriesTimerId, isActive, isScheduled)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getTimers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getTimers")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.String**| Optional. Filter by channel id. | [optional]
 **seriesTimerId** | **kotlin.String**| Optional. Filter by timers belonging to a series timer. | [optional]
 **isActive** | **kotlin.Boolean**| Optional. Filter by timers that are active. | [optional]
 **isScheduled** | **kotlin.Boolean**| Optional. Filter by timers that are scheduled. | [optional]

### Return type

[**TimerInfoDtoQueryResult**](TimerInfoDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getTunerHostTypes"></a>
# **getTunerHostTypes**
> kotlin.collections.List&lt;NameIdPair&gt; getTunerHostTypes()

Get tuner host types.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
try {
    val result : kotlin.collections.List<NameIdPair> = apiInstance.getTunerHostTypes()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#getTunerHostTypes")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#getTunerHostTypes")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;NameIdPair&gt;**](NameIdPair.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="resetTuner"></a>
# **resetTuner**
> resetTuner(tunerId)

Resets a tv tuner.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val tunerId : kotlin.String = tunerId_example // kotlin.String | Tuner id.
try {
    apiInstance.resetTuner(tunerId)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#resetTuner")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#resetTuner")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tunerId** | **kotlin.String**| Tuner id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setChannelMapping"></a>
# **setChannelMapping**
> TunerChannelMapping setChannelMapping(setChannelMappingRequest)

Set channel mappings.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val setChannelMappingRequest : SetChannelMappingRequest =  // SetChannelMappingRequest | The set channel mapping dto.
try {
    val result : TunerChannelMapping = apiInstance.setChannelMapping(setChannelMappingRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#setChannelMapping")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#setChannelMapping")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setChannelMappingRequest** | [**SetChannelMappingRequest**](SetChannelMappingRequest.md)| The set channel mapping dto. |

### Return type

[**TunerChannelMapping**](TunerChannelMapping.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateSeriesTimer"></a>
# **updateSeriesTimer**
> updateSeriesTimer(timerId, createSeriesTimerRequest)

Updates a live tv series timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val timerId : kotlin.String = timerId_example // kotlin.String | Timer id.
val createSeriesTimerRequest : CreateSeriesTimerRequest =  // CreateSeriesTimerRequest | New series timer info.
try {
    apiInstance.updateSeriesTimer(timerId, createSeriesTimerRequest)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#updateSeriesTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#updateSeriesTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **kotlin.String**| Timer id. |
 **createSeriesTimerRequest** | [**CreateSeriesTimerRequest**](CreateSeriesTimerRequest.md)| New series timer info. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="updateTimer"></a>
# **updateTimer**
> updateTimer(timerId, createTimerRequest)

Updates a live tv timer.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LiveTvApi()
val timerId : kotlin.String = timerId_example // kotlin.String | Timer id.
val createTimerRequest : CreateTimerRequest =  // CreateTimerRequest | New timer info.
try {
    apiInstance.updateTimer(timerId, createTimerRequest)
} catch (e: ClientException) {
    println("4xx response calling LiveTvApi#updateTimer")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveTvApi#updateTimer")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **kotlin.String**| Timer id. |
 **createTimerRequest** | [**CreateTimerRequest**](CreateTimerRequest.md)| New timer info. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

