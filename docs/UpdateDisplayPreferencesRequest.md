
# UpdateDisplayPreferencesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | Gets or sets the user id. |  [optional]
**viewType** | **kotlin.String** | Gets or sets the type of the view. |  [optional]
**sortBy** | **kotlin.String** | Gets or sets the sort by. |  [optional]
**indexBy** | **kotlin.String** | Gets or sets the index by. |  [optional]
**rememberIndexing** | **kotlin.Boolean** | Gets or sets a value indicating whether [remember indexing]. |  [optional]
**primaryImageHeight** | **kotlin.Int** | Gets or sets the height of the primary image. |  [optional]
**primaryImageWidth** | **kotlin.Int** | Gets or sets the width of the primary image. |  [optional]
**customPrefs** | **kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;** | Gets or sets the custom prefs. |  [optional]
**scrollDirection** | [**ScrollDirection**](ScrollDirection.md) |  |  [optional]
**showBackdrop** | **kotlin.Boolean** | Gets or sets a value indicating whether to show backdrops on this item. |  [optional]
**rememberSorting** | **kotlin.Boolean** | Gets or sets a value indicating whether [remember sorting]. |  [optional]
**sortOrder** | [**SortOrder**](SortOrder.md) |  |  [optional]
**showSidebar** | **kotlin.Boolean** | Gets or sets a value indicating whether [show sidebar]. |  [optional]
**client** | **kotlin.String** | Gets or sets the client. |  [optional]



