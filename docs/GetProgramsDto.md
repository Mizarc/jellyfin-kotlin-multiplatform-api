
# GetProgramsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelIds** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the channels to return guide information for. |  [optional]
**userId** | **kotlin.String** | Gets or sets optional. Filter by user id. |  [optional]
**minStartDate** | **kotlin.String** | Gets or sets the minimum premiere start date.  Optional. |  [optional]
**hasAired** | **kotlin.Boolean** | Gets or sets filter by programs that have completed airing, or not.  Optional. |  [optional]
**isAiring** | **kotlin.Boolean** | Gets or sets filter by programs that are currently airing, or not.  Optional. |  [optional]
**maxStartDate** | **kotlin.String** | Gets or sets the maximum premiere start date.  Optional. |  [optional]
**minEndDate** | **kotlin.String** | Gets or sets the minimum premiere end date.  Optional. |  [optional]
**maxEndDate** | **kotlin.String** | Gets or sets the maximum premiere end date.  Optional. |  [optional]
**isMovie** | **kotlin.Boolean** | Gets or sets filter for movies.  Optional. |  [optional]
**isSeries** | **kotlin.Boolean** | Gets or sets filter for series.  Optional. |  [optional]
**isNews** | **kotlin.Boolean** | Gets or sets filter for news.  Optional. |  [optional]
**isKids** | **kotlin.Boolean** | Gets or sets filter for kids.  Optional. |  [optional]
**isSports** | **kotlin.Boolean** | Gets or sets filter for sports.  Optional. |  [optional]
**startIndex** | **kotlin.Int** | Gets or sets the record index to start at. All items with a lower index will be dropped from the results.  Optional. |  [optional]
**limit** | **kotlin.Int** | Gets or sets the maximum number of records to return.  Optional. |  [optional]
**sortBy** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets specify one or more sort orders, comma delimited. Options: Name, StartDate.  Optional. |  [optional]
**sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md) | Gets or sets sort Order - Ascending,Descending. |  [optional]
**genres** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the genres to return guide information for. |  [optional]
**genreIds** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the genre ids to return guide information for. |  [optional]
**enableImages** | **kotlin.Boolean** | Gets or sets include image information in output.  Optional. |  [optional]
**enableTotalRecordCount** | **kotlin.Boolean** | Gets or sets a value indicating whether retrieve total record count. |  [optional]
**imageTypeLimit** | **kotlin.Int** | Gets or sets the max number of images to return, per image type.  Optional. |  [optional]
**enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md) | Gets or sets the image types to include in the output.  Optional. |  [optional]
**enableUserData** | **kotlin.Boolean** | Gets or sets include user data.  Optional. |  [optional]
**seriesTimerId** | **kotlin.String** | Gets or sets filter by series timer id.  Optional. |  [optional]
**librarySeriesId** | **kotlin.String** | Gets or sets filter by library series id.  Optional. |  [optional]
**fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md) | Gets or sets specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines.  Optional. |  [optional]



