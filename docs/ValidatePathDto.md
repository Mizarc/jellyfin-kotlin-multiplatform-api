
# ValidatePathDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**validateWritable** | **kotlin.Boolean** | Gets or sets a value indicating whether validate if path is writable. |  [optional]
**path** | **kotlin.String** | Gets or sets the path. |  [optional]
**isFile** | **kotlin.Boolean** | Gets or sets is path file. |  [optional]



