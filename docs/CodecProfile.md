
# CodecProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**CodecType**](CodecType.md) |  |  [optional]
**conditions** | [**kotlin.collections.List&lt;ProfileCondition&gt;**](ProfileCondition.md) |  |  [optional]
**applyConditions** | [**kotlin.collections.List&lt;ProfileCondition&gt;**](ProfileCondition.md) |  |  [optional]
**codec** | **kotlin.String** |  |  [optional]
**container** | **kotlin.String** |  |  [optional]



