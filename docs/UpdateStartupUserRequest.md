
# UpdateStartupUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the username. |  [optional]
**password** | **kotlin.String** | Gets or sets the user&#39;s password. |  [optional]



