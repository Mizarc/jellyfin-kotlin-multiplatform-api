# ChannelsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllChannelFeatures**](ChannelsApi.md#getAllChannelFeatures) | **GET** /Channels/Features | Get all channel features.
[**getChannelFeatures**](ChannelsApi.md#getChannelFeatures) | **GET** /Channels/{channelId}/Features | Get channel features.
[**getChannelItems**](ChannelsApi.md#getChannelItems) | **GET** /Channels/{channelId}/Items | Get channel items.
[**getChannels**](ChannelsApi.md#getChannels) | **GET** /Channels | Gets available channels.
[**getLatestChannelItems**](ChannelsApi.md#getLatestChannelItems) | **GET** /Channels/Items/Latest | Gets latest channel items.


<a name="getAllChannelFeatures"></a>
# **getAllChannelFeatures**
> kotlin.collections.List&lt;ChannelFeatures&gt; getAllChannelFeatures()

Get all channel features.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ChannelsApi()
try {
    val result : kotlin.collections.List<ChannelFeatures> = apiInstance.getAllChannelFeatures()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ChannelsApi#getAllChannelFeatures")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ChannelsApi#getAllChannelFeatures")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;ChannelFeatures&gt;**](ChannelFeatures.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getChannelFeatures"></a>
# **getChannelFeatures**
> ChannelFeatures getChannelFeatures(channelId)

Get channel features.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ChannelsApi()
val channelId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Channel id.
try {
    val result : ChannelFeatures = apiInstance.getChannelFeatures(channelId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ChannelsApi#getChannelFeatures")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ChannelsApi#getChannelFeatures")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.String**| Channel id. |

### Return type

[**ChannelFeatures**](ChannelFeatures.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getChannelItems"></a>
# **getChannelItems**
> BaseItemDtoQueryResult getChannelItems(channelId, folderId, userId, startIndex, limit, sortOrder, filters, sortBy, fields)

Get channel items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ChannelsApi()
val channelId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Channel Id.
val folderId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Folder Id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. User Id.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val sortOrder : kotlin.collections.List<SortOrder> =  // kotlin.collections.List<SortOrder> | Optional. Sort Order - Ascending,Descending.
val filters : kotlin.collections.List<ItemFilter> =  // kotlin.collections.List<ItemFilter> | Optional. Specify additional filters to apply.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getChannelItems(channelId, folderId, userId, startIndex, limit, sortOrder, filters, sortBy, fields)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ChannelsApi#getChannelItems")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ChannelsApi#getChannelItems")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.String**| Channel Id. |
 **folderId** | **kotlin.String**| Optional. Folder Id. | [optional]
 **userId** | **kotlin.String**| Optional. User Id. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md)| Optional. Sort Order - Ascending,Descending. | [optional]
 **filters** | [**kotlin.collections.List&lt;ItemFilter&gt;**](ItemFilter.md)| Optional. Specify additional filters to apply. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getChannels"></a>
# **getChannels**
> BaseItemDtoQueryResult getChannels(userId, startIndex, limit, supportsLatestItems, supportsMediaDeletion, isFavorite)

Gets available channels.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ChannelsApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User Id to filter by. Use System.Guid.Empty to not filter by user.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val supportsLatestItems : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by channels that support getting latest items.
val supportsMediaDeletion : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by channels that support media deletion.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by channels that are favorite.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getChannels(userId, startIndex, limit, supportsLatestItems, supportsMediaDeletion, isFavorite)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ChannelsApi#getChannels")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ChannelsApi#getChannels")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| User Id to filter by. Use System.Guid.Empty to not filter by user. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **supportsLatestItems** | **kotlin.Boolean**| Optional. Filter by channels that support getting latest items. | [optional]
 **supportsMediaDeletion** | **kotlin.Boolean**| Optional. Filter by channels that support media deletion. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional. Filter by channels that are favorite. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLatestChannelItems"></a>
# **getLatestChannelItems**
> BaseItemDtoQueryResult getLatestChannelItems(userId, startIndex, limit, filters, fields, channelIds)

Gets latest channel items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ChannelsApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. User Id.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val filters : kotlin.collections.List<ItemFilter> =  // kotlin.collections.List<ItemFilter> | Optional. Specify additional filters to apply.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val channelIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more channel id's, comma delimited.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getLatestChannelItems(userId, startIndex, limit, filters, fields, channelIds)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ChannelsApi#getLatestChannelItems")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ChannelsApi#getLatestChannelItems")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. User Id. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **filters** | [**kotlin.collections.List&lt;ItemFilter&gt;**](ItemFilter.md)| Optional. Specify additional filters to apply. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **channelIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more channel id&#39;s, comma delimited. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

