# StartupApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**completeWizard**](StartupApi.md#completeWizard) | **POST** /Startup/Complete | Completes the startup wizard.
[**getFirstUser**](StartupApi.md#getFirstUser) | **GET** /Startup/User | Gets the first user.
[**getFirstUser2**](StartupApi.md#getFirstUser2) | **GET** /Startup/FirstUser | Gets the first user.
[**getStartupConfiguration**](StartupApi.md#getStartupConfiguration) | **GET** /Startup/Configuration | Gets the initial startup wizard configuration.
[**setRemoteAccess**](StartupApi.md#setRemoteAccess) | **POST** /Startup/RemoteAccess | Sets remote access and UPnP.
[**updateInitialConfiguration**](StartupApi.md#updateInitialConfiguration) | **POST** /Startup/Configuration | Sets the initial startup wizard configuration.
[**updateStartupUser**](StartupApi.md#updateStartupUser) | **POST** /Startup/User | Sets the user name and password.


<a name="completeWizard"></a>
# **completeWizard**
> completeWizard()

Completes the startup wizard.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StartupApi()
try {
    apiInstance.completeWizard()
} catch (e: ClientException) {
    println("4xx response calling StartupApi#completeWizard")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StartupApi#completeWizard")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getFirstUser"></a>
# **getFirstUser**
> StartupUserDto getFirstUser()

Gets the first user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StartupApi()
try {
    val result : StartupUserDto = apiInstance.getFirstUser()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling StartupApi#getFirstUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StartupApi#getFirstUser")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StartupUserDto**](StartupUserDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getFirstUser2"></a>
# **getFirstUser2**
> StartupUserDto getFirstUser2()

Gets the first user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StartupApi()
try {
    val result : StartupUserDto = apiInstance.getFirstUser2()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling StartupApi#getFirstUser2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StartupApi#getFirstUser2")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StartupUserDto**](StartupUserDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getStartupConfiguration"></a>
# **getStartupConfiguration**
> StartupConfigurationDto getStartupConfiguration()

Gets the initial startup wizard configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StartupApi()
try {
    val result : StartupConfigurationDto = apiInstance.getStartupConfiguration()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling StartupApi#getStartupConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StartupApi#getStartupConfiguration")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StartupConfigurationDto**](StartupConfigurationDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="setRemoteAccess"></a>
# **setRemoteAccess**
> setRemoteAccess(setRemoteAccessRequest)

Sets remote access and UPnP.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StartupApi()
val setRemoteAccessRequest : SetRemoteAccessRequest =  // SetRemoteAccessRequest | The startup remote access dto.
try {
    apiInstance.setRemoteAccess(setRemoteAccessRequest)
} catch (e: ClientException) {
    println("4xx response calling StartupApi#setRemoteAccess")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StartupApi#setRemoteAccess")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setRemoteAccessRequest** | [**SetRemoteAccessRequest**](SetRemoteAccessRequest.md)| The startup remote access dto. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="updateInitialConfiguration"></a>
# **updateInitialConfiguration**
> updateInitialConfiguration(updateInitialConfigurationRequest)

Sets the initial startup wizard configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StartupApi()
val updateInitialConfigurationRequest : UpdateInitialConfigurationRequest =  // UpdateInitialConfigurationRequest | The updated startup configuration.
try {
    apiInstance.updateInitialConfiguration(updateInitialConfigurationRequest)
} catch (e: ClientException) {
    println("4xx response calling StartupApi#updateInitialConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StartupApi#updateInitialConfiguration")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateInitialConfigurationRequest** | [**UpdateInitialConfigurationRequest**](UpdateInitialConfigurationRequest.md)| The updated startup configuration. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="updateStartupUser"></a>
# **updateStartupUser**
> updateStartupUser(updateStartupUserRequest)

Sets the user name and password.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = StartupApi()
val updateStartupUserRequest : UpdateStartupUserRequest =  // UpdateStartupUserRequest | The DTO containing username and password.
try {
    apiInstance.updateStartupUser(updateStartupUserRequest)
} catch (e: ClientException) {
    println("4xx response calling StartupApi#updateStartupUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling StartupApi#updateStartupUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateStartupUserRequest** | [**UpdateStartupUserRequest**](UpdateStartupUserRequest.md)| The DTO containing username and password. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

