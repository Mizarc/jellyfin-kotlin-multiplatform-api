
# DeviceOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | Gets the id. |  [optional] [readonly]
**deviceId** | **kotlin.String** | Gets the device id. |  [optional]
**customName** | **kotlin.String** | Gets or sets the custom name. |  [optional]



