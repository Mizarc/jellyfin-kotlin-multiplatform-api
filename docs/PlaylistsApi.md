# PlaylistsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addToPlaylist**](PlaylistsApi.md#addToPlaylist) | **POST** /Playlists/{playlistId}/Items | Adds items to a playlist.
[**createPlaylist**](PlaylistsApi.md#createPlaylist) | **POST** /Playlists | Creates a new playlist.
[**getPlaylistItems**](PlaylistsApi.md#getPlaylistItems) | **GET** /Playlists/{playlistId}/Items | Gets the original items of a playlist.
[**moveItem**](PlaylistsApi.md#moveItem) | **POST** /Playlists/{playlistId}/Items/{itemId}/Move/{newIndex} | Moves a playlist item.
[**removeFromPlaylist**](PlaylistsApi.md#removeFromPlaylist) | **DELETE** /Playlists/{playlistId}/Items | Removes items from a playlist.


<a name="addToPlaylist"></a>
# **addToPlaylist**
> addToPlaylist(playlistId, ids, userId)

Adds items to a playlist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaylistsApi()
val playlistId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The playlist id.
val ids : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Item id, comma delimited.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The userId.
try {
    apiInstance.addToPlaylist(playlistId, ids, userId)
} catch (e: ClientException) {
    println("4xx response calling PlaylistsApi#addToPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaylistsApi#addToPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.String**| The playlist id. |
 **ids** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Item id, comma delimited. | [optional]
 **userId** | **kotlin.String**| The userId. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createPlaylist"></a>
# **createPlaylist**
> PlaylistCreationResult createPlaylist(name, ids, userId, mediaType, createPlaylistRequest)

Creates a new playlist.

For backwards compatibility parameters can be sent via Query or Body, with Query having higher precedence.  Query parameters are obsolete.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaylistsApi()
val name : kotlin.String = name_example // kotlin.String | The playlist name.
val ids : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The item ids.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val mediaType : kotlin.String = mediaType_example // kotlin.String | The media type.
val createPlaylistRequest : CreatePlaylistRequest =  // CreatePlaylistRequest | The create playlist payload.
try {
    val result : PlaylistCreationResult = apiInstance.createPlaylist(name, ids, userId, mediaType, createPlaylistRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PlaylistsApi#createPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaylistsApi#createPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The playlist name. | [optional]
 **ids** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The item ids. | [optional]
 **userId** | **kotlin.String**| The user id. | [optional]
 **mediaType** | **kotlin.String**| The media type. | [optional]
 **createPlaylistRequest** | [**CreatePlaylistRequest**](CreatePlaylistRequest.md)| The create playlist payload. | [optional]

### Return type

[**PlaylistCreationResult**](PlaylistCreationResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPlaylistItems"></a>
# **getPlaylistItems**
> BaseItemDtoQueryResult getPlaylistItems(playlistId, userId, startIndex, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Gets the original items of a playlist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaylistsApi()
val playlistId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The playlist id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getPlaylistItems(playlistId, userId, startIndex, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PlaylistsApi#getPlaylistItems")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaylistsApi#getPlaylistItems")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.String**| The playlist id. |
 **userId** | **kotlin.String**| User id. |
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="moveItem"></a>
# **moveItem**
> moveItem(playlistId, itemId, newIndex)

Moves a playlist item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaylistsApi()
val playlistId : kotlin.String = playlistId_example // kotlin.String | The playlist id.
val itemId : kotlin.String = itemId_example // kotlin.String | The item id.
val newIndex : kotlin.Int = 56 // kotlin.Int | The new index.
try {
    apiInstance.moveItem(playlistId, itemId, newIndex)
} catch (e: ClientException) {
    println("4xx response calling PlaylistsApi#moveItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaylistsApi#moveItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.String**| The playlist id. |
 **itemId** | **kotlin.String**| The item id. |
 **newIndex** | **kotlin.Int**| The new index. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeFromPlaylist"></a>
# **removeFromPlaylist**
> removeFromPlaylist(playlistId, entryIds)

Removes items from a playlist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = PlaylistsApi()
val playlistId : kotlin.String = playlistId_example // kotlin.String | The playlist id.
val entryIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The item ids, comma delimited.
try {
    apiInstance.removeFromPlaylist(playlistId, entryIds)
} catch (e: ClientException) {
    println("4xx response calling PlaylistsApi#removeFromPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PlaylistsApi#removeFromPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.String**| The playlist id. |
 **entryIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The item ids, comma delimited. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

