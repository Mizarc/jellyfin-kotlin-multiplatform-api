
# SearchHintResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**searchHints** | [**kotlin.collections.List&lt;SearchHint&gt;**](SearchHint.md) | Gets the search hints. |  [optional]
**totalRecordCount** | **kotlin.Int** | Gets the total record count. |  [optional]



