
# QuickConnectResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authenticated** | **kotlin.Boolean** | Gets or sets a value indicating whether this request is authorized. |  [optional]
**secret** | **kotlin.String** | Gets the secret value used to uniquely identify this request. Can be used to retrieve authentication information. |  [optional]
**code** | **kotlin.String** | Gets the user facing code used so the user can quickly differentiate this request from others. |  [optional]
**deviceId** | **kotlin.String** | Gets the requesting device id. |  [optional]
**deviceName** | **kotlin.String** | Gets the requesting device name. |  [optional]
**appName** | **kotlin.String** | Gets the requesting app name. |  [optional]
**appVersion** | **kotlin.String** | Gets the requesting app version. |  [optional]
**dateAdded** | **kotlin.String** | Gets or sets the DateTime that this request was created. |  [optional]



