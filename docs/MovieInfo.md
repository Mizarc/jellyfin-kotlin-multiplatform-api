
# MovieInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**originalTitle** | **kotlin.String** | Gets or sets the original title. |  [optional]
**path** | **kotlin.String** | Gets or sets the path. |  [optional]
**metadataLanguage** | **kotlin.String** | Gets or sets the metadata language. |  [optional]
**metadataCountryCode** | **kotlin.String** | Gets or sets the metadata country code. |  [optional]
**providerIds** | **kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;** | Gets or sets the provider ids. |  [optional]
**year** | **kotlin.Int** | Gets or sets the year. |  [optional]
**indexNumber** | **kotlin.Int** |  |  [optional]
**parentIndexNumber** | **kotlin.Int** |  |  [optional]
**premiereDate** | **kotlin.String** |  |  [optional]
**isAutomated** | **kotlin.Boolean** |  |  [optional]



