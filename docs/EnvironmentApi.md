# EnvironmentApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDefaultDirectoryBrowser**](EnvironmentApi.md#getDefaultDirectoryBrowser) | **GET** /Environment/DefaultDirectoryBrowser | Get Default directory browser.
[**getDirectoryContents**](EnvironmentApi.md#getDirectoryContents) | **GET** /Environment/DirectoryContents | Gets the contents of a given directory in the file system.
[**getDrives**](EnvironmentApi.md#getDrives) | **GET** /Environment/Drives | Gets available drives from the server&#39;s file system.
[**getNetworkShares**](EnvironmentApi.md#getNetworkShares) | **GET** /Environment/NetworkShares | Gets network paths.
[**getParentPath**](EnvironmentApi.md#getParentPath) | **GET** /Environment/ParentPath | Gets the parent path of a given path.
[**validatePath**](EnvironmentApi.md#validatePath) | **POST** /Environment/ValidatePath | Validates path.


<a name="getDefaultDirectoryBrowser"></a>
# **getDefaultDirectoryBrowser**
> DefaultDirectoryBrowserInfoDto getDefaultDirectoryBrowser()

Get Default directory browser.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = EnvironmentApi()
try {
    val result : DefaultDirectoryBrowserInfoDto = apiInstance.getDefaultDirectoryBrowser()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling EnvironmentApi#getDefaultDirectoryBrowser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling EnvironmentApi#getDefaultDirectoryBrowser")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DefaultDirectoryBrowserInfoDto**](DefaultDirectoryBrowserInfoDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDirectoryContents"></a>
# **getDirectoryContents**
> kotlin.collections.List&lt;FileSystemEntryInfo&gt; getDirectoryContents(path, includeFiles, includeDirectories)

Gets the contents of a given directory in the file system.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = EnvironmentApi()
val path : kotlin.String = path_example // kotlin.String | The path.
val includeFiles : kotlin.Boolean = true // kotlin.Boolean | An optional filter to include or exclude files from the results. true/false.
val includeDirectories : kotlin.Boolean = true // kotlin.Boolean | An optional filter to include or exclude folders from the results. true/false.
try {
    val result : kotlin.collections.List<FileSystemEntryInfo> = apiInstance.getDirectoryContents(path, includeFiles, includeDirectories)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling EnvironmentApi#getDirectoryContents")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling EnvironmentApi#getDirectoryContents")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **kotlin.String**| The path. |
 **includeFiles** | **kotlin.Boolean**| An optional filter to include or exclude files from the results. true/false. | [optional] [default to false]
 **includeDirectories** | **kotlin.Boolean**| An optional filter to include or exclude folders from the results. true/false. | [optional] [default to false]

### Return type

[**kotlin.collections.List&lt;FileSystemEntryInfo&gt;**](FileSystemEntryInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDrives"></a>
# **getDrives**
> kotlin.collections.List&lt;FileSystemEntryInfo&gt; getDrives()

Gets available drives from the server&#39;s file system.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = EnvironmentApi()
try {
    val result : kotlin.collections.List<FileSystemEntryInfo> = apiInstance.getDrives()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling EnvironmentApi#getDrives")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling EnvironmentApi#getDrives")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;FileSystemEntryInfo&gt;**](FileSystemEntryInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getNetworkShares"></a>
# **getNetworkShares**
> kotlin.collections.List&lt;FileSystemEntryInfo&gt; getNetworkShares()

Gets network paths.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = EnvironmentApi()
try {
    val result : kotlin.collections.List<FileSystemEntryInfo> = apiInstance.getNetworkShares()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling EnvironmentApi#getNetworkShares")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling EnvironmentApi#getNetworkShares")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;FileSystemEntryInfo&gt;**](FileSystemEntryInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getParentPath"></a>
# **getParentPath**
> kotlin.String getParentPath(path)

Gets the parent path of a given path.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = EnvironmentApi()
val path : kotlin.String = path_example // kotlin.String | The path.
try {
    val result : kotlin.String = apiInstance.getParentPath(path)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling EnvironmentApi#getParentPath")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling EnvironmentApi#getParentPath")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **kotlin.String**| The path. |

### Return type

**kotlin.String**

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="validatePath"></a>
# **validatePath**
> validatePath(validatePathRequest)

Validates path.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = EnvironmentApi()
val validatePathRequest : ValidatePathRequest =  // ValidatePathRequest | Validate request object.
try {
    apiInstance.validatePath(validatePathRequest)
} catch (e: ClientException) {
    println("4xx response calling EnvironmentApi#validatePath")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling EnvironmentApi#validatePath")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **validatePathRequest** | [**ValidatePathRequest**](ValidatePathRequest.md)| Validate request object. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

