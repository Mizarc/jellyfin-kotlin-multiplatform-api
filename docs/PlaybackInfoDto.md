
# PlaybackInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **kotlin.String** | Gets or sets the playback userId. |  [optional]
**maxStreamingBitrate** | **kotlin.Int** | Gets or sets the max streaming bitrate. |  [optional]
**startTimeTicks** | **kotlin.Long** | Gets or sets the start time in ticks. |  [optional]
**audioStreamIndex** | **kotlin.Int** | Gets or sets the audio stream index. |  [optional]
**subtitleStreamIndex** | **kotlin.Int** | Gets or sets the subtitle stream index. |  [optional]
**maxAudioChannels** | **kotlin.Int** | Gets or sets the max audio channels. |  [optional]
**mediaSourceId** | **kotlin.String** | Gets or sets the media source id. |  [optional]
**liveStreamId** | **kotlin.String** | Gets or sets the live stream id. |  [optional]
**deviceProfile** | [**ClientCapabilitiesDtoDeviceProfile**](ClientCapabilitiesDtoDeviceProfile.md) |  |  [optional]
**enableDirectPlay** | **kotlin.Boolean** | Gets or sets a value indicating whether to enable direct play. |  [optional]
**enableDirectStream** | **kotlin.Boolean** | Gets or sets a value indicating whether to enable direct stream. |  [optional]
**enableTranscoding** | **kotlin.Boolean** | Gets or sets a value indicating whether to enable transcoding. |  [optional]
**allowVideoStreamCopy** | **kotlin.Boolean** | Gets or sets a value indicating whether to enable video stream copy. |  [optional]
**allowAudioStreamCopy** | **kotlin.Boolean** | Gets or sets a value indicating whether to allow audio stream copy. |  [optional]
**autoOpenLiveStream** | **kotlin.Boolean** | Gets or sets a value indicating whether to auto open the live stream. |  [optional]



