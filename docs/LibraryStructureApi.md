# LibraryStructureApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addMediaPath**](LibraryStructureApi.md#addMediaPath) | **POST** /Library/VirtualFolders/Paths | Add a media path to a library.
[**addVirtualFolder**](LibraryStructureApi.md#addVirtualFolder) | **POST** /Library/VirtualFolders | Adds a virtual folder.
[**getVirtualFolders**](LibraryStructureApi.md#getVirtualFolders) | **GET** /Library/VirtualFolders | Gets all virtual folders.
[**removeMediaPath**](LibraryStructureApi.md#removeMediaPath) | **DELETE** /Library/VirtualFolders/Paths | Remove a media path.
[**removeVirtualFolder**](LibraryStructureApi.md#removeVirtualFolder) | **DELETE** /Library/VirtualFolders | Removes a virtual folder.
[**renameVirtualFolder**](LibraryStructureApi.md#renameVirtualFolder) | **POST** /Library/VirtualFolders/Name | Renames a virtual folder.
[**updateLibraryOptions**](LibraryStructureApi.md#updateLibraryOptions) | **POST** /Library/VirtualFolders/LibraryOptions | Update library options.
[**updateMediaPath**](LibraryStructureApi.md#updateMediaPath) | **POST** /Library/VirtualFolders/Paths/Update | Updates a media path.


<a name="addMediaPath"></a>
# **addMediaPath**
> addMediaPath(addMediaPathRequest, refreshLibrary)

Add a media path to a library.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
val addMediaPathRequest : AddMediaPathRequest =  // AddMediaPathRequest | The media path dto.
val refreshLibrary : kotlin.Boolean = true // kotlin.Boolean | Whether to refresh the library.
try {
    apiInstance.addMediaPath(addMediaPathRequest, refreshLibrary)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#addMediaPath")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#addMediaPath")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addMediaPathRequest** | [**AddMediaPathRequest**](AddMediaPathRequest.md)| The media path dto. |
 **refreshLibrary** | **kotlin.Boolean**| Whether to refresh the library. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="addVirtualFolder"></a>
# **addVirtualFolder**
> addVirtualFolder(name, collectionType, paths, refreshLibrary, addVirtualFolderRequest)

Adds a virtual folder.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
val name : kotlin.String = name_example // kotlin.String | The name of the virtual folder.
val collectionType : CollectionTypeOptions =  // CollectionTypeOptions | The type of the collection.
val paths : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The paths of the virtual folder.
val refreshLibrary : kotlin.Boolean = true // kotlin.Boolean | Whether to refresh the library.
val addVirtualFolderRequest : AddVirtualFolderRequest =  // AddVirtualFolderRequest | The library options.
try {
    apiInstance.addVirtualFolder(name, collectionType, paths, refreshLibrary, addVirtualFolderRequest)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#addVirtualFolder")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#addVirtualFolder")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the virtual folder. | [optional]
 **collectionType** | [**CollectionTypeOptions**](.md)| The type of the collection. | [optional] [enum: Movies, TvShows, Music, MusicVideos, HomeVideos, BoxSets, Books, Mixed]
 **paths** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The paths of the virtual folder. | [optional]
 **refreshLibrary** | **kotlin.Boolean**| Whether to refresh the library. | [optional] [default to false]
 **addVirtualFolderRequest** | [**AddVirtualFolderRequest**](AddVirtualFolderRequest.md)| The library options. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="getVirtualFolders"></a>
# **getVirtualFolders**
> kotlin.collections.List&lt;VirtualFolderInfo&gt; getVirtualFolders()

Gets all virtual folders.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
try {
    val result : kotlin.collections.List<VirtualFolderInfo> = apiInstance.getVirtualFolders()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#getVirtualFolders")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#getVirtualFolders")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;VirtualFolderInfo&gt;**](VirtualFolderInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="removeMediaPath"></a>
# **removeMediaPath**
> removeMediaPath(name, path, refreshLibrary)

Remove a media path.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
val name : kotlin.String = name_example // kotlin.String | The name of the library.
val path : kotlin.String = path_example // kotlin.String | The path to remove.
val refreshLibrary : kotlin.Boolean = true // kotlin.Boolean | Whether to refresh the library.
try {
    apiInstance.removeMediaPath(name, path, refreshLibrary)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#removeMediaPath")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#removeMediaPath")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the library. | [optional]
 **path** | **kotlin.String**| The path to remove. | [optional]
 **refreshLibrary** | **kotlin.Boolean**| Whether to refresh the library. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeVirtualFolder"></a>
# **removeVirtualFolder**
> removeVirtualFolder(name, refreshLibrary)

Removes a virtual folder.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
val name : kotlin.String = name_example // kotlin.String | The name of the folder.
val refreshLibrary : kotlin.Boolean = true // kotlin.Boolean | Whether to refresh the library.
try {
    apiInstance.removeVirtualFolder(name, refreshLibrary)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#removeVirtualFolder")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#removeVirtualFolder")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the folder. | [optional]
 **refreshLibrary** | **kotlin.Boolean**| Whether to refresh the library. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="renameVirtualFolder"></a>
# **renameVirtualFolder**
> renameVirtualFolder(name, newName, refreshLibrary)

Renames a virtual folder.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
val name : kotlin.String = name_example // kotlin.String | The name of the virtual folder.
val newName : kotlin.String = newName_example // kotlin.String | The new name.
val refreshLibrary : kotlin.Boolean = true // kotlin.Boolean | Whether to refresh the library.
try {
    apiInstance.renameVirtualFolder(name, newName, refreshLibrary)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#renameVirtualFolder")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#renameVirtualFolder")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the virtual folder. | [optional]
 **newName** | **kotlin.String**| The new name. | [optional]
 **refreshLibrary** | **kotlin.Boolean**| Whether to refresh the library. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateLibraryOptions"></a>
# **updateLibraryOptions**
> updateLibraryOptions(updateLibraryOptionsRequest)

Update library options.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
val updateLibraryOptionsRequest : UpdateLibraryOptionsRequest =  // UpdateLibraryOptionsRequest | The library name and options.
try {
    apiInstance.updateLibraryOptions(updateLibraryOptionsRequest)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#updateLibraryOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#updateLibraryOptions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateLibraryOptionsRequest** | [**UpdateLibraryOptionsRequest**](UpdateLibraryOptionsRequest.md)| The library name and options. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="updateMediaPath"></a>
# **updateMediaPath**
> updateMediaPath(updateMediaPathRequest)

Updates a media path.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryStructureApi()
val updateMediaPathRequest : UpdateMediaPathRequest =  // UpdateMediaPathRequest | The name of the library and path infos.
try {
    apiInstance.updateMediaPath(updateMediaPathRequest)
} catch (e: ClientException) {
    println("4xx response calling LibraryStructureApi#updateMediaPath")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryStructureApi#updateMediaPath")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateMediaPathRequest** | [**UpdateMediaPathRequest**](UpdateMediaPathRequest.md)| The name of the library and path infos. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

