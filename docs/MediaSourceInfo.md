
# MediaSourceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**protocol** | [**MediaProtocol**](MediaProtocol.md) |  |  [optional]
**id** | **kotlin.String** |  |  [optional]
**path** | **kotlin.String** |  |  [optional]
**encoderPath** | **kotlin.String** |  |  [optional]
**encoderProtocol** | [**MediaProtocol**](MediaProtocol.md) |  |  [optional]
**type** | [**MediaSourceType**](MediaSourceType.md) |  |  [optional]
**container** | **kotlin.String** |  |  [optional]
**propertySize** | **kotlin.Long** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**isRemote** | **kotlin.Boolean** | Gets or sets a value indicating whether the media is remote.  Differentiate internet url vs local network. |  [optional]
**etag** | **kotlin.String** |  |  [optional]
**runTimeTicks** | **kotlin.Long** |  |  [optional]
**readAtNativeFramerate** | **kotlin.Boolean** |  |  [optional]
**ignoreDts** | **kotlin.Boolean** |  |  [optional]
**ignoreIndex** | **kotlin.Boolean** |  |  [optional]
**genPtsInput** | **kotlin.Boolean** |  |  [optional]
**supportsTranscoding** | **kotlin.Boolean** |  |  [optional]
**supportsDirectStream** | **kotlin.Boolean** |  |  [optional]
**supportsDirectPlay** | **kotlin.Boolean** |  |  [optional]
**isInfiniteStream** | **kotlin.Boolean** |  |  [optional]
**requiresOpening** | **kotlin.Boolean** |  |  [optional]
**openToken** | **kotlin.String** |  |  [optional]
**requiresClosing** | **kotlin.Boolean** |  |  [optional]
**liveStreamId** | **kotlin.String** |  |  [optional]
**bufferMs** | **kotlin.Int** |  |  [optional]
**requiresLooping** | **kotlin.Boolean** |  |  [optional]
**supportsProbing** | **kotlin.Boolean** |  |  [optional]
**videoType** | [**VideoType**](VideoType.md) |  |  [optional]
**isoType** | [**IsoType**](IsoType.md) |  |  [optional]
**video3DFormat** | [**Video3DFormat**](Video3DFormat.md) |  |  [optional]
**mediaStreams** | [**kotlin.collections.List&lt;MediaStream&gt;**](MediaStream.md) |  |  [optional]
**mediaAttachments** | [**kotlin.collections.List&lt;MediaAttachment&gt;**](MediaAttachment.md) |  |  [optional]
**formats** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**bitrate** | **kotlin.Int** |  |  [optional]
**timestamp** | [**TransportStreamTimestamp**](TransportStreamTimestamp.md) |  |  [optional]
**requiredHttpHeaders** | **kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;** |  |  [optional]
**transcodingUrl** | **kotlin.String** |  |  [optional]
**transcodingSubProtocol** | **kotlin.String** |  |  [optional]
**transcodingContainer** | **kotlin.String** |  |  [optional]
**analyzeDurationMs** | **kotlin.Int** |  |  [optional]
**defaultAudioStreamIndex** | **kotlin.Int** |  |  [optional]
**defaultSubtitleStreamIndex** | **kotlin.Int** |  |  [optional]



