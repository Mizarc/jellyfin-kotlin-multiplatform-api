
# ControlResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**headers** | **kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;** |  |  [optional] [readonly]
**xml** | **kotlin.String** |  |  [optional]
**isSuccessful** | **kotlin.Boolean** |  |  [optional]



