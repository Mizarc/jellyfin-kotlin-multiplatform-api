
# GroupUpdateType

## Enum


    * `userJoined` (value: `"UserJoined"`)

    * `userLeft` (value: `"UserLeft"`)

    * `groupJoined` (value: `"GroupJoined"`)

    * `groupLeft` (value: `"GroupLeft"`)

    * `stateUpdate` (value: `"StateUpdate"`)

    * `playQueue` (value: `"PlayQueue"`)

    * `notInGroup` (value: `"NotInGroup"`)

    * `groupDoesNotExist` (value: `"GroupDoesNotExist"`)

    * `createGroupDenied` (value: `"CreateGroupDenied"`)

    * `joinGroupDenied` (value: `"JoinGroupDenied"`)

    * `libraryAccessDenied` (value: `"LibraryAccessDenied"`)



