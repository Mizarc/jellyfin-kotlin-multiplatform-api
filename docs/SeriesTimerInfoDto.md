
# SeriesTimerInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | Gets or sets the Id of the recording. |  [optional]
**type** | **kotlin.String** |  |  [optional]
**serverId** | **kotlin.String** | Gets or sets the server identifier. |  [optional]
**externalId** | **kotlin.String** | Gets or sets the external identifier. |  [optional]
**channelId** | **kotlin.String** | Gets or sets the channel id of the recording. |  [optional]
**externalChannelId** | **kotlin.String** | Gets or sets the external channel identifier. |  [optional]
**channelName** | **kotlin.String** | Gets or sets the channel name of the recording. |  [optional]
**channelPrimaryImageTag** | **kotlin.String** |  |  [optional]
**programId** | **kotlin.String** | Gets or sets the program identifier. |  [optional]
**externalProgramId** | **kotlin.String** | Gets or sets the external program identifier. |  [optional]
**name** | **kotlin.String** | Gets or sets the name of the recording. |  [optional]
**overview** | **kotlin.String** | Gets or sets the description of the recording. |  [optional]
**startDate** | **kotlin.String** | Gets or sets the start date of the recording, in UTC. |  [optional]
**endDate** | **kotlin.String** | Gets or sets the end date of the recording, in UTC. |  [optional]
**serviceName** | **kotlin.String** | Gets or sets the name of the service. |  [optional]
**priority** | **kotlin.Int** | Gets or sets the priority. |  [optional]
**prePaddingSeconds** | **kotlin.Int** | Gets or sets the pre padding seconds. |  [optional]
**postPaddingSeconds** | **kotlin.Int** | Gets or sets the post padding seconds. |  [optional]
**isPrePaddingRequired** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is pre padding required. |  [optional]
**parentBackdropItemId** | **kotlin.String** | Gets or sets the Id of the Parent that has a backdrop if the item does not have one. |  [optional]
**parentBackdropImageTags** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the parent backdrop image tags. |  [optional]
**isPostPaddingRequired** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is post padding required. |  [optional]
**keepUntil** | [**KeepUntil**](KeepUntil.md) |  |  [optional]
**recordAnyTime** | **kotlin.Boolean** | Gets or sets a value indicating whether [record any time]. |  [optional]
**skipEpisodesInLibrary** | **kotlin.Boolean** |  |  [optional]
**recordAnyChannel** | **kotlin.Boolean** | Gets or sets a value indicating whether [record any channel]. |  [optional]
**keepUpTo** | **kotlin.Int** |  |  [optional]
**recordNewOnly** | **kotlin.Boolean** | Gets or sets a value indicating whether [record new only]. |  [optional]
**days** | [**kotlin.collections.List&lt;DayOfWeek&gt;**](DayOfWeek.md) | Gets or sets the days. |  [optional]
**dayPattern** | [**DayPattern**](DayPattern.md) |  |  [optional]
**imageTags** | **kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;** | Gets or sets the image tags. |  [optional]
**parentThumbItemId** | **kotlin.String** | Gets or sets the parent thumb item id. |  [optional]
**parentThumbImageTag** | **kotlin.String** | Gets or sets the parent thumb image tag. |  [optional]
**parentPrimaryImageItemId** | **kotlin.String** | Gets or sets the parent primary image item identifier. |  [optional]
**parentPrimaryImageTag** | **kotlin.String** | Gets or sets the parent primary image tag. |  [optional]



