
# PlayRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playingQueue** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the playing queue. |  [optional]
**playingItemPosition** | **kotlin.Int** | Gets or sets the position of the playing item in the queue. |  [optional]
**startPositionTicks** | **kotlin.Long** | Gets or sets the start position ticks. |  [optional]



