# SystemApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEndpointInfo**](SystemApi.md#getEndpointInfo) | **GET** /System/Endpoint | Gets information about the request endpoint.
[**getLogFile**](SystemApi.md#getLogFile) | **GET** /System/Logs/Log | Gets a log file.
[**getPingSystem**](SystemApi.md#getPingSystem) | **GET** /System/Ping | Pings the system.
[**getPublicSystemInfo**](SystemApi.md#getPublicSystemInfo) | **GET** /System/Info/Public | Gets public information about the server.
[**getServerLogs**](SystemApi.md#getServerLogs) | **GET** /System/Logs | Gets a list of available server log files.
[**getSystemInfo**](SystemApi.md#getSystemInfo) | **GET** /System/Info | Gets information about the server.
[**getWakeOnLanInfo**](SystemApi.md#getWakeOnLanInfo) | **GET** /System/WakeOnLanInfo | Gets wake on lan information.
[**postPingSystem**](SystemApi.md#postPingSystem) | **POST** /System/Ping | Pings the system.
[**restartApplication**](SystemApi.md#restartApplication) | **POST** /System/Restart | Restarts the application.
[**shutdownApplication**](SystemApi.md#shutdownApplication) | **POST** /System/Shutdown | Shuts down the application.


<a name="getEndpointInfo"></a>
# **getEndpointInfo**
> EndPointInfo getEndpointInfo()

Gets information about the request endpoint.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    val result : EndPointInfo = apiInstance.getEndpointInfo()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#getEndpointInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#getEndpointInfo")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EndPointInfo**](EndPointInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLogFile"></a>
# **getLogFile**
> org.openapitools.client.infrastructure.OctetByteArray getLogFile(name)

Gets a log file.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
val name : kotlin.String = name_example // kotlin.String | The name of the log file to get.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getLogFile(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#getLogFile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#getLogFile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the log file to get. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getPingSystem"></a>
# **getPingSystem**
> kotlin.String getPingSystem()

Pings the system.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    val result : kotlin.String = apiInstance.getPingSystem()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#getPingSystem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#getPingSystem")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPublicSystemInfo"></a>
# **getPublicSystemInfo**
> PublicSystemInfo getPublicSystemInfo()

Gets public information about the server.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    val result : PublicSystemInfo = apiInstance.getPublicSystemInfo()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#getPublicSystemInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#getPublicSystemInfo")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PublicSystemInfo**](PublicSystemInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getServerLogs"></a>
# **getServerLogs**
> kotlin.collections.List&lt;LogFile&gt; getServerLogs()

Gets a list of available server log files.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    val result : kotlin.collections.List<LogFile> = apiInstance.getServerLogs()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#getServerLogs")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#getServerLogs")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;LogFile&gt;**](LogFile.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSystemInfo"></a>
# **getSystemInfo**
> SystemInfo getSystemInfo()

Gets information about the server.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    val result : SystemInfo = apiInstance.getSystemInfo()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#getSystemInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#getSystemInfo")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SystemInfo**](SystemInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getWakeOnLanInfo"></a>
# **getWakeOnLanInfo**
> kotlin.collections.List&lt;WakeOnLanInfo&gt; getWakeOnLanInfo()

Gets wake on lan information.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    val result : kotlin.collections.List<WakeOnLanInfo> = apiInstance.getWakeOnLanInfo()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#getWakeOnLanInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#getWakeOnLanInfo")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;WakeOnLanInfo&gt;**](WakeOnLanInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="postPingSystem"></a>
# **postPingSystem**
> kotlin.String postPingSystem()

Pings the system.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    val result : kotlin.String = apiInstance.postPingSystem()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SystemApi#postPingSystem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#postPingSystem")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="restartApplication"></a>
# **restartApplication**
> restartApplication()

Restarts the application.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    apiInstance.restartApplication()
} catch (e: ClientException) {
    println("4xx response calling SystemApi#restartApplication")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#restartApplication")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="shutdownApplication"></a>
# **shutdownApplication**
> shutdownApplication()

Shuts down the application.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SystemApi()
try {
    apiInstance.shutdownApplication()
} catch (e: ClientException) {
    println("4xx response calling SystemApi#shutdownApplication")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SystemApi#shutdownApplication")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

