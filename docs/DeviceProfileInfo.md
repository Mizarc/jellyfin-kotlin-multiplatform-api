
# DeviceProfileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | Gets or sets the identifier. |  [optional]
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**type** | [**DeviceProfileType**](DeviceProfileType.md) |  |  [optional]



