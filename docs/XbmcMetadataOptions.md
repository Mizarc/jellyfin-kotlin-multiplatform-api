
# XbmcMetadataOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **kotlin.String** |  |  [optional]
**releaseDateFormat** | **kotlin.String** |  |  [optional]
**saveImagePathsInNfo** | **kotlin.Boolean** |  |  [optional]
**enablePathSubstitution** | **kotlin.Boolean** |  |  [optional]
**enableExtraThumbsDuplication** | **kotlin.Boolean** |  |  [optional]



