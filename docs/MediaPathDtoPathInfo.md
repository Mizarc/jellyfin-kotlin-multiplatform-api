
# MediaPathDtoPathInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **kotlin.String** |  |  [optional]
**networkPath** | **kotlin.String** |  |  [optional]



