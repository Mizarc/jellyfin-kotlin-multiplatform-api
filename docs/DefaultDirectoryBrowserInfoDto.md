
# DefaultDirectoryBrowserInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **kotlin.String** | Gets or sets the path. |  [optional]



