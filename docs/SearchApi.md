# SearchApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](SearchApi.md#get) | **GET** /Search/Hints | Gets the search hint result.


<a name="get"></a>
# **get**
> SearchHintResult get(searchTerm, startIndex, limit, userId, includeItemTypes, excludeItemTypes, mediaTypes, parentId, isMovie, isSeries, isNews, isKids, isSports, includePeople, includeMedia, includeGenres, includeStudios, includeArtists)

Gets the search hint result.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SearchApi()
val searchTerm : kotlin.String = searchTerm_example // kotlin.String | The search term to filter on.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Supply a user id to search within a user's library or omit to search all.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | If specified, only results with the specified item types are returned. This allows multiple, comma delimeted.
val excludeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | If specified, results with these item types are filtered out. This allows multiple, comma delimeted.
val mediaTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | If specified, only results with the specified media types are returned. This allows multiple, comma delimeted.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | If specified, only children of the parent are returned.
val isMovie : kotlin.Boolean = true // kotlin.Boolean | Optional filter for movies.
val isSeries : kotlin.Boolean = true // kotlin.Boolean | Optional filter for series.
val isNews : kotlin.Boolean = true // kotlin.Boolean | Optional filter for news.
val isKids : kotlin.Boolean = true // kotlin.Boolean | Optional filter for kids.
val isSports : kotlin.Boolean = true // kotlin.Boolean | Optional filter for sports.
val includePeople : kotlin.Boolean = true // kotlin.Boolean | Optional filter whether to include people.
val includeMedia : kotlin.Boolean = true // kotlin.Boolean | Optional filter whether to include media.
val includeGenres : kotlin.Boolean = true // kotlin.Boolean | Optional filter whether to include genres.
val includeStudios : kotlin.Boolean = true // kotlin.Boolean | Optional filter whether to include studios.
val includeArtists : kotlin.Boolean = true // kotlin.Boolean | Optional filter whether to include artists.
try {
    val result : SearchHintResult = apiInstance.get(searchTerm, startIndex, limit, userId, includeItemTypes, excludeItemTypes, mediaTypes, parentId, isMovie, isSeries, isNews, isKids, isSports, includePeople, includeMedia, includeGenres, includeStudios, includeArtists)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SearchApi#get")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SearchApi#get")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchTerm** | **kotlin.String**| The search term to filter on. |
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **userId** | **kotlin.String**| Optional. Supply a user id to search within a user&#39;s library or omit to search all. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| If specified, only results with the specified item types are returned. This allows multiple, comma delimeted. | [optional]
 **excludeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| If specified, results with these item types are filtered out. This allows multiple, comma delimeted. | [optional]
 **mediaTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| If specified, only results with the specified media types are returned. This allows multiple, comma delimeted. | [optional]
 **parentId** | **kotlin.String**| If specified, only children of the parent are returned. | [optional]
 **isMovie** | **kotlin.Boolean**| Optional filter for movies. | [optional]
 **isSeries** | **kotlin.Boolean**| Optional filter for series. | [optional]
 **isNews** | **kotlin.Boolean**| Optional filter for news. | [optional]
 **isKids** | **kotlin.Boolean**| Optional filter for kids. | [optional]
 **isSports** | **kotlin.Boolean**| Optional filter for sports. | [optional]
 **includePeople** | **kotlin.Boolean**| Optional filter whether to include people. | [optional] [default to true]
 **includeMedia** | **kotlin.Boolean**| Optional filter whether to include media. | [optional] [default to true]
 **includeGenres** | **kotlin.Boolean**| Optional filter whether to include genres. | [optional] [default to true]
 **includeStudios** | **kotlin.Boolean**| Optional filter whether to include studios. | [optional] [default to true]
 **includeArtists** | **kotlin.Boolean**| Optional filter whether to include artists. | [optional] [default to true]

### Return type

[**SearchHintResult**](SearchHintResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

