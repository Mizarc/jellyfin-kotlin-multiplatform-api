# LibraryApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteItem**](LibraryApi.md#deleteItem) | **DELETE** /Items/{itemId} | Deletes an item from the library and filesystem.
[**deleteItems**](LibraryApi.md#deleteItems) | **DELETE** /Items | Deletes items from the library and filesystem.
[**getAncestors**](LibraryApi.md#getAncestors) | **GET** /Items/{itemId}/Ancestors | Gets all parents of an item.
[**getCriticReviews**](LibraryApi.md#getCriticReviews) | **GET** /Items/{itemId}/CriticReviews | Gets critic review for an item.
[**getDownload**](LibraryApi.md#getDownload) | **GET** /Items/{itemId}/Download | Downloads item media.
[**getFile**](LibraryApi.md#getFile) | **GET** /Items/{itemId}/File | Get the original file of an item.
[**getItemCounts**](LibraryApi.md#getItemCounts) | **GET** /Items/Counts | Get item counts.
[**getLibraryOptionsInfo**](LibraryApi.md#getLibraryOptionsInfo) | **GET** /Libraries/AvailableOptions | Gets the library options info.
[**getMediaFolders**](LibraryApi.md#getMediaFolders) | **GET** /Library/MediaFolders | Gets all user media folders.
[**getPhysicalPaths**](LibraryApi.md#getPhysicalPaths) | **GET** /Library/PhysicalPaths | Gets a list of physical paths from virtual folders.
[**getSimilarAlbums**](LibraryApi.md#getSimilarAlbums) | **GET** /Albums/{itemId}/Similar | Gets similar items.
[**getSimilarArtists**](LibraryApi.md#getSimilarArtists) | **GET** /Artists/{itemId}/Similar | Gets similar items.
[**getSimilarItems**](LibraryApi.md#getSimilarItems) | **GET** /Items/{itemId}/Similar | Gets similar items.
[**getSimilarMovies**](LibraryApi.md#getSimilarMovies) | **GET** /Movies/{itemId}/Similar | Gets similar items.
[**getSimilarShows**](LibraryApi.md#getSimilarShows) | **GET** /Shows/{itemId}/Similar | Gets similar items.
[**getSimilarTrailers**](LibraryApi.md#getSimilarTrailers) | **GET** /Trailers/{itemId}/Similar | Gets similar items.
[**getThemeMedia**](LibraryApi.md#getThemeMedia) | **GET** /Items/{itemId}/ThemeMedia | Get theme songs and videos for an item.
[**getThemeSongs**](LibraryApi.md#getThemeSongs) | **GET** /Items/{itemId}/ThemeSongs | Get theme songs for an item.
[**getThemeVideos**](LibraryApi.md#getThemeVideos) | **GET** /Items/{itemId}/ThemeVideos | Get theme videos for an item.
[**postAddedMovies**](LibraryApi.md#postAddedMovies) | **POST** /Library/Movies/Added | Reports that new movies have been added by an external source.
[**postAddedSeries**](LibraryApi.md#postAddedSeries) | **POST** /Library/Series/Added | Reports that new episodes of a series have been added by an external source.
[**postUpdatedMedia**](LibraryApi.md#postUpdatedMedia) | **POST** /Library/Media/Updated | Reports that new movies have been added by an external source.
[**postUpdatedMovies**](LibraryApi.md#postUpdatedMovies) | **POST** /Library/Movies/Updated | Reports that new movies have been added by an external source.
[**postUpdatedSeries**](LibraryApi.md#postUpdatedSeries) | **POST** /Library/Series/Updated | Reports that new episodes of a series have been added by an external source.
[**refreshLibrary**](LibraryApi.md#refreshLibrary) | **POST** /Library/Refresh | Starts a library scan.


<a name="deleteItem"></a>
# **deleteItem**
> deleteItem(itemId)

Deletes an item from the library and filesystem.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
try {
    apiInstance.deleteItem(itemId)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#deleteItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#deleteItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="deleteItems"></a>
# **deleteItems**
> deleteItems(ids)

Deletes items from the library and filesystem.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val ids : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The item ids.
try {
    apiInstance.deleteItems(ids)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#deleteItems")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#deleteItems")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The item ids. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getAncestors"></a>
# **getAncestors**
> kotlin.collections.List&lt;BaseItemDto&gt; getAncestors(itemId, userId)

Gets all parents of an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
try {
    val result : kotlin.collections.List<BaseItemDto> = apiInstance.getAncestors(itemId, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getAncestors")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getAncestors")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]

### Return type

[**kotlin.collections.List&lt;BaseItemDto&gt;**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getCriticReviews"></a>
# **getCriticReviews**
> BaseItemDtoQueryResult getCriticReviews(itemId)

Gets critic review for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = itemId_example // kotlin.String | 
try {
    val result : BaseItemDtoQueryResult = apiInstance.getCriticReviews(itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getCriticReviews")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getCriticReviews")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**|  |

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDownload"></a>
# **getDownload**
> org.openapitools.client.infrastructure.OctetByteArray getDownload(itemId)

Downloads item media.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getDownload(itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getDownload")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getDownload")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: video/*, audio/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getFile"></a>
# **getFile**
> org.openapitools.client.infrastructure.OctetByteArray getFile(itemId)

Get the original file of an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getFile(itemId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getFile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getFile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: video/*, audio/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getItemCounts"></a>
# **getItemCounts**
> ItemCounts getItemCounts(userId, isFavorite)

Get item counts.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Get counts from a specific user's library.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional. Get counts of favorite items.
try {
    val result : ItemCounts = apiInstance.getItemCounts(userId, isFavorite)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getItemCounts")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getItemCounts")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| Optional. Get counts from a specific user&#39;s library. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional. Get counts of favorite items. | [optional]

### Return type

[**ItemCounts**](ItemCounts.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getLibraryOptionsInfo"></a>
# **getLibraryOptionsInfo**
> LibraryOptionsResultDto getLibraryOptionsInfo(libraryContentType, isNewLibrary)

Gets the library options info.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val libraryContentType : kotlin.String = libraryContentType_example // kotlin.String | Library content type.
val isNewLibrary : kotlin.Boolean = true // kotlin.Boolean | Whether this is a new library.
try {
    val result : LibraryOptionsResultDto = apiInstance.getLibraryOptionsInfo(libraryContentType, isNewLibrary)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getLibraryOptionsInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getLibraryOptionsInfo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **libraryContentType** | **kotlin.String**| Library content type. | [optional]
 **isNewLibrary** | **kotlin.Boolean**| Whether this is a new library. | [optional] [default to false]

### Return type

[**LibraryOptionsResultDto**](LibraryOptionsResultDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMediaFolders"></a>
# **getMediaFolders**
> BaseItemDtoQueryResult getMediaFolders(isHidden)

Gets all user media folders.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val isHidden : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by folders that are marked hidden, or not.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getMediaFolders(isHidden)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getMediaFolders")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getMediaFolders")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isHidden** | **kotlin.Boolean**| Optional. Filter by folders that are marked hidden, or not. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPhysicalPaths"></a>
# **getPhysicalPaths**
> kotlin.collections.List&lt;kotlin.String&gt; getPhysicalPaths()

Gets a list of physical paths from virtual folders.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
try {
    val result : kotlin.collections.List<kotlin.String> = apiInstance.getPhysicalPaths()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getPhysicalPaths")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getPhysicalPaths")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.collections.List&lt;kotlin.String&gt;**

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSimilarAlbums"></a>
# **getSimilarAlbums**
> BaseItemDtoQueryResult getSimilarAlbums(itemId, excludeArtistIds, userId, limit, fields)

Gets similar items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val excludeArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Exclude artist ids.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSimilarAlbums(itemId, excludeArtistIds, userId, limit, fields)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getSimilarAlbums")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getSimilarAlbums")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **excludeArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Exclude artist ids. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSimilarArtists"></a>
# **getSimilarArtists**
> BaseItemDtoQueryResult getSimilarArtists(itemId, excludeArtistIds, userId, limit, fields)

Gets similar items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val excludeArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Exclude artist ids.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSimilarArtists(itemId, excludeArtistIds, userId, limit, fields)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getSimilarArtists")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getSimilarArtists")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **excludeArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Exclude artist ids. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSimilarItems"></a>
# **getSimilarItems**
> BaseItemDtoQueryResult getSimilarItems(itemId, excludeArtistIds, userId, limit, fields)

Gets similar items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val excludeArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Exclude artist ids.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSimilarItems(itemId, excludeArtistIds, userId, limit, fields)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getSimilarItems")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getSimilarItems")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **excludeArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Exclude artist ids. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSimilarMovies"></a>
# **getSimilarMovies**
> BaseItemDtoQueryResult getSimilarMovies(itemId, excludeArtistIds, userId, limit, fields)

Gets similar items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val excludeArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Exclude artist ids.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSimilarMovies(itemId, excludeArtistIds, userId, limit, fields)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getSimilarMovies")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getSimilarMovies")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **excludeArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Exclude artist ids. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSimilarShows"></a>
# **getSimilarShows**
> BaseItemDtoQueryResult getSimilarShows(itemId, excludeArtistIds, userId, limit, fields)

Gets similar items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val excludeArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Exclude artist ids.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSimilarShows(itemId, excludeArtistIds, userId, limit, fields)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getSimilarShows")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getSimilarShows")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **excludeArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Exclude artist ids. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSimilarTrailers"></a>
# **getSimilarTrailers**
> BaseItemDtoQueryResult getSimilarTrailers(itemId, excludeArtistIds, userId, limit, fields)

Gets similar items.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val excludeArtistIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Exclude artist ids.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSimilarTrailers(itemId, excludeArtistIds, userId, limit, fields)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getSimilarTrailers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getSimilarTrailers")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **excludeArtistIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Exclude artist ids. | [optional]
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getThemeMedia"></a>
# **getThemeMedia**
> AllThemeMediaResult getThemeMedia(itemId, userId, inheritFromParent)

Get theme songs and videos for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val inheritFromParent : kotlin.Boolean = true // kotlin.Boolean | Optional. Determines whether or not parent items should be searched for theme media.
try {
    val result : AllThemeMediaResult = apiInstance.getThemeMedia(itemId, userId, inheritFromParent)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getThemeMedia")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getThemeMedia")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **inheritFromParent** | **kotlin.Boolean**| Optional. Determines whether or not parent items should be searched for theme media. | [optional] [default to false]

### Return type

[**AllThemeMediaResult**](AllThemeMediaResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getThemeSongs"></a>
# **getThemeSongs**
> ThemeMediaResult getThemeSongs(itemId, userId, inheritFromParent)

Get theme songs for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val inheritFromParent : kotlin.Boolean = true // kotlin.Boolean | Optional. Determines whether or not parent items should be searched for theme media.
try {
    val result : ThemeMediaResult = apiInstance.getThemeSongs(itemId, userId, inheritFromParent)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getThemeSongs")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getThemeSongs")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **inheritFromParent** | **kotlin.Boolean**| Optional. Determines whether or not parent items should be searched for theme media. | [optional] [default to false]

### Return type

[**ThemeMediaResult**](ThemeMediaResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getThemeVideos"></a>
# **getThemeVideos**
> ThemeMediaResult getThemeVideos(itemId, userId, inheritFromParent)

Get theme videos for an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val inheritFromParent : kotlin.Boolean = true // kotlin.Boolean | Optional. Determines whether or not parent items should be searched for theme media.
try {
    val result : ThemeMediaResult = apiInstance.getThemeVideos(itemId, userId, inheritFromParent)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#getThemeVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#getThemeVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **inheritFromParent** | **kotlin.Boolean**| Optional. Determines whether or not parent items should be searched for theme media. | [optional] [default to false]

### Return type

[**ThemeMediaResult**](ThemeMediaResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="postAddedMovies"></a>
# **postAddedMovies**
> postAddedMovies(tmdbId, imdbId)

Reports that new movies have been added by an external source.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val tmdbId : kotlin.String = tmdbId_example // kotlin.String | The tmdbId.
val imdbId : kotlin.String = imdbId_example // kotlin.String | The imdbId.
try {
    apiInstance.postAddedMovies(tmdbId, imdbId)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#postAddedMovies")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#postAddedMovies")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tmdbId** | **kotlin.String**| The tmdbId. | [optional]
 **imdbId** | **kotlin.String**| The imdbId. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postAddedSeries"></a>
# **postAddedSeries**
> postAddedSeries(tvdbId)

Reports that new episodes of a series have been added by an external source.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val tvdbId : kotlin.String = tvdbId_example // kotlin.String | The tvdbId.
try {
    apiInstance.postAddedSeries(tvdbId)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#postAddedSeries")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#postAddedSeries")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tvdbId** | **kotlin.String**| The tvdbId. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postUpdatedMedia"></a>
# **postUpdatedMedia**
> postUpdatedMedia(postUpdatedMediaRequest)

Reports that new movies have been added by an external source.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val postUpdatedMediaRequest : PostUpdatedMediaRequest =  // PostUpdatedMediaRequest | The update paths.
try {
    apiInstance.postUpdatedMedia(postUpdatedMediaRequest)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#postUpdatedMedia")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#postUpdatedMedia")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postUpdatedMediaRequest** | [**PostUpdatedMediaRequest**](PostUpdatedMediaRequest.md)| The update paths. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="postUpdatedMovies"></a>
# **postUpdatedMovies**
> postUpdatedMovies(tmdbId, imdbId)

Reports that new movies have been added by an external source.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val tmdbId : kotlin.String = tmdbId_example // kotlin.String | The tmdbId.
val imdbId : kotlin.String = imdbId_example // kotlin.String | The imdbId.
try {
    apiInstance.postUpdatedMovies(tmdbId, imdbId)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#postUpdatedMovies")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#postUpdatedMovies")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tmdbId** | **kotlin.String**| The tmdbId. | [optional]
 **imdbId** | **kotlin.String**| The imdbId. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postUpdatedSeries"></a>
# **postUpdatedSeries**
> postUpdatedSeries(tvdbId)

Reports that new episodes of a series have been added by an external source.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
val tvdbId : kotlin.String = tvdbId_example // kotlin.String | The tvdbId.
try {
    apiInstance.postUpdatedSeries(tvdbId)
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#postUpdatedSeries")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#postUpdatedSeries")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tvdbId** | **kotlin.String**| The tvdbId. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="refreshLibrary"></a>
# **refreshLibrary**
> refreshLibrary()

Starts a library scan.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = LibraryApi()
try {
    apiInstance.refreshLibrary()
} catch (e: ClientException) {
    println("4xx response calling LibraryApi#refreshLibrary")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LibraryApi#refreshLibrary")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

