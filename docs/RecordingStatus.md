
# RecordingStatus

## Enum


    * `new` (value: `"New"`)

    * `inProgress` (value: `"InProgress"`)

    * `completed` (value: `"Completed"`)

    * `cancelled` (value: `"Cancelled"`)

    * `conflictedOk` (value: `"ConflictedOk"`)

    * `conflictedNotOk` (value: `"ConflictedNotOk"`)

    * `error` (value: `"Error"`)



