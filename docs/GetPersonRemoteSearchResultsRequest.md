
# GetPersonRemoteSearchResultsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**searchInfo** | [**PersonLookupInfo**](PersonLookupInfo.md) |  |  [optional]
**itemId** | **kotlin.String** |  |  [optional]
**searchProviderName** | **kotlin.String** | Gets or sets the provider name to search within if set. |  [optional]
**includeDisabledProviders** | **kotlin.Boolean** | Gets or sets a value indicating whether disabled providers should be included. |  [optional]



