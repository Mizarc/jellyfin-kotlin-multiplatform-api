
# FileSystemEntryType

## Enum


    * `file` (value: `"File"`)

    * `directory` (value: `"Directory"`)

    * `networkComputer` (value: `"NetworkComputer"`)

    * `networkShare` (value: `"NetworkShare"`)



