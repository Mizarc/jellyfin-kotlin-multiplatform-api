
# SyncPlayQueueRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemIds** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the items to enqueue. |  [optional]
**mode** | [**GroupQueueMode**](GroupQueueMode.md) |  |  [optional]



