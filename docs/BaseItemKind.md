
# BaseItemKind

## Enum


    * `aggregateFolder` (value: `"AggregateFolder"`)

    * `audio` (value: `"Audio"`)

    * `audioBook` (value: `"AudioBook"`)

    * `basePluginFolder` (value: `"BasePluginFolder"`)

    * `book` (value: `"Book"`)

    * `boxSet` (value: `"BoxSet"`)

    * `channel` (value: `"Channel"`)

    * `channelFolderItem` (value: `"ChannelFolderItem"`)

    * `collectionFolder` (value: `"CollectionFolder"`)

    * `episode` (value: `"Episode"`)

    * `folder` (value: `"Folder"`)

    * `genre` (value: `"Genre"`)

    * `manualPlaylistsFolder` (value: `"ManualPlaylistsFolder"`)

    * `movie` (value: `"Movie"`)

    * `liveTvChannel` (value: `"LiveTvChannel"`)

    * `liveTvProgram` (value: `"LiveTvProgram"`)

    * `musicAlbum` (value: `"MusicAlbum"`)

    * `musicArtist` (value: `"MusicArtist"`)

    * `musicGenre` (value: `"MusicGenre"`)

    * `musicVideo` (value: `"MusicVideo"`)

    * `person` (value: `"Person"`)

    * `photo` (value: `"Photo"`)

    * `photoAlbum` (value: `"PhotoAlbum"`)

    * `playlist` (value: `"Playlist"`)

    * `playlistsFolder` (value: `"PlaylistsFolder"`)

    * `program` (value: `"Program"`)

    * `recording` (value: `"Recording"`)

    * `season` (value: `"Season"`)

    * `series` (value: `"Series"`)

    * `studio` (value: `"Studio"`)

    * `trailer` (value: `"Trailer"`)

    * `tvChannel` (value: `"TvChannel"`)

    * `tvProgram` (value: `"TvProgram"`)

    * `userRootFolder` (value: `"UserRootFolder"`)

    * `userView` (value: `"UserView"`)

    * `video` (value: `"Video"`)

    * `year` (value: `"Year"`)



