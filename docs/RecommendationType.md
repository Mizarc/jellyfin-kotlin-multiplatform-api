
# RecommendationType

## Enum


    * `similarToRecentlyPlayed` (value: `"SimilarToRecentlyPlayed"`)

    * `similarToLikedItem` (value: `"SimilarToLikedItem"`)

    * `hasDirectorFromRecentlyPlayed` (value: `"HasDirectorFromRecentlyPlayed"`)

    * `hasActorFromRecentlyPlayed` (value: `"HasActorFromRecentlyPlayed"`)

    * `hasLikedDirector` (value: `"HasLikedDirector"`)

    * `hasLikedActor` (value: `"HasLikedActor"`)



