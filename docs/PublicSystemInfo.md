
# PublicSystemInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**localAddress** | **kotlin.String** | Gets or sets the local address. |  [optional]
**serverName** | **kotlin.String** | Gets or sets the name of the server. |  [optional]
**version** | **kotlin.String** | Gets or sets the server version. |  [optional]
**productName** | **kotlin.String** | Gets or sets the product name. This is the AssemblyProduct name. |  [optional]
**operatingSystem** | **kotlin.String** | Gets or sets the operating system. |  [optional]
**id** | **kotlin.String** | Gets or sets the id. |  [optional]
**startupWizardCompleted** | **kotlin.Boolean** | Gets or sets a value indicating whether the startup wizard is completed. |  [optional]



