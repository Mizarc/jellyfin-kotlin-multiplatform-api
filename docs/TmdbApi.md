# TmdbApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tmdbClientConfiguration**](TmdbApi.md#tmdbClientConfiguration) | **GET** /Tmdb/ClientConfiguration | Gets the TMDb image configuration options.


<a name="tmdbClientConfiguration"></a>
# **tmdbClientConfiguration**
> ConfigImageTypes tmdbClientConfiguration()

Gets the TMDb image configuration options.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = TmdbApi()
try {
    val result : ConfigImageTypes = apiInstance.tmdbClientConfiguration()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling TmdbApi#tmdbClientConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling TmdbApi#tmdbClientConfiguration")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ConfigImageTypes**](ConfigImageTypes.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

