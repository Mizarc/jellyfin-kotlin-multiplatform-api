
# ProfileCondition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**condition** | [**ProfileConditionType**](ProfileConditionType.md) |  |  [optional]
**&#x60;property&#x60;** | [**ProfileConditionValue**](ProfileConditionValue.md) |  |  [optional]
**&#x60;value&#x60;** | **kotlin.String** |  |  [optional]
**isRequired** | **kotlin.Boolean** |  |  [optional]



