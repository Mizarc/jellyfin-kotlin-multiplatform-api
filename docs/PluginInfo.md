
# PluginInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**version** | **kotlin.String** | Gets or sets the version. |  [optional]
**configurationFileName** | **kotlin.String** | Gets or sets the name of the configuration file. |  [optional]
**description** | **kotlin.String** | Gets or sets the description. |  [optional]
**id** | **kotlin.String** | Gets or sets the unique id. |  [optional]
**canUninstall** | **kotlin.Boolean** | Gets or sets a value indicating whether the plugin can be uninstalled. |  [optional]
**hasImage** | **kotlin.Boolean** | Gets or sets a value indicating whether this plugin has a valid image. |  [optional]
**status** | [**PluginStatus**](PluginStatus.md) |  |  [optional]



