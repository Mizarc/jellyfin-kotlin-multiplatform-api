
# MetadataEditorInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentalRatingOptions** | [**kotlin.collections.List&lt;ParentalRating&gt;**](ParentalRating.md) |  |  [optional]
**countries** | [**kotlin.collections.List&lt;CountryInfo&gt;**](CountryInfo.md) |  |  [optional]
**cultures** | [**kotlin.collections.List&lt;CultureDto&gt;**](CultureDto.md) |  |  [optional]
**externalIdInfos** | [**kotlin.collections.List&lt;ExternalIdInfo&gt;**](ExternalIdInfo.md) |  |  [optional]
**contentType** | **kotlin.String** |  |  [optional]
**contentTypeOptions** | [**kotlin.collections.List&lt;NameValuePair&gt;**](NameValuePair.md) |  |  [optional]



