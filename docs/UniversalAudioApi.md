# UniversalAudioApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUniversalAudioStream**](UniversalAudioApi.md#getUniversalAudioStream) | **GET** /Audio/{itemId}/universal | Gets an audio stream.
[**headUniversalAudioStream**](UniversalAudioApi.md#headUniversalAudioStream) | **HEAD** /Audio/{itemId}/universal | Gets an audio stream.


<a name="getUniversalAudioStream"></a>
# **getUniversalAudioStream**
> org.openapitools.client.infrastructure.OctetByteArray getUniversalAudioStream(itemId, container, mediaSourceId, deviceId, userId, audioCodec, maxAudioChannels, transcodingAudioChannels, maxStreamingBitrate, audioBitRate, startTimeTicks, transcodingContainer, transcodingProtocol, maxAudioSampleRate, maxAudioBitDepth, enableRemoteMedia, breakOnNonKeyFrames, enableRedirection)

Gets an audio stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UniversalAudioApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val container : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. The audio container.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. The user id.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. The audio codec to transcode to.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels.
val transcodingAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The number of how many audio channels to transcode to.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum streaming bitrate.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val transcodingContainer : kotlin.String = transcodingContainer_example // kotlin.String | Optional. The container to transcode to.
val transcodingProtocol : kotlin.String = transcodingProtocol_example // kotlin.String | Optional. The transcoding protocol.
val maxAudioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio sample rate.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val enableRemoteMedia : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable remote media.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val enableRedirection : kotlin.Boolean = true // kotlin.Boolean | Whether to enable redirection. Defaults to true.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getUniversalAudioStream(itemId, container, mediaSourceId, deviceId, userId, audioCodec, maxAudioChannels, transcodingAudioChannels, maxStreamingBitrate, audioBitRate, startTimeTicks, transcodingContainer, transcodingProtocol, maxAudioSampleRate, maxAudioBitDepth, enableRemoteMedia, breakOnNonKeyFrames, enableRedirection)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UniversalAudioApi#getUniversalAudioStream")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UniversalAudioApi#getUniversalAudioStream")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **container** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. The audio container. | [optional]
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **userId** | **kotlin.String**| Optional. The user id. | [optional]
 **audioCodec** | **kotlin.String**| Optional. The audio codec to transcode to. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels. | [optional]
 **transcodingAudioChannels** | **kotlin.Int**| Optional. The number of how many audio channels to transcode to. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| Optional. The maximum streaming bitrate. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **transcodingContainer** | **kotlin.String**| Optional. The container to transcode to. | [optional]
 **transcodingProtocol** | **kotlin.String**| Optional. The transcoding protocol. | [optional]
 **maxAudioSampleRate** | **kotlin.Int**| Optional. The maximum audio sample rate. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **enableRemoteMedia** | **kotlin.Boolean**| Optional. Whether to enable remote media. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional] [default to false]
 **enableRedirection** | **kotlin.Boolean**| Whether to enable redirection. Defaults to true. | [optional] [default to true]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: audio/*

<a name="headUniversalAudioStream"></a>
# **headUniversalAudioStream**
> org.openapitools.client.infrastructure.OctetByteArray headUniversalAudioStream(itemId, container, mediaSourceId, deviceId, userId, audioCodec, maxAudioChannels, transcodingAudioChannels, maxStreamingBitrate, audioBitRate, startTimeTicks, transcodingContainer, transcodingProtocol, maxAudioSampleRate, maxAudioBitDepth, enableRemoteMedia, breakOnNonKeyFrames, enableRedirection)

Gets an audio stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = UniversalAudioApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val container : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. The audio container.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. The user id.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. The audio codec to transcode to.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels.
val transcodingAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The number of how many audio channels to transcode to.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum streaming bitrate.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val transcodingContainer : kotlin.String = transcodingContainer_example // kotlin.String | Optional. The container to transcode to.
val transcodingProtocol : kotlin.String = transcodingProtocol_example // kotlin.String | Optional. The transcoding protocol.
val maxAudioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio sample rate.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val enableRemoteMedia : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable remote media.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val enableRedirection : kotlin.Boolean = true // kotlin.Boolean | Whether to enable redirection. Defaults to true.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headUniversalAudioStream(itemId, container, mediaSourceId, deviceId, userId, audioCodec, maxAudioChannels, transcodingAudioChannels, maxStreamingBitrate, audioBitRate, startTimeTicks, transcodingContainer, transcodingProtocol, maxAudioSampleRate, maxAudioBitDepth, enableRemoteMedia, breakOnNonKeyFrames, enableRedirection)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UniversalAudioApi#headUniversalAudioStream")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UniversalAudioApi#headUniversalAudioStream")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **container** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. The audio container. | [optional]
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **userId** | **kotlin.String**| Optional. The user id. | [optional]
 **audioCodec** | **kotlin.String**| Optional. The audio codec to transcode to. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels. | [optional]
 **transcodingAudioChannels** | **kotlin.Int**| Optional. The number of how many audio channels to transcode to. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| Optional. The maximum streaming bitrate. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **transcodingContainer** | **kotlin.String**| Optional. The container to transcode to. | [optional]
 **transcodingProtocol** | **kotlin.String**| Optional. The transcoding protocol. | [optional]
 **maxAudioSampleRate** | **kotlin.Int**| Optional. The maximum audio sample rate. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **enableRemoteMedia** | **kotlin.Boolean**| Optional. Whether to enable remote media. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional] [default to false]
 **enableRedirection** | **kotlin.Boolean**| Whether to enable redirection. Defaults to true. | [optional] [default to true]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: audio/*

