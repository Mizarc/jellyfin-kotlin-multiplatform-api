# DlnaApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProfile**](DlnaApi.md#createProfile) | **POST** /Dlna/Profiles | Creates a profile.
[**deleteProfile**](DlnaApi.md#deleteProfile) | **DELETE** /Dlna/Profiles/{profileId} | Deletes a profile.
[**getDefaultProfile**](DlnaApi.md#getDefaultProfile) | **GET** /Dlna/Profiles/Default | Gets the default profile.
[**getProfile**](DlnaApi.md#getProfile) | **GET** /Dlna/Profiles/{profileId} | Gets a single profile.
[**getProfileInfos**](DlnaApi.md#getProfileInfos) | **GET** /Dlna/ProfileInfos | Get profile infos.
[**updateProfile**](DlnaApi.md#updateProfile) | **POST** /Dlna/Profiles/{profileId} | Updates a profile.


<a name="createProfile"></a>
# **createProfile**
> createProfile(createProfileRequest)

Creates a profile.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaApi()
val createProfileRequest : CreateProfileRequest =  // CreateProfileRequest | Device profile.
try {
    apiInstance.createProfile(createProfileRequest)
} catch (e: ClientException) {
    println("4xx response calling DlnaApi#createProfile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaApi#createProfile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createProfileRequest** | [**CreateProfileRequest**](CreateProfileRequest.md)| Device profile. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="deleteProfile"></a>
# **deleteProfile**
> deleteProfile(profileId)

Deletes a profile.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaApi()
val profileId : kotlin.String = profileId_example // kotlin.String | Profile id.
try {
    apiInstance.deleteProfile(profileId)
} catch (e: ClientException) {
    println("4xx response calling DlnaApi#deleteProfile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaApi#deleteProfile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileId** | **kotlin.String**| Profile id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDefaultProfile"></a>
# **getDefaultProfile**
> DeviceProfile getDefaultProfile()

Gets the default profile.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaApi()
try {
    val result : DeviceProfile = apiInstance.getDefaultProfile()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaApi#getDefaultProfile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaApi#getDefaultProfile")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DeviceProfile**](DeviceProfile.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getProfile"></a>
# **getProfile**
> DeviceProfile getProfile(profileId)

Gets a single profile.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaApi()
val profileId : kotlin.String = profileId_example // kotlin.String | Profile Id.
try {
    val result : DeviceProfile = apiInstance.getProfile(profileId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaApi#getProfile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaApi#getProfile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileId** | **kotlin.String**| Profile Id. |

### Return type

[**DeviceProfile**](DeviceProfile.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getProfileInfos"></a>
# **getProfileInfos**
> kotlin.collections.List&lt;DeviceProfileInfo&gt; getProfileInfos()

Get profile infos.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaApi()
try {
    val result : kotlin.collections.List<DeviceProfileInfo> = apiInstance.getProfileInfos()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaApi#getProfileInfos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaApi#getProfileInfos")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;DeviceProfileInfo&gt;**](DeviceProfileInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="updateProfile"></a>
# **updateProfile**
> updateProfile(profileId, createProfileRequest)

Updates a profile.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaApi()
val profileId : kotlin.String = profileId_example // kotlin.String | Profile id.
val createProfileRequest : CreateProfileRequest =  // CreateProfileRequest | Device profile.
try {
    apiInstance.updateProfile(profileId, createProfileRequest)
} catch (e: ClientException) {
    println("4xx response calling DlnaApi#updateProfile")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaApi#updateProfile")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileId** | **kotlin.String**| Profile id. |
 **createProfileRequest** | [**CreateProfileRequest**](CreateProfileRequest.md)| Device profile. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

