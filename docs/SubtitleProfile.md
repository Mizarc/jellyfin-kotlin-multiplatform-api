
# SubtitleProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | **kotlin.String** |  |  [optional]
**method** | [**SubtitleDeliveryMethod**](SubtitleDeliveryMethod.md) |  |  [optional]
**didlMode** | **kotlin.String** |  |  [optional]
**language** | **kotlin.String** |  |  [optional]
**container** | **kotlin.String** |  |  [optional]



