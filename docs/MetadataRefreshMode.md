
# MetadataRefreshMode

## Enum


    * `none` (value: `"None"`)

    * `validationOnly` (value: `"ValidationOnly"`)

    * `default` (value: `"Default"`)

    * `fullRefresh` (value: `"FullRefresh"`)



