# GenresApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGenre**](GenresApi.md#getGenre) | **GET** /Genres/{genreName} | Gets a genre, by name.
[**getGenres**](GenresApi.md#getGenres) | **GET** /Genres | Gets all genres from a given item, folder, or the entire library.


<a name="getGenre"></a>
# **getGenre**
> BaseItemDto getGenre(genreName, userId)

Gets a genre, by name.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = GenresApi()
val genreName : kotlin.String = genreName_example // kotlin.String | The genre name.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
try {
    val result : BaseItemDto = apiInstance.getGenre(genreName, userId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling GenresApi#getGenre")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling GenresApi#getGenre")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **genreName** | **kotlin.String**| The genre name. |
 **userId** | **kotlin.String**| The user id. | [optional]

### Return type

[**BaseItemDto**](BaseItemDto.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getGenres"></a>
# **getGenres**
> BaseItemDtoQueryResult getGenres(startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, isFavorite, imageTypeLimit, enableImageTypes, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, sortBy, sortOrder, enableImages, enableTotalRecordCount)

Gets all genres from a given item, folder, or the entire library.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = GenresApi()
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val searchTerm : kotlin.String = searchTerm_example // kotlin.String | The search term.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Specify this to localize the search to a specific item or folder. Omit to use the root.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val excludeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited.
val includeItemTypes : kotlin.collections.List<BaseItemKind> =  // kotlin.collections.List<BaseItemKind> | Optional. If specified, results will be filtered in based on item type. This allows multiple, comma delimited.
val isFavorite : kotlin.Boolean = true // kotlin.Boolean | Optional filter by items that are marked as favorite, or not.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional, the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | User id.
val nameStartsWithOrGreater : kotlin.String = nameStartsWithOrGreater_example // kotlin.String | Optional filter by items whose name is sorted equally or greater than a given input string.
val nameStartsWith : kotlin.String = nameStartsWith_example // kotlin.String | Optional filter by items whose name is sorted equally than a given input string.
val nameLessThan : kotlin.String = nameLessThan_example // kotlin.String | Optional filter by items whose name is equally or lesser than a given input string.
val sortBy : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | Optional. Specify one or more sort orders, comma delimited.
val sortOrder : kotlin.collections.List<SortOrder> =  // kotlin.collections.List<SortOrder> | Sort Order - Ascending,Descending.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional, include image information in output.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Optional. Include total record count.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getGenres(startIndex, limit, searchTerm, parentId, fields, excludeItemTypes, includeItemTypes, isFavorite, imageTypeLimit, enableImageTypes, userId, nameStartsWithOrGreater, nameStartsWith, nameLessThan, sortBy, sortOrder, enableImages, enableTotalRecordCount)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling GenresApi#getGenres")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling GenresApi#getGenres")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **searchTerm** | **kotlin.String**| The search term. | [optional]
 **parentId** | **kotlin.String**| Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **excludeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered out based on item type. This allows multiple, comma delimited. | [optional]
 **includeItemTypes** | [**kotlin.collections.List&lt;BaseItemKind&gt;**](BaseItemKind.md)| Optional. If specified, results will be filtered in based on item type. This allows multiple, comma delimited. | [optional]
 **isFavorite** | **kotlin.Boolean**| Optional filter by items that are marked as favorite, or not. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional, the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **userId** | **kotlin.String**| User id. | [optional]
 **nameStartsWithOrGreater** | **kotlin.String**| Optional filter by items whose name is sorted equally or greater than a given input string. | [optional]
 **nameStartsWith** | **kotlin.String**| Optional filter by items whose name is sorted equally than a given input string. | [optional]
 **nameLessThan** | **kotlin.String**| Optional filter by items whose name is equally or lesser than a given input string. | [optional]
 **sortBy** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Optional. Specify one or more sort orders, comma delimited. | [optional]
 **sortOrder** | [**kotlin.collections.List&lt;SortOrder&gt;**](SortOrder.md)| Sort Order - Ascending,Descending. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional, include image information in output. | [optional] [default to true]
 **enableTotalRecordCount** | **kotlin.Boolean**| Optional. Include total record count. | [optional] [default to true]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

