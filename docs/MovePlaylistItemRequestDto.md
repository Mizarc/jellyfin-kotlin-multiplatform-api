
# MovePlaylistItemRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlistItemId** | **kotlin.String** | Gets or sets the playlist identifier of the item. |  [optional]
**newIndex** | **kotlin.Int** | Gets or sets the new position. |  [optional]



