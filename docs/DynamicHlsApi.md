# DynamicHlsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHlsAudioSegment**](DynamicHlsApi.md#getHlsAudioSegment) | **GET** /Audio/{itemId}/hls1/{playlistId}/{segmentId}.{container} | Gets a video stream using HTTP live streaming.
[**getHlsVideoSegment**](DynamicHlsApi.md#getHlsVideoSegment) | **GET** /Videos/{itemId}/hls1/{playlistId}/{segmentId}.{container} | Gets a video stream using HTTP live streaming.
[**getLiveHlsStream**](DynamicHlsApi.md#getLiveHlsStream) | **GET** /Videos/{itemId}/live.m3u8 | Gets a hls live stream.
[**getMasterHlsAudioPlaylist**](DynamicHlsApi.md#getMasterHlsAudioPlaylist) | **GET** /Audio/{itemId}/master.m3u8 | Gets an audio hls playlist stream.
[**getMasterHlsVideoPlaylist**](DynamicHlsApi.md#getMasterHlsVideoPlaylist) | **GET** /Videos/{itemId}/master.m3u8 | Gets a video hls playlist stream.
[**getVariantHlsAudioPlaylist**](DynamicHlsApi.md#getVariantHlsAudioPlaylist) | **GET** /Audio/{itemId}/main.m3u8 | Gets an audio stream using HTTP live streaming.
[**getVariantHlsVideoPlaylist**](DynamicHlsApi.md#getVariantHlsVideoPlaylist) | **GET** /Videos/{itemId}/main.m3u8 | Gets a video stream using HTTP live streaming.
[**headMasterHlsAudioPlaylist**](DynamicHlsApi.md#headMasterHlsAudioPlaylist) | **HEAD** /Audio/{itemId}/master.m3u8 | Gets an audio hls playlist stream.
[**headMasterHlsVideoPlaylist**](DynamicHlsApi.md#headMasterHlsVideoPlaylist) | **HEAD** /Videos/{itemId}/master.m3u8 | Gets a video hls playlist stream.


<a name="getHlsAudioSegment"></a>
# **getHlsAudioSegment**
> org.openapitools.client.infrastructure.OctetByteArray getHlsAudioSegment(itemId, playlistId, segmentId, container, runtimeTicks, actualSegmentLengthTicks, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)

Gets a video stream using HTTP live streaming.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val playlistId : kotlin.String = playlistId_example // kotlin.String | The playlist id.
val segmentId : kotlin.Int = 56 // kotlin.Int | The segment id.
val container : kotlin.String = container_example // kotlin.String | The video container. Possible values are: ts, webm, asf, wmv, ogv, mp4, m4v, mkv, mpeg, mpg, avi, 3gp, wmv, wtv, m2ts, mov, iso, flv.
val runtimeTicks : kotlin.Long = 789 // kotlin.Long | The position of the requested segment in ticks.
val actualSegmentLengthTicks : kotlin.Long = 789 // kotlin.Long | The length of the requested segment in ticks.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum streaming bitrate.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vpx, wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getHlsAudioSegment(itemId, playlistId, segmentId, container, runtimeTicks, actualSegmentLengthTicks, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#getHlsAudioSegment")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#getHlsAudioSegment")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **playlistId** | **kotlin.String**| The playlist id. |
 **segmentId** | **kotlin.Int**| The segment id. |
 **container** | **kotlin.String**| The video container. Possible values are: ts, webm, asf, wmv, ogv, mp4, m4v, mkv, mpeg, mpg, avi, 3gp, wmv, wtv, m2ts, mov, iso, flv. |
 **runtimeTicks** | **kotlin.Long**| The position of the requested segment in ticks. |
 **actualSegmentLengthTicks** | **kotlin.Long**| The length of the requested segment in ticks. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| Optional. The maximum streaming bitrate. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vpx, wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: audio/*

<a name="getHlsVideoSegment"></a>
# **getHlsVideoSegment**
> org.openapitools.client.infrastructure.OctetByteArray getHlsVideoSegment(itemId, playlistId, segmentId, container, runtimeTicks, actualSegmentLengthTicks, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)

Gets a video stream using HTTP live streaming.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val playlistId : kotlin.String = playlistId_example // kotlin.String | The playlist id.
val segmentId : kotlin.Int = 56 // kotlin.Int | The segment id.
val container : kotlin.String = container_example // kotlin.String | The video container. Possible values are: ts, webm, asf, wmv, ogv, mp4, m4v, mkv, mpeg, mpg, avi, 3gp, wmv, wtv, m2ts, mov, iso, flv.
val runtimeTicks : kotlin.Long = 789 // kotlin.Long | The position of the requested segment in ticks.
val actualSegmentLengthTicks : kotlin.Long = 789 // kotlin.Long | The length of the requested segment in ticks.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The desired segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val maxWidth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum horizontal resolution of the encoded video.
val maxHeight : kotlin.Int = 56 // kotlin.Int | Optional. The maximum vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getHlsVideoSegment(itemId, playlistId, segmentId, container, runtimeTicks, actualSegmentLengthTicks, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#getHlsVideoSegment")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#getHlsVideoSegment")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **playlistId** | **kotlin.String**| The playlist id. |
 **segmentId** | **kotlin.Int**| The segment id. |
 **container** | **kotlin.String**| The video container. Possible values are: ts, webm, asf, wmv, ogv, mp4, m4v, mkv, mpeg, mpg, avi, 3gp, wmv, wtv, m2ts, mov, iso, flv. |
 **runtimeTicks** | **kotlin.Long**| The position of the requested segment in ticks. |
 **actualSegmentLengthTicks** | **kotlin.Long**| The length of the requested segment in ticks. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The desired segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **maxWidth** | **kotlin.Int**| Optional. The maximum horizontal resolution of the encoded video. | [optional]
 **maxHeight** | **kotlin.Int**| Optional. The maximum vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: video/*

<a name="getLiveHlsStream"></a>
# **getLiveHlsStream**
> org.openapitools.client.infrastructure.OctetByteArray getLiveHlsStream(itemId, container, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, maxWidth, maxHeight, enableSubtitlesInManifest)

Gets a hls live stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val container : kotlin.String = container_example // kotlin.String | The audio container.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment lenght.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
val maxWidth : kotlin.Int = 56 // kotlin.Int | Optional. The max width.
val maxHeight : kotlin.Int = 56 // kotlin.Int | Optional. The max height.
val enableSubtitlesInManifest : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable subtitles in the manifest.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getLiveHlsStream(itemId, container, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, maxWidth, maxHeight, enableSubtitlesInManifest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#getLiveHlsStream")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#getLiveHlsStream")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **container** | **kotlin.String**| The audio container. | [optional]
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment lenght. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]
 **maxWidth** | **kotlin.Int**| Optional. The max width. | [optional]
 **maxHeight** | **kotlin.Int**| Optional. The max height. | [optional]
 **enableSubtitlesInManifest** | **kotlin.Boolean**| Optional. Whether to enable subtitles in the manifest. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="getMasterHlsAudioPlaylist"></a>
# **getMasterHlsAudioPlaylist**
> org.openapitools.client.infrastructure.OctetByteArray getMasterHlsAudioPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)

Gets an audio hls playlist stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum streaming bitrate.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
val enableAdaptiveBitrateStreaming : kotlin.Boolean = true // kotlin.Boolean | Enable adaptive bitrate streaming.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMasterHlsAudioPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#getMasterHlsAudioPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#getMasterHlsAudioPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| Optional. The maximum streaming bitrate. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]
 **enableAdaptiveBitrateStreaming** | **kotlin.Boolean**| Enable adaptive bitrate streaming. | [optional] [default to true]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="getMasterHlsVideoPlaylist"></a>
# **getMasterHlsVideoPlaylist**
> org.openapitools.client.infrastructure.OctetByteArray getMasterHlsVideoPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)

Gets a video hls playlist stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val maxWidth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum horizontal resolution of the encoded video.
val maxHeight : kotlin.Int = 56 // kotlin.Int | Optional. The maximum vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
val enableAdaptiveBitrateStreaming : kotlin.Boolean = true // kotlin.Boolean | Enable adaptive bitrate streaming.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMasterHlsVideoPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#getMasterHlsVideoPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#getMasterHlsVideoPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **maxWidth** | **kotlin.Int**| Optional. The maximum horizontal resolution of the encoded video. | [optional]
 **maxHeight** | **kotlin.Int**| Optional. The maximum vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]
 **enableAdaptiveBitrateStreaming** | **kotlin.Boolean**| Enable adaptive bitrate streaming. | [optional] [default to true]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="getVariantHlsAudioPlaylist"></a>
# **getVariantHlsAudioPlaylist**
> org.openapitools.client.infrastructure.OctetByteArray getVariantHlsAudioPlaylist(itemId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)

Gets an audio stream using HTTP live streaming.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum streaming bitrate.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vpx, wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getVariantHlsAudioPlaylist(itemId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#getVariantHlsAudioPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#getVariantHlsAudioPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| Optional. The maximum streaming bitrate. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vpx, wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="getVariantHlsVideoPlaylist"></a>
# **getVariantHlsVideoPlaylist**
> org.openapitools.client.infrastructure.OctetByteArray getVariantHlsVideoPlaylist(itemId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)

Gets a video stream using HTTP live streaming.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val maxWidth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum horizontal resolution of the encoded video.
val maxHeight : kotlin.Int = 56 // kotlin.Int | Optional. The maximum vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getVariantHlsVideoPlaylist(itemId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, mediaSourceId, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#getVariantHlsVideoPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#getVariantHlsVideoPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **maxWidth** | **kotlin.Int**| Optional. The maximum horizontal resolution of the encoded video. | [optional]
 **maxHeight** | **kotlin.Int**| Optional. The maximum vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="headMasterHlsAudioPlaylist"></a>
# **headMasterHlsAudioPlaylist**
> org.openapitools.client.infrastructure.OctetByteArray headMasterHlsAudioPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)

Gets an audio hls playlist stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val maxStreamingBitrate : kotlin.Int = 56 // kotlin.Int | Optional. The maximum streaming bitrate.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
val enableAdaptiveBitrateStreaming : kotlin.Boolean = true // kotlin.Boolean | Enable adaptive bitrate streaming.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headMasterHlsAudioPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, maxStreamingBitrate, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#headMasterHlsAudioPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#headMasterHlsAudioPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **maxStreamingBitrate** | **kotlin.Int**| Optional. The maximum streaming bitrate. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]
 **enableAdaptiveBitrateStreaming** | **kotlin.Boolean**| Enable adaptive bitrate streaming. | [optional] [default to true]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

<a name="headMasterHlsVideoPlaylist"></a>
# **headMasterHlsVideoPlaylist**
> org.openapitools.client.infrastructure.OctetByteArray headMasterHlsVideoPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)

Gets a video hls playlist stream.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DynamicHlsApi()
val itemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | The media version id, if playing an alternate version.
val static : kotlin.Boolean = true // kotlin.Boolean | Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false.
val params : kotlin.String = params_example // kotlin.String | The streaming parameters.
val tag : kotlin.String = tag_example // kotlin.String | The tag.
val deviceProfileId : kotlin.String = deviceProfileId_example // kotlin.String | Optional. The dlna device profile id to utilize.
val playSessionId : kotlin.String = playSessionId_example // kotlin.String | The play session id.
val segmentContainer : kotlin.String = segmentContainer_example // kotlin.String | The segment container.
val segmentLength : kotlin.Int = 56 // kotlin.Int | The segment length.
val minSegments : kotlin.Int = 56 // kotlin.Int | The minimum number of segments.
val deviceId : kotlin.String = deviceId_example // kotlin.String | The device id of the client requesting. Used to stop encoding processes when needed.
val audioCodec : kotlin.String = audioCodec_example // kotlin.String | Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url's extension. Options: aac, mp3, vorbis, wma.
val enableAutoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true.
val allowVideoStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the video stream url.
val allowAudioStreamCopy : kotlin.Boolean = true // kotlin.Boolean | Whether or not to allow copying of the audio stream url.
val breakOnNonKeyFrames : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to break on non key frames.
val audioSampleRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific audio sample rate, e.g. 44100.
val maxAudioBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum audio bit depth.
val audioBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults.
val audioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a specific number of audio channels to encode to, e.g. 2.
val maxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. Specify a maximum number of audio channels to encode to, e.g. 2.
val profile : kotlin.String = profile_example // kotlin.String | Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high.
val level : kotlin.String = level_example // kotlin.String | Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1.
val framerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val maxFramerate : kotlin.Float = 3.4 // kotlin.Float | Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements.
val copyTimestamps : kotlin.Boolean = true // kotlin.Boolean | Whether or not to copy timestamps when transcoding with an offset. Defaults to false.
val startTimeTicks : kotlin.Long = 789 // kotlin.Long | Optional. Specify a starting offset, in ticks. 1 tick = 10000 ms.
val width : kotlin.Int = 56 // kotlin.Int | Optional. The fixed horizontal resolution of the encoded video.
val height : kotlin.Int = 56 // kotlin.Int | Optional. The fixed vertical resolution of the encoded video.
val maxWidth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum horizontal resolution of the encoded video.
val maxHeight : kotlin.Int = 56 // kotlin.Int | Optional. The maximum vertical resolution of the encoded video.
val videoBitRate : kotlin.Int = 56 // kotlin.Int | Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to use. If omitted no subtitles will be used.
val subtitleMethod : SubtitleDeliveryMethod =  // SubtitleDeliveryMethod | Optional. Specify the subtitle delivery method.
val maxRefFrames : kotlin.Int = 56 // kotlin.Int | Optional.
val maxVideoBitDepth : kotlin.Int = 56 // kotlin.Int | Optional. The maximum video bit depth.
val requireAvc : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require avc.
val deInterlace : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to deinterlace the video.
val requireNonAnamorphic : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to require a non anamorphic stream.
val transcodingMaxAudioChannels : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of audio channels to transcode.
val cpuCoreLimit : kotlin.Int = 56 // kotlin.Int | Optional. The limit of how many cpu cores to use.
val liveStreamId : kotlin.String = liveStreamId_example // kotlin.String | The live stream id.
val enableMpegtsM2TsMode : kotlin.Boolean = true // kotlin.Boolean | Optional. Whether to enable the MpegtsM2Ts mode.
val videoCodec : kotlin.String = videoCodec_example // kotlin.String | Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url's extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv.
val subtitleCodec : kotlin.String = subtitleCodec_example // kotlin.String | Optional. Specify a subtitle codec to encode to.
val transcodeReasons : kotlin.String = transcodeReasons_example // kotlin.String | Optional. The transcoding reason.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to use. If omitted the first audio stream will be used.
val videoStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the video stream to use. If omitted the first video stream will be used.
val context : EncodingContext =  // EncodingContext | Optional. The MediaBrowser.Model.Dlna.EncodingContext.
val streamOptions : kotlin.collections.Map<kotlin.String, kotlin.String> =  // kotlin.collections.Map<kotlin.String, kotlin.String> | Optional. The streaming options.
val enableAdaptiveBitrateStreaming : kotlin.Boolean = true // kotlin.Boolean | Enable adaptive bitrate streaming.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.headMasterHlsVideoPlaylist(itemId, mediaSourceId, static, params, tag, deviceProfileId, playSessionId, segmentContainer, segmentLength, minSegments, deviceId, audioCodec, enableAutoStreamCopy, allowVideoStreamCopy, allowAudioStreamCopy, breakOnNonKeyFrames, audioSampleRate, maxAudioBitDepth, audioBitRate, audioChannels, maxAudioChannels, profile, level, framerate, maxFramerate, copyTimestamps, startTimeTicks, width, height, maxWidth, maxHeight, videoBitRate, subtitleStreamIndex, subtitleMethod, maxRefFrames, maxVideoBitDepth, requireAvc, deInterlace, requireNonAnamorphic, transcodingMaxAudioChannels, cpuCoreLimit, liveStreamId, enableMpegtsM2TsMode, videoCodec, subtitleCodec, transcodeReasons, audioStreamIndex, videoStreamIndex, context, streamOptions, enableAdaptiveBitrateStreaming)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DynamicHlsApi#headMasterHlsVideoPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DynamicHlsApi#headMasterHlsVideoPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **mediaSourceId** | **kotlin.String**| The media version id, if playing an alternate version. |
 **static** | **kotlin.Boolean**| Optional. If true, the original file will be streamed statically without any encoding. Use either no url extension or the original file extension. true/false. | [optional]
 **params** | **kotlin.String**| The streaming parameters. | [optional]
 **tag** | **kotlin.String**| The tag. | [optional]
 **deviceProfileId** | **kotlin.String**| Optional. The dlna device profile id to utilize. | [optional]
 **playSessionId** | **kotlin.String**| The play session id. | [optional]
 **segmentContainer** | **kotlin.String**| The segment container. | [optional]
 **segmentLength** | **kotlin.Int**| The segment length. | [optional]
 **minSegments** | **kotlin.Int**| The minimum number of segments. | [optional]
 **deviceId** | **kotlin.String**| The device id of the client requesting. Used to stop encoding processes when needed. | [optional]
 **audioCodec** | **kotlin.String**| Optional. Specify a audio codec to encode to, e.g. mp3. If omitted the server will auto-select using the url&#39;s extension. Options: aac, mp3, vorbis, wma. | [optional]
 **enableAutoStreamCopy** | **kotlin.Boolean**| Whether or not to allow automatic stream copy if requested values match the original source. Defaults to true. | [optional]
 **allowVideoStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the video stream url. | [optional]
 **allowAudioStreamCopy** | **kotlin.Boolean**| Whether or not to allow copying of the audio stream url. | [optional]
 **breakOnNonKeyFrames** | **kotlin.Boolean**| Optional. Whether to break on non key frames. | [optional]
 **audioSampleRate** | **kotlin.Int**| Optional. Specify a specific audio sample rate, e.g. 44100. | [optional]
 **maxAudioBitDepth** | **kotlin.Int**| Optional. The maximum audio bit depth. | [optional]
 **audioBitRate** | **kotlin.Int**| Optional. Specify an audio bitrate to encode to, e.g. 128000. If omitted this will be left to encoder defaults. | [optional]
 **audioChannels** | **kotlin.Int**| Optional. Specify a specific number of audio channels to encode to, e.g. 2. | [optional]
 **maxAudioChannels** | **kotlin.Int**| Optional. Specify a maximum number of audio channels to encode to, e.g. 2. | [optional]
 **profile** | **kotlin.String**| Optional. Specify a specific an encoder profile (varies by encoder), e.g. main, baseline, high. | [optional]
 **level** | **kotlin.String**| Optional. Specify a level for the encoder profile (varies by encoder), e.g. 3, 3.1. | [optional]
 **framerate** | **kotlin.Float**| Optional. A specific video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **maxFramerate** | **kotlin.Float**| Optional. A specific maximum video framerate to encode to, e.g. 23.976. Generally this should be omitted unless the device has specific requirements. | [optional]
 **copyTimestamps** | **kotlin.Boolean**| Whether or not to copy timestamps when transcoding with an offset. Defaults to false. | [optional]
 **startTimeTicks** | **kotlin.Long**| Optional. Specify a starting offset, in ticks. 1 tick &#x3D; 10000 ms. | [optional]
 **width** | **kotlin.Int**| Optional. The fixed horizontal resolution of the encoded video. | [optional]
 **height** | **kotlin.Int**| Optional. The fixed vertical resolution of the encoded video. | [optional]
 **maxWidth** | **kotlin.Int**| Optional. The maximum horizontal resolution of the encoded video. | [optional]
 **maxHeight** | **kotlin.Int**| Optional. The maximum vertical resolution of the encoded video. | [optional]
 **videoBitRate** | **kotlin.Int**| Optional. Specify a video bitrate to encode to, e.g. 500000. If omitted this will be left to encoder defaults. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to use. If omitted no subtitles will be used. | [optional]
 **subtitleMethod** | [**SubtitleDeliveryMethod**](.md)| Optional. Specify the subtitle delivery method. | [optional] [enum: Encode, Embed, External, Hls, Drop]
 **maxRefFrames** | **kotlin.Int**| Optional. | [optional]
 **maxVideoBitDepth** | **kotlin.Int**| Optional. The maximum video bit depth. | [optional]
 **requireAvc** | **kotlin.Boolean**| Optional. Whether to require avc. | [optional]
 **deInterlace** | **kotlin.Boolean**| Optional. Whether to deinterlace the video. | [optional]
 **requireNonAnamorphic** | **kotlin.Boolean**| Optional. Whether to require a non anamorphic stream. | [optional]
 **transcodingMaxAudioChannels** | **kotlin.Int**| Optional. The maximum number of audio channels to transcode. | [optional]
 **cpuCoreLimit** | **kotlin.Int**| Optional. The limit of how many cpu cores to use. | [optional]
 **liveStreamId** | **kotlin.String**| The live stream id. | [optional]
 **enableMpegtsM2TsMode** | **kotlin.Boolean**| Optional. Whether to enable the MpegtsM2Ts mode. | [optional]
 **videoCodec** | **kotlin.String**| Optional. Specify a video codec to encode to, e.g. h264. If omitted the server will auto-select using the url&#39;s extension. Options: h265, h264, mpeg4, theora, vp8, vp9, vpx (deprecated), wmv. | [optional]
 **subtitleCodec** | **kotlin.String**| Optional. Specify a subtitle codec to encode to. | [optional]
 **transcodeReasons** | **kotlin.String**| Optional. The transcoding reason. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to use. If omitted the first audio stream will be used. | [optional]
 **videoStreamIndex** | **kotlin.Int**| Optional. The index of the video stream to use. If omitted the first video stream will be used. | [optional]
 **context** | [**EncodingContext**](.md)| Optional. The MediaBrowser.Model.Dlna.EncodingContext. | [optional] [enum: Streaming, Static]
 **streamOptions** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.String&gt;**](kotlin.String.md)| Optional. The streaming options. | [optional]
 **enableAdaptiveBitrateStreaming** | **kotlin.Boolean**| Enable adaptive bitrate streaming. | [optional] [default to true]

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-mpegURL

