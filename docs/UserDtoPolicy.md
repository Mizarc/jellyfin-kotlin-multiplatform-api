
# UserDtoPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isAdministrator** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is administrator. |  [optional]
**isHidden** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is hidden. |  [optional]
**isDisabled** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is disabled. |  [optional]
**maxParentalRating** | **kotlin.Int** | Gets or sets the max parental rating. |  [optional]
**blockedTags** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**enableUserPreferenceAccess** | **kotlin.Boolean** |  |  [optional]
**accessSchedules** | [**kotlin.collections.List&lt;AccessSchedule&gt;**](AccessSchedule.md) |  |  [optional]
**blockUnratedItems** | [**kotlin.collections.List&lt;UnratedItem&gt;**](UnratedItem.md) |  |  [optional]
**enableRemoteControlOfOtherUsers** | **kotlin.Boolean** |  |  [optional]
**enableSharedDeviceControl** | **kotlin.Boolean** |  |  [optional]
**enableRemoteAccess** | **kotlin.Boolean** |  |  [optional]
**enableLiveTvManagement** | **kotlin.Boolean** |  |  [optional]
**enableLiveTvAccess** | **kotlin.Boolean** |  |  [optional]
**enableMediaPlayback** | **kotlin.Boolean** |  |  [optional]
**enableAudioPlaybackTranscoding** | **kotlin.Boolean** |  |  [optional]
**enableVideoPlaybackTranscoding** | **kotlin.Boolean** |  |  [optional]
**enablePlaybackRemuxing** | **kotlin.Boolean** |  |  [optional]
**forceRemoteSourceTranscoding** | **kotlin.Boolean** |  |  [optional]
**enableContentDeletion** | **kotlin.Boolean** |  |  [optional]
**enableContentDeletionFromFolders** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**enableContentDownloading** | **kotlin.Boolean** |  |  [optional]
**enableSyncTranscoding** | **kotlin.Boolean** | Gets or sets a value indicating whether [enable synchronize]. |  [optional]
**enableMediaConversion** | **kotlin.Boolean** |  |  [optional]
**enabledDevices** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**enableAllDevices** | **kotlin.Boolean** |  |  [optional]
**enabledChannels** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**enableAllChannels** | **kotlin.Boolean** |  |  [optional]
**enabledFolders** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**enableAllFolders** | **kotlin.Boolean** |  |  [optional]
**invalidLoginAttemptCount** | **kotlin.Int** |  |  [optional]
**loginAttemptsBeforeLockout** | **kotlin.Int** |  |  [optional]
**maxActiveSessions** | **kotlin.Int** |  |  [optional]
**enablePublicSharing** | **kotlin.Boolean** |  |  [optional]
**blockedMediaFolders** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**blockedChannels** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**remoteClientBitrateLimit** | **kotlin.Int** |  |  [optional]
**authenticationProviderId** | **kotlin.String** |  |  [optional]
**passwordResetProviderId** | **kotlin.String** |  |  [optional]
**syncPlayAccess** | [**SyncPlayUserAccessType**](SyncPlayUserAccessType.md) |  |  [optional]



