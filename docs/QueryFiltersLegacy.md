
# QueryFiltersLegacy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**genres** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**tags** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**officialRatings** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**years** | **kotlin.collections.List&lt;kotlin.Int&gt;** |  |  [optional]



