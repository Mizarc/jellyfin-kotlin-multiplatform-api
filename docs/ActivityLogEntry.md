
# ActivityLogEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Long** | Gets or sets the identifier. |  [optional]
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**overview** | **kotlin.String** | Gets or sets the overview. |  [optional]
**shortOverview** | **kotlin.String** | Gets or sets the short overview. |  [optional]
**type** | **kotlin.String** | Gets or sets the type. |  [optional]
**itemId** | **kotlin.String** | Gets or sets the item identifier. |  [optional]
**date** | **kotlin.String** | Gets or sets the date. |  [optional]
**userId** | **kotlin.String** | Gets or sets the user identifier. |  [optional]
**userPrimaryImageTag** | **kotlin.String** | Gets or sets the user primary image tag. |  [optional]
**severity** | [**LogLevel**](LogLevel.md) |  |  [optional]



