
# NotificationTypeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **kotlin.String** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**enabled** | **kotlin.Boolean** |  |  [optional]
**category** | **kotlin.String** |  |  [optional]
**isBasedOnUserEvent** | **kotlin.Boolean** |  |  [optional]



