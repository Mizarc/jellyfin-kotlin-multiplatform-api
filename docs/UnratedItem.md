
# UnratedItem

## Enum


    * `movie` (value: `"Movie"`)

    * `trailer` (value: `"Trailer"`)

    * `series` (value: `"Series"`)

    * `music` (value: `"Music"`)

    * `book` (value: `"Book"`)

    * `liveTvChannel` (value: `"LiveTvChannel"`)

    * `liveTvProgram` (value: `"LiveTvProgram"`)

    * `channelContent` (value: `"ChannelContent"`)

    * `other` (value: `"Other"`)



