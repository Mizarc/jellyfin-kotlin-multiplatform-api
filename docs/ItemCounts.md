
# ItemCounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**movieCount** | **kotlin.Int** | Gets or sets the movie count. |  [optional]
**seriesCount** | **kotlin.Int** | Gets or sets the series count. |  [optional]
**episodeCount** | **kotlin.Int** | Gets or sets the episode count. |  [optional]
**artistCount** | **kotlin.Int** | Gets or sets the artist count. |  [optional]
**programCount** | **kotlin.Int** | Gets or sets the program count. |  [optional]
**trailerCount** | **kotlin.Int** | Gets or sets the trailer count. |  [optional]
**songCount** | **kotlin.Int** | Gets or sets the song count. |  [optional]
**albumCount** | **kotlin.Int** | Gets or sets the album count. |  [optional]
**musicVideoCount** | **kotlin.Int** | Gets or sets the music video count. |  [optional]
**boxSetCount** | **kotlin.Int** | Gets or sets the box set count. |  [optional]
**bookCount** | **kotlin.Int** | Gets or sets the book count. |  [optional]
**itemCount** | **kotlin.Int** | Gets or sets the item count. |  [optional]



