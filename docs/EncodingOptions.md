
# EncodingOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encodingThreadCount** | **kotlin.Int** |  |  [optional]
**transcodingTempPath** | **kotlin.String** |  |  [optional]
**fallbackFontPath** | **kotlin.String** |  |  [optional]
**enableFallbackFont** | **kotlin.Boolean** |  |  [optional]
**downMixAudioBoost** | **kotlin.Double** |  |  [optional]
**maxMuxingQueueSize** | **kotlin.Int** |  |  [optional]
**enableThrottling** | **kotlin.Boolean** |  |  [optional]
**throttleDelaySeconds** | **kotlin.Int** |  |  [optional]
**hardwareAccelerationType** | **kotlin.String** |  |  [optional]
**encoderAppPath** | **kotlin.String** | Gets or sets the FFmpeg path as set by the user via the UI. |  [optional]
**encoderAppPathDisplay** | **kotlin.String** | Gets or sets the current FFmpeg path being used by the system and displayed on the transcode page. |  [optional]
**vaapiDevice** | **kotlin.String** |  |  [optional]
**enableTonemapping** | **kotlin.Boolean** |  |  [optional]
**enableVppTonemapping** | **kotlin.Boolean** |  |  [optional]
**tonemappingAlgorithm** | **kotlin.String** |  |  [optional]
**tonemappingRange** | **kotlin.String** |  |  [optional]
**tonemappingDesat** | **kotlin.Double** |  |  [optional]
**tonemappingThreshold** | **kotlin.Double** |  |  [optional]
**tonemappingPeak** | **kotlin.Double** |  |  [optional]
**tonemappingParam** | **kotlin.Double** |  |  [optional]
**vppTonemappingBrightness** | **kotlin.Double** |  |  [optional]
**vppTonemappingContrast** | **kotlin.Double** |  |  [optional]
**h264Crf** | **kotlin.Int** |  |  [optional]
**h265Crf** | **kotlin.Int** |  |  [optional]
**encoderPreset** | **kotlin.String** |  |  [optional]
**deinterlaceDoubleRate** | **kotlin.Boolean** |  |  [optional]
**deinterlaceMethod** | **kotlin.String** |  |  [optional]
**enableDecodingColorDepth10Hevc** | **kotlin.Boolean** |  |  [optional]
**enableDecodingColorDepth10Vp9** | **kotlin.Boolean** |  |  [optional]
**enableEnhancedNvdecDecoder** | **kotlin.Boolean** |  |  [optional]
**preferSystemNativeHwDecoder** | **kotlin.Boolean** |  |  [optional]
**enableIntelLowPowerH264HwEncoder** | **kotlin.Boolean** |  |  [optional]
**enableIntelLowPowerHevcHwEncoder** | **kotlin.Boolean** |  |  [optional]
**enableHardwareEncoding** | **kotlin.Boolean** |  |  [optional]
**allowHevcEncoding** | **kotlin.Boolean** |  |  [optional]
**enableSubtitleExtraction** | **kotlin.Boolean** |  |  [optional]
**hardwareDecodingCodecs** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**allowOnDemandMetadataBasedKeyframeExtractionForExtensions** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



