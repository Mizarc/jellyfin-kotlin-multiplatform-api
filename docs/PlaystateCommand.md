
# PlaystateCommand

## Enum


    * `stop` (value: `"Stop"`)

    * `pause` (value: `"Pause"`)

    * `unpause` (value: `"Unpause"`)

    * `nextTrack` (value: `"NextTrack"`)

    * `previousTrack` (value: `"PreviousTrack"`)

    * `seek` (value: `"Seek"`)

    * `rewind` (value: `"Rewind"`)

    * `fastForward` (value: `"FastForward"`)

    * `playPause` (value: `"PlayPause"`)



