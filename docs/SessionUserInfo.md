
# SessionUserInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **kotlin.String** | Gets or sets the user identifier. |  [optional]
**userName** | **kotlin.String** | Gets or sets the name of the user. |  [optional]



