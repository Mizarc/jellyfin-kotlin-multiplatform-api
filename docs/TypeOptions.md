
# TypeOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **kotlin.String** |  |  [optional]
**metadataFetchers** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**metadataFetcherOrder** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**imageFetchers** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**imageFetcherOrder** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**imageOptions** | [**kotlin.collections.List&lt;ImageOption&gt;**](ImageOption.md) |  |  [optional]



