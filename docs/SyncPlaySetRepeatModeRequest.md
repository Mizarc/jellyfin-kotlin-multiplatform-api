
# SyncPlaySetRepeatModeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mode** | [**GroupRepeatMode**](GroupRepeatMode.md) |  |  [optional]



