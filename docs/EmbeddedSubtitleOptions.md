
# EmbeddedSubtitleOptions

## Enum


    * `allowAll` (value: `"AllowAll"`)

    * `allowText` (value: `"AllowText"`)

    * `allowImage` (value: `"AllowImage"`)

    * `allowNone` (value: `"AllowNone"`)



