
# CreateAdminNotificationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the notification name. |  [optional]
**description** | **kotlin.String** | Gets or sets the notification description. |  [optional]
**notificationLevel** | [**NotificationLevel**](NotificationLevel.md) |  |  [optional]
**url** | **kotlin.String** | Gets or sets the notification url. |  [optional]



