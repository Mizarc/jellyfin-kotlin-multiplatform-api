
# HttpHeaderInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  |  [optional]
**&#x60;value&#x60;** | **kotlin.String** |  |  [optional]
**match** | [**HeaderMatchType**](HeaderMatchType.md) |  |  [optional]



