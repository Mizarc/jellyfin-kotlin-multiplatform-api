# ConfigurationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getConfiguration**](ConfigurationApi.md#getConfiguration) | **GET** /System/Configuration | Gets application configuration.
[**getDefaultMetadataOptions**](ConfigurationApi.md#getDefaultMetadataOptions) | **GET** /System/Configuration/MetadataOptions/Default | Gets a default MetadataOptions object.
[**getNamedConfiguration**](ConfigurationApi.md#getNamedConfiguration) | **GET** /System/Configuration/{key} | Gets a named configuration.
[**updateConfiguration**](ConfigurationApi.md#updateConfiguration) | **POST** /System/Configuration | Updates application configuration.
[**updateMediaEncoderPath**](ConfigurationApi.md#updateMediaEncoderPath) | **POST** /System/MediaEncoder/Path | Updates the path to the media encoder.
[**updateNamedConfiguration**](ConfigurationApi.md#updateNamedConfiguration) | **POST** /System/Configuration/{key} | Updates named configuration.


<a name="getConfiguration"></a>
# **getConfiguration**
> ServerConfiguration getConfiguration()

Gets application configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ConfigurationApi()
try {
    val result : ServerConfiguration = apiInstance.getConfiguration()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigurationApi#getConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigurationApi#getConfiguration")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfiguration**](ServerConfiguration.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getDefaultMetadataOptions"></a>
# **getDefaultMetadataOptions**
> MetadataOptions getDefaultMetadataOptions()

Gets a default MetadataOptions object.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ConfigurationApi()
try {
    val result : MetadataOptions = apiInstance.getDefaultMetadataOptions()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigurationApi#getDefaultMetadataOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigurationApi#getDefaultMetadataOptions")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MetadataOptions**](MetadataOptions.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getNamedConfiguration"></a>
# **getNamedConfiguration**
> org.openapitools.client.infrastructure.OctetByteArray getNamedConfiguration(key)

Gets a named configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ConfigurationApi()
val key : kotlin.String = key_example // kotlin.String | Configuration key.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getNamedConfiguration(key)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigurationApi#getNamedConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigurationApi#getNamedConfiguration")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **kotlin.String**| Configuration key. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateConfiguration"></a>
# **updateConfiguration**
> updateConfiguration(updateConfigurationRequest)

Updates application configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ConfigurationApi()
val updateConfigurationRequest : UpdateConfigurationRequest =  // UpdateConfigurationRequest | Configuration.
try {
    apiInstance.updateConfiguration(updateConfigurationRequest)
} catch (e: ClientException) {
    println("4xx response calling ConfigurationApi#updateConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigurationApi#updateConfiguration")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateConfigurationRequest** | [**UpdateConfigurationRequest**](UpdateConfigurationRequest.md)| Configuration. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="updateMediaEncoderPath"></a>
# **updateMediaEncoderPath**
> updateMediaEncoderPath(updateMediaEncoderPathRequest)

Updates the path to the media encoder.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ConfigurationApi()
val updateMediaEncoderPathRequest : UpdateMediaEncoderPathRequest =  // UpdateMediaEncoderPathRequest | Media encoder path form body.
try {
    apiInstance.updateMediaEncoderPath(updateMediaEncoderPathRequest)
} catch (e: ClientException) {
    println("4xx response calling ConfigurationApi#updateMediaEncoderPath")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigurationApi#updateMediaEncoderPath")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateMediaEncoderPathRequest** | [**UpdateMediaEncoderPathRequest**](UpdateMediaEncoderPathRequest.md)| Media encoder path form body. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="updateNamedConfiguration"></a>
# **updateNamedConfiguration**
> updateNamedConfiguration(key, body)

Updates named configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = ConfigurationApi()
val key : kotlin.String = key_example // kotlin.String | Configuration key.
val body : kotlin.Any =  // kotlin.Any | Configuration.
try {
    apiInstance.updateNamedConfiguration(key, body)
} catch (e: ClientException) {
    println("4xx response calling ConfigurationApi#updateNamedConfiguration")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigurationApi#updateNamedConfiguration")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **kotlin.String**| Configuration key. |
 **body** | **kotlin.Any**| Configuration. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

