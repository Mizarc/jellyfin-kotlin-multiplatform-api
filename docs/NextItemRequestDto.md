
# NextItemRequestDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlistItemId** | **kotlin.String** | Gets or sets the playing item identifier. |  [optional]



