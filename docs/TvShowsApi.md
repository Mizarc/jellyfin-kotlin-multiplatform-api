# TvShowsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEpisodes**](TvShowsApi.md#getEpisodes) | **GET** /Shows/{seriesId}/Episodes | Gets episodes for a tv season.
[**getNextUp**](TvShowsApi.md#getNextUp) | **GET** /Shows/NextUp | Gets a list of next up episodes.
[**getSeasons**](TvShowsApi.md#getSeasons) | **GET** /Shows/{seriesId}/Seasons | Gets seasons for a tv series.
[**getUpcomingEpisodes**](TvShowsApi.md#getUpcomingEpisodes) | **GET** /Shows/Upcoming | Gets a list of upcoming episodes.


<a name="getEpisodes"></a>
# **getEpisodes**
> BaseItemDtoQueryResult getEpisodes(seriesId, userId, fields, season, seasonId, isMissing, adjacentTo, startItemId, startIndex, limit, enableImages, imageTypeLimit, enableImageTypes, enableUserData, sortBy)

Gets episodes for a tv season.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = TvShowsApi()
val seriesId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The series id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
val season : kotlin.Int = 56 // kotlin.Int | Optional filter by season number.
val seasonId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by season id.
val isMissing : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by items that are missing episodes or not.
val adjacentTo : kotlin.String = adjacentTo_example // kotlin.String | Optional. Return items that are siblings of a supplied item.
val startItemId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Skip through the list until a given item is found.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional, include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional, the max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val sortBy : kotlin.String = sortBy_example // kotlin.String | Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getEpisodes(seriesId, userId, fields, season, seasonId, isMissing, adjacentTo, startItemId, startIndex, limit, enableImages, imageTypeLimit, enableImageTypes, enableUserData, sortBy)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling TvShowsApi#getEpisodes")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling TvShowsApi#getEpisodes")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seriesId** | **kotlin.String**| The series id. |
 **userId** | **kotlin.String**| The user id. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]
 **season** | **kotlin.Int**| Optional filter by season number. | [optional]
 **seasonId** | **kotlin.String**| Optional. Filter by season id. | [optional]
 **isMissing** | **kotlin.Boolean**| Optional. Filter by items that are missing episodes or not. | [optional]
 **adjacentTo** | **kotlin.String**| Optional. Return items that are siblings of a supplied item. | [optional]
 **startItemId** | **kotlin.String**| Optional. Skip through the list until a given item is found. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional, include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional, the max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **sortBy** | **kotlin.String**| Optional. Specify one or more sort orders, comma delimited. Options: Album, AlbumArtist, Artist, Budget, CommunityRating, CriticRating, DateCreated, DatePlayed, PlayCount, PremiereDate, ProductionYear, SortName, Random, Revenue, Runtime. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getNextUp"></a>
# **getNextUp**
> BaseItemDtoQueryResult getNextUp(userId, startIndex, limit, fields, seriesId, parentId, enableImages, imageTypeLimit, enableImageTypes, enableUserData, nextUpDateCutoff, enableTotalRecordCount, disableFirstEpisode, enableRewatching)

Gets a list of next up episodes.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = TvShowsApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id of the user to get the next up episodes for.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val seriesId : kotlin.String = seriesId_example // kotlin.String | Optional. Filter by series id.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Specify this to localize the search to a specific item or folder. Omit to use the root.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val nextUpDateCutoff : kotlin.String = 2013-10-20T19:20:30+01:00 // kotlin.String | Optional. Starting date of shows to show in Next Up section.
val enableTotalRecordCount : kotlin.Boolean = true // kotlin.Boolean | Whether to enable the total records count. Defaults to true.
val disableFirstEpisode : kotlin.Boolean = true // kotlin.Boolean | Whether to disable sending the first episode in a series as next up.
val enableRewatching : kotlin.Boolean = true // kotlin.Boolean | Whether to include watched episode in next up results.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getNextUp(userId, startIndex, limit, fields, seriesId, parentId, enableImages, imageTypeLimit, enableImageTypes, enableUserData, nextUpDateCutoff, enableTotalRecordCount, disableFirstEpisode, enableRewatching)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling TvShowsApi#getNextUp")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling TvShowsApi#getNextUp")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id of the user to get the next up episodes for. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **seriesId** | **kotlin.String**| Optional. Filter by series id. | [optional]
 **parentId** | **kotlin.String**| Optional. Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **nextUpDateCutoff** | **kotlin.String**| Optional. Starting date of shows to show in Next Up section. | [optional]
 **enableTotalRecordCount** | **kotlin.Boolean**| Whether to enable the total records count. Defaults to true. | [optional] [default to true]
 **disableFirstEpisode** | **kotlin.Boolean**| Whether to disable sending the first episode in a series as next up. | [optional] [default to false]
 **enableRewatching** | **kotlin.Boolean**| Whether to include watched episode in next up results. | [optional] [default to false]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSeasons"></a>
# **getSeasons**
> BaseItemDtoQueryResult getSeasons(seriesId, userId, fields, isSpecialSeason, isMissing, adjacentTo, enableImages, imageTypeLimit, enableImageTypes, enableUserData)

Gets seasons for a tv series.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = TvShowsApi()
val seriesId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The series id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls.
val isSpecialSeason : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by special season.
val isMissing : kotlin.Boolean = true // kotlin.Boolean | Optional. Filter by items that are missing episodes or not.
val adjacentTo : kotlin.String = adjacentTo_example // kotlin.String | Optional. Return items that are siblings of a supplied item.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getSeasons(seriesId, userId, fields, isSpecialSeason, isMissing, adjacentTo, enableImages, imageTypeLimit, enableImageTypes, enableUserData)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling TvShowsApi#getSeasons")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling TvShowsApi#getSeasons")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seriesId** | **kotlin.String**| The series id. |
 **userId** | **kotlin.String**| The user id. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines, TrailerUrls. | [optional]
 **isSpecialSeason** | **kotlin.Boolean**| Optional. Filter by special season. | [optional]
 **isMissing** | **kotlin.Boolean**| Optional. Filter by items that are missing episodes or not. | [optional]
 **adjacentTo** | **kotlin.String**| Optional. Return items that are siblings of a supplied item. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getUpcomingEpisodes"></a>
# **getUpcomingEpisodes**
> BaseItemDtoQueryResult getUpcomingEpisodes(userId, startIndex, limit, fields, parentId, enableImages, imageTypeLimit, enableImageTypes, enableUserData)

Gets a list of upcoming episodes.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = TvShowsApi()
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id of the user to get the upcoming episodes for.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The record index to start at. All items with a lower index will be dropped from the results.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val parentId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Specify this to localize the search to a specific item or folder. Omit to use the root.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getUpcomingEpisodes(userId, startIndex, limit, fields, parentId, enableImages, imageTypeLimit, enableImageTypes, enableUserData)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling TvShowsApi#getUpcomingEpisodes")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling TvShowsApi#getUpcomingEpisodes")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **kotlin.String**| The user id of the user to get the upcoming episodes for. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The record index to start at. All items with a lower index will be dropped from the results. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **parentId** | **kotlin.String**| Optional. Specify this to localize the search to a specific item or folder. Omit to use the root. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

