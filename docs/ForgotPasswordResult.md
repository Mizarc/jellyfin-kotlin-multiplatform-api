
# ForgotPasswordResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | [**ForgotPasswordAction**](ForgotPasswordAction.md) |  |  [optional]
**pinFile** | **kotlin.String** | Gets or sets the pin file. |  [optional]
**pinExpirationDate** | **kotlin.String** | Gets or sets the pin expiration date. |  [optional]



