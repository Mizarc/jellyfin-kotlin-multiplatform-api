
# GroupStateType

## Enum


    * `idle` (value: `"Idle"`)

    * `waiting` (value: `"Waiting"`)

    * `paused` (value: `"Paused"`)

    * `playing` (value: `"Playing"`)



