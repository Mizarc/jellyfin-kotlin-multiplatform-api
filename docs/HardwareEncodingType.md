
# HardwareEncodingType

## Enum


    * `aMF` (value: `"AMF"`)

    * `qSV` (value: `"QSV"`)

    * `nVENC` (value: `"NVENC"`)

    * `v4L2M2M` (value: `"V4L2M2M"`)

    * `vAAPI` (value: `"VAAPI"`)

    * `videoToolBox` (value: `"VideoToolBox"`)



