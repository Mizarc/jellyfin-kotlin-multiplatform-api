
# SeriesTimerInfoDtoQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**kotlin.collections.List&lt;SeriesTimerInfoDto&gt;**](SeriesTimerInfoDto.md) | Gets or sets the items. |  [optional]
**totalRecordCount** | **kotlin.Int** | Gets or sets the total number of records available. |  [optional]
**startIndex** | **kotlin.Int** | Gets or sets the index of the first record in Items. |  [optional]



