
# TaskTriggerInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **kotlin.String** | Gets or sets the type. |  [optional]
**timeOfDayTicks** | **kotlin.Long** | Gets or sets the time of day. |  [optional]
**intervalTicks** | **kotlin.Long** | Gets or sets the interval. |  [optional]
**dayOfWeek** | [**DayOfWeek**](DayOfWeek.md) |  |  [optional]
**maxRuntimeTicks** | **kotlin.Long** | Gets or sets the maximum runtime ticks. |  [optional]



