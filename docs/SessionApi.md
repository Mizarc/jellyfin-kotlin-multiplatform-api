# SessionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addUserToSession**](SessionApi.md#addUserToSession) | **POST** /Sessions/{sessionId}/User/{userId} | Adds an additional user to a session.
[**displayContent**](SessionApi.md#displayContent) | **POST** /Sessions/{sessionId}/Viewing | Instructs a session to browse to an item or view.
[**getAuthProviders**](SessionApi.md#getAuthProviders) | **GET** /Auth/Providers | Get all auth providers.
[**getPasswordResetProviders**](SessionApi.md#getPasswordResetProviders) | **GET** /Auth/PasswordResetProviders | Get all password reset providers.
[**getSessions**](SessionApi.md#getSessions) | **GET** /Sessions | Gets a list of sessions.
[**play**](SessionApi.md#play) | **POST** /Sessions/{sessionId}/Playing | Instructs a session to play an item.
[**postCapabilities**](SessionApi.md#postCapabilities) | **POST** /Sessions/Capabilities | Updates capabilities for a device.
[**postFullCapabilities**](SessionApi.md#postFullCapabilities) | **POST** /Sessions/Capabilities/Full | Updates capabilities for a device.
[**removeUserFromSession**](SessionApi.md#removeUserFromSession) | **DELETE** /Sessions/{sessionId}/User/{userId} | Removes an additional user from a session.
[**reportSessionEnded**](SessionApi.md#reportSessionEnded) | **POST** /Sessions/Logout | Reports that a session has ended.
[**reportViewing**](SessionApi.md#reportViewing) | **POST** /Sessions/Viewing | Reports that a session is viewing an item.
[**sendFullGeneralCommand**](SessionApi.md#sendFullGeneralCommand) | **POST** /Sessions/{sessionId}/Command | Issues a full general command to a client.
[**sendGeneralCommand**](SessionApi.md#sendGeneralCommand) | **POST** /Sessions/{sessionId}/Command/{command} | Issues a general command to a client.
[**sendMessageCommand**](SessionApi.md#sendMessageCommand) | **POST** /Sessions/{sessionId}/Message | Issues a command to a client to display a message to the user.
[**sendPlaystateCommand**](SessionApi.md#sendPlaystateCommand) | **POST** /Sessions/{sessionId}/Playing/{command} | Issues a playstate command to a client.
[**sendSystemCommand**](SessionApi.md#sendSystemCommand) | **POST** /Sessions/{sessionId}/System/{command} | Issues a system command to a client.


<a name="addUserToSession"></a>
# **addUserToSession**
> addUserToSession(sessionId, userId)

Adds an additional user to a session.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
try {
    apiInstance.addUserToSession(sessionId, userId)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#addUserToSession")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#addUserToSession")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **userId** | **kotlin.String**| The user id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="displayContent"></a>
# **displayContent**
> displayContent(sessionId, itemType, itemId, itemName)

Instructs a session to browse to an item or view.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session Id.
val itemType : BaseItemKind =  // BaseItemKind | The type of item to browse to.
val itemId : kotlin.String = itemId_example // kotlin.String | The Id of the item.
val itemName : kotlin.String = itemName_example // kotlin.String | The name of the item.
try {
    apiInstance.displayContent(sessionId, itemType, itemId, itemName)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#displayContent")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#displayContent")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session Id. |
 **itemType** | [**BaseItemKind**](.md)| The type of item to browse to. | [enum: AggregateFolder, Audio, AudioBook, BasePluginFolder, Book, BoxSet, Channel, ChannelFolderItem, CollectionFolder, Episode, Folder, Genre, ManualPlaylistsFolder, Movie, LiveTvChannel, LiveTvProgram, MusicAlbum, MusicArtist, MusicGenre, MusicVideo, Person, Photo, PhotoAlbum, Playlist, PlaylistsFolder, Program, Recording, Season, Series, Studio, Trailer, TvChannel, TvProgram, UserRootFolder, UserView, Video, Year]
 **itemId** | **kotlin.String**| The Id of the item. |
 **itemName** | **kotlin.String**| The name of the item. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAuthProviders"></a>
# **getAuthProviders**
> kotlin.collections.List&lt;NameIdPair&gt; getAuthProviders()

Get all auth providers.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
try {
    val result : kotlin.collections.List<NameIdPair> = apiInstance.getAuthProviders()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#getAuthProviders")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#getAuthProviders")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;NameIdPair&gt;**](NameIdPair.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getPasswordResetProviders"></a>
# **getPasswordResetProviders**
> kotlin.collections.List&lt;NameIdPair&gt; getPasswordResetProviders()

Get all password reset providers.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
try {
    val result : kotlin.collections.List<NameIdPair> = apiInstance.getPasswordResetProviders()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#getPasswordResetProviders")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#getPasswordResetProviders")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;NameIdPair&gt;**](NameIdPair.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getSessions"></a>
# **getSessions**
> kotlin.collections.List&lt;SessionInfo&gt; getSessions(controllableByUserId, deviceId, activeWithinSeconds)

Gets a list of sessions.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val controllableByUserId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Filter by sessions that a given user is allowed to remote control.
val deviceId : kotlin.String = deviceId_example // kotlin.String | Filter by device Id.
val activeWithinSeconds : kotlin.Int = 56 // kotlin.Int | Optional. Filter by sessions that were active in the last n seconds.
try {
    val result : kotlin.collections.List<SessionInfo> = apiInstance.getSessions(controllableByUserId, deviceId, activeWithinSeconds)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#getSessions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#getSessions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **controllableByUserId** | **kotlin.String**| Filter by sessions that a given user is allowed to remote control. | [optional]
 **deviceId** | **kotlin.String**| Filter by device Id. | [optional]
 **activeWithinSeconds** | **kotlin.Int**| Optional. Filter by sessions that were active in the last n seconds. | [optional]

### Return type

[**kotlin.collections.List&lt;SessionInfo&gt;**](SessionInfo.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="play"></a>
# **play**
> play(sessionId, playCommand, itemIds, startPositionTicks, mediaSourceId, audioStreamIndex, subtitleStreamIndex, startIndex)

Instructs a session to play an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val playCommand : PlayCommand =  // PlayCommand | The type of play command to issue (PlayNow, PlayNext, PlayLast). Clients who have not yet implemented play next and play last may play now.
val itemIds : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | The ids of the items to play, comma delimited.
val startPositionTicks : kotlin.Long = 789 // kotlin.Long | The starting position of the first item.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | Optional. The media source id.
val audioStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the audio stream to play.
val subtitleStreamIndex : kotlin.Int = 56 // kotlin.Int | Optional. The index of the subtitle stream to play.
val startIndex : kotlin.Int = 56 // kotlin.Int | Optional. The start index.
try {
    apiInstance.play(sessionId, playCommand, itemIds, startPositionTicks, mediaSourceId, audioStreamIndex, subtitleStreamIndex, startIndex)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#play")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#play")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **playCommand** | [**PlayCommand**](.md)| The type of play command to issue (PlayNow, PlayNext, PlayLast). Clients who have not yet implemented play next and play last may play now. | [enum: PlayNow, PlayNext, PlayLast, PlayInstantMix, PlayShuffle]
 **itemIds** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| The ids of the items to play, comma delimited. |
 **startPositionTicks** | **kotlin.Long**| The starting position of the first item. | [optional]
 **mediaSourceId** | **kotlin.String**| Optional. The media source id. | [optional]
 **audioStreamIndex** | **kotlin.Int**| Optional. The index of the audio stream to play. | [optional]
 **subtitleStreamIndex** | **kotlin.Int**| Optional. The index of the subtitle stream to play. | [optional]
 **startIndex** | **kotlin.Int**| Optional. The start index. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postCapabilities"></a>
# **postCapabilities**
> postCapabilities(id, playableMediaTypes, supportedCommands, supportsMediaControl, supportsSync, supportsPersistentIdentifier)

Updates capabilities for a device.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val id : kotlin.String = id_example // kotlin.String | The session id.
val playableMediaTypes : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | A list of playable media types, comma delimited. Audio, Video, Book, Photo.
val supportedCommands : kotlin.collections.List<GeneralCommandType> =  // kotlin.collections.List<GeneralCommandType> | A list of supported remote control commands, comma delimited.
val supportsMediaControl : kotlin.Boolean = true // kotlin.Boolean | Determines whether media can be played remotely..
val supportsSync : kotlin.Boolean = true // kotlin.Boolean | Determines whether sync is supported.
val supportsPersistentIdentifier : kotlin.Boolean = true // kotlin.Boolean | Determines whether the device supports a unique identifier.
try {
    apiInstance.postCapabilities(id, playableMediaTypes, supportedCommands, supportsMediaControl, supportsSync, supportsPersistentIdentifier)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#postCapabilities")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#postCapabilities")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The session id. | [optional]
 **playableMediaTypes** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| A list of playable media types, comma delimited. Audio, Video, Book, Photo. | [optional]
 **supportedCommands** | [**kotlin.collections.List&lt;GeneralCommandType&gt;**](GeneralCommandType.md)| A list of supported remote control commands, comma delimited. | [optional]
 **supportsMediaControl** | **kotlin.Boolean**| Determines whether media can be played remotely.. | [optional] [default to false]
 **supportsSync** | **kotlin.Boolean**| Determines whether sync is supported. | [optional] [default to false]
 **supportsPersistentIdentifier** | **kotlin.Boolean**| Determines whether the device supports a unique identifier. | [optional] [default to true]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postFullCapabilities"></a>
# **postFullCapabilities**
> postFullCapabilities(postFullCapabilitiesRequest, id)

Updates capabilities for a device.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val postFullCapabilitiesRequest : PostFullCapabilitiesRequest =  // PostFullCapabilitiesRequest | The MediaBrowser.Model.Session.ClientCapabilities.
val id : kotlin.String = id_example // kotlin.String | The session id.
try {
    apiInstance.postFullCapabilities(postFullCapabilitiesRequest, id)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#postFullCapabilities")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#postFullCapabilities")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postFullCapabilitiesRequest** | [**PostFullCapabilitiesRequest**](PostFullCapabilitiesRequest.md)| The MediaBrowser.Model.Session.ClientCapabilities. |
 **id** | **kotlin.String**| The session id. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="removeUserFromSession"></a>
# **removeUserFromSession**
> removeUserFromSession(sessionId, userId)

Removes an additional user from a session.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The user id.
try {
    apiInstance.removeUserFromSession(sessionId, userId)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#removeUserFromSession")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#removeUserFromSession")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **userId** | **kotlin.String**| The user id. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="reportSessionEnded"></a>
# **reportSessionEnded**
> reportSessionEnded()

Reports that a session has ended.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
try {
    apiInstance.reportSessionEnded()
} catch (e: ClientException) {
    println("4xx response calling SessionApi#reportSessionEnded")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#reportSessionEnded")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="reportViewing"></a>
# **reportViewing**
> reportViewing(itemId, sessionId)

Reports that a session is viewing an item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val itemId : kotlin.String = itemId_example // kotlin.String | The item id.
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
try {
    apiInstance.reportViewing(itemId, sessionId)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#reportViewing")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#reportViewing")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **kotlin.String**| The item id. |
 **sessionId** | **kotlin.String**| The session id. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="sendFullGeneralCommand"></a>
# **sendFullGeneralCommand**
> sendFullGeneralCommand(sessionId, sendFullGeneralCommandRequest)

Issues a full general command to a client.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val sendFullGeneralCommandRequest : SendFullGeneralCommandRequest =  // SendFullGeneralCommandRequest | The MediaBrowser.Model.Session.GeneralCommand.
try {
    apiInstance.sendFullGeneralCommand(sessionId, sendFullGeneralCommandRequest)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#sendFullGeneralCommand")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#sendFullGeneralCommand")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **sendFullGeneralCommandRequest** | [**SendFullGeneralCommandRequest**](SendFullGeneralCommandRequest.md)| The MediaBrowser.Model.Session.GeneralCommand. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="sendGeneralCommand"></a>
# **sendGeneralCommand**
> sendGeneralCommand(sessionId, command)

Issues a general command to a client.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val command : GeneralCommandType =  // GeneralCommandType | The command to send.
try {
    apiInstance.sendGeneralCommand(sessionId, command)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#sendGeneralCommand")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#sendGeneralCommand")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **command** | [**GeneralCommandType**](.md)| The command to send. | [enum: MoveUp, MoveDown, MoveLeft, MoveRight, PageUp, PageDown, PreviousLetter, NextLetter, ToggleOsd, ToggleContextMenu, Select, Back, TakeScreenshot, SendKey, SendString, GoHome, GoToSettings, VolumeUp, VolumeDown, Mute, Unmute, ToggleMute, SetVolume, SetAudioStreamIndex, SetSubtitleStreamIndex, ToggleFullscreen, DisplayContent, GoToSearch, DisplayMessage, SetRepeatMode, ChannelUp, ChannelDown, Guide, ToggleStats, PlayMediaSource, PlayTrailers, SetShuffleQueue, PlayState, PlayNext, ToggleOsdMenu, Play, SetMaxStreamingBitrate]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="sendMessageCommand"></a>
# **sendMessageCommand**
> sendMessageCommand(sessionId, sendMessageCommandRequest)

Issues a command to a client to display a message to the user.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val sendMessageCommandRequest : SendMessageCommandRequest =  // SendMessageCommandRequest | The MediaBrowser.Model.Session.MessageCommand object containing Header, Message Text, and TimeoutMs.
try {
    apiInstance.sendMessageCommand(sessionId, sendMessageCommandRequest)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#sendMessageCommand")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#sendMessageCommand")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **sendMessageCommandRequest** | [**SendMessageCommandRequest**](SendMessageCommandRequest.md)| The MediaBrowser.Model.Session.MessageCommand object containing Header, Message Text, and TimeoutMs. |

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined

<a name="sendPlaystateCommand"></a>
# **sendPlaystateCommand**
> sendPlaystateCommand(sessionId, command, seekPositionTicks, controllingUserId)

Issues a playstate command to a client.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val command : PlaystateCommand =  // PlaystateCommand | The MediaBrowser.Model.Session.PlaystateCommand.
val seekPositionTicks : kotlin.Long = 789 // kotlin.Long | The optional position ticks.
val controllingUserId : kotlin.String = controllingUserId_example // kotlin.String | The optional controlling user id.
try {
    apiInstance.sendPlaystateCommand(sessionId, command, seekPositionTicks, controllingUserId)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#sendPlaystateCommand")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#sendPlaystateCommand")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **command** | [**PlaystateCommand**](.md)| The MediaBrowser.Model.Session.PlaystateCommand. | [enum: Stop, Pause, Unpause, NextTrack, PreviousTrack, Seek, Rewind, FastForward, PlayPause]
 **seekPositionTicks** | **kotlin.Long**| The optional position ticks. | [optional]
 **controllingUserId** | **kotlin.String**| The optional controlling user id. | [optional]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="sendSystemCommand"></a>
# **sendSystemCommand**
> sendSystemCommand(sessionId, command)

Issues a system command to a client.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = SessionApi()
val sessionId : kotlin.String = sessionId_example // kotlin.String | The session id.
val command : GeneralCommandType =  // GeneralCommandType | The command to send.
try {
    apiInstance.sendSystemCommand(sessionId, command)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#sendSystemCommand")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#sendSystemCommand")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **kotlin.String**| The session id. |
 **command** | [**GeneralCommandType**](.md)| The command to send. | [enum: MoveUp, MoveDown, MoveLeft, MoveRight, PageUp, PageDown, PreviousLetter, NextLetter, ToggleOsd, ToggleContextMenu, Select, Back, TakeScreenshot, SendKey, SendString, GoHome, GoToSettings, VolumeUp, VolumeDown, Mute, Unmute, ToggleMute, SetVolume, SetAudioStreamIndex, SetSubtitleStreamIndex, ToggleFullscreen, DisplayContent, GoToSearch, DisplayMessage, SetRepeatMode, ChannelUp, ChannelDown, Guide, ToggleStats, PlayMediaSource, PlayTrailers, SetShuffleQueue, PlayState, PlayNext, ToggleOsdMenu, Play, SetMaxStreamingBitrate]

### Return type

null (empty response body)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

