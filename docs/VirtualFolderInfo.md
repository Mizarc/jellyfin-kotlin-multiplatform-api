
# VirtualFolderInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**locations** | **kotlin.collections.List&lt;kotlin.String&gt;** | Gets or sets the locations. |  [optional]
**collectionType** | [**CollectionTypeOptions**](CollectionTypeOptions.md) |  |  [optional]
**libraryOptions** | [**LibraryOptions**](LibraryOptions.md) |  |  [optional]
**itemId** | **kotlin.String** | Gets or sets the item identifier. |  [optional]
**primaryImageItemId** | **kotlin.String** | Gets or sets the primary image item identifier. |  [optional]
**refreshProgress** | **kotlin.Double** |  |  [optional]
**refreshStatus** | **kotlin.String** |  |  [optional]



