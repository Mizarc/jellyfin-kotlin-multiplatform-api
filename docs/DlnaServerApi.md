# DlnaServerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getConnectionManager**](DlnaServerApi.md#getConnectionManager) | **GET** /Dlna/{serverId}/ConnectionManager | Gets Dlna media receiver registrar xml.
[**getConnectionManager2**](DlnaServerApi.md#getConnectionManager2) | **GET** /Dlna/{serverId}/ConnectionManager/ConnectionManager | Gets Dlna media receiver registrar xml.
[**getConnectionManager3**](DlnaServerApi.md#getConnectionManager3) | **GET** /Dlna/{serverId}/ConnectionManager/ConnectionManager.xml | Gets Dlna media receiver registrar xml.
[**getContentDirectory**](DlnaServerApi.md#getContentDirectory) | **GET** /Dlna/{serverId}/ContentDirectory | Gets Dlna content directory xml.
[**getContentDirectory2**](DlnaServerApi.md#getContentDirectory2) | **GET** /Dlna/{serverId}/ContentDirectory/ContentDirectory | Gets Dlna content directory xml.
[**getContentDirectory3**](DlnaServerApi.md#getContentDirectory3) | **GET** /Dlna/{serverId}/ContentDirectory/ContentDirectory.xml | Gets Dlna content directory xml.
[**getDescriptionXml**](DlnaServerApi.md#getDescriptionXml) | **GET** /Dlna/{serverId}/description | Get Description Xml.
[**getDescriptionXml2**](DlnaServerApi.md#getDescriptionXml2) | **GET** /Dlna/{serverId}/description.xml | Get Description Xml.
[**getIcon**](DlnaServerApi.md#getIcon) | **GET** /Dlna/icons/{fileName} | Gets a server icon.
[**getIconId**](DlnaServerApi.md#getIconId) | **GET** /Dlna/{serverId}/icons/{fileName} | Gets a server icon.
[**getMediaReceiverRegistrar**](DlnaServerApi.md#getMediaReceiverRegistrar) | **GET** /Dlna/{serverId}/MediaReceiverRegistrar | Gets Dlna media receiver registrar xml.
[**getMediaReceiverRegistrar2**](DlnaServerApi.md#getMediaReceiverRegistrar2) | **GET** /Dlna/{serverId}/MediaReceiverRegistrar/MediaReceiverRegistrar | Gets Dlna media receiver registrar xml.
[**getMediaReceiverRegistrar3**](DlnaServerApi.md#getMediaReceiverRegistrar3) | **GET** /Dlna/{serverId}/MediaReceiverRegistrar/MediaReceiverRegistrar.xml | Gets Dlna media receiver registrar xml.
[**processConnectionManagerControlRequest**](DlnaServerApi.md#processConnectionManagerControlRequest) | **POST** /Dlna/{serverId}/ConnectionManager/Control | Process a connection manager control request.
[**processContentDirectoryControlRequest**](DlnaServerApi.md#processContentDirectoryControlRequest) | **POST** /Dlna/{serverId}/ContentDirectory/Control | Process a content directory control request.
[**processMediaReceiverRegistrarControlRequest**](DlnaServerApi.md#processMediaReceiverRegistrarControlRequest) | **POST** /Dlna/{serverId}/MediaReceiverRegistrar/Control | Process a media receiver registrar control request.


<a name="getConnectionManager"></a>
# **getConnectionManager**
> org.openapitools.client.infrastructure.OctetByteArray getConnectionManager(serverId)

Gets Dlna media receiver registrar xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getConnectionManager(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getConnectionManager")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getConnectionManager")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getConnectionManager2"></a>
# **getConnectionManager2**
> org.openapitools.client.infrastructure.OctetByteArray getConnectionManager2(serverId)

Gets Dlna media receiver registrar xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getConnectionManager2(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getConnectionManager2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getConnectionManager2")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getConnectionManager3"></a>
# **getConnectionManager3**
> org.openapitools.client.infrastructure.OctetByteArray getConnectionManager3(serverId)

Gets Dlna media receiver registrar xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getConnectionManager3(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getConnectionManager3")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getConnectionManager3")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getContentDirectory"></a>
# **getContentDirectory**
> org.openapitools.client.infrastructure.OctetByteArray getContentDirectory(serverId)

Gets Dlna content directory xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getContentDirectory(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getContentDirectory")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getContentDirectory")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getContentDirectory2"></a>
# **getContentDirectory2**
> org.openapitools.client.infrastructure.OctetByteArray getContentDirectory2(serverId)

Gets Dlna content directory xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getContentDirectory2(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getContentDirectory2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getContentDirectory2")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getContentDirectory3"></a>
# **getContentDirectory3**
> org.openapitools.client.infrastructure.OctetByteArray getContentDirectory3(serverId)

Gets Dlna content directory xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getContentDirectory3(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getContentDirectory3")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getContentDirectory3")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getDescriptionXml"></a>
# **getDescriptionXml**
> org.openapitools.client.infrastructure.OctetByteArray getDescriptionXml(serverId)

Get Description Xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getDescriptionXml(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getDescriptionXml")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getDescriptionXml")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getDescriptionXml2"></a>
# **getDescriptionXml2**
> org.openapitools.client.infrastructure.OctetByteArray getDescriptionXml2(serverId)

Get Description Xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getDescriptionXml2(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getDescriptionXml2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getDescriptionXml2")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getIcon"></a>
# **getIcon**
> org.openapitools.client.infrastructure.OctetByteArray getIcon(fileName)

Gets a server icon.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val fileName : kotlin.String = fileName_example // kotlin.String | The icon filename.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getIcon(fileName)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getIcon")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getIcon")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileName** | **kotlin.String**| The icon filename. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getIconId"></a>
# **getIconId**
> org.openapitools.client.infrastructure.OctetByteArray getIconId(serverId, fileName)

Gets a server icon.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
val fileName : kotlin.String = fileName_example // kotlin.String | The icon filename.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getIconId(serverId, fileName)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getIconId")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getIconId")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |
 **fileName** | **kotlin.String**| The icon filename. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getMediaReceiverRegistrar"></a>
# **getMediaReceiverRegistrar**
> org.openapitools.client.infrastructure.OctetByteArray getMediaReceiverRegistrar(serverId)

Gets Dlna media receiver registrar xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMediaReceiverRegistrar(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getMediaReceiverRegistrar")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getMediaReceiverRegistrar")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getMediaReceiverRegistrar2"></a>
# **getMediaReceiverRegistrar2**
> org.openapitools.client.infrastructure.OctetByteArray getMediaReceiverRegistrar2(serverId)

Gets Dlna media receiver registrar xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMediaReceiverRegistrar2(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getMediaReceiverRegistrar2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getMediaReceiverRegistrar2")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="getMediaReceiverRegistrar3"></a>
# **getMediaReceiverRegistrar3**
> org.openapitools.client.infrastructure.OctetByteArray getMediaReceiverRegistrar3(serverId)

Gets Dlna media receiver registrar xml.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getMediaReceiverRegistrar3(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#getMediaReceiverRegistrar3")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#getMediaReceiverRegistrar3")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="processConnectionManagerControlRequest"></a>
# **processConnectionManagerControlRequest**
> org.openapitools.client.infrastructure.OctetByteArray processConnectionManagerControlRequest(serverId)

Process a connection manager control request.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.processConnectionManagerControlRequest(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#processConnectionManagerControlRequest")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#processConnectionManagerControlRequest")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="processContentDirectoryControlRequest"></a>
# **processContentDirectoryControlRequest**
> org.openapitools.client.infrastructure.OctetByteArray processContentDirectoryControlRequest(serverId)

Process a content directory control request.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.processContentDirectoryControlRequest(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#processContentDirectoryControlRequest")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#processContentDirectoryControlRequest")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

<a name="processMediaReceiverRegistrarControlRequest"></a>
# **processMediaReceiverRegistrarControlRequest**
> org.openapitools.client.infrastructure.OctetByteArray processMediaReceiverRegistrarControlRequest(serverId)

Process a media receiver registrar control request.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = DlnaServerApi()
val serverId : kotlin.String = serverId_example // kotlin.String | Server UUID.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.processMediaReceiverRegistrarControlRequest(serverId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DlnaServerApi#processMediaReceiverRegistrarControlRequest")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DlnaServerApi#processMediaReceiverRegistrarControlRequest")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **kotlin.String**| Server UUID. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

