# BrandingApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBrandingCss**](BrandingApi.md#getBrandingCss) | **GET** /Branding/Css | Gets branding css.
[**getBrandingCss2**](BrandingApi.md#getBrandingCss2) | **GET** /Branding/Css.css | Gets branding css.
[**getBrandingOptions**](BrandingApi.md#getBrandingOptions) | **GET** /Branding/Configuration | Gets branding configuration.


<a name="getBrandingCss"></a>
# **getBrandingCss**
> kotlin.String getBrandingCss()

Gets branding css.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = BrandingApi()
try {
    val result : kotlin.String = apiInstance.getBrandingCss()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling BrandingApi#getBrandingCss")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling BrandingApi#getBrandingCss")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/css, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getBrandingCss2"></a>
# **getBrandingCss2**
> kotlin.String getBrandingCss2()

Gets branding css.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = BrandingApi()
try {
    val result : kotlin.String = apiInstance.getBrandingCss2()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling BrandingApi#getBrandingCss2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling BrandingApi#getBrandingCss2")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/css, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getBrandingOptions"></a>
# **getBrandingOptions**
> BrandingOptions getBrandingOptions()

Gets branding configuration.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = BrandingApi()
try {
    val result : BrandingOptions = apiInstance.getBrandingOptions()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling BrandingApi#getBrandingOptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling BrandingApi#getBrandingOptions")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BrandingOptions**](BrandingOptions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

