
# AuthenticationResultUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**serverId** | **kotlin.String** | Gets or sets the server identifier. |  [optional]
**serverName** | **kotlin.String** | Gets or sets the name of the server.  This is not used by the server and is for client-side usage only. |  [optional]
**id** | **kotlin.String** | Gets or sets the id. |  [optional]
**primaryImageTag** | **kotlin.String** | Gets or sets the primary image tag. |  [optional]
**hasPassword** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance has password. |  [optional]
**hasConfiguredPassword** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance has configured password. |  [optional]
**hasConfiguredEasyPassword** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance has configured easy password. |  [optional]
**enableAutoLogin** | **kotlin.Boolean** | Gets or sets whether async login is enabled or not. |  [optional]
**lastLoginDate** | **kotlin.String** | Gets or sets the last login date. |  [optional]
**lastActivityDate** | **kotlin.String** | Gets or sets the last activity date. |  [optional]
**configuration** | [**UserDtoConfiguration**](UserDtoConfiguration.md) |  |  [optional]
**policy** | [**UserDtoPolicy**](UserDtoPolicy.md) |  |  [optional]
**primaryImageAspectRatio** | **kotlin.Double** | Gets or sets the primary image aspect ratio. |  [optional]



