
# ForgotPasswordPinDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pin** | **kotlin.String** | Gets or sets the entered pin to have the password reset. | 



