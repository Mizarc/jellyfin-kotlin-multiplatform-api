
# BaseItemDtoUserData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rating** | **kotlin.Double** | Gets or sets the rating. |  [optional]
**playedPercentage** | **kotlin.Double** | Gets or sets the played percentage. |  [optional]
**unplayedItemCount** | **kotlin.Int** | Gets or sets the unplayed item count. |  [optional]
**playbackPositionTicks** | **kotlin.Long** | Gets or sets the playback position ticks. |  [optional]
**playCount** | **kotlin.Int** | Gets or sets the play count. |  [optional]
**isFavorite** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance is favorite. |  [optional]
**likes** | **kotlin.Boolean** | Gets or sets a value indicating whether this MediaBrowser.Model.Dto.UserItemDataDto is likes. |  [optional]
**lastPlayedDate** | **kotlin.String** | Gets or sets the last played date. |  [optional]
**played** | **kotlin.Boolean** | Gets or sets a value indicating whether this MediaBrowser.Model.Dto.UserItemDataDto is played. |  [optional]
**key** | **kotlin.String** | Gets or sets the key. |  [optional]
**itemId** | **kotlin.String** | Gets or sets the item identifier. |  [optional]



