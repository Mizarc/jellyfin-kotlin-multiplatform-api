
# NameIdPair

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**id** | **kotlin.String** | Gets or sets the identifier. |  [optional]



