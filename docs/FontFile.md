
# FontFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the name. |  [optional]
**propertySize** | **kotlin.Long** | Gets or sets the size. |  [optional]
**dateCreated** | **kotlin.String** | Gets or sets the date created. |  [optional]
**dateModified** | **kotlin.String** | Gets or sets the date modified. |  [optional]



