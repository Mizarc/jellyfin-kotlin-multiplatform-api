
# AuthenticationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**AuthenticationResultUser**](AuthenticationResultUser.md) |  |  [optional]
**sessionInfo** | [**AuthenticationResultSessionInfo**](AuthenticationResultSessionInfo.md) |  |  [optional]
**accessToken** | **kotlin.String** |  |  [optional]
**serverId** | **kotlin.String** |  |  [optional]



