
# UpdateUserEasyPasswordRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newPassword** | **kotlin.String** | Gets or sets the new sha1-hashed password. |  [optional]
**newPw** | **kotlin.String** | Gets or sets the new password. |  [optional]
**resetPassword** | **kotlin.Boolean** | Gets or sets a value indicating whether to reset the password. |  [optional]



