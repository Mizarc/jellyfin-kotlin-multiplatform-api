
# RemoteSubtitleInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**threeLetterISOLanguageName** | **kotlin.String** |  |  [optional]
**id** | **kotlin.String** |  |  [optional]
**providerName** | **kotlin.String** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**format** | **kotlin.String** |  |  [optional]
**author** | **kotlin.String** |  |  [optional]
**comment** | **kotlin.String** |  |  [optional]
**dateCreated** | **kotlin.String** |  |  [optional]
**communityRating** | **kotlin.Float** |  |  [optional]
**downloadCount** | **kotlin.Int** |  |  [optional]
**isHashMatch** | **kotlin.Boolean** |  |  [optional]



