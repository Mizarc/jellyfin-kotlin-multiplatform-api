
# ItemFilter

## Enum


    * `isFolder` (value: `"IsFolder"`)

    * `isNotFolder` (value: `"IsNotFolder"`)

    * `isUnplayed` (value: `"IsUnplayed"`)

    * `isPlayed` (value: `"IsPlayed"`)

    * `isFavorite` (value: `"IsFavorite"`)

    * `isResumable` (value: `"IsResumable"`)

    * `likes` (value: `"Likes"`)

    * `dislikes` (value: `"Dislikes"`)

    * `isFavoriteOrLikes` (value: `"IsFavoriteOrLikes"`)



