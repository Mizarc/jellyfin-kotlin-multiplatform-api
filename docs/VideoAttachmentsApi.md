# VideoAttachmentsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAttachment**](VideoAttachmentsApi.md#getAttachment) | **GET** /Videos/{videoId}/{mediaSourceId}/Attachments/{index} | Get video attachment.


<a name="getAttachment"></a>
# **getAttachment**
> org.openapitools.client.infrastructure.OctetByteArray getAttachment(videoId, mediaSourceId, index)

Get video attachment.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = VideoAttachmentsApi()
val videoId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Video ID.
val mediaSourceId : kotlin.String = mediaSourceId_example // kotlin.String | Media Source ID.
val index : kotlin.Int = 56 // kotlin.Int | Attachment Index.
try {
    val result : org.openapitools.client.infrastructure.OctetByteArray = apiInstance.getAttachment(videoId, mediaSourceId, index)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoAttachmentsApi#getAttachment")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoAttachmentsApi#getAttachment")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **kotlin.String**| Video ID. |
 **mediaSourceId** | **kotlin.String**| Media Source ID. |
 **index** | **kotlin.Int**| Attachment Index. |

### Return type

[**org.openapitools.client.infrastructure.OctetByteArray**](org.openapitools.client.infrastructure.OctetByteArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

