
# LiveStreamResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediaSource** | [**LiveStreamResponseMediaSource**](LiveStreamResponseMediaSource.md) |  |  [optional]



