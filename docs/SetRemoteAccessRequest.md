
# SetRemoteAccessRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enableRemoteAccess** | **kotlin.Boolean** | Gets or sets a value indicating whether enable remote access. | 
**enableAutomaticPortMapping** | **kotlin.Boolean** | Gets or sets a value indicating whether enable automatic port mapping. | 



