# InstantMixApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getInstantMixFromAlbum**](InstantMixApi.md#getInstantMixFromAlbum) | **GET** /Albums/{id}/InstantMix | Creates an instant playlist based on a given album.
[**getInstantMixFromArtists**](InstantMixApi.md#getInstantMixFromArtists) | **GET** /Artists/{id}/InstantMix | Creates an instant playlist based on a given artist.
[**getInstantMixFromArtists2**](InstantMixApi.md#getInstantMixFromArtists2) | **GET** /Artists/InstantMix | Creates an instant playlist based on a given artist.
[**getInstantMixFromItem**](InstantMixApi.md#getInstantMixFromItem) | **GET** /Items/{id}/InstantMix | Creates an instant playlist based on a given item.
[**getInstantMixFromMusicGenreById**](InstantMixApi.md#getInstantMixFromMusicGenreById) | **GET** /MusicGenres/InstantMix | Creates an instant playlist based on a given genre.
[**getInstantMixFromMusicGenreByName**](InstantMixApi.md#getInstantMixFromMusicGenreByName) | **GET** /MusicGenres/{name}/InstantMix | Creates an instant playlist based on a given genre.
[**getInstantMixFromPlaylist**](InstantMixApi.md#getInstantMixFromPlaylist) | **GET** /Playlists/{id}/InstantMix | Creates an instant playlist based on a given playlist.
[**getInstantMixFromSong**](InstantMixApi.md#getInstantMixFromSong) | **GET** /Songs/{id}/InstantMix | Creates an instant playlist based on a given song.


<a name="getInstantMixFromAlbum"></a>
# **getInstantMixFromAlbum**
> BaseItemDtoQueryResult getInstantMixFromAlbum(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given album.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val id : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromAlbum(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromAlbum")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromAlbum")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getInstantMixFromArtists"></a>
# **getInstantMixFromArtists**
> BaseItemDtoQueryResult getInstantMixFromArtists(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given artist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val id : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromArtists(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromArtists")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromArtists")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getInstantMixFromArtists2"></a>
# **getInstantMixFromArtists2**
> BaseItemDtoQueryResult getInstantMixFromArtists2(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given artist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val id : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromArtists2(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromArtists2")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromArtists2")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getInstantMixFromItem"></a>
# **getInstantMixFromItem**
> BaseItemDtoQueryResult getInstantMixFromItem(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given item.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val id : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromItem(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromItem")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromItem")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getInstantMixFromMusicGenreById"></a>
# **getInstantMixFromMusicGenreById**
> BaseItemDtoQueryResult getInstantMixFromMusicGenreById(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given genre.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val id : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromMusicGenreById(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromMusicGenreById")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromMusicGenreById")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getInstantMixFromMusicGenreByName"></a>
# **getInstantMixFromMusicGenreByName**
> BaseItemDtoQueryResult getInstantMixFromMusicGenreByName(name, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given genre.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val name : kotlin.String = name_example // kotlin.String | The genre name.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromMusicGenreByName(name, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromMusicGenreByName")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromMusicGenreByName")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The genre name. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getInstantMixFromPlaylist"></a>
# **getInstantMixFromPlaylist**
> BaseItemDtoQueryResult getInstantMixFromPlaylist(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given playlist.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val id : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromPlaylist(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

<a name="getInstantMixFromSong"></a>
# **getInstantMixFromSong**
> BaseItemDtoQueryResult getInstantMixFromSong(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)

Creates an instant playlist based on a given song.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import org.openapitools.client.models.*

val apiInstance = InstantMixApi()
val id : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | The item id.
val userId : kotlin.String = 38400000-8cf0-11bd-b23e-10b96e4ef00d // kotlin.String | Optional. Filter by user id, and attach user data.
val limit : kotlin.Int = 56 // kotlin.Int | Optional. The maximum number of records to return.
val fields : kotlin.collections.List<ItemFields> =  // kotlin.collections.List<ItemFields> | Optional. Specify additional fields of information to return in the output.
val enableImages : kotlin.Boolean = true // kotlin.Boolean | Optional. Include image information in output.
val enableUserData : kotlin.Boolean = true // kotlin.Boolean | Optional. Include user data.
val imageTypeLimit : kotlin.Int = 56 // kotlin.Int | Optional. The max number of images to return, per image type.
val enableImageTypes : kotlin.collections.List<ImageType> =  // kotlin.collections.List<ImageType> | Optional. The image types to include in the output.
try {
    val result : BaseItemDtoQueryResult = apiInstance.getInstantMixFromSong(id, userId, limit, fields, enableImages, enableUserData, imageTypeLimit, enableImageTypes)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstantMixApi#getInstantMixFromSong")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstantMixApi#getInstantMixFromSong")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The item id. |
 **userId** | **kotlin.String**| Optional. Filter by user id, and attach user data. | [optional]
 **limit** | **kotlin.Int**| Optional. The maximum number of records to return. | [optional]
 **fields** | [**kotlin.collections.List&lt;ItemFields&gt;**](ItemFields.md)| Optional. Specify additional fields of information to return in the output. | [optional]
 **enableImages** | **kotlin.Boolean**| Optional. Include image information in output. | [optional]
 **enableUserData** | **kotlin.Boolean**| Optional. Include user data. | [optional]
 **imageTypeLimit** | **kotlin.Int**| Optional. The max number of images to return, per image type. | [optional]
 **enableImageTypes** | [**kotlin.collections.List&lt;ImageType&gt;**](ImageType.md)| Optional. The image types to include in the output. | [optional]

### Return type

[**BaseItemDtoQueryResult**](BaseItemDtoQueryResult.md)

### Authorization


Configure CustomAuthentication:
    ApiClient.apiKey["Authorization"] = ""
    ApiClient.apiKeyPrefix["Authorization"] = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/json; profile=CamelCase, application/json; profile=PascalCase

