
# UpdateMediaPathRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Gets or sets the library name. | 
**pathInfo** | [**UpdateMediaPathRequestDtoPathInfo**](UpdateMediaPathRequestDtoPathInfo.md) |  | 



