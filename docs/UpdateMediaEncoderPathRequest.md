
# UpdateMediaEncoderPathRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **kotlin.String** | Gets or sets media encoder path. |  [optional]
**pathType** | **kotlin.String** | Gets or sets media encoder path type. |  [optional]



