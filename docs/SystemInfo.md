
# SystemInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**localAddress** | **kotlin.String** | Gets or sets the local address. |  [optional]
**serverName** | **kotlin.String** | Gets or sets the name of the server. |  [optional]
**version** | **kotlin.String** | Gets or sets the server version. |  [optional]
**productName** | **kotlin.String** | Gets or sets the product name. This is the AssemblyProduct name. |  [optional]
**operatingSystem** | **kotlin.String** | Gets or sets the operating system. |  [optional]
**id** | **kotlin.String** | Gets or sets the id. |  [optional]
**startupWizardCompleted** | **kotlin.Boolean** | Gets or sets a value indicating whether the startup wizard is completed. |  [optional]
**operatingSystemDisplayName** | **kotlin.String** | Gets or sets the display name of the operating system. |  [optional]
**packageName** | **kotlin.String** | Gets or sets the package name. |  [optional]
**hasPendingRestart** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance has pending restart. |  [optional]
**isShuttingDown** | **kotlin.Boolean** |  |  [optional]
**supportsLibraryMonitor** | **kotlin.Boolean** | Gets or sets a value indicating whether [supports library monitor]. |  [optional]
**webSocketPortNumber** | **kotlin.Int** | Gets or sets the web socket port number. |  [optional]
**completedInstallations** | [**kotlin.collections.List&lt;InstallationInfo&gt;**](InstallationInfo.md) | Gets or sets the completed installations. |  [optional]
**canSelfRestart** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance can self restart. |  [optional]
**canLaunchWebBrowser** | **kotlin.Boolean** |  |  [optional]
**programDataPath** | **kotlin.String** | Gets or sets the program data path. |  [optional]
**webPath** | **kotlin.String** | Gets or sets the web UI resources path. |  [optional]
**itemsByNamePath** | **kotlin.String** | Gets or sets the items by name path. |  [optional]
**cachePath** | **kotlin.String** | Gets or sets the cache path. |  [optional]
**logPath** | **kotlin.String** | Gets or sets the log path. |  [optional]
**internalMetadataPath** | **kotlin.String** | Gets or sets the internal metadata path. |  [optional]
**transcodingTempPath** | **kotlin.String** | Gets or sets the transcode path. |  [optional]
**hasUpdateAvailable** | **kotlin.Boolean** | Gets or sets a value indicating whether this instance has update available. |  [optional]
**encoderLocation** | [**FFmpegLocation**](FFmpegLocation.md) |  |  [optional]
**systemArchitecture** | [**Architecture**](Architecture.md) |  |  [optional]



