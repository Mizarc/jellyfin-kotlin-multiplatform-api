/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*

/**
 * Enum TaskCompletionStatus.
 *
 * Values: completed,failed,cancelled,aborted
 */
@Serializable
enum class TaskCompletionStatus(val value: kotlin.String) {

    @SerialName(value = "Completed")
    completed("Completed"),

    @SerialName(value = "Failed")
    failed("Failed"),

    @SerialName(value = "Cancelled")
    cancelled("Cancelled"),

    @SerialName(value = "Aborted")
    aborted("Aborted");

    /**
     * Override toString() to avoid using the enum variable name as the value, and instead use
     * the actual value defined in the API spec file.
     *
     * This solves a problem when the variable name and its value are different, and ensures that
     * the client sends the correct enum values to the server always.
     */
    override fun toString(): String = value

    companion object {
        /**
         * Converts the provided [data] to a [String] on success, null otherwise.
         */
        fun encode(data: kotlin.Any?): kotlin.String? = if (data is TaskCompletionStatus) "$data" else null

        /**
         * Returns a valid [TaskCompletionStatus] for [data], null otherwise.
         */
        fun decode(data: kotlin.Any?): TaskCompletionStatus? = data?.let {
          val normalizedData = "$it".lowercase()
          values().firstOrNull { value ->
            it == value || normalizedData == "$value".lowercase()
          }
        }
    }
}

