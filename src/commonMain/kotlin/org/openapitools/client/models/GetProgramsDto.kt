/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models

import org.openapitools.client.models.ImageType
import org.openapitools.client.models.ItemFields
import org.openapitools.client.models.SortOrder

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Get programs dto.
 *
 * @param channelIds Gets or sets the channels to return guide information for.
 * @param userId Gets or sets optional. Filter by user id.
 * @param minStartDate Gets or sets the minimum premiere start date.  Optional.
 * @param hasAired Gets or sets filter by programs that have completed airing, or not.  Optional.
 * @param isAiring Gets or sets filter by programs that are currently airing, or not.  Optional.
 * @param maxStartDate Gets or sets the maximum premiere start date.  Optional.
 * @param minEndDate Gets or sets the minimum premiere end date.  Optional.
 * @param maxEndDate Gets or sets the maximum premiere end date.  Optional.
 * @param isMovie Gets or sets filter for movies.  Optional.
 * @param isSeries Gets or sets filter for series.  Optional.
 * @param isNews Gets or sets filter for news.  Optional.
 * @param isKids Gets or sets filter for kids.  Optional.
 * @param isSports Gets or sets filter for sports.  Optional.
 * @param startIndex Gets or sets the record index to start at. All items with a lower index will be dropped from the results.  Optional.
 * @param limit Gets or sets the maximum number of records to return.  Optional.
 * @param sortBy Gets or sets specify one or more sort orders, comma delimited. Options: Name, StartDate.  Optional.
 * @param sortOrder Gets or sets sort Order - Ascending,Descending.
 * @param genres Gets or sets the genres to return guide information for.
 * @param genreIds Gets or sets the genre ids to return guide information for.
 * @param enableImages Gets or sets include image information in output.  Optional.
 * @param enableTotalRecordCount Gets or sets a value indicating whether retrieve total record count.
 * @param imageTypeLimit Gets or sets the max number of images to return, per image type.  Optional.
 * @param enableImageTypes Gets or sets the image types to include in the output.  Optional.
 * @param enableUserData Gets or sets include user data.  Optional.
 * @param seriesTimerId Gets or sets filter by series timer id.  Optional.
 * @param librarySeriesId Gets or sets filter by library series id.  Optional.
 * @param fields Gets or sets specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines.  Optional.
 */
@Serializable

data class GetProgramsDto (

    /* Gets or sets the channels to return guide information for. */
    @SerialName(value = "ChannelIds") val channelIds: kotlin.collections.List<kotlin.String>? = null,

    /* Gets or sets optional. Filter by user id. */
    @SerialName(value = "UserId") val userId: kotlin.String? = null,

    /* Gets or sets the minimum premiere start date.  Optional. */
    @SerialName(value = "MinStartDate") val minStartDate: kotlin.String? = null,

    /* Gets or sets filter by programs that have completed airing, or not.  Optional. */
    @SerialName(value = "HasAired") val hasAired: kotlin.Boolean? = null,

    /* Gets or sets filter by programs that are currently airing, or not.  Optional. */
    @SerialName(value = "IsAiring") val isAiring: kotlin.Boolean? = null,

    /* Gets or sets the maximum premiere start date.  Optional. */
    @SerialName(value = "MaxStartDate") val maxStartDate: kotlin.String? = null,

    /* Gets or sets the minimum premiere end date.  Optional. */
    @SerialName(value = "MinEndDate") val minEndDate: kotlin.String? = null,

    /* Gets or sets the maximum premiere end date.  Optional. */
    @SerialName(value = "MaxEndDate") val maxEndDate: kotlin.String? = null,

    /* Gets or sets filter for movies.  Optional. */
    @SerialName(value = "IsMovie") val isMovie: kotlin.Boolean? = null,

    /* Gets or sets filter for series.  Optional. */
    @SerialName(value = "IsSeries") val isSeries: kotlin.Boolean? = null,

    /* Gets or sets filter for news.  Optional. */
    @SerialName(value = "IsNews") val isNews: kotlin.Boolean? = null,

    /* Gets or sets filter for kids.  Optional. */
    @SerialName(value = "IsKids") val isKids: kotlin.Boolean? = null,

    /* Gets or sets filter for sports.  Optional. */
    @SerialName(value = "IsSports") val isSports: kotlin.Boolean? = null,

    /* Gets or sets the record index to start at. All items with a lower index will be dropped from the results.  Optional. */
    @SerialName(value = "StartIndex") val startIndex: kotlin.Int? = null,

    /* Gets or sets the maximum number of records to return.  Optional. */
    @SerialName(value = "Limit") val limit: kotlin.Int? = null,

    /* Gets or sets specify one or more sort orders, comma delimited. Options: Name, StartDate.  Optional. */
    @SerialName(value = "SortBy") val sortBy: kotlin.collections.List<kotlin.String>? = null,

    /* Gets or sets sort Order - Ascending,Descending. */
    @SerialName(value = "SortOrder") val sortOrder: kotlin.collections.List<SortOrder>? = null,

    /* Gets or sets the genres to return guide information for. */
    @SerialName(value = "Genres") val genres: kotlin.collections.List<kotlin.String>? = null,

    /* Gets or sets the genre ids to return guide information for. */
    @SerialName(value = "GenreIds") val genreIds: kotlin.collections.List<kotlin.String>? = null,

    /* Gets or sets include image information in output.  Optional. */
    @SerialName(value = "EnableImages") val enableImages: kotlin.Boolean? = null,

    /* Gets or sets a value indicating whether retrieve total record count. */
    @SerialName(value = "EnableTotalRecordCount") val enableTotalRecordCount: kotlin.Boolean? = null,

    /* Gets or sets the max number of images to return, per image type.  Optional. */
    @SerialName(value = "ImageTypeLimit") val imageTypeLimit: kotlin.Int? = null,

    /* Gets or sets the image types to include in the output.  Optional. */
    @SerialName(value = "EnableImageTypes") val enableImageTypes: kotlin.collections.List<ImageType>? = null,

    /* Gets or sets include user data.  Optional. */
    @SerialName(value = "EnableUserData") val enableUserData: kotlin.Boolean? = null,

    /* Gets or sets filter by series timer id.  Optional. */
    @SerialName(value = "SeriesTimerId") val seriesTimerId: kotlin.String? = null,

    /* Gets or sets filter by library series id.  Optional. */
    @SerialName(value = "LibrarySeriesId") val librarySeriesId: kotlin.String? = null,

    /* Gets or sets specify additional fields of information to return in the output. This allows multiple, comma delimited. Options: Budget, Chapters, DateCreated, Genres, HomePageUrl, IndexOptions, MediaStreams, Overview, ParentId, Path, People, ProviderIds, PrimaryImageAspectRatio, Revenue, SortName, Studios, Taglines.  Optional. */
    @SerialName(value = "Fields") val fields: kotlin.collections.List<ItemFields>? = null

)

