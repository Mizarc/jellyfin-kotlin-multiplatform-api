/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Set channel mapping dto.
 *
 * @param providerId Gets or sets the provider id.
 * @param tunerChannelId Gets or sets the tuner channel id.
 * @param providerChannelId Gets or sets the provider channel id.
 */
@Serializable

data class SetChannelMappingDto (

    /* Gets or sets the provider id. */
    @SerialName(value = "ProviderId") @Required val providerId: kotlin.String,

    /* Gets or sets the tuner channel id. */
    @SerialName(value = "TunerChannelId") @Required val tunerChannelId: kotlin.String,

    /* Gets or sets the provider channel id. */
    @SerialName(value = "ProviderChannelId") @Required val providerChannelId: kotlin.String

)

