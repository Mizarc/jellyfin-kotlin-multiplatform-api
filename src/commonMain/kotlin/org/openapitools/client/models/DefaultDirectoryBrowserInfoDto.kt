/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Default directory browser info.
 *
 * @param path Gets or sets the path.
 */
@Serializable

data class DefaultDirectoryBrowserInfoDto (

    /* Gets or sets the path. */
    @SerialName(value = "Path") val path: kotlin.String? = null

)

