/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models

import org.openapitools.client.models.ClientCapabilitiesDeviceProfile
import org.openapitools.client.models.GeneralCommandType

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Gets or sets the capabilities.
 *
 * @param playableMediaTypes 
 * @param supportedCommands 
 * @param supportsMediaControl 
 * @param supportsContentUploading 
 * @param messageCallbackUrl 
 * @param supportsPersistentIdentifier 
 * @param supportsSync 
 * @param deviceProfile 
 * @param appStoreUrl 
 * @param iconUrl 
 */
@Serializable

data class DeviceInfoCapabilities (

    @SerialName(value = "PlayableMediaTypes") val playableMediaTypes: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "SupportedCommands") val supportedCommands: kotlin.collections.List<GeneralCommandType>? = null,

    @SerialName(value = "SupportsMediaControl") val supportsMediaControl: kotlin.Boolean? = null,

    @SerialName(value = "SupportsContentUploading") val supportsContentUploading: kotlin.Boolean? = null,

    @SerialName(value = "MessageCallbackUrl") val messageCallbackUrl: kotlin.String? = null,

    @SerialName(value = "SupportsPersistentIdentifier") val supportsPersistentIdentifier: kotlin.Boolean? = null,

    @SerialName(value = "SupportsSync") val supportsSync: kotlin.Boolean? = null,

    @SerialName(value = "DeviceProfile") val deviceProfile: ClientCapabilitiesDeviceProfile? = null,

    @SerialName(value = "AppStoreUrl") val appStoreUrl: kotlin.String? = null,

    @SerialName(value = "IconUrl") val iconUrl: kotlin.String? = null

)

