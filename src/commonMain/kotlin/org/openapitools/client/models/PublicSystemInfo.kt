/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * 
 *
 * @param localAddress Gets or sets the local address.
 * @param serverName Gets or sets the name of the server.
 * @param version Gets or sets the server version.
 * @param productName Gets or sets the product name. This is the AssemblyProduct name.
 * @param operatingSystem Gets or sets the operating system.
 * @param id Gets or sets the id.
 * @param startupWizardCompleted Gets or sets a value indicating whether the startup wizard is completed.
 */
@Serializable

data class PublicSystemInfo (

    /* Gets or sets the local address. */
    @SerialName(value = "LocalAddress") val localAddress: kotlin.String? = null,

    /* Gets or sets the name of the server. */
    @SerialName(value = "ServerName") val serverName: kotlin.String? = null,

    /* Gets or sets the server version. */
    @SerialName(value = "Version") val version: kotlin.String? = null,

    /* Gets or sets the product name. This is the AssemblyProduct name. */
    @SerialName(value = "ProductName") val productName: kotlin.String? = null,

    /* Gets or sets the operating system. */
    @SerialName(value = "OperatingSystem") val operatingSystem: kotlin.String? = null,

    /* Gets or sets the id. */
    @SerialName(value = "Id") val id: kotlin.String? = null,

    /* Gets or sets a value indicating whether the startup wizard is completed. */
    @SerialName(value = "StartupWizardCompleted") val startupWizardCompleted: kotlin.Boolean? = null

)

