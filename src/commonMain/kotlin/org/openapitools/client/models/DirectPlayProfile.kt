/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models

import org.openapitools.client.models.DlnaProfileType

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * 
 *
 * @param container 
 * @param audioCodec 
 * @param videoCodec 
 * @param type 
 */
@Serializable

data class DirectPlayProfile (

    @SerialName(value = "Container") val container: kotlin.String? = null,

    @SerialName(value = "AudioCodec") val audioCodec: kotlin.String? = null,

    @SerialName(value = "VideoCodec") val videoCodec: kotlin.String? = null,

    @SerialName(value = "Type") val type: DlnaProfileType? = null

)

