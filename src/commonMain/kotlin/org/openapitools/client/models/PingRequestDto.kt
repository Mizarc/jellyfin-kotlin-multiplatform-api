/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Class PingRequestDto.
 *
 * @param ping Gets or sets the ping time.
 */
@Serializable

data class PingRequestDto (

    /* Gets or sets the ping time. */
    @SerialName(value = "Ping") val ping: kotlin.Long? = null

)

