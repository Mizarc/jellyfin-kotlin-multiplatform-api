/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Class LibrarySummary.
 *
 * @param movieCount Gets or sets the movie count.
 * @param seriesCount Gets or sets the series count.
 * @param episodeCount Gets or sets the episode count.
 * @param artistCount Gets or sets the artist count.
 * @param programCount Gets or sets the program count.
 * @param trailerCount Gets or sets the trailer count.
 * @param songCount Gets or sets the song count.
 * @param albumCount Gets or sets the album count.
 * @param musicVideoCount Gets or sets the music video count.
 * @param boxSetCount Gets or sets the box set count.
 * @param bookCount Gets or sets the book count.
 * @param itemCount Gets or sets the item count.
 */
@Serializable

data class ItemCounts (

    /* Gets or sets the movie count. */
    @SerialName(value = "MovieCount") val movieCount: kotlin.Int? = null,

    /* Gets or sets the series count. */
    @SerialName(value = "SeriesCount") val seriesCount: kotlin.Int? = null,

    /* Gets or sets the episode count. */
    @SerialName(value = "EpisodeCount") val episodeCount: kotlin.Int? = null,

    /* Gets or sets the artist count. */
    @SerialName(value = "ArtistCount") val artistCount: kotlin.Int? = null,

    /* Gets or sets the program count. */
    @SerialName(value = "ProgramCount") val programCount: kotlin.Int? = null,

    /* Gets or sets the trailer count. */
    @SerialName(value = "TrailerCount") val trailerCount: kotlin.Int? = null,

    /* Gets or sets the song count. */
    @SerialName(value = "SongCount") val songCount: kotlin.Int? = null,

    /* Gets or sets the album count. */
    @SerialName(value = "AlbumCount") val albumCount: kotlin.Int? = null,

    /* Gets or sets the music video count. */
    @SerialName(value = "MusicVideoCount") val musicVideoCount: kotlin.Int? = null,

    /* Gets or sets the box set count. */
    @SerialName(value = "BoxSetCount") val boxSetCount: kotlin.Int? = null,

    /* Gets or sets the book count. */
    @SerialName(value = "BookCount") val bookCount: kotlin.Int? = null,

    /* Gets or sets the item count. */
    @SerialName(value = "ItemCount") val itemCount: kotlin.Int? = null

)

