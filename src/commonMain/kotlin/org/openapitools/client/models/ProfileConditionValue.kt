/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*

/**
 * 
 *
 * Values: audioChannels,audioBitrate,audioProfile,width,height,has64BitOffsets,packetLength,videoBitDepth,videoBitrate,videoFramerate,videoLevel,videoProfile,videoTimestamp,isAnamorphic,refFrames,numAudioStreams,numVideoStreams,isSecondaryAudio,videoCodecTag,isAvc,isInterlaced,audioSampleRate,audioBitDepth,videoRangeType
 */
@Serializable
enum class ProfileConditionValue(val value: kotlin.String) {

    @SerialName(value = "AudioChannels")
    audioChannels("AudioChannels"),

    @SerialName(value = "AudioBitrate")
    audioBitrate("AudioBitrate"),

    @SerialName(value = "AudioProfile")
    audioProfile("AudioProfile"),

    @SerialName(value = "Width")
    width("Width"),

    @SerialName(value = "Height")
    height("Height"),

    @SerialName(value = "Has64BitOffsets")
    has64BitOffsets("Has64BitOffsets"),

    @SerialName(value = "PacketLength")
    packetLength("PacketLength"),

    @SerialName(value = "VideoBitDepth")
    videoBitDepth("VideoBitDepth"),

    @SerialName(value = "VideoBitrate")
    videoBitrate("VideoBitrate"),

    @SerialName(value = "VideoFramerate")
    videoFramerate("VideoFramerate"),

    @SerialName(value = "VideoLevel")
    videoLevel("VideoLevel"),

    @SerialName(value = "VideoProfile")
    videoProfile("VideoProfile"),

    @SerialName(value = "VideoTimestamp")
    videoTimestamp("VideoTimestamp"),

    @SerialName(value = "IsAnamorphic")
    isAnamorphic("IsAnamorphic"),

    @SerialName(value = "RefFrames")
    refFrames("RefFrames"),

    @SerialName(value = "NumAudioStreams")
    numAudioStreams("NumAudioStreams"),

    @SerialName(value = "NumVideoStreams")
    numVideoStreams("NumVideoStreams"),

    @SerialName(value = "IsSecondaryAudio")
    isSecondaryAudio("IsSecondaryAudio"),

    @SerialName(value = "VideoCodecTag")
    videoCodecTag("VideoCodecTag"),

    @SerialName(value = "IsAvc")
    isAvc("IsAvc"),

    @SerialName(value = "IsInterlaced")
    isInterlaced("IsInterlaced"),

    @SerialName(value = "AudioSampleRate")
    audioSampleRate("AudioSampleRate"),

    @SerialName(value = "AudioBitDepth")
    audioBitDepth("AudioBitDepth"),

    @SerialName(value = "VideoRangeType")
    videoRangeType("VideoRangeType");

    /**
     * Override toString() to avoid using the enum variable name as the value, and instead use
     * the actual value defined in the API spec file.
     *
     * This solves a problem when the variable name and its value are different, and ensures that
     * the client sends the correct enum values to the server always.
     */
    override fun toString(): String = value

    companion object {
        /**
         * Converts the provided [data] to a [String] on success, null otherwise.
         */
        fun encode(data: kotlin.Any?): kotlin.String? = if (data is ProfileConditionValue) "$data" else null

        /**
         * Returns a valid [ProfileConditionValue] for [data], null otherwise.
         */
        fun decode(data: kotlin.Any?): ProfileConditionValue? = data?.let {
          val normalizedData = "$it".lowercase()
          values().firstOrNull { value ->
            it == value || normalizedData == "$value".lowercase()
          }
        }
    }
}

