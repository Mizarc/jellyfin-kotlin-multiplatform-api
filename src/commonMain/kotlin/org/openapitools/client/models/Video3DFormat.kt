/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*

/**
 * 
 *
 * Values: halfSideBySide,fullSideBySide,fullTopAndBottom,halfTopAndBottom,mVC
 */
@Serializable
enum class Video3DFormat(val value: kotlin.String) {

    @SerialName(value = "HalfSideBySide")
    halfSideBySide("HalfSideBySide"),

    @SerialName(value = "FullSideBySide")
    fullSideBySide("FullSideBySide"),

    @SerialName(value = "FullTopAndBottom")
    fullTopAndBottom("FullTopAndBottom"),

    @SerialName(value = "HalfTopAndBottom")
    halfTopAndBottom("HalfTopAndBottom"),

    @SerialName(value = "MVC")
    mVC("MVC");

    /**
     * Override toString() to avoid using the enum variable name as the value, and instead use
     * the actual value defined in the API spec file.
     *
     * This solves a problem when the variable name and its value are different, and ensures that
     * the client sends the correct enum values to the server always.
     */
    override fun toString(): String = value

    companion object {
        /**
         * Converts the provided [data] to a [String] on success, null otherwise.
         */
        fun encode(data: kotlin.Any?): kotlin.String? = if (data is Video3DFormat) "$data" else null

        /**
         * Returns a valid [Video3DFormat] for [data], null otherwise.
         */
        fun decode(data: kotlin.Any?): Video3DFormat? = data?.let {
          val normalizedData = "$it".lowercase()
          values().firstOrNull { value ->
            it == value || normalizedData == "$value".lowercase()
          }
        }
    }
}

