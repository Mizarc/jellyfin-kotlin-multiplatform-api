/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models

import org.openapitools.client.models.ImageType

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * 
 *
 * @param type 
 * @param limit Gets or sets the limit.
 * @param minWidth Gets or sets the minimum width.
 */
@Serializable

data class ImageOption (

    @SerialName(value = "Type") val type: ImageType? = null,

    /* Gets or sets the limit. */
    @SerialName(value = "Limit") val limit: kotlin.Int? = null,

    /* Gets or sets the minimum width. */
    @SerialName(value = "MinWidth") val minWidth: kotlin.Int? = null

)

