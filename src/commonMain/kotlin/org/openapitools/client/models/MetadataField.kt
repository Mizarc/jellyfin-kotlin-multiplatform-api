/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*

/**
 * Enum MetadataFields.
 *
 * Values: cast,genres,productionLocations,studios,tags,name,overview,runtime,officialRating
 */
@Serializable
enum class MetadataField(val value: kotlin.String) {

    @SerialName(value = "Cast")
    cast("Cast"),

    @SerialName(value = "Genres")
    genres("Genres"),

    @SerialName(value = "ProductionLocations")
    productionLocations("ProductionLocations"),

    @SerialName(value = "Studios")
    studios("Studios"),

    @SerialName(value = "Tags")
    tags("Tags"),

    @SerialName(value = "Name")
    name("Name"),

    @SerialName(value = "Overview")
    overview("Overview"),

    @SerialName(value = "Runtime")
    runtime("Runtime"),

    @SerialName(value = "OfficialRating")
    officialRating("OfficialRating");

    /**
     * Override toString() to avoid using the enum variable name as the value, and instead use
     * the actual value defined in the API spec file.
     *
     * This solves a problem when the variable name and its value are different, and ensures that
     * the client sends the correct enum values to the server always.
     */
    override fun toString(): String = value

    companion object {
        /**
         * Converts the provided [data] to a [String] on success, null otherwise.
         */
        fun encode(data: kotlin.Any?): kotlin.String? = if (data is MetadataField) "$data" else null

        /**
         * Returns a valid [MetadataField] for [data], null otherwise.
         */
        fun decode(data: kotlin.Any?): MetadataField? = data?.let {
          val normalizedData = "$it".lowercase()
          values().firstOrNull { value ->
            it == value || normalizedData == "$value".lowercase()
          }
        }
    }
}

