/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * The media update info path.
 *
 * @param path Gets or sets media path.
 * @param updateType Gets or sets media update type.  Created, Modified, Deleted.
 */
@Serializable

data class MediaUpdateInfoPathDto (

    /* Gets or sets media path. */
    @SerialName(value = "Path") val path: kotlin.String? = null,

    /* Gets or sets media update type.  Created, Modified, Deleted. */
    @SerialName(value = "UpdateType") val updateType: kotlin.String? = null

)

