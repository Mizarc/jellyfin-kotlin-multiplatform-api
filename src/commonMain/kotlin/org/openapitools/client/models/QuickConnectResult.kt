/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Stores the state of an quick connect request.
 *
 * @param authenticated Gets or sets a value indicating whether this request is authorized.
 * @param secret Gets the secret value used to uniquely identify this request. Can be used to retrieve authentication information.
 * @param code Gets the user facing code used so the user can quickly differentiate this request from others.
 * @param deviceId Gets the requesting device id.
 * @param deviceName Gets the requesting device name.
 * @param appName Gets the requesting app name.
 * @param appVersion Gets the requesting app version.
 * @param dateAdded Gets or sets the DateTime that this request was created.
 */
@Serializable

data class QuickConnectResult (

    /* Gets or sets a value indicating whether this request is authorized. */
    @SerialName(value = "Authenticated") val authenticated: kotlin.Boolean? = null,

    /* Gets the secret value used to uniquely identify this request. Can be used to retrieve authentication information. */
    @SerialName(value = "Secret") val secret: kotlin.String? = null,

    /* Gets the user facing code used so the user can quickly differentiate this request from others. */
    @SerialName(value = "Code") val code: kotlin.String? = null,

    /* Gets the requesting device id. */
    @SerialName(value = "DeviceId") val deviceId: kotlin.String? = null,

    /* Gets the requesting device name. */
    @SerialName(value = "DeviceName") val deviceName: kotlin.String? = null,

    /* Gets the requesting app name. */
    @SerialName(value = "AppName") val appName: kotlin.String? = null,

    /* Gets the requesting app version. */
    @SerialName(value = "AppVersion") val appVersion: kotlin.String? = null,

    /* Gets or sets the DateTime that this request was created. */
    @SerialName(value = "DateAdded") val dateAdded: kotlin.String? = null

)

