/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models


import kotlinx.serialization.*

/**
 * 
 *
 * Values: file,http,rtmp,rtsp,udp,rtp,ftp
 */
@Serializable
enum class MediaProtocol(val value: kotlin.String) {

    @SerialName(value = "File")
    file("File"),

    @SerialName(value = "Http")
    http("Http"),

    @SerialName(value = "Rtmp")
    rtmp("Rtmp"),

    @SerialName(value = "Rtsp")
    rtsp("Rtsp"),

    @SerialName(value = "Udp")
    udp("Udp"),

    @SerialName(value = "Rtp")
    rtp("Rtp"),

    @SerialName(value = "Ftp")
    ftp("Ftp");

    /**
     * Override toString() to avoid using the enum variable name as the value, and instead use
     * the actual value defined in the API spec file.
     *
     * This solves a problem when the variable name and its value are different, and ensures that
     * the client sends the correct enum values to the server always.
     */
    override fun toString(): String = value

    companion object {
        /**
         * Converts the provided [data] to a [String] on success, null otherwise.
         */
        fun encode(data: kotlin.Any?): kotlin.String? = if (data is MediaProtocol) "$data" else null

        /**
         * Returns a valid [MediaProtocol] for [data], null otherwise.
         */
        fun decode(data: kotlin.Any?): MediaProtocol? = data?.let {
          val normalizedData = "$it".lowercase()
          values().firstOrNull { value ->
            it == value || normalizedData == "$value".lowercase()
          }
        }
    }
}

