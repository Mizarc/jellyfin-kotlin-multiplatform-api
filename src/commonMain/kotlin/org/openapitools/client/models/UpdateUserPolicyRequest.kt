/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.models

import org.openapitools.client.models.AccessSchedule
import org.openapitools.client.models.SyncPlayUserAccessType
import org.openapitools.client.models.UnratedItem

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * 
 *
 * @param isAdministrator Gets or sets a value indicating whether this instance is administrator.
 * @param isHidden Gets or sets a value indicating whether this instance is hidden.
 * @param isDisabled Gets or sets a value indicating whether this instance is disabled.
 * @param maxParentalRating Gets or sets the max parental rating.
 * @param blockedTags 
 * @param enableUserPreferenceAccess 
 * @param accessSchedules 
 * @param blockUnratedItems 
 * @param enableRemoteControlOfOtherUsers 
 * @param enableSharedDeviceControl 
 * @param enableRemoteAccess 
 * @param enableLiveTvManagement 
 * @param enableLiveTvAccess 
 * @param enableMediaPlayback 
 * @param enableAudioPlaybackTranscoding 
 * @param enableVideoPlaybackTranscoding 
 * @param enablePlaybackRemuxing 
 * @param forceRemoteSourceTranscoding 
 * @param enableContentDeletion 
 * @param enableContentDeletionFromFolders 
 * @param enableContentDownloading 
 * @param enableSyncTranscoding Gets or sets a value indicating whether [enable synchronize].
 * @param enableMediaConversion 
 * @param enabledDevices 
 * @param enableAllDevices 
 * @param enabledChannels 
 * @param enableAllChannels 
 * @param enabledFolders 
 * @param enableAllFolders 
 * @param invalidLoginAttemptCount 
 * @param loginAttemptsBeforeLockout 
 * @param maxActiveSessions 
 * @param enablePublicSharing 
 * @param blockedMediaFolders 
 * @param blockedChannels 
 * @param remoteClientBitrateLimit 
 * @param authenticationProviderId 
 * @param passwordResetProviderId 
 * @param syncPlayAccess 
 */
@Serializable

data class UpdateUserPolicyRequest (

    /* Gets or sets a value indicating whether this instance is administrator. */
    @SerialName(value = "IsAdministrator") val isAdministrator: kotlin.Boolean? = null,

    /* Gets or sets a value indicating whether this instance is hidden. */
    @SerialName(value = "IsHidden") val isHidden: kotlin.Boolean? = null,

    /* Gets or sets a value indicating whether this instance is disabled. */
    @SerialName(value = "IsDisabled") val isDisabled: kotlin.Boolean? = null,

    /* Gets or sets the max parental rating. */
    @SerialName(value = "MaxParentalRating") val maxParentalRating: kotlin.Int? = null,

    @SerialName(value = "BlockedTags") val blockedTags: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "EnableUserPreferenceAccess") val enableUserPreferenceAccess: kotlin.Boolean? = null,

    @SerialName(value = "AccessSchedules") val accessSchedules: kotlin.collections.List<AccessSchedule>? = null,

    @SerialName(value = "BlockUnratedItems") val blockUnratedItems: kotlin.collections.List<UnratedItem>? = null,

    @SerialName(value = "EnableRemoteControlOfOtherUsers") val enableRemoteControlOfOtherUsers: kotlin.Boolean? = null,

    @SerialName(value = "EnableSharedDeviceControl") val enableSharedDeviceControl: kotlin.Boolean? = null,

    @SerialName(value = "EnableRemoteAccess") val enableRemoteAccess: kotlin.Boolean? = null,

    @SerialName(value = "EnableLiveTvManagement") val enableLiveTvManagement: kotlin.Boolean? = null,

    @SerialName(value = "EnableLiveTvAccess") val enableLiveTvAccess: kotlin.Boolean? = null,

    @SerialName(value = "EnableMediaPlayback") val enableMediaPlayback: kotlin.Boolean? = null,

    @SerialName(value = "EnableAudioPlaybackTranscoding") val enableAudioPlaybackTranscoding: kotlin.Boolean? = null,

    @SerialName(value = "EnableVideoPlaybackTranscoding") val enableVideoPlaybackTranscoding: kotlin.Boolean? = null,

    @SerialName(value = "EnablePlaybackRemuxing") val enablePlaybackRemuxing: kotlin.Boolean? = null,

    @SerialName(value = "ForceRemoteSourceTranscoding") val forceRemoteSourceTranscoding: kotlin.Boolean? = null,

    @SerialName(value = "EnableContentDeletion") val enableContentDeletion: kotlin.Boolean? = null,

    @SerialName(value = "EnableContentDeletionFromFolders") val enableContentDeletionFromFolders: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "EnableContentDownloading") val enableContentDownloading: kotlin.Boolean? = null,

    /* Gets or sets a value indicating whether [enable synchronize]. */
    @SerialName(value = "EnableSyncTranscoding") val enableSyncTranscoding: kotlin.Boolean? = null,

    @SerialName(value = "EnableMediaConversion") val enableMediaConversion: kotlin.Boolean? = null,

    @SerialName(value = "EnabledDevices") val enabledDevices: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "EnableAllDevices") val enableAllDevices: kotlin.Boolean? = null,

    @SerialName(value = "EnabledChannels") val enabledChannels: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "EnableAllChannels") val enableAllChannels: kotlin.Boolean? = null,

    @SerialName(value = "EnabledFolders") val enabledFolders: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "EnableAllFolders") val enableAllFolders: kotlin.Boolean? = null,

    @SerialName(value = "InvalidLoginAttemptCount") val invalidLoginAttemptCount: kotlin.Int? = null,

    @SerialName(value = "LoginAttemptsBeforeLockout") val loginAttemptsBeforeLockout: kotlin.Int? = null,

    @SerialName(value = "MaxActiveSessions") val maxActiveSessions: kotlin.Int? = null,

    @SerialName(value = "EnablePublicSharing") val enablePublicSharing: kotlin.Boolean? = null,

    @SerialName(value = "BlockedMediaFolders") val blockedMediaFolders: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "BlockedChannels") val blockedChannels: kotlin.collections.List<kotlin.String>? = null,

    @SerialName(value = "RemoteClientBitrateLimit") val remoteClientBitrateLimit: kotlin.Int? = null,

    @SerialName(value = "AuthenticationProviderId") val authenticationProviderId: kotlin.String? = null,

    @SerialName(value = "PasswordResetProviderId") val passwordResetProviderId: kotlin.String? = null,

    @SerialName(value = "SyncPlayAccess") val syncPlayAccess: SyncPlayUserAccessType? = null

)

