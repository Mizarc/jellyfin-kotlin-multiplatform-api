/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.apis

import org.openapitools.client.models.EndPointInfo
import org.openapitools.client.models.LogFile
import org.openapitools.client.models.PublicSystemInfo
import org.openapitools.client.models.SystemInfo
import org.openapitools.client.models.WakeOnLanInfo

import org.openapitools.client.infrastructure.*
import io.ktor.client.HttpClientConfig
import io.ktor.client.request.forms.formData
import io.ktor.client.engine.HttpClientEngine
import kotlinx.serialization.json.Json
import io.ktor.http.ParametersBuilder
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

open class SystemApi(
    baseUrl: String = ApiClient.BASE_URL,
    httpClientEngine: HttpClientEngine? = null,
    httpClientConfig: ((HttpClientConfig<*>) -> Unit)? = null,
    jsonSerializer: Json = ApiClient.JSON_DEFAULT
) : ApiClient(baseUrl, httpClientEngine, httpClientConfig, jsonSerializer) {

    /**
     * Gets information about the request endpoint.
     * 
     * @return EndPointInfo
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getEndpointInfo(): HttpResponse<EndPointInfo> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/System/Endpoint",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets a log file.
     * 
     * @param name The name of the log file to get.
     * @return org.openapitools.client.infrastructure.OctetByteArray
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getLogFile(name: kotlin.String): HttpResponse<org.openapitools.client.infrastructure.OctetByteArray> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        name?.apply { localVariableQuery["name"] = listOf("$name") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/System/Logs/Log",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Pings the system.
     * 
     * @return kotlin.String
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getPingSystem(): HttpResponse<kotlin.String> {

        val localVariableAuthNames = listOf<String>()

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/System/Ping",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = false,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets public information about the server.
     * 
     * @return PublicSystemInfo
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getPublicSystemInfo(): HttpResponse<PublicSystemInfo> {

        val localVariableAuthNames = listOf<String>()

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/System/Info/Public",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = false,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets a list of available server log files.
     * 
     * @return kotlin.collections.List<LogFile>
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getServerLogs(): HttpResponse<kotlin.collections.List<LogFile>> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/System/Logs",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap<GetServerLogsResponse>().map { value }
    }

    @Serializable
    private class GetServerLogsResponse(val value: List<LogFile>) {
        @Serializer(GetServerLogsResponse::class)
        companion object : KSerializer<GetServerLogsResponse> {
            private val serializer: KSerializer<List<LogFile>> = serializer<List<LogFile>>()
            override val descriptor = serializer.descriptor
            override fun serialize(encoder: Encoder, obj: GetServerLogsResponse) = serializer.serialize(encoder, obj.value)
            override fun deserialize(decoder: Decoder) = GetServerLogsResponse(serializer.deserialize(decoder))
        }
    }

    /**
     * Gets information about the server.
     * 
     * @return SystemInfo
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getSystemInfo(): HttpResponse<SystemInfo> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/System/Info",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets wake on lan information.
     * 
     * @return kotlin.collections.List<WakeOnLanInfo>
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getWakeOnLanInfo(): HttpResponse<kotlin.collections.List<WakeOnLanInfo>> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/System/WakeOnLanInfo",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap<GetWakeOnLanInfoResponse>().map { value }
    }

    @Serializable
    private class GetWakeOnLanInfoResponse(val value: List<WakeOnLanInfo>) {
        @Serializer(GetWakeOnLanInfoResponse::class)
        companion object : KSerializer<GetWakeOnLanInfoResponse> {
            private val serializer: KSerializer<List<WakeOnLanInfo>> = serializer<List<WakeOnLanInfo>>()
            override val descriptor = serializer.descriptor
            override fun serialize(encoder: Encoder, obj: GetWakeOnLanInfoResponse) = serializer.serialize(encoder, obj.value)
            override fun deserialize(decoder: Decoder) = GetWakeOnLanInfoResponse(serializer.deserialize(decoder))
        }
    }

    /**
     * Pings the system.
     * 
     * @return kotlin.String
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun postPingSystem(): HttpResponse<kotlin.String> {

        val localVariableAuthNames = listOf<String>()

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.POST,
            "/System/Ping",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = false,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Restarts the application.
     * 
     * @return void
     */
    open suspend fun restartApplication(): HttpResponse<Unit> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.POST,
            "/System/Restart",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Shuts down the application.
     * 
     * @return void
     */
    open suspend fun shutdownApplication(): HttpResponse<Unit> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.POST,
            "/System/Shutdown",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


}
