/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.apis

import org.openapitools.client.models.PackageInfo
import org.openapitools.client.models.ProblemDetails
import org.openapitools.client.models.RepositoryInfo

import org.openapitools.client.infrastructure.*
import io.ktor.client.HttpClientConfig
import io.ktor.client.request.forms.formData
import io.ktor.client.engine.HttpClientEngine
import kotlinx.serialization.json.Json
import io.ktor.http.ParametersBuilder
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

open class PackageApi(
    baseUrl: String = ApiClient.BASE_URL,
    httpClientEngine: HttpClientEngine? = null,
    httpClientConfig: ((HttpClientConfig<*>) -> Unit)? = null,
    jsonSerializer: Json = ApiClient.JSON_DEFAULT
) : ApiClient(baseUrl, httpClientEngine, httpClientConfig, jsonSerializer) {

    /**
     * Cancels a package installation.
     * 
     * @param packageId Installation Id.
     * @return void
     */
    open suspend fun cancelPackageInstallation(packageId: kotlin.String): HttpResponse<Unit> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.DELETE,
            "/Packages/Installing/{packageId}".replace("{" + "packageId" + "}", "$packageId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets a package by name or assembly GUID.
     * 
     * @param name The name of the package.
     * @param assemblyGuid The GUID of the associated assembly. (optional)
     * @return PackageInfo
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getPackageInfo(name: kotlin.String, assemblyGuid: kotlin.String? = null): HttpResponse<PackageInfo> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        assemblyGuid?.apply { localVariableQuery["assemblyGuid"] = listOf("$assemblyGuid") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Packages/{name}".replace("{" + "name" + "}", "$name"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets available packages.
     * 
     * @return kotlin.collections.List<PackageInfo>
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getPackages(): HttpResponse<kotlin.collections.List<PackageInfo>> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Packages",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap<GetPackagesResponse>().map { value }
    }

    @Serializable
    private class GetPackagesResponse(val value: List<PackageInfo>) {
        @Serializer(GetPackagesResponse::class)
        companion object : KSerializer<GetPackagesResponse> {
            private val serializer: KSerializer<List<PackageInfo>> = serializer<List<PackageInfo>>()
            override val descriptor = serializer.descriptor
            override fun serialize(encoder: Encoder, obj: GetPackagesResponse) = serializer.serialize(encoder, obj.value)
            override fun deserialize(decoder: Decoder) = GetPackagesResponse(serializer.deserialize(decoder))
        }
    }

    /**
     * Gets all package repositories.
     * 
     * @return kotlin.collections.List<RepositoryInfo>
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getRepositories(): HttpResponse<kotlin.collections.List<RepositoryInfo>> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Repositories",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap<GetRepositoriesResponse>().map { value }
    }

    @Serializable
    private class GetRepositoriesResponse(val value: List<RepositoryInfo>) {
        @Serializer(GetRepositoriesResponse::class)
        companion object : KSerializer<GetRepositoriesResponse> {
            private val serializer: KSerializer<List<RepositoryInfo>> = serializer<List<RepositoryInfo>>()
            override val descriptor = serializer.descriptor
            override fun serialize(encoder: Encoder, obj: GetRepositoriesResponse) = serializer.serialize(encoder, obj.value)
            override fun deserialize(decoder: Decoder) = GetRepositoriesResponse(serializer.deserialize(decoder))
        }
    }

    /**
     * Installs a package.
     * 
     * @param name Package name.
     * @param assemblyGuid GUID of the associated assembly. (optional)
     * @param version Optional version. Defaults to latest version. (optional)
     * @param repositoryUrl Optional. Specify the repository to install from. (optional)
     * @return void
     */
    open suspend fun installPackage(name: kotlin.String, assemblyGuid: kotlin.String? = null, version: kotlin.String? = null, repositoryUrl: kotlin.String? = null): HttpResponse<Unit> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        assemblyGuid?.apply { localVariableQuery["assemblyGuid"] = listOf("$assemblyGuid") }
        version?.apply { localVariableQuery["version"] = listOf("$version") }
        repositoryUrl?.apply { localVariableQuery["repositoryUrl"] = listOf("$repositoryUrl") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.POST,
            "/Packages/Installed/{name}".replace("{" + "name" + "}", "$name"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Sets the enabled and existing package repositories.
     * 
     * @param repositoryInfo The list of package repositories.
     * @return void
     */
    open suspend fun setRepositories(repositoryInfo: kotlin.collections.List<RepositoryInfo>): HttpResponse<Unit> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = SetRepositoriesRequest(repositoryInfo)

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.POST,
            "/Repositories",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return jsonRequest(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }

    @Serializable
    private class SetRepositoriesRequest(val value: List<RepositoryInfo>) {
        @Serializer(SetRepositoriesRequest::class)
        companion object : KSerializer<SetRepositoriesRequest> {
            private val serializer: KSerializer<List<RepositoryInfo>> = serializer<List<RepositoryInfo>>()
            override val descriptor = serializer.descriptor
            override fun serialize(encoder: Encoder, obj: SetRepositoriesRequest) = serializer.serialize(encoder, obj.value)
            override fun deserialize(decoder: Decoder) = SetRepositoriesRequest(serializer.deserialize(decoder))
        }
    }

}
