/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.apis

import org.openapitools.client.models.ProblemDetails

import org.openapitools.client.infrastructure.*
import io.ktor.client.HttpClientConfig
import io.ktor.client.request.forms.formData
import io.ktor.client.engine.HttpClientEngine
import kotlinx.serialization.json.Json
import io.ktor.http.ParametersBuilder
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

open class HlsSegmentApi(
    baseUrl: String = ApiClient.BASE_URL,
    httpClientEngine: HttpClientEngine? = null,
    httpClientConfig: ((HttpClientConfig<*>) -> Unit)? = null,
    jsonSerializer: Json = ApiClient.JSON_DEFAULT
) : ApiClient(baseUrl, httpClientEngine, httpClientConfig, jsonSerializer) {

    /**
     * Gets the specified audio segment for an audio item.
     * 
     * @param itemId The item id.
     * @param segmentId The segment id.
     * @return org.openapitools.client.infrastructure.OctetByteArray
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getHlsAudioSegmentLegacyAac(itemId: kotlin.String, segmentId: kotlin.String): HttpResponse<org.openapitools.client.infrastructure.OctetByteArray> {

        val localVariableAuthNames = listOf<String>()

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Audio/{itemId}/hls/{segmentId}/stream.aac".replace("{" + "itemId" + "}", "$itemId").replace("{" + "segmentId" + "}", "$segmentId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = false,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets the specified audio segment for an audio item.
     * 
     * @param itemId The item id.
     * @param segmentId The segment id.
     * @return org.openapitools.client.infrastructure.OctetByteArray
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getHlsAudioSegmentLegacyMp3(itemId: kotlin.String, segmentId: kotlin.String): HttpResponse<org.openapitools.client.infrastructure.OctetByteArray> {

        val localVariableAuthNames = listOf<String>()

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Audio/{itemId}/hls/{segmentId}/stream.mp3".replace("{" + "itemId" + "}", "$itemId").replace("{" + "segmentId" + "}", "$segmentId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = false,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets a hls video playlist.
     * 
     * @param itemId The video id.
     * @param playlistId The playlist id.
     * @return org.openapitools.client.infrastructure.OctetByteArray
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getHlsPlaylistLegacy(itemId: kotlin.String, playlistId: kotlin.String): HttpResponse<org.openapitools.client.infrastructure.OctetByteArray> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Videos/{itemId}/hls/{playlistId}/stream.m3u8".replace("{" + "itemId" + "}", "$itemId").replace("{" + "playlistId" + "}", "$playlistId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets a hls video segment.
     * 
     * @param itemId The item id.
     * @param playlistId The playlist id.
     * @param segmentId The segment id.
     * @param segmentContainer The segment container.
     * @return org.openapitools.client.infrastructure.OctetByteArray
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getHlsVideoSegmentLegacy(itemId: kotlin.String, playlistId: kotlin.String, segmentId: kotlin.String, segmentContainer: kotlin.String): HttpResponse<org.openapitools.client.infrastructure.OctetByteArray> {

        val localVariableAuthNames = listOf<String>()

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Videos/{itemId}/hls/{playlistId}/{segmentId}.{segmentContainer}".replace("{" + "itemId" + "}", "$itemId").replace("{" + "playlistId" + "}", "$playlistId").replace("{" + "segmentId" + "}", "$segmentId").replace("{" + "segmentContainer" + "}", "$segmentContainer"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = false,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Stops an active encoding.
     * 
     * @param deviceId The device id of the client requesting. Used to stop encoding processes when needed.
     * @param playSessionId The play session id.
     * @return void
     */
    open suspend fun stopEncodingProcess(deviceId: kotlin.String, playSessionId: kotlin.String): HttpResponse<Unit> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        deviceId?.apply { localVariableQuery["deviceId"] = listOf("$deviceId") }
        playSessionId?.apply { localVariableQuery["playSessionId"] = listOf("$playSessionId") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.DELETE,
            "/Videos/ActiveEncodings",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


}
