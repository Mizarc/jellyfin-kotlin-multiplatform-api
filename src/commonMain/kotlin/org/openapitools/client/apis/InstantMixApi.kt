/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.apis

import org.openapitools.client.models.BaseItemDtoQueryResult
import org.openapitools.client.models.ImageType
import org.openapitools.client.models.ItemFields

import org.openapitools.client.infrastructure.*
import io.ktor.client.HttpClientConfig
import io.ktor.client.request.forms.formData
import io.ktor.client.engine.HttpClientEngine
import kotlinx.serialization.json.Json
import io.ktor.http.ParametersBuilder
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

open class InstantMixApi(
    baseUrl: String = ApiClient.BASE_URL,
    httpClientEngine: HttpClientEngine? = null,
    httpClientConfig: ((HttpClientConfig<*>) -> Unit)? = null,
    jsonSerializer: Json = ApiClient.JSON_DEFAULT
) : ApiClient(baseUrl, httpClientEngine, httpClientConfig, jsonSerializer) {

    /**
     * Creates an instant playlist based on a given album.
     * 
     * @param id The item id.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromAlbum(id: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Albums/{id}/InstantMix".replace("{" + "id" + "}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Creates an instant playlist based on a given artist.
     * 
     * @param id The item id.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromArtists(id: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Artists/{id}/InstantMix".replace("{" + "id" + "}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Creates an instant playlist based on a given artist.
     * 
     * @param id The item id.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromArtists2(id: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        id?.apply { localVariableQuery["id"] = listOf("$id") }
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Artists/InstantMix",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Creates an instant playlist based on a given item.
     * 
     * @param id The item id.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromItem(id: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Items/{id}/InstantMix".replace("{" + "id" + "}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Creates an instant playlist based on a given genre.
     * 
     * @param id The item id.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromMusicGenreById(id: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        id?.apply { localVariableQuery["id"] = listOf("$id") }
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/MusicGenres/InstantMix",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Creates an instant playlist based on a given genre.
     * 
     * @param name The genre name.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromMusicGenreByName(name: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/MusicGenres/{name}/InstantMix".replace("{" + "name" + "}", "$name"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Creates an instant playlist based on a given playlist.
     * 
     * @param id The item id.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromPlaylist(id: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Playlists/{id}/InstantMix".replace("{" + "id" + "}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Creates an instant playlist based on a given song.
     * 
     * @param id The item id.
     * @param userId Optional. Filter by user id, and attach user data. (optional)
     * @param limit Optional. The maximum number of records to return. (optional)
     * @param fields Optional. Specify additional fields of information to return in the output. (optional)
     * @param enableImages Optional. Include image information in output. (optional)
     * @param enableUserData Optional. Include user data. (optional)
     * @param imageTypeLimit Optional. The max number of images to return, per image type. (optional)
     * @param enableImageTypes Optional. The image types to include in the output. (optional)
     * @return BaseItemDtoQueryResult
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getInstantMixFromSong(id: kotlin.String, userId: kotlin.String? = null, limit: kotlin.Int? = null, fields: kotlin.collections.List<ItemFields>? = null, enableImages: kotlin.Boolean? = null, enableUserData: kotlin.Boolean? = null, imageTypeLimit: kotlin.Int? = null, enableImageTypes: kotlin.collections.List<ImageType>? = null): HttpResponse<BaseItemDtoQueryResult> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        limit?.apply { localVariableQuery["limit"] = listOf("$limit") }
        fields?.apply { localVariableQuery["fields"] = toMultiValue(this, "multi") }
        enableImages?.apply { localVariableQuery["enableImages"] = listOf("$enableImages") }
        enableUserData?.apply { localVariableQuery["enableUserData"] = listOf("$enableUserData") }
        imageTypeLimit?.apply { localVariableQuery["imageTypeLimit"] = listOf("$imageTypeLimit") }
        enableImageTypes?.apply { localVariableQuery["enableImageTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Songs/{id}/InstantMix".replace("{" + "id" + "}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


}
