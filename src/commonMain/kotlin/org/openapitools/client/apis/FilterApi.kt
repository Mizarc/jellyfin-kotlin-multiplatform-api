/**
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 *
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.openapitools.client.apis

import org.openapitools.client.models.BaseItemKind
import org.openapitools.client.models.QueryFilters
import org.openapitools.client.models.QueryFiltersLegacy

import org.openapitools.client.infrastructure.*
import io.ktor.client.HttpClientConfig
import io.ktor.client.request.forms.formData
import io.ktor.client.engine.HttpClientEngine
import kotlinx.serialization.json.Json
import io.ktor.http.ParametersBuilder
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

open class FilterApi(
    baseUrl: String = ApiClient.BASE_URL,
    httpClientEngine: HttpClientEngine? = null,
    httpClientConfig: ((HttpClientConfig<*>) -> Unit)? = null,
    jsonSerializer: Json = ApiClient.JSON_DEFAULT
) : ApiClient(baseUrl, httpClientEngine, httpClientConfig, jsonSerializer) {

    /**
     * Gets query filters.
     * 
     * @param userId Optional. User id. (optional)
     * @param parentId Optional. Specify this to localize the search to a specific item or folder. Omit to use the root. (optional)
     * @param includeItemTypes Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. (optional)
     * @param isAiring Optional. Is item airing. (optional)
     * @param isMovie Optional. Is item movie. (optional)
     * @param isSports Optional. Is item sports. (optional)
     * @param isKids Optional. Is item kids. (optional)
     * @param isNews Optional. Is item news. (optional)
     * @param isSeries Optional. Is item series. (optional)
     * @param recursive Optional. Search recursive. (optional)
     * @return QueryFilters
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getQueryFilters(userId: kotlin.String? = null, parentId: kotlin.String? = null, includeItemTypes: kotlin.collections.List<BaseItemKind>? = null, isAiring: kotlin.Boolean? = null, isMovie: kotlin.Boolean? = null, isSports: kotlin.Boolean? = null, isKids: kotlin.Boolean? = null, isNews: kotlin.Boolean? = null, isSeries: kotlin.Boolean? = null, recursive: kotlin.Boolean? = null): HttpResponse<QueryFilters> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        parentId?.apply { localVariableQuery["parentId"] = listOf("$parentId") }
        includeItemTypes?.apply { localVariableQuery["includeItemTypes"] = toMultiValue(this, "multi") }
        isAiring?.apply { localVariableQuery["isAiring"] = listOf("$isAiring") }
        isMovie?.apply { localVariableQuery["isMovie"] = listOf("$isMovie") }
        isSports?.apply { localVariableQuery["isSports"] = listOf("$isSports") }
        isKids?.apply { localVariableQuery["isKids"] = listOf("$isKids") }
        isNews?.apply { localVariableQuery["isNews"] = listOf("$isNews") }
        isSeries?.apply { localVariableQuery["isSeries"] = listOf("$isSeries") }
        recursive?.apply { localVariableQuery["recursive"] = listOf("$recursive") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Items/Filters2",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


    /**
     * Gets legacy query filters.
     * 
     * @param userId Optional. User id. (optional)
     * @param parentId Optional. Parent id. (optional)
     * @param includeItemTypes Optional. If specified, results will be filtered based on item type. This allows multiple, comma delimited. (optional)
     * @param mediaTypes Optional. Filter by MediaType. Allows multiple, comma delimited. (optional)
     * @return QueryFiltersLegacy
     */
    @Suppress("UNCHECKED_CAST")
    open suspend fun getQueryFiltersLegacy(userId: kotlin.String? = null, parentId: kotlin.String? = null, includeItemTypes: kotlin.collections.List<BaseItemKind>? = null, mediaTypes: kotlin.collections.List<kotlin.String>? = null): HttpResponse<QueryFiltersLegacy> {

        val localVariableAuthNames = listOf<String>("CustomAuthentication")

        val localVariableBody = 
            io.ktor.client.utils.EmptyContent

        val localVariableQuery = mutableMapOf<String, List<String>>()
        userId?.apply { localVariableQuery["userId"] = listOf("$userId") }
        parentId?.apply { localVariableQuery["parentId"] = listOf("$parentId") }
        includeItemTypes?.apply { localVariableQuery["includeItemTypes"] = toMultiValue(this, "multi") }
        mediaTypes?.apply { localVariableQuery["mediaTypes"] = toMultiValue(this, "multi") }
        val localVariableHeaders = mutableMapOf<String, String>()

        val localVariableConfig = RequestConfig<kotlin.Any?>(
            RequestMethod.GET,
            "/Items/Filters",
            query = localVariableQuery,
            headers = localVariableHeaders,
            requiresAuthentication = true,
        )

        return request(
            localVariableConfig,
            localVariableBody,
            localVariableAuthNames
        ).wrap()
    }


}
